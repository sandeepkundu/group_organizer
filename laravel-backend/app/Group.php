<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Nicolaslopezj\Searchable\SearchableTrait;
use PhpSoft\Users\Models\UserTrait;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;

class Group extends Model implements AuthenticatableContract,
                                    //AuthorizableContract,
                                    CanResetPasswordContract,
                                    BillableContract
{
    use SearchableTrait;
    use Billable;
    protected $dates = ['trial_ends_at', 'subscription_ends_at'];
    
    protected $searchable = [
        'columns' => [
            'groups.name' => 20,
            'groups.city' => 20,
            'groups.country' => 20,
            'groups.state' => 20,
            'groups.postalcode' => 20,
            'groups.longitude' => 20,
            'groups.latitude' => 20,
        ],
    ];
    use Authenticatable, CanResetPassword,UserTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'header',
        'avatar_url',
        'status',
        'city',
        'state',
        'postalcode',
        'latitude',
        'longitude',
        'country',
        'user_id',
        'description',
        'terms_and_condition',
        'slug',
        'layout',
        'stripe_active',
        'stripe_subscription',
        'stripe_id',
        'stripe_subscription',
        'stripe_plan',
        'last_four',
        'trial_ends_at',
        'subscription_ends_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    //protected $hidden = ['password', 'remember_token'];

    public function User()
    {
        return $this->belongsTo('App\User');
    }
    
    public function GroupMember()
    {
        return $this->hasMany('App\GroupMember');
    }
    
    public function GroupInterest()
    {
        return $this->hasOne('App\GroupInterest')
        ->join('categories', 'categories.id','=', 'group_interests.category_id');
        
    }
    
    public function GroupEvent()
    {
        return $this->hasMany('App\GroupEvent');
    }
    
//    public function getEmployeeshifts() {
//            $usertype = DB::table('usertype')
//                                ->select('usertype')
//                                ->get();
//
//             return Response::json($usertype);
//         }  
    
   #### import fields
    public  $import_fields=[
        'name',
        'avatar_url',
        'status',
        'city',
        'header',
        'state',
        'postalcode',
        'country',
        'user_id',
        'description',
        'terms_and_condition',
            ];
}
