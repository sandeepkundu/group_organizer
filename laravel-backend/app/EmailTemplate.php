<?php

namespace App;


use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Nicolaslopezj\Searchable\SearchableTrait;
use PhpSoft\Users\Models\UserTrait;
//use Nicolaslopezj\Searchable\SearchableTrait;


    class EmailTemplate extends Model{
    
         
        protected $fillable= [
//            'email_type',
            'email_subject',
            'email_body'];
        
        protected $searchable = [
            'columns' => [
                'email_templates.email_subject' => 1,
                'email_templates.email_body' => 200,
            ],
        ];
        
        protected $import_fields = [
//            'email_type',
            'email_subject',
            'email_body',
        ];
    }

?>