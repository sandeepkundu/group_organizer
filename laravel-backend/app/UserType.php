<?php

namespace App;


use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Nicolaslopezj\Searchable\SearchableTrait;
use PhpSoft\Users\Models\UserTrait;
//use Nicolaslopezj\Searchable\SearchableTrait;


    class UserType extends Model{
        use SearchableTrait;
        protected $searchable = [
            'columns' => [
                'user_types.usertype' => 1,
                
            ],
        ];
        
        public function User()
        {
            return $this->hasMany('App\User','usertype');

        }
         
            protected $fillable= ['usertype'];
    }

?>