<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stripTransaction extends Model
{
    //

		protected $table = 'strip_transactions';
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'strip_id',
        'object',
        'amount',
        'captured',
        'currency',

            ];
}
