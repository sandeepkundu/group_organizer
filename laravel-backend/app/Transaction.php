<?php

namespace App;


use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Nicolaslopezj\Searchable\SearchableTrait;
use PhpSoft\Users\Models\UserTrait;
//use Nicolaslopezj\Searchable\SearchableTrait;


    class Transaction extends Model{
        
        protected $fillable= ['cart_id','transaction_id','status'];
        
        protected $searchable = [
            'columns' => [
                'transactions.cart_id' => 20,
                'transactions.transaction_id' => 200,
                'transactions.status' => 20,
            ],
        ];
        
        protected $import_fields = [
            'cart_id',
            'transaction_id',
            'status',
        ];
        
        public function transaction()
        {
            return $this->belongsTo('App\User','user_id', 'id');
        }

//        protected $fillable= [''];
    }

?>