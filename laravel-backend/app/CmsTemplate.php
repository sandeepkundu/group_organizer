<?php

namespace App;


use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Nicolaslopezj\Searchable\SearchableTrait;
use PhpSoft\Users\Models\UserTrait;
//use Nicolaslopezj\Searchable\SearchableTrait;


    class CmsTemplate extends Model{
    
         
        protected $fillable= ['page_heading','sub_heading','description'];
        
        protected $searchable = [
            'columns' => [
                'cms_templates.page_heading' => 1,
                'cms_templates.sub_heading' => 200,
                'cms_templates.description' => 4,
            ],
        ];
        
        protected $import_fields = [
            'page_heading',
            'sub_heading',
            'description',
        ];
        
        public function CmsTemplateImage()
        {
            return $this->hasMany('App\CmsTemplateImage','cms_template_id');

        }
    }

?>