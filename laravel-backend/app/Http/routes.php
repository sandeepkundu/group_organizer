<?php

Route::group(['prefix' => 'api'], function() {

    /*
     * import excel csv Route
     */
    Route::post('user/import_excel_csv', 'ImportController@importCSVEXCEl');
    Route::post('user/import_excel_csv_database', 'ImportController@importCSVEXCElDatabase');
    Route::post('user/{id}/delete_excel_csv', 'ImportController@deleteCSVEXCEl');

    // Facebook Login
    Route::post('fb_login', 'FindGroupController@fb_login');

    // Password reset link request routes...
    Route::post('password/email', 'Auth\PasswordController@postEmail');

    // Password reset routes...
    Route::post('password/reset', 'Auth\PasswordController@postReset');

    // authentication
    Route::post('register', 'RegisterController@register');
    Route::get('register/verify/{confirmationCode}', 'RegisterController@confirm');
    Route::get('register/update/{confirmationCode}', 'RegisterController@update_email_con');
    Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
    Route::post('authenticate', 'AuthenticateController@authenticate');


    // front end user signup authentication
    Route::post('signup', 'RegisterController@signup');
    Route::post('editprofile', 'RegisterController@editprofile');
    Route::post('changePassword', 'RegisterController@chpassword');
    Route::post('setCurrentLocation', 'RegisterController@setCurrentLocation');
    Route::post('setCurrentLocationfind', 'FindGroupController@setCurrentLocation');
    Route::get('getaddress', 'RegisterController@getaddress');

    /*
     * group join with user registration 
     */

    Route::post('create-group', 'RegisterController@grpOrgSignup');

    Route::post('create-temp-group', 'RegisterController@getTempGroup');

    Route::get('getAllCat', 'RegisterController@getAllCityByword');
    /*
     * User Route
     */
    Route::post('users/update_email', 'RegisterController@update_email');
    Route::post('user/memberType', 'UserController@memberType');
    Route::post('user/status', 'UserController@status');
    Route::post('loginFirst', 'UserController@loginFirst');
    Route::post('organizer_mail', 'UserController@organizerEmail');
    Route::post('user/user-categories', 'UserController@updateInterest');
    Route::post('user/search_usertype', 'UserController@search_usertype');
    Route::post('user/user_list', 'UserController@user_list');
    Route::put('user/{id}/profile', 'UserController@editProfile');
    Route::get('user/search', 'UserController@search');
    Route::resource('user', 'UserController');
    Route::get('user/export/file', 'UserController@exportFile');



    /*
     * Group Route
     */
    Route::post('groups/check', 'FindGroupController@group_check');
    Route::get('groups/events', 'FindGroupController@event_list');
    Route::get('get_terms_cond', 'FindGroupController@get_terms_cond');
    Route::get('howitwork', 'FindGroupController@howitworks');
    Route::get('latestnews', 'FindGroupController@latestnews');
    Route::get('doanddonts', 'FindGroupController@doanddonts');
    Route::get('faqpricing', 'FindGroupController@faqpricing');
    Route::post('group/event_rsvp', 'GroupController@event_rsvp');
    Route::post('group/user_event_status', 'GroupController@user_event_status');
    Route::post('group/get_rsvp', 'GroupController@getRsvp');
    Route::post('group/get_userrsvp', 'GroupController@get_userrsvp');
    Route::post('group/cancel_rsvp', 'GroupController@cancel_rsvp');
    Route::post('group/confirm_rsvp', 'GroupController@confirmRsvp');
    Route::post('event/con', 'GroupController@event_con');
    Route::post('group/get_list', 'GroupController@get_list');
    Route::post('group/dashboard', 'GroupController@dashboard');
    Route::post('member/list', 'GroupController@member_list');
    Route::post('group/status', 'GroupController@status');
    Route::post('/groupimages', 'GroupController@uploadimage');
    Route::post('delgro/{id}', 'GroupController@deleteUpload');
    Route::post('delete/member', 'GroupController@delete_member');
    Route::put('group/{id}/profile', 'GroupController@editProfile');
    Route::post('event/add', 'GroupController@create_event');
    Route::post('group/show_event', 'GroupController@show_single_event');
    Route::post('event/delete', 'GroupController@delete_event');
    Route::post('event/show', 'GroupController@show_event');
    Route::post('event/payment', 'GroupController@event_payment');
    Route::post('event/get_list', 'GroupController@get_event');
    Route::get('event_date/{date}', 'GroupController@event_date');
    Route::get('group/search', 'GroupController@search');
    Route::resource('delete/group', 'GroupController@destroy');
    Route::resource('group', 'GroupController');
    Route::get('cmsDetail', 'FindGroupController@cmsDetail');
    Route::get('allSetCategory', 'FindGroupController@allSetCategory');
    Route::get('randCategory', 'FindGroupController@randCategory');
    Route::get('groups/getAllCat', 'FindGroupController@getAllCatByword');
    Route::post('groups/getGroupByInterest', 'FindGroupController@getGroupByInterest');
    Route::get('groups/test', 'FindGroupController@test');
    Route::get('user-categories', 'FindGroupController@userCategory');
    Route::get('groups/category', 'FindGroupController@category');
    Route::post('groups/id_category', 'FindGroupController@category_id');
    Route::post('user/category_delete','FindGroupController@intersetCategorydelete');
    Route::post('groups/membership_status', 'PaymentController@membership_status');
    Route::post('strip-payment', 'PaymentController@payment');
    
    //Route::post('stripe/subscriber','WebhookController@handleInvoicePaymentSucceeded');
    Route::post('regorgStripPayment', 'PaymentController@regOrgpayment');
    Route::post('orgStripPayment', 'PaymentController@Orgpayment');
    Route::post('groups/orgStripPaymentfind', 'FindGroupController@Orgpayment');
    Route::post('groups/index', 'FindGroupController@index_group');
    Route::get('events/search', 'FindGroupController@getEventByDate');
    Route::post('event/get', 'FindGroupController@get_event');
    Route::get('events_groups', 'FindGroupController@events_groups');
    Route::resource('groups', 'FindGroupController');
    Route::get('group/export/file', 'FindGroupController@exportFile');
//    Route::post('delgro/{id}','GroupController@deleteUpload');
    Route::post('member/accept', 'GroupController@member_accept');
    Route::post('member/reject', 'GroupController@member_reject');
    Route::post('group/show', 'RegGroupController@show');
    Route::post('reggroup/update', 'RegGroupController@update');
    Route::post('reggroup/get', 'RegGroupController@index');
    Route::post('reggroup/inactive', 'RegGroupController@inactive_group');
    Route::post('riggroup/export', 'RegGroupController@exportFile');
    Route::post('reggroup/members', 'RegGroupController@getmember');
    Route::post('member/status', 'RegGroupController@member_status');
    Route::post('reggroup/pending_member', 'RegGroupController@member_list');
    Route::post('reggroup/events', 'RegGroupController@get_event');
    Route::post('reggroup/get_event', 'RegGroupController@single_event');
    Route::post('group/event_update', 'RegGroupController@update_event');
    Route::post('group/event_create', 'RegGroupController@create_event');
    Route::post('reggroup/get_images', 'RegGroupController@get_images');
    Route::post('group/groupimages', 'RegGroupController@uploadimage');
    Route::post('group/delgro/{id}', 'RegGroupController@deleteUpload');
    Route::post('grpgroup/groupimages', 'RegGroupController@uploadimage');
    Route::post('grpgroup/delgro/{id}', 'RegGroupController@deleteUpload');
    Route::post('grpgroup/deleteimage', 'RegGroupController@deleteImage');
    Route::post('groups/get_list', 'RegGroupController@get_lists');
    Route::post('groups/group_join', 'RegGroupController@group_join');
    Route::post('group/event_change_status', 'RegGroupController@event_change_status');
    Route::post('reggroup/send_mail_member', 'RegGroupController@send_mail_member');
    Route::post('reggroup/update_stripe', 'PaymentController@update_stripe');
    Route::post('reg_event', 'RegGroupController@reg_event');
    Route::post('duplicate_event', 'RegGroupController@duplicate_event');
    /*
     * Transaction Route
     */
    Route::post('transaction/search_usertype', 'TransactionController@search_usertype');
    Route::post('transaction/search', 'TransactionController@search');
    Route::post('transaction/index', 'TransactionController@index');

    /*
     * Mail Route
     */
    Route::get('/fb_feed', 'FindGroupController@fb_feed');
    Route::get('/insta_feed', 'FindGroupController@insta_feed');
    Route::post('user_mail/send', 'UserMailController@send');

    /*
     * Membership Route
     */
    Route::get('membership', 'FindGroupController@membership');

    /*
     * Suggestion Route
     */
    Route::post('suggestion', 'FindGroupController@suggestion');
    Route::get('terms_privacy', 'FindGroupController@terms_privacy');

    /*
     * CMS Route
     */
    Route::post('cms/index', 'CmsTemplateController@index');
    Route::post('cms/show', 'CmsTemplateController@show');
    Route::post('cms/update', 'CmsTemplateController@update');

    /*
     * misec Route
     */
    Route::post('email/index', 'EmailTemplateController@index');
    Route::post('email/show', 'EmailTemplateController@show');
    Route::post('email/update', 'EmailTemplateController@update');
    Route::post('email/create', 'EmailTemplateController@create');
    Route::post('email/search', 'EmailTemplateController@search');
    Route::post('email/delete', 'EmailTemplateController@delete');

    /*
     * Permission Route
     */
    Route::get('permission/search', 'PermissionController@search');
    Route::resource('permission', 'PermissionController');

    /*
     * Role Route
     */
    Route::get('role/search', 'RoleController@search');
    Route::resource('role', 'RoleController');

    /*
     * Task Route
     */
    Route::get('task/search', 'TaskController@search');
    Route::resource('task', 'TaskController');
    Route::get('task/export/file', 'TaskController@exportFile');
    /*
     * Comment Route
     */
    Route::get('comment/search', 'CommentController@search');
    Route::resource('comment', 'CommentController');

    /*
     * tag Route
     */
    Route::get('tag/search', 'TagController@search');
    Route::resource('tag', 'TagController');

    /*
     * Gallery Route
     */
    Route::get('gallery/search', 'GalleryController@search');
    Route::resource('gallery', 'GalleryController');


    /*
     * Category Route
     */
    Route::post('get_cat', 'CategoryController@get_cat');
    Route::resource('delete/category', 'CategoryController@destroy');
    Route::post('category/update', 'CategoryController@update');
    Route::resource('category', 'CategoryController');

    /*
     * Upload image Controller
     */
    Route::post('/uploadimage', 'UploadController@uploadimage');
    Route::post('/uploadlayout', 'UploadController@uploadlayout');
    Route::post('/deleteimage/{id}', 'UploadController@deleteUpload');
    Route::post('/deletelayout/{id}', 'UploadController@deletelayout');
});
Route::get('stripe/failure', 'WebhookController@testfailure');
// Route::post('stripe/webhook', '\Laravel\Cashier\WebhookController@handleWebhook');
Route::any('stripe/subscriber','WebhookController@handleInvoicePaymentSucceeded');
Route::get('test', function () {
    return view('emails.password');
});

Route::get('user/invoice/{invoice}', function ($invoiceId) {
    return Auth::user()->downloadInvoice($invoiceId, [
                'vendor' => 'Your Company',
                'product' => 'Your Product',
    ]);
});
