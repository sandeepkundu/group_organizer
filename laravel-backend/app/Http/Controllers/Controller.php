<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;



    public function getLnt($address){
        
         $formattedAddr = str_replace(' ','+',$address);
        //Send request and receive json data by address
        $geocodeFromAddr = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false'); 
        $output = json_decode($geocodeFromAddr);
        //Get latitude and longitute from json data
            
        if(isset($output->results[0])){
            

            for($j=0;$j<count($output->results[0]->address_components);$j++){
                
              $types = implode(',',$output->results[0]->address_components[$j]->types);

                  if ($types == "sublocality,political" || $types == "locality,political" || $types == "neighborhood,political" || $types == "administrative_area_level_2,political") {
                      $data['city'] = $output->results[0]->address_components[$j]->long_name;
                  }
                  if($types == "administrative_area_level_1,political"){
                      $data['state'] = $output->results[0]->address_components[$j]->long_name;

                  }
                  if($types == "country" || $types == "country,political"){
                      $data['country'] = $output->results[0]->address_components[$j]->long_name;
                  }
                  if ($types == "postal_code" || $types == "postal_code_prefix,postal_code") {
                      $data['postalcode'] = $output->results[0]->address_components[$j]->long_name;
                  }

            }
            if($data['country']=='USA'){
                $data['country']='United States';
            }
            $data['latitude']  = $output->results[0]->geometry->location->lat;
            $data['longitude'] = $output->results[0]->geometry->location->lng;
            //dd($data);
        }
        if(!empty($data)){
            return $data;
        }else{
            return false;
        }
    }
    
    public function getadd($address){
        
         $formattedAddr = str_replace(' ','+',$address);
        //Send request and receive json data by address
        $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/place/autocomplete/json?input='.$formattedAddr.'&key=AIzaSyCpx6aa6Mj4MM16B9ray6IFcnbvkBXOUuk'); 
        $output = json_decode($geocodeFromAddr);
        //Get latitude and longitute from json data
        //dd($output);    
        if(isset($output)){
            

//            for($j=0;$j<count($output->results[0]->address_components);$j++){
//                
//              $types = implode(',',$output->results[0]->address_components[$j]->types);
//
//                  if ($types == "sublocality,political" || $types == "locality,political" || $types == "neighborhood,political" || $types == "administrative_area_level_2,political") {
//                      $data['city'] = $output->results[0]->address_components[$j]->long_name;
//                  }
//                  if($types == "administrative_area_level_1,political"){
//                      $data['state'] = $output->results[0]->address_components[$j]->long_name;
//
//                  }
//                  if($types == "country" || $types == "country,political"){
//                      $data['country'] = $output->results[0]->address_components[$j]->long_name;
//                  }
//                  if ($types == "postal_code" || $types == "postal_code_prefix,postal_code") {
//                      $data['postalcode'] = $output->results[0]->address_components[$j]->long_name;
//                  }
//
//            }
            //$data['latitude']  = $output->results[0]->geometry->location->lat;
            //$data['longitude'] = $output->results[0]->geometry->location->lng;
            $data=array();
            $num = 0;
            foreach($output->predictions as $singleoutput){
                $data[$num] = $singleoutput->description;
                $num++;
            }
            //dd($data);
        }
        if(!empty($data)){
            return $data;
        }else{
            return false;
        }
    }


    
}
