<?php

namespace App\Http\Controllers;

use App\Role;
use App\role_user;
use App\EmailTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\DB;
use Hash;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class EmailTemplateController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    public function index(){
            
        if (Auth::user()->can('view_group')) {
            
            $email = EmailTemplate::all();
//            dd($email);
            return $email;
            
        }else
            return response()->json(['error' => 'You not have Permission'], 403);
    }
    
    public function update(Request $request){
//         dd($request);
        if (Auth::user()->can('view_group')) {
            $id = $request['id'];
            $validator = Validator::make($request->all(), [
//                        'email_type' => 'required',
                        'email_subject' => 'required',
                        'email_body' => 'required',
                            ], $messages = [
//                        'email_type.required' => 'The Email Type field is required',
                        'email_subject.required' => 'The Subject field is required',
                        'email_body.required' => 'The Body field is required',
                            ]
            );
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 406);
            }
            //dd($validator);
            $email_templates = EmailTemplate::find($id);
            
            $email_templates->update($request->all());
            
            return response()->json(['success'], 200);
         }else
            return response()->json(['error' => 'You not have Permission'], 403);
    }
    
    public function show(Request $request){
        $id =  $request['id']; 
        if (Auth::user()->can('view_group')) {

               $cms = EmailTemplate::where('id', '=', $id)->first();
               
               return $cms;

           }else
               return response()->json(['error' => 'You not have Permission'], 403);
   }
   
   public function create(Request $request){
       if (Auth::user()->can('view_group')) {
           
          $id = $request['id'];
            $validator = Validator::make($request->all(), [
                        'email_type' => 'required',
                        'email_subject' => 'required',
                        'email_body' => 'required',
                            ], $messages = [
                        'email_type.required' => 'The Email Type field is required',
                        'email_subject.required' => 'The Subject field is required',
                        'email_body.required' => 'The Body field is required',
                            ]
            );
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 406);
            }
            
            $temp = EmailTemplate::create($request->all());
           if($temp){
               return response()->json(['success'], 200);
           }
           
        }else
            return response()->json(['error' => 'You not have Permission'], 403);
       
   }
   
   public function search(Request $request){}
   
   public function delete(Request $request){
//       dd($request);
       $id =  $request['id']; 
       if (Auth::user()->can('view_group')) {
           
           $email_template = EmailTemplate::find($id);
           $email_template->delete();
           return response()->json(['success'], 200);
        }else
               return response()->json(['error' => 'You not have Permission'], 403);
   }     
}
