<?php

namespace App\Http\Controllers;

use App\Role;
use App\Group;
use App\TempGroup;
use App\TempGroupInterest;
use App\GroupInterest;
use App\role_user;
use App\User;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Hash;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
//use mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use Laravel\Cashier\Billable;
use Stripe\Charge;
use Stripe\Stripe;
use Carbon\Carbon;
use Stripe\Error;

class PaymentController extends Controller {

    public function __construct() {
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    public function payment(Request $request) {

        //$configKey = config('services.stripe.key');
        $configSecret = config('services.stripe.secret');

        $ID = Auth::user()->id;
        $total_price = 100 * 100;

        if (empty($ID)) {
            $mess = ['status' => "ERROR", "message" => "user Not found"];
            return $mess;
        }

        if (empty($request)) {
            $mess = ['status' => "ERROR", "message" => "data is empty"];
            return $mess;
        }

        if ($request['stripeToken'] == "") {

            $mess = ['status' => "ERROR", "message" => "token is empty"];
            return $mess;
        }

        $token = $request['stripeToken'];
        $user = User::find($ID);

        $gtdata = $user->charge($total_price, [
            'source' => $token,
            'receipt_email' => $user->email,
            'description' => 'One time payment'
        ]);

        if (!$gtdata) {

            $mess = ['status' => "ERROR", "message" => "error while captureing the payment"];
            return $mess;
        } else {

            $getPayment = DB::table('strip_transactions')->insert([
                'strip_id' => $gtdata->id,
                'object' => $gtdata->object,
                'amount' => $gtdata->amount,
                'balance_transaction' => $gtdata->balance_transaction,
                'captured' => $gtdata->captured,
                'currency' => $gtdata->currency,
                'description' => $gtdata->description,
                'receipt_email' => $gtdata->receipt_email,
                'receipt_number' => !empty($gtdata->receipt_number) ? $gtdata->receipt_number : 0,
                'status' => $gtdata->status,
                'invoice' => !empty($gtdata->invoice) ? $gtdata->invoice : 0,
                'livemode' => $gtdata->livemode,
                'card_detail' => json_encode($gtdata->source),
                'jsonresponse' => json_encode($gtdata),
            ]);

            if ($getPayment) {

                $getStripeconfirm = DB::table('strip_group_payments')->insert([
                    'user_id' => $ID,
                    'group_id' => $request['GroupID'],
                    'status' => 1,
                    'group_owner' => 0,
                    'payment' => $gtdata->captured,
                ]);
                if ($getStripeconfirm) {
                    DB::table('group_members')->insert([
                        'user_id' => $ID,
                        'group_id' => $request['GroupID'],
                        'status' => 2,
                    ]);


                    $mess = ['status' => "Success", "message" => "Payment Accepted"];
                    return $mess;
                } else {
                    $mess = ['status' => "ERROR", "message" => "Payment Accepted data saved but groups not saved on our server"];
                    return $mess;
                }
            } else {
                $mess = ['status' => "ERROR", "message" => "Payment Accepted but datas have not saved on our server"];
                return $mess;
            }
        }
    }

    public function Orgpayment(Request $request) {

        $pay_details = json_decode($request['pay']);
        $ID = Auth::user()->id;
        $total_price = $pay_details->amount * 100;
        $pay_id = $pay_details->id;
        $pay_p_id = $pay_details->p_id;
        $month = $pay_details->month;
        $strip_plan_id = $pay_details->strip_plan_id;

        if (empty($ID)) {

            return response()->json(['error' => 'Your signup details are mismatched'], 406);
        }

        if (empty($request)) {
            return response()->json(['error' => 'Error while fetching your data'], 406);
        }

        if ($request['stripeToken'] == "") {
            return response()->json(['error' => 'There is a problem in your card'], 406);
        }

        if (!isset($request['tempGroupID']) || empty($request['tempGroupID'])) {
            return response()->json(['error' => 'You group is not saved successfully, please try again'], 406);
        } else {

            $tempgroup = TempGroup::find($request['tempGroupID'])->toArray();
            $TempGroupInterest = TempGroupInterest::where('temp_group_interests.group_id', $request['tempGroupID'])->get()->toArray();
        }
        $tempgroup['user_id'] = $ID;
        $Group = new Group($tempgroup);
        $grpsave = $Group->save();

        if (empty($Group->id)) {
            return response()->json(['error' => 'You group is not saved successfully, please try again'], 406);
        }

        $token = $request['stripeToken'];
        $user = Group::find($Group->id);

        try {
            $user->subscription($strip_plan_id)->create($token, [
                'email' => $user->email, 'description' => 'monthly desc'
            ]);
            
            $user->trial_ends_at = Carbon::now()->addDays(90);
            $user->save();
        } catch (\Stripe\Error\Card $e) {
            DB::table('groups')->where('id', '=', $Group->id)->delete();
            return response()->json(['error' => 'Card declined'], 406);
        } catch (\Stripe\Error\InvalidRequest $e) {
            DB::table('groups')->where('id', '=', $Group->id)->delete();
            return response()->json(['error' => 'Invalid parameters were supplied to Stripes API'], 406);
        } catch (\Stripe\Error\Authentication $e) {
            DB::table('groups')->where('id', '=', $Group->id)->delete();
            return response()->json(['error' => 'Authentication with Stripes API failed'."->".$e->getMessage()], 406);
        } catch (\Stripe\Error\ApiConnection $e) {
            DB::table('groups')->where('id', '=', $Group->id)->delete();
            return response()->json(['error' => 'Network communication with Stripe failed'], 406);
        } catch (\Stripe\Error\Base $e) {
            DB::table('groups')->where('id', '=', $Group->id)->delete();
            return response()->json(['error' => 'Display a very generic error to the user'."-->".$e->getMessage()], 406);
        } catch (Exception $e) {
            DB::table('groups')->where('id', '=', $Group->id)->delete();
            return response()->json(['error' => 'Something else happened, completely unrelated to Stripe'], 406);
        }
        if (!$user->subscribed()) {
            DB::table('groups')->where('id', '=', $Group->id)->delete();
            return response()->json(['error' => 'Error while captureing the payment'], 406);
        } else {
            foreach ($TempGroupInterest as $key => $value) {
                $value['group_id'] = $Group->id;
                $GroupInterest = new GroupInterest($value);
                $ct = $GroupInterest->save();
            }
            
            $current_date = date("Y-m-d");
            $month=$month+3;
            $membership_end_date = date('Y-m-d', strtotime('+' . $month . ' months'));
            $getStripeconfirm = DB::table('strip_group_payments')->insert([
                'user_id' => $ID,
                'group_id' =>$Group->id,
                'status' => 1,
                'group_owner' =>$ID,
                'payment' =>1,
                'plan_id' => $pay_id,
                'payment_plan_id' => $pay_p_id,
                'membership_start' => $current_date,
                'membership_end' => $membership_end_date,
                'amount' => $total_price,
            ]);
            
            $msg = DB::table('email_templates')->where('id', '=', 8)->select('email_body as body','email_subject as subject')->first();
            $msg = (array)$msg;
        
            $user_data = DB::table('users')->where('id', '=', $ID)->select('first_name','email')->first();
            $user_data = (array)$user_data;
            
            $plan_data = DB::table('group_payment_plans')->where('id', '=', $pay_id)->select('strip_plan_id as plan')->first();
            
            $data['first_name'] = $user_data['first_name'];
            $data['group_name'] = $Group->name;
            $data['plan'] = $plan_data->plan;
            $data['amount'] = $total_price/100;
            $data['month'] = $month;
            $data['email'] = $user_data['email'];
            
            $data['subject'] = $msg['subject'];
            
            $message['body'] = str_replace(array('{{name}}','{{group_name}}','{{plan}}','{{amount}}','{{month}}'), array(trim($data['first_name']),$data['group_name'],$data['plan'],$data['amount'],$data['month']), $msg['body']);
            
            //dd($message->body);
            Mail::queue('emails.group_con', $message, function($message) use ($data) {
                $message->to($data['email'])->subject($data['subject']);
            });
                
            return "Success";
        }
    }

    public function regOrgpayment(Request $request) {
        $groupid = explode(":", $request['tempGroupID']);
        $groupid = $groupid[1];
        $ID = Auth::user()->id;
        $pay_details = json_decode($request['pay']);
        $total_price = $pay_details->amount * 100;
        $pay_id = $pay_details->id;
        $pay_p_id = $pay_details->p_id;
        $month = $pay_details->month;
        $strip_plan_id = $pay_details->strip_plan_id;
        //dd($month);
        if (empty($ID)) {

            return response()->json(['error' => 'Your signup details are mismatched'], 406);
        }

        if (empty($request)) {
            return response()->json(['error' => 'Error while fetching your data'], 406);
        }

        if ($request['stripeToken'] == "") {
            return response()->json(['error' => 'There is a problem in your card'], 406);
        }
        if (!isset($groupid) || empty($groupid)) {

            return response()->json(['error' => 'You group is not saved successfully, please try again'], 406);
        } else {

            $tempgroup = DB::table('temp_groups')->where('id', $groupid)->first();
            $TempGroupInterest = DB::table('temp_group_interests')->where('temp_group_interests.group_id', $groupid)->get();
        }
        $tempgroup->user_id = $ID;
        $details_store = DB::table('groups')->insert(['user_id' => $tempgroup->user_id, 'name' => $tempgroup->name, 'header' => $tempgroup->header, 'status' => $tempgroup->status, 'city' => $tempgroup->city, 'state' => $tempgroup->state, 'country' => $tempgroup->country, 'postalcode' => $tempgroup->postalcode, 'latitude' => $tempgroup->latitude, 'longitude' => $tempgroup->longitude, 'description' => $tempgroup->description, 'terms_and_condition' => $tempgroup->terms_and_condition, 'avatar_url' => $tempgroup->avatar_url, 'slug' => $tempgroup->slug]);
        $name = $tempgroup->name;
        $user_id = $tempgroup->user_id;
        $header = $tempgroup->header;
        $city = $tempgroup->city;
        $state = $tempgroup->state;
        $country = $tempgroup->country;
        $postalcode = $tempgroup->postalcode;
        $group_id = Group::select('id')->where('name', $name)->where('user_id', $user_id)->where('header', $header)->where('city', $city)->where('state', $state)->where('country', $country)->where('postalcode', $postalcode)->first();
        $group_id = $group_id['id'];


        

        $token = $request['stripeToken'];
        $user = Group::find($group_id);


        try {
            $user->subscription($strip_plan_id)->create($token, [
                'email' => $user->email, 'description' => 'monthly desc'
            ]);
            $user->trial_ends_at = Carbon::now()->addDays(90);
            $user->save();
        } catch (\Stripe\Error\Card $e) {
            DB::table('groups')->where('id', '=', $group_id)->delete();
            return response()->json(['error' => 'Card declined'], 406);
        } catch (\Stripe\Error\InvalidRequest $e) {
            DB::table('groups')->where('id', '=', $group_id)->delete();
            return response()->json(['error' => 'Invalid parameters were supplied to Stripes API'], 406);
        } catch (\Stripe\Error\Authentication $e) {
            DB::table('groups')->where('id', '=', $group_id)->delete();
            return response()->json(['error' => 'Authentication with Stripes API failed'], 406);
        } catch (\Stripe\Error\ApiConnection $e) {
            DB::table('groups')->where('id', '=', $group_id)->delete();
            return response()->json(['error' => 'Network communication with Stripe failed'], 406);
        } catch (\Stripe\Error\Base $e) {
            DB::table('groups')->where('id', '=', $group_id)->delete();
            return response()->json(['error' => 'Display a very generic error to the user'], 406);
        } catch (Exception $e) {
            DB::table('groups')->where('id', '=', $group_id)->delete();
            return response()->json(['error' => 'Something else happened, completely unrelated to Stripe'], 406);
        }
        if (!$user->subscribed()) {
            DB::table('groups')->where('id', '=', $group_id)->delete();
            return response()->json(['error' => 'Error while captureing the payment'], 406);
        } else {
            foreach ($TempGroupInterest as $key => $value) {
                DB::table('group_interests')->insert(['group_id' => $group_id, 'category_id' => $value->category_id]);
            }
            $current_date = date("Y-m-d");
            $month=$month+3;
            $membership_end_date = date('Y-m-d', strtotime('+' . $month . ' months'));
            $getStripeconfirm = DB::table('strip_group_payments')->insert([
                    'user_id' => $ID,
                    'group_id' =>$group_id,
                    'status' => 1,
                    'group_owner' =>$ID,
                    'payment' =>1,
                    'plan_id' => $pay_id,
                    'payment_plan_id' => $pay_p_id,
                    'membership_start' => $current_date,
                    'membership_end' => $membership_end_date,
                    'amount' => $total_price,
                ]);
                
            return "Success";
        }
    }
    
     public function update_stripe(Request $request){
       // dd($request);
       $group_id = $request['group_id'];
       $c_no = $request['card']['number'];
       $c_month = $request['card']['exp_month'];
       $c_year = $request['card']['exp_year'];
       $c_cvc = $request['card']['cvc'];
      // $group_id = 103;
       //dd($request['card']);
       \Stripe\Stripe::setApiKey("pk_live_UlybiREYClTtDLmNjE4gaibE");
        //dd($c_no);
        $a = \Stripe\Token::create(array(
          "card" => array(
            "number" => $c_no,
            "exp_month" => $c_month,
            "exp_year" => $c_year,
            "cvc" =>  $c_cvc
          )
        ));
        //$a = json_decode($a);
        $token = $a->id;
        //dd($token);
        $customer_id = DB::table('groups')->select('stripe_id')->where('id','=',$group_id)->first();
       // dd();
        if(!empty($customer_id)){
            $customer_id=$customer_id->stripe_id;
            $cu = \Stripe\Customer::retrieve($customer_id); // stored in your application
            $cu->source = $token; // obtained with Checkout
            $cu->save();
            //dd($cu);
            return "Success";
        }
        
        
    }
    
    
    public function membership_status(Request $request) {

        $user_id = $request['id'];
        $current_date = date("Y-m-d");
        $membership_status = DB::table('strip_group_payments')->select('membership_end', 'group_id')->where('user_id', $user_id)->orderBy('id', 'desc')->first();
        $membership_date = $membership_status->membership_end;

        $group_id = $membership_status->group_id;
        if (isset($membership_date)) {

            if ($membership_date > $current_date) {
                $data['status'] = "true";
            } else {
                $data['status'] = 'false';
            }
        }
        $data['group_id'] = $group_id;
        return $data;
    }

}
