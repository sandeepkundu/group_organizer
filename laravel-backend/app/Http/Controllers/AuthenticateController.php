<?php

namespace App\Http\Controllers;

use App\role_user;
use Illuminate\Http\Request;
use App\Role;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthenticateController extends Controller
{

    public function authenticate(Request $request)
    {
        //dd($request->all());
        // check active or non-active user
        $tmp= User::where('email', $request['email'])->select('status','email_confirmation')->first();
        $tmp_email= User::where('temp_email', $request['email'])->select('status','email_confirmation')->first(); 
        
        if($tmp_email!=null){
            return response()->json(['error' => 'Your Email is not verified. Please try again'], 401); 
        }
        
        if($tmp['status'] == 0 && $tmp['email_confirmation'] == 0){
            /*return response()->json(['error' => 'Your email was not found in our records. Please try again or click on the "Forgot Password" link below to reset your password.'], 401); */
            $error_msg="<b>We were unable to verify your account.</b><br>You have either entered an invalid password, or there is no account associated with this email address. Please try again, or create a new account now.";
            return response()->json(['error' => $error_msg], 401);

        }
        if($tmp['status'] == 1)
        {
            $request['status'] = $tmp['status'];
        }
        else
        {
            return response()->json(['error' => 'Your account is not active! Please contact to Admin. Please try again'], 401);
        }
        if($tmp['email_confirmation'] == 0) 
        {
            return response()->json(['error' => '<b>Your email address has not been verified.</b><br> To access the site, please check your email for the Bevylife Team account verification message and click “Confirm Email Address".'], 401);
        }

        $permissions = array();
        $credentials = $request->only('email', 'password','status');
        
        //$lifetime = time() + 60 * 60 * 24 * 365; // one year
        $lifetime = time() + 120 * 60 * 1; // one year
        $customClaims = ['exp' => $lifetime];
        //Session::put('Expires', $$lifetime);
        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials,$customClaims)) {
                return response()->json(['error' => '<b>We were unable to verify your account.</b><br>You have either entered an invalid password, or there is no account associated with this email address. Please try again, or create a new account now.'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $role = DB::table('role_user')->where('user_id',Auth::user()->id)->first();
        $role = Role::find($role->role_id);
        $user=Auth::user();
             $temp=$role->perms()->get()->lists('name');
        foreach($temp as $value){
            $permissions[]=$value;
         }
        $user->permissions=$permissions;
        return response()->json(compact('token','user'));
 
    }

}
