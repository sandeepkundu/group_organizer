<?php

namespace App\Http\Controllers;

use App\Role;
use App\role_user;
use App\CmsTemplateImage;
use App\CmsTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\DB;
use Hash;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CmsTemplateController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    public function index(){
            
        if (Auth::user()->can('view_group')) {
            
            $cms = CmsTemplate::all();
            //dd($cms);
            return $cms;
            
        }else
            return response()->json(['error' => 'You not have User'], 403);
    }
    
    public function update(Request $request){
//         dd($request);
        if (Auth::user()->can('view_group')) {
            $id = $request['id'];
            if($id==1){
                $validator = Validator::make($request->all(), [
                        'page_heading' => 'required',
                        'sub_heading' => 'required',
                        'description' => 'required',
                            ], $messages = [
                        'page_heading.required' => 'The Heading field is required',
                        'sub_heading.required' => 'The Sub Heading field is required',
                        'description.required' => 'The Description field is required',
                            ]
                );
                
            }else{
                $validator = Validator::make($request->all(), [
                        'page_heading' => 'required',
                        'description' => 'required',
                            ], $messages = [
                        'page_heading.required' => 'The Heading field is required',
                        'description.required' => 'The Description field is required',
                            ]
                );
            }
            
            if ($validator->fails()) {

                return response()->json(['error' => $validator->errors()], 406);
            }
            $cms_templates = CmsTemplate::find($id);
            $images = $request['images'];
//            foreach($images as $image){
                if (file_exists("temp/" . $request['image']) && $request['image'] != '') {
                    File::move("temp/" . $request['image'], "uploads/" . $request['image']);
                }
                DB::table('cms_template_images')->where('cms_template_id', $id)->update(['image' => $request['image']]);
//            }
           //a dd($request->all());
            $cms_templates->update($request->all());
            
            return response()->json(['success'], 200);
         }else
            return response()->json(['error' => 'You not have User'], 403);
    }
    
    public function show(Request $request){
        $id =  $request['id']; 
        if($id==1){
               $cms = CmsTemplate::where('cms_templates.id', '=',$id)->join('cms_template_images','cms_template_images.cms_template_id','=','cms_templates.id')->select('cms_templates.*','cms_template_images.image')->get();
        }else{
                $cms = CmsTemplate::where('cms_templates.id', '=',$id)->get();
        }     
             // dd($cms);
               return $cms;
   }
   
   
}
