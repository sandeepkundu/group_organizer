<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\role_user;
use App\User;
use App\TempGroup;
use App\Group;
use App\TempGroupInterest;
use App\EmailTemplate;
use App\GroupInterest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\DB;
use Hash;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RegisterController extends Controller {

    /**
     * @param Request $request
     * @return mixed
     */
    public function register(Request $request) {
        if ($request) {
            $validator = Validator::make($request->all(), [
                        'first_name' => 'required|min:5',
                        'last_name' => 'required|min:5',
                        'email' => 'required|email|unique:users,email',
                        'password' => 'required|confirmed|min:6|max:50',
                        'password_confirmation' => 'required_with:password|min:6'
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 406);
            }

            // generate random code to verify
            $request['confirmation_code'] = str_random(30);

            // set default user role ID
            $request['role_id'] = 1;

            // define password
            $user = User::create([
                        'first_name' => $request['first_name'],
                        'last_name' => $request['last_name'],
                        'email' => $request['email'],
                        'password' => bcrypt($request['password']),
                        'confirmation_code' => $request['confirmation_code'],
                        'status' => 0
            ]);

            $data = $request->only('name', 'email', 'confirmation_code');

            // set user role id in relationship table
            DB::insert('insert into role_user (user_id, role_id) values (?, ?)', [$user->id, $request['role_id']]);

            // send email
            Mail::queue('emails.verify', $data, function($message) use ($data) {
                $message->to($data['email'])->subject('Verify Your Email Address');
            });

            return response()->json(['Congratulations! Your account created. Already ngLaravel system sent a verification email.'], 200);
        } else {
            return response()->json(['error' => 'Your field save '], 401);
        }
    }
    
    
    public function update_email(Request $request){
        
        if ($request) {
            $validator = Validator::make($request->all(), [
                        'email' => 'required|email|unique:users,email|unique:users,temp_email',
            ]);
            
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 406);
            }
            
            $request['confirmation_code'] = str_random(30);
            $user = DB::table('users')->where('id',$request['id'])->first();
            
            
            $data['confirmation_code'] = $request['confirmation_code'];
            $data['email'] = $request['email'];
            //$data['name'] = trim($user->first_name);
            
            $email_templates = EmailTemplate::find(9);

            $message = $email_templates->email_body;

            $data['email_body'] = str_replace("{{name}}", trim($user->first_name), $message);
            //dd($data);
            // set user role id in relationship table['confirmation_code' => $data['confirmation_code']]
            
            DB::table('users')->where('id', $request['id'])->update(['temp_email' => $data['email']]);
            DB::table('users')->where('id', $request['id'])->update(['confirmation_code' => $data['confirmation_code']]);
            // send email
            Mail::queue(['html' => 'emails.updateemail'], $data, function($message) use ($data) {
                $message->to($data['email'])->subject('Update Your Email Address');
            });
        }    
    }
    
    
    public function update_email_con($confirmation_code) {
        if (!$confirmation_code) {
            return response()->json('Your activation code doesn\'t exist.', 406);
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if (!$user) {
            return response()->json('Your code doesn\'t valid.', 406);
        } 

        //$user->email_confirmation = 1;
        
        $user = DB::table('users')->where('confirmation_code',$confirmation_code)->first();
        $old_email = $user->email;
        $new_email = $user->temp_email;
        
        DB::table('users')->where('id', $user->id)->update(['email' => $new_email]);
        DB::table('users')->where('id', $user->id)->update(['temp_email' => '']);
        //DB::table('users')->select('temp_email')->where('id', $user->id)->delete();
        //DB::table('users')->where('id', $request['id'])->update(['confirmation_code' => $data['confirmation_code']]);
        //$user->save();

        return response()->json('Your account verified successfully.', 200);
    }
    
    
    /**
     * @param $confirmation_code
     * @return mixed
     * @throws InvalidConfirmationCodeException
     */
    public function confirm($confirmation_code) {
        if (!$confirmation_code) {
            return response()->json('Your activation code doesn\'t exist.', 406);
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if (!$user) {
            return response()->json('Your code doesn\'t valid.', 406);
        } else if ($user->email_confirmation == 1) {
            return response()->json('you have already verified your account.', 406);
        }

        $user->email_confirmation = 1;
        $user->save();

        return response()->json('Your account verified successfully.', 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function signup(Request $request) {
        //dd($request);
        $request['userType'] = 'paiduser';
        if ($request) {
            $validator = Validator::make($request->all(), [
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'email' => 'required|email|unique:users,email',
                        'password' => 'required|min:6|max:50',
                        'userType' => 'required',
                            ], $messages = [
                        'userType.required' => 'please select sign-up radio button',
                        'userType.first_name' => 'Please enter First Name',
                        'userType.last_name' => 'Please enter Last Name',
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 406);
            }
            // generate random code to verify
            $request['confirmation_code'] = str_random(30);
            if ($request['userType'] == 'freeuser') {
                $request['userType_chossen'] = 4;
                $request['role_id'] = 4;
            } else if ($request['userType'] == 'paiduser') {
                $request['userType_chossen'] = 2;
                $request['role_id'] = 2;
            } else {
                $request['userType_chossen'] = 3;
                $request['role_id'] = 3;
            }
            
            // set default user role ID
            // define password
            $user = User::create([
                        'first_name' => $request['first_name'],
                        'last_name' => $request['last_name'],
                        'email' => $request['email'],
                        'password' => bcrypt($request['password']),
                        'confirmation_code' => $request['confirmation_code'],
                        'status' => 1,
                        'email_confirmation' => 0,
                        'payment_status' => 0,
                        'username' => trim($request['first_name']) . '_' . rand('1', '999'),
                        'createdby' => 'user Itself',
                        'memberships' => $request['userType'],
                        'user_type_id' => $request['userType_chossen'],
            ]);
            
            $email_templates = EmailTemplate::find(1);

            
            $data = $request->only('first_name', 'email', 'confirmation_code', 'email_type', 'email_subject', 'email_body');
            $data['email_type'] = $email_templates->email_type;
            $data['email_subject'] = $email_templates->email_subject;
            $message = $email_templates->email_body;

            $data['email_body'] = str_replace("{{name}}", $request['first_name'], $message);
            
            // set user role id in relationship table
            $qw = DB::insert('insert into role_user (user_id, role_id) values (?, ?)', [$user->id, $request['role_id']]);

            // send email
            Mail::queue(['html' => 'emails.verify'], $data, function($message) use ($data) {
                $message->to($data['email'])->subject('Verify Your Email Address');
            });
            $user_id = $user->id;
            if(!empty($request['event_id'])){
                $event_id = $request['event_id'];
                $event = DB::table('group_events')->select('group_id')->where('id',$event_id)->first();
                $group_id = $event->group_id;
                DB::table('event_confirmations')->insert(['event_id' => $event_id, 'user_id' => $user_id]);
                DB::table('group_members')->insert(['group_id' => $group_id, 'user_id' => $user_id,'status'=>1]);
                
            }
            return response()->json(['status' => 'success', 'data' => $data['email'], 'message' => 'Congratulations! Your account created successfully.'], 200);
        } else {
            return response()->json(['error' => 'you are fail to signup as a new use'], 401);
        }
    }
    

    /**
     * @param Request $request
     * @return mixed
     * user to create a gropu with registration 
     */
    public function grpOrgSignup(Request $request) {
        //dd($request);
        if ($request) {
            $validator = Validator::make($request->all(), [
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'email' => 'required|email|unique:users,email',
                        'password' => 'required|min:6|max:50'
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 406);
            }

            $request['userType_chossen'] = 3;
            $request['role_id'] = 3;

            if (!isset($request['tempGroupID']) || empty($request['tempGroupID'])) {
                return response()->json(['error' => 'You group is not saved successfully, please try again.'], 401);
            } else {

                //$tempgroup=TempGroup::find($request['tempGroupID'])->toArray();
                //$TempGroupInterest=TempGroupInterest::where('temp_group_interests.group_id',$request['tempGroupID'])->get()->toArray();
            }

            $user = User::create([
                        'first_name' => $request['first_name'],
                        'last_name' => $request['last_name'],
                        'email' => $request['email'],
                        'password' => bcrypt($request['password']),
                        'email_confirmation' => 1,
                        'status' => 1,
                        'payment_status' => 0,
                        'username' => trim($request['name']) . '_' . rand('1', '99'),
                        'createdby' => 'Organizer Itself',
                        'memberships' => 'Group Organizer',
                        'user_type_id' => 3,
            ]);

            if (empty($user->id)) {
                return response()->json(['error' => array('error' => [0 => 'registration failed, please try again.'])], 401);
            }

            //       $tempgroup['user_id']=$user->id;
            //       $Group=new Group($tempgroup);
            //       $grpsave= $Group->save();
            //       if(empty($Group->id)){
            //           return response()->json(['error' => array('error'=>[0=>'group failed, please try again.'])], 401);
            //       }
            //       foreach($TempGroupInterest as $key => $value) {
            //       $value['group_id']=$Group->id;
            //       $GroupInterest=new GroupInterest($value);
            //           $ct = $GroupInterest->save();
            //       }
            //       if(empty($GroupInterest->id)){
            //           return response()->json(['error' => array('error'=>[0=>'group Interest failed, please try again.'])], 401);
            //       }
            //       //$data = $request->only('name', 'email', 'confirmation_code');
            //   // set user role id in relationship table
            $qw = DB::insert('insert into role_user (user_id, role_id) values (?, ?)', [$user->id, $request['role_id']]);

            return response()->json(['status' => 'success', 'data' => $request['tempGroupID'], 'message' => 'Congratulations! Your account created successfully.'], 200);
        } else {
            return response()->json(['error' => 'you are fail to signup as a new use'], 401);
        }
    }

    /**
     * @param Request $request
     * @return mixed
     * user to create a gropu with registration 
     */
    public function getTempGroup(Request $request) {

        // dd(Auth::user());
        if ($request) {
            $validator = Validator::make($request->all(), [
                        'groupname' => 'required',
                        'description' => 'required',
                        'acceptTerms' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 406);
            }

            if (isset($request['location']) && !empty($request['location'])) {

                $getPlace = $this->getLnt($request['location']);

                if (!$getPlace) {
                    $getPlace = $this->getLnt('12345');
                }
                $city = isset($getPlace['city']) && !empty($getPlace['city']) ? $getPlace['city'] : 'city';
                $state = isset($getPlace['state']) && !empty($getPlace['state']) ? $getPlace['state'] : 'state';
                $country = isset($getPlace['country']) && !empty($getPlace['country']) ? $getPlace['country'] : 'country';
                $postalcode = isset($getPlace['postalcode']) && !empty($getPlace['postalcode']) ? $getPlace['postalcode'] : '';
                $latitude = $getPlace['latitude'];
                $longitude = $getPlace['longitude'];
            }

            $userID = '';
            $newData = TempGroup::create([
                        'name' => $request['groupname'],
                        'user_id' => $userID,
                        'header' => $request['header'],
                        'status' => 1,
                        'city' => $city,
                        'state' => $state,
                        'country' => $country,
                        'postalcode' => $postalcode,
                        'latitude' => $latitude,
                        'longitude' => $longitude,
                        'description' => $request['description']
            ]);
            $tempID = $newData->id;

            $grpInst = explode(',', $request['allInt']);
            foreach ($grpInst as $key => $value) {

                $newIntData = TempGroupInterest::create([
                            'group_id' => $tempID,
                            'category_id' => $value,
                ]);
            }

            return response()->json(['status' => 'success', 'data' => $tempID, 'message' => 'group added temporary'], 200);
        } else {
            return response()->json(['error' => 'you are fail to signup as a new use'], 401);
        }
    }

    public function editprofile(Request $request) {
        //dd($request);
        if ($request['changePasswordStatus'] == 'true') {
            $roleValidation = [
                'first_name' => 'required',
                'phone' => 'numeric|digits_between:0,11',
                'postalcode' => 'required|regex:/\b\d{6}\b/',
                'password' => 'required|confirmed|min:6|max:50',
                'password2' => 'required_with:password|min:6'
            ];
        } else {
            $roleValidation = [
                'first_name' => 'required',
                'last_name' => 'required',
                'city' => 'required',
                'state' => 'required',
                'country' => 'required',
                'postalcode' => 'required|numeric',
                'phone' => 'numeric|digits_between:0,11',
            ];
        }
        $validator = Validator::make($request->all(), $roleValidation, $message = [
            'postalcode.numeric' => 'zip code is invalid',
            'first_name.numeric' => 'First name required',
            'last_name.numeric' => 'Last name required',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 406);
        }
        ###  upload avatar
        if (file_exists("temp/" . $request['avatar_url']) && $request['avatar_url'] != '') {
            File::move("temp/" . $request['avatar_url'], "uploads/" . $request['avatar_url']);
        }
        ####
        if ($request['password'] != "")
            $request['password'] = bcrypt($request['password']);
        else
            unset($request['password']);
        if ($request['postalcode'] != "") {

            $er = $this->getLnt($request['postalcode']);
            $request['latitude'] = $er['latitude'];
            $request['longitude'] = $er['longitude'];
        }

        $User = User::find($request['id']);
        unset($request['id']);
        unset($request['email']);
        $User->update($request->all());
        return response()->json(['success'], 200);
    }

    public function setCurrentLocation(Request $request) {
        //dd($request['C_location']);
        if ($request['C_location'] != "") {
            $er = $this->getLnt($request['C_location']);
            return response()->json(['status' => 'success', 'data' => $er], 200);
        } else {
            return response()->json(['error'], 401);
        }
    }

    public function getaddress(Request $request) {
        if ($request['term'] != "") {
            $er = $this->getadd($request['term']);
            return response()->json(['status' => 'success', 'data' => $er], 200);
        } else {
            return response()->json(['error'], 401);
        }
    }

    public function chpassword(Request $request) {

        $roleValidation = [
            'password' => 'required|min:6|max:50',
            'password2' => 'required_with:password|min:6'
        ];

        $validator = Validator::make($request->all(), $roleValidation);
        if ($validator->fails())
            return response()->json(['error' => $validator->errors()], 406);


        if (!$request['id']) {
            return response()->json(['error' => 'user is not logged in '], 406);
        }
        $User = User::find($request['id']);


        if (Hash::check($request['oldpass'], $User->password)) {
            $request['password'] = bcrypt($request['password']);
        } else {
            return response()->json(['error' => ['data' => ['Current password is incorrect']]], 404);
        }

        unset($request['id']);
        unset($request['email']);
        $User->update($request->all());
        return response()->json(['success', 'message' => 'password change successfully'], 200);
    }

    /*
     * get all groups based on given params 
     * used for userside, for all group page.
     * created by dev
     * created on 12/16/2016
     */

    public function getAllCityByword(Request $request) {
        // dd($request['arg_token']);//arg_token
        $bsqd = $request['term'];
        $groups = Group::where('city', 'like', '%' . $bsqd . '%')->select('groups.city')->distinct()->get()->toArray();
        $Arr = [];
        foreach ($groups as $key => $value) {
            $Arr[] = $value['city'];
        }
        return $Arr;
    }

}
