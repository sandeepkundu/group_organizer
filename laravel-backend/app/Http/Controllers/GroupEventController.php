<?php

namespace App\Http\Controllers;

use App\Role;
use App\role_user;
use App\User;
use App\Group;
use App\Category;
use App\GroupEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\DB;
use Hash;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        /**
         * in demo version you can't delete default task manager user permission.
         * in production you should remove it
         */
//        if($id==1)
//            return response()->json(['error' => ['data'=>['You not have permission to edit this item in demo mode']]], 403);

        if(Auth::user()->can('edit_user')) {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'required|min:6',
                'phone' => 'required||min:10',
                'city' => 'required',
                'state' => 'required',
                'country' => 'required',
                'postalcode' => 'required|numeric',
                'aboutme' => 'required',
                'usertype' => 'required',
                'userinterest' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 406);
            }
            ###  upload avatar
            if (file_exists("temp/" . $request['avatar_url']) && $request['avatar_url'] != ''){
                File::move("temp/".$request['avatar_url'], "uploads/". $request['avatar_url']);
            }
            ####
            $User = User::find($id);
            //DB::table('role_user')->where('user_id',$User->id)->delete();
            //DB::insert('insert into role_user (user_id, role_id) values (?, ?)', [$User->id, $request['role_id']]);
            if ($request['password'] != '********')
                $request['password'] = bcrypt($request['password']);
            else
                $request['password'] = $User->password;
            if ($User) {
                $User->update($request->all());
                return response()->json(['success'], 200);
                //return redirect()->action('Index');
            } else
                return response()->json(['error' => 'not found item'], 404);
        } else
            return response()->json(['error' =>'You not have User'], 403);
    }

   
}
