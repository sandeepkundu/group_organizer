<?php
namespace App\Http\Controllers;
use Log;
use Laravel\Cashier\WebhookController as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\EmailTemplate;


class WebhookController extends BaseController
{
    /**
     * Handle a Stripe webhook.
     *
     * @param  array  $payload
     * @return Response
     */
    public function handleInvoicePaymentSucceeded(Request $payload)
    {   
        $data = $payload->all();
       //Log::info($payload);
       Log::info($data);
    
     if($data['type']=='invoice.payment_succeeded'){ 

        $getPayment = \DB::table('strip_transactions')->insert([
                'user_id' =>@$data['data']['object']['id'],
                'strip_id' =>@$data['data']['object']['customer'],
                'object' =>@$data['data']['object']['object'],
                'amount' =>isset($data['data']['object']['plan']['amount'])?$data['data']['object']['plan']['amount']:0,
                'balance_transaction' =>'',
                'captured' => '',
                'currency' =>isset($data['data']['object']['plan']['currency'])?$data['data']['object']['plan']['currency']:0,
                'description' =>json_encode(@$data['data']['object']['plan']),
                'receipt_email' =>'',
                'receipt_number' => 0,
                'status' =>1,
                'invoice' =>isset($data['data']['object']['plan']['amount'])?$data['data']['object']['plan']['amount']:10,
                'livemode' => '',
                'card_detail' =>json_encode(@$data['data']['object']['items']['data']), 
                'jsonresponse' => json_encode(@$data['data']['object']),
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]);
         $strip_id=$data['data']['object']['customer'];
         $group_id = \DB::table('groups')->select('id','user_id')->where('stripe_id',$strip_id)->first();
          $email_id = \DB::table('users')->select('email','first_name')->where('id','=',$group_id->user_id)->first();
         

         $month = \DB::table('group_payment_plans')->join('strip_group_payments','strip_group_payments.plan_id','=','group_payment_plans.id')->select('group_payment_plans.month','strip_group_payments.id','strip_group_payments.payment_plan_id','group_payment_plans.strip_plan_id','group_payment_plans.amount')->where('strip_group_payments.group_id',$group_id->id)->orderBy('group_payment_plans.id','desc')->first();

         
        $plan = \DB::table('group_payment_plans')->select('type','strip_plan_id')->where('id',$month->payment_plan_id)->first();
         
         $new_month = date('Y-m-d', strtotime('+'.$month->month.' months'));
         \DB::table('strip_group_payments')
                    ->where('id',$month->id )
                    ->update(['membership_end' => $new_month]);

        
                    $datas = [];
           $datas['startdate'] = date('Y-m-d',@$data['data']['object']['period_start']);
           $datas['enddate'] = date('Y-m-d',@$data['data']['object']['period_end']);         
            $datas['message'] = 'Payment Done';
           // $data['email'] = $email_id->email;
            $datas['subject'] = 'success mail';
            $datas['UserName'] = $email_id->first_name;
            $datas['plan'] = $month->strip_plan_id;
            $datas['amount'] = $month->amount;
            $sender='yashpal.singh@webnyxa.com';
            $single =  $email_id->email;

              /*mail::queue('emails.success_subscription',$data, function($message) use ($data) {
>>>>>>> b75d6f09c17eb1f024fffe39bf8c5a078f5390d5
                $message->to($data['email'])->subject($data['subject']);
            });*/
            Mail::send('emails.success_subscription', ['data' => $datas], function($m) use ($datas,$single,$sender) {
                       $m->from($sender);

                       $m->to($single)->subject($datas['subject']);
               });
        }else if($data['type']=='invoice.payment_failed')
        {
            $getPayment = \DB::table('strip_transactions')->insert([
                'user_id' =>@$data['data']['object']['id'],
                'strip_id' =>@$data['data']['object']['customer'],
                'object' =>@$data['data']['object']['object'],
                'amount' =>isset($data['data']['object']['plan']['amount'])?$data['data']['object']['plan']['amount']:0,
                'balance_transaction' =>'',
                'captured' => '',
                'currency' =>isset($data['data']['object']['plan']['currency'])?$data['data']['object']['plan']['currency']:0,
                'description' =>json_encode(@$data['data']['object']['plan']),
                'receipt_email' =>'',
                'receipt_number' => 0,
                'status' =>'failed',
                'invoice' =>isset($data['data']['object']['plan']['amount'])?$data['data']['object']['plan']['amount']:10,
                'livemode' => '',
                'card_detail' =>json_encode(@$data['data']['object']['items']['data']), 
                'jsonresponse' => json_encode(@$data['data']['object']),
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]);
             $strip_id=$data['data']['object']['customer'];
            $group_id = \DB::table('groups')->select('id','user_id')->where('stripe_id',$strip_id)->first();
        $email_id = \DB::table('users')->select('email','first_name')->where('id','=',$group_id->user_id)->first();

         $month = \DB::table('group_payment_plans')->join('strip_group_payments','strip_group_payments.plan_id','=','group_payment_plans.id')->select('group_payment_plans.month','strip_group_payments.id','strip_group_payments.payment_plan_id','group_payment_plans.strip_plan_id','group_payment_plans.amount')->where('strip_group_payments.group_id',$group_id->id)->orderBy('group_payment_plans.id','desc')->first();
         $new_month = date('Y-m-d', strtotime('+'.$month->month.' months'));
         /*\DB::table('strip_group_payments')
                    ->where('id',$month->id )
                    ->update(['membership_end' => $new_month]);*/


            $datas = [];
           $datas['startdate'] = date('Y-m-d',@$data['data']['object']['period_start']);
           $datas['enddate'] = date('Y-m-d',@$data['data']['object']['period_end']);          
            $datas['message'] = 'Payment failure';
           // $data['email'] = $email_id->email;
            $datas['subject'] = 'failure mail';
            $datas['UserName'] = $email_id->first_name;
            $datas['plan'] = $month->strip_plan_id;
            $datas['amount'] = $month->amount;
            $sender='yashpal.singh@webnyxa.com';
            $single =  $email_id->email;
         Mail::send('emails.failure_subscription', ['data' => $datas], function($m) use ($datas,$single,$sender) {
                       $m->from($sender);

                       $m->to($single)->subject($datas['subject']);
               });
        }

 
        
        
        
                
    }
    
}