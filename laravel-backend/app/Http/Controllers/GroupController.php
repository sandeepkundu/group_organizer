<?php

namespace App\Http\Controllers;

use App\Role;
use App\role_user;
use App\User;
use App\EventConfirmation;
use App\Group;
use App\Category;
use App\GroupEvent;
use App\EmailTemplate;
use App\EventPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Support\Facades\DB;
use Hash;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GroupImage;
class GroupController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
       $this->middleware('jwt.auth', ['except' => ['authenticate','uploadimage','deleteUpload']]);
    }


    /*
     * Export Excel Method
     */
    public function exportFile(Request $request) {
        //dd($request);
        ### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export CSV permission
        if ($request['export_type'] == 'csv' && !Auth::user()->can('export_csv'))
            return 'You not have this permission';

        ### Check export EXCEL permission
        if ($request['export_type'] == 'xls' && !Auth::user()->can('export_xls'))
            return 'You not have this permission';



        ### record_type 1 equal whole records and 2 equals selected records
        if ($request['record_type'] == 1) {
            $users = User::all();
        } else if ($request['record_type'] == 2) {
//            return $request['selection'];
//            $temp = explode(",", $request['selection']);
            //        foreach($temp as $val) {
            //             $users = User::find($val);
//            }
            $users = User::findMany($request['selection']);
        }

        ###
        if ($request['export_type'] == 'pdf') { //export PDF
            $html = '<h1 style="text-align: center">YEP ngLaravel PDF </h1>';
            $html .= '<style> table, th, td {text-align: center;} th, td {padding: 5px;} th {color: #43A047;border-color: black;background-color: #C5E1A5} </style> <table border="2" style="width:100%;"> <tr> <th>Name</th> <th>Email</th> </tr>';
            foreach ($users as $user) {
                $html .="<tr> <td>$user->name</td> <td>$user->email</td> </tr>";
            }
            $html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $headers = array(
                'Content-Type: application/pdf',
            );
            $pdf->loadHTML($html);
//            return $pdf->download('permission.pdf', $headers);
        } else {
            Excel::create('user', function ($excel) use ($users) {
                $excel->sheet('Sheet 1', function ($sheet) use ($users) {
                    $sheet->fromArray($users);
                });
            })->download($request['export_type']);
        }
    }





    /*
     *  Search Method
     */
     public function search(Request $request)
    {
        //dd($request);
        $per_page = \Request::get('per_page') ?: 10;
        ### search
        if ($request['query']) {
            $Group = Group::search($request['query'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Group->count();
            $Group = $Group->slice($page * $per_page, $per_page);
            $Group = new \Illuminate\Pagination\LengthAwarePaginator($Group, $total, $per_page);
            return  $Group;
        }
        return 'not found';
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per_page = \Request::get('per_page') ?: 10;
                
        if(Auth::user()->can('view_group')){
            $groupdata=Group::orderBy('name', 'asc')->paginate($per_page);
            $category = array();
            $num=0;
            $current_date = date("Y-m-d");
            foreach($groupdata as $single_group){
                
		$group_id = $single_group->id;
                
//                if(!empty($single_group->GroupInterest)){
//                    $group_interest = $single_group->GroupInterest;
//                    $group_int=$group_interest->category_name;
//                }else{
//                    $group_int = "N/A";
//                }
//                dd($group_int);
                $count_group_member = DB::table('group_members')->where('group_id','=',$group_id)->where('status','=','1')->count();
                
                $count_events = DB::table('group_events')->where('group_id','=',$group_id)->where('event_date','>',$current_date)->count();
                
                $pending_group_member = DB::table('group_members')->where('group_id','=',$group_id)->where('status','=','2')->count();
               
                $group_int=DB::table('group_interests')->select(DB::raw('GROUP_CONCAT(categories.category_name) AS category'))
                        ->join('categories','categories.id','=','group_interests.category_id')
                        ->where('group_id', '=', $group_id)
                        ->first();
               
                if(!empty($group_int->category)){
                    $single_group->group_int= $group_int->category;
                }else{
                    $single_group->group_int = 'N/A';
                }
                //dd($category);
                //dd($single_group);
                $group_owner = $single_group->User;
                if(!empty($group_owner)){
                    $owner_name=$group_owner->first_name;
                }else{
                    $owner_name = "Owner Deleted";
                }
                
                $single_group->pending_request= $pending_group_member;
                $single_group->owner_name=$owner_name;
                 $single_group->counts_event = $count_events;
//                $single_group->group_member=$group_member;
                $single_group->count_group_member=$count_group_member;
                $groupdata[$num] =$single_group;

                $num++;
            }
            //dd($groupdata);
            
            
            return $groupdata;
        }
        else
            return response()->json(['error' =>'You have not any permission'], 403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//       dd($request);
       if(Auth::user()->can('add_group')) {
            if ($request) {
             $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'description'=>'required',
//                    'terms_and_condition'=>'required',
                    'city' => 'required',
                    'state' => 'required',
                    'country' => 'required',
                    'postalcode' => 'numeric|required',
                    'category_name' => 'required',
                    'group_owner_name' =>'required',
                    'avatar_url'=>'required'
                ],
                     $messages = [
                'group_owner_name.required'   => 'The Group owner field is required',
                'avatar_url.required'   => 'The Image field is required',         
                'category_name.required'   => 'The Category name field is required',
                ]);
                
                if ($validator->fails()) {
                    return response()->json(['error' => $validator->errors()], 406);
                }
                $temp_name=$request['name'];
                $temp_loc=$request['city'];
                $g_data = DB::table('groups')
                    ->Where('city','like','%'.$temp_loc.'%')
                    ->Where('name','like','%'.$temp_name.'%')
                    ->get();
                if(count($g_data)>0){
                    $val['messages'][0]="Group name already taken, Please change.";
                    return response()->json(['error' => $val], 406);
                }

                ###  upload avatar
                if (file_exists("temp/" . $request['avatar_url']) && $request['avatar_url'] != ''){
                    File::move("temp/".$request['avatar_url'], "uploads/". $request['avatar_url']);
                }
                ####
//                dd($request['group_owner_name']);
//                $request['user_id'] = $request['group_owner_name']; 
                
                //if(Auth::user()->id==1){
                    $request['status'] = 1;
                //}
                
//                $name_email = explode("(", $request['group_owner_name']);
                $name_email = strstr($request['group_owner_name'] ,"(");
//                dd($name_email);
                $email= str_replace(array("(",")"),array("",""),$name_email);
                
                $user_id = DB::table('users')->select('id')->where('email','=',$email)->first();
                $request['user_id'] = $user_id->id;
                
                if($request['postalcode'] !=""){
                
                    $er= $this->getLnt($request['postalcode']);
                    $request['latitude'] = $er['latitude'];
                    $request['longitude'] = $er['longitude'];
                } 

                // If the user created by admin then created by will be 1, otherwise created by will be 0  
                if(Auth::user()->id==1){
                    $request['createdby'] = 1;
                }
                
                $group=Group::create($request->all());
                
                
                $category_name = $request['category_name'];
                
                foreach($category_name as $single_category){
                    
                    $category_id = DB::table('categories')->select('id')->where('category_name','=',$single_category)->first();
                    DB::insert('insert into group_interests (group_id, category_id) values (?, ?)', [$group->id, $category_id->id]);
                    
                }
                
                
                
                
                    //dd($request);
                
//                dd($request['category_name']);
                
                
                //DB::insert('insert into role_user (user_id, role_id) values (?, ?)', [$user->id, $request['role_id']]);
                return response()->json(['success'], 200);
            } else {
                return response()->json(['error' => 'can not save product'], 401);
            }
      }else
            return response()->json(['error' =>'You not have User'], 403);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //dd($id);
        $Group = Group::find($id);
        
        if(!empty($Group->GroupInterest)){
            
            $group_interest=DB::table('categories')
                            ->join('group_interests', 'group_interests.category_id', '=', 'categories.id')
                            ->where('group_interests.group_id', '=', $id)
                            ->select('categories.category_name')
                            ->get();
        
           
        }
        if(!empty($group_interest)){
            foreach($group_interest as $singleinterest){
                $instArr[]= $singleinterest->category_name;
            }
            $Group->category_name=$instArr;
        }
        
         
       
//        dd($group_interest);
        $group_member = $Group->GroupMember;
        
        $count_group_member = count($group_member);
        //$num=0;
        //$member=array();
        $user_id=array();
        foreach($group_member as $single_member){
            $user_id[] = $single_member->user_id;
  
        }
		
        $user= implode(",",$user_id);
        if(!empty($user)){
            $member_details =  DB::table('users')->select('id','first_name as name', 'email')->whereIn('id',$user_id)->get();
        }else{
            $member_details = "";
        }
                //dd($member_details);
        
        
        $owner_id = $Group['user_id'];
        
        $user_list = DB::table('users')
                        ->select(DB::raw('CONCAT(first_name,"(", email,")") AS name'))
                        ->where('id', '=', $owner_id)->first();
        if(!empty($user_list)){
            $Group->group_owner_name = $user_list->name;
        }else{
            $Group->group_owner_name = "Owner Deleted";
        }
        $current_date = date("Y-m-d");
        $event = DB::table('group_events')->select('id','group_id','name','event_date','description')->where('group_id',$id)->where('event_date','>=',$current_date)->orderBy('event_date')->get();
        //dd($event);
        $Group->members = $member_details;
        
        $Group->members_count = $count_group_member;
        $Group->events=$event;
        
//        $Group->categories=category::all();
        $Group->ImageGallery=GroupImage::where('group_id',$id)->get();    
        
        if($Group)
            return $Group;
        else
            return response()->json(['error' => 'not found item'], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dd($id);
        $editGroup = Group::find($id);
        //dd($editGroup);
        if($editGroup)
            return response()->json(['success'=>$editGroup], 200);
        else
            return response()->json(['error' => 'not found item'], 404);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /**
         * in demo version you can't delete default task manager user permission.
         * in production you should remove it
         */
//        if($id==1)
//            return response()->json(['error' => ['data'=>['You not have permission to edit this item in demo mode']]], 403);

        
        if(Auth::user()->can('edit_group')) {
            $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'description'=>'required',
//                    'terms_and_condition'=>'required',
                    'city' => 'required',
                    'state' => 'required',
                    'country' => 'required',
                    'avatar_url'=> 'required',
                    'postalcode' => 'numeric|required',
                    'category_name' => 'required',
            ],
                    $messages = [
                'category_name.required'   => 'The Category name field is required',
                'avatar_url.required'   => 'The Image field is required',  
                ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 406);
            }
            $temp_name=$request['name'];
            $temp_loc=$request['city'];
            $g_data = DB::table('groups')
                ->Where('city','like','%'.$temp_loc.'%')
                ->Where('name','like','%'.$temp_name.'%')
                ->Where('id','<>',$id)
                ->get();
            if(count($g_data)>0){
                $val['messages'][0]="Group name already taken, Please change.";
                return response()->json(['error' => $val], 406);
            }
            ###  upload avatar
            if (file_exists("temp/" . $request['avatar_url']) && $request['avatar_url'] != ''){
                File::move("temp/".$request['avatar_url'], "uploads/". $request['avatar_url']);
            }
            ####
            $Group = Group::find($id);
             if($request['postalcode'] !=""){
                
                $er= $this->getLnt($request['postalcode']);
                    $request['latitude'] = $er['latitude'];
                    $request['longitude'] = $er['longitude'];
            } 
            
            
            
            $category_name = $request['category_name'];
            $category_id = DB::table('categories')->select('id')->where('category_name','=',$category_name)->first();
            //dd($request['category_name']);
            DB::table('group_interests')->where('group_id',$id)->delete();
            foreach($request['category_name'] as $groupinterest){
                
                $category_id = DB::table('categories')->select('id')->where('category_name', '=' ,$groupinterest)->first();
                
                
               
                DB::insert('insert into group_interests (group_id, category_id) values (?, ?)', [$id, $category_id->id]);
            }

            //
            //DB::insert('insert into group_interests (group_id, category_id) values (?, ?)', [$id, $category_id->id]);

            if ($Group) {
                $Group->update($request->all());
                return response()->json(['success'], 200);
                //return redirect()->action('Index');
            } else
                return response()->json(['error' => 'not found item'], 404);
        } else
            return response()->json(['error' =>'You not have User'], 403);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function editProfile(Request $request, $id)
    {
//        if($id==1)
//            return response()->json(['error' => ['data'=>['You not have permission to edit this item in demo mode']]], 403);

        if(Auth::user()->can('edit_profile')) {
            if ($request['changePasswordStatus'] == 'true'){
                $roleValidation=[
                    'name' => 'required',
                    'email' => 'required|email',
                    'phone' => 'numeric|digits_between:0,11',
                    'currentPassword' => 'required',
                    'newPassword' => 'required|confirmed|min:6|max:50|different:currentPassword',
                    'newPassword_confirmation' => 'required_with:newPassword|min:6'
                ];
            }else{
                $roleValidation=[
                    'name' => 'required|min:5',
                    'email' => 'required|email',
                    'phone' => 'numeric|digits_between:0,11',
                ];
            }
            $validator = Validator::make($request->all(), $roleValidation);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 406);
            }
            ###  upload avatar
            if (file_exists("temp/" . $request['avatar_url']) && $request['avatar_url'] != ''){
                File::move("temp/".$request['avatar_url'], "uploads/". $request['avatar_url']);
            }
            ####
            $User = User::find($id);
            if ($request['changePasswordStatus'] == 'true')
            {
                if(Hash::check($request['currentPassword'], $User->password )){
                    $request['newPassword'] = bcrypt($request['newPassword']);
                }else{
                    return response()->json(['error' => ['data'=>['Current password is incorrect']]], 404);
                }
            }
            else
                $request['newPassword'] = $User->password;

                $request['password']=$request['newPassword'];
                $User->update($request->all());
                return response()->json(['success'], 200);


        } else
            return response()->json(['error' =>['data'=>['You don\'t have permission']]], 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $temp= $request['id'];
        
        if(Auth::user()->can('delete_group')) {
//            $temp = explode(",", $id);
            foreach($temp as $val){
                /**
                 * in demo version you can't delete default task manager user permission.
                 * in production you should remove it
                 */
//                if($val==1)
//                    return response()->json(['error' =>'You not have permission to delete this item in demo mode'], 403);

                DB::table('groups')->where('id', '=', $val)->delete();
//                $Group = Group::find($val);
                //dd($Group);
//                $Group->delete();
                
                // Delete group interest table
                $a=DB::table('group_interests')->where('group_id',$val)->get();
               
                if(!empty($a)){
                    DB::table('group_interests')->where('group_id',$val)->delete();
                }
                
                // Delete group events table
                $b = DB::table('group_events')->where('group_id',$val)->get();
                if(!empty($b)){
                    DB::table('group_events')->where('group_id',$val)->delete();
                }
                
                // Delete group images table
                $c= DB::table('group_images')->where('group_id',$val)->get();
                if(!empty($c)){
                    DB::table('group_images')->where('group_id',$val)->delete();
                }
                
                // Delete group member table
                $d = DB::table('group_members')->where('group_id',$val)->get();
                if(!empty($d)){
                    DB::table('group_members')->where('group_id',$val)->delete();
                }    
            }
            return response()->json(['success'], 200);
        } else
            return response()->json(['error' =>'You not have User'], 403);
    }
    
    
    
    public function event_payment(Request $request)
    {
        if(count($request)>0){
            
            $data = new EventPayment;
            
            $data->user_id = $request['user_id'];
            $data->event_id = $request['event_id'];
            $data->intent = $request['payment_detail']['intent'];
            $data->payer_id = $request['payment_detail']['payerID'];
            $data->payment_id = $request['payment_detail']['paymentID'];
            $data->payment_token = $request['payment_detail']['paymentToken'];
            
            $data->save();
            return response()->json(['success'], 200);
        }
        
    }
    
    
    
    public function create_event(Request $request)
    {
        
       //dd($request); 
       if(Auth::user()->can('add_event')) {
           //dd($request->all());
            if ($request) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'event_date'=>'required',
                    'description'=>'required',
                    'start_time'=>'required',
                    'end_time'=>'required',
                    'end_date'=>'required',
                    //'location'=>'required',
                    'address'=>'required',
                    //'avatar_url'=>'required',

                    'status'=>'required',
                ],
                        $messages = [
                'event_date.required'   => 'The Event date field is required',
                'start_time.required'   => 'The Event Start Time is required',
                'end_date.required'   => 'The Event End Date is required',
                'end_time.required'   => 'The Event End Time is required',
                //'location.required'   => 'The Event Location is required',
                'address.required'   => 'The Event Address is required',
                //'avatar_url.required' => 'The Image field is required',
                ]);
                
                $from=strtotime($request['start_time']);
                $to=strtotime($request['end_time']);
                if($from>$to){
                    $errMsg['error']=array('msg'=>'start time should be smaller than end time');
                    return response()->json(['error' =>$errMsg], 406);
                }

                if ($validator->fails()) {
                    return response()->json(['error' => $validator->errors()], 406);
                }
                
//                $current_date = date("Y-m-d");
//                
//                if($current_date>$request['event_date']){
//                    return response()->json(['error' => 'Select Valid Date'], 401);
//                }
                //dd($request['event_date']);
                if(!$request['recurring']){
                    $request['recurring']=0;
                }
                
                $event['group_id']= $request['group_id'];
                $event['name']= $request['name'];
                $event['description']= $request['description'];
                
                //$event['recurring']= $request['recurring'];
                $event['start_time']= $request['start_time'];
                $event['end_time']= $request['end_time'];
                $event['location']= $request['location'];
                $event['address']= $request['address'];
                
                $event['status']= $request['status'];
                $event['cost'] = $request['cost'];
                $event['end_date']=date("Y-m-d", strtotime($request['end_date']));
                $event['recurring_payment'] = $request['recurring_payment'];
                $event['recurring_week'] = $request['recurring_week'];
                $event['recurring_day'] = $request['recurring_day'];
                $event['recurring_month'] = $request['recurring_month'];
                $event['recurring_date'] = $request['recurring_date'];
                $event['max_member'] = $request['max_member'];
                
                if(!empty($request['recur__week_number']) || !empty($request['recur__mon_number'])){
                    
                    $key=0;
                    if(!empty($request['recur__week_number'])){
                        $count = $request['recur__week_number'];
                    }else{
                        $count = $request['recur__mon_number'];
                    }
                    $event_date = date("Y-m-d", strtotime($request['event_date']));
                    for($key = 0;$key < $count; $key++){
                        if(!empty($request['recur__week_number'])){
                        $event['event_date']=date('Y-m-d',strtotime("+".$key." week", strtotime($event_date)));
                        }else{
                            $event['event_date']=date('Y-m-d',strtotime("+".$key." month", strtotime($event_date)));
                        }
                        
                        $random_name = $this->generateRandomString();
                        //dd($random_name);
                        if (file_exists("temp/" . $request['avatar_url']) && $request['avatar_url'] != '') {
                            File::copy("temp/" . $request['avatar_url'], "uploads/" . $random_name.".jpg");
                            $event['avatar_url']= $random_name.".jpg";
                        }
                        
                        
                        $group=  GroupEvent::create($event);
                        if(!empty($request['category_name'])){

                        //DB::table('event_interests')->where('event_id', '=', $request['id'])->delete();
                        foreach($request['category_name'] as $single_category){
                                $category_id = DB::table('categories')->select('id')->where('category_name','=',$single_category)->first();
                                //dd();
                                DB::insert('insert into event_interests (event_id, category_id) values (?, ?)', [$group['id'], $category_id->id]);
                            }

                        }
                        
                    }
                    
                }else{
                    if (file_exists("temp/" . $request['avatar_url']) && $request['avatar_url'] != '') {
                        File::move("temp/" . $request['avatar_url'], "uploads/" . $request['avatar_url']);
                        $event['avatar_url']= $request['avatar_url'];
                    }
                    $event['event_date']=date("Y-m-d", strtotime($request['event_date']));
                    
                    if(isset($request['id']) && $request['id'] !=''){
                        if(!empty($request['id'])){
                            if(!empty($request['category_name'])){

                            DB::table('event_interests')->where('event_id', '=', $request['id'])->delete();
                            foreach($request['category_name'] as $single_category){
                                    $category_id = DB::table('categories')->select('id')->where('category_name','=',$single_category)->first();
                                    //dd();
                                    DB::insert('insert into event_interests (event_id, category_id) values (?, ?)', [$request['id'], $category_id->id]);
                                }

                            }
                        }
                        $group=GroupEvent::find($request['id'])->update($event);
                    }else{
                       // $event['status'] = 1;
                        $group=  GroupEvent::create($event);
                        if(!empty($request['category_name'])){

                            //DB::table('event_interests')->where('event_id', '=', $request['id'])->delete();
                            foreach($request['category_name'] as $single_category){
                                    $category_id = DB::table('categories')->select('id')->where('category_name','=',$single_category)->first();
                                    //dd();
                                    DB::insert('insert into event_interests (event_id, category_id) values (?, ?)', [$group['id'], $category_id->id]);
                                }

                            }
                    }
                }
                
                
                
                
                
                
                 if($group)   
                   return response()->json(['success'], 200);
                 else
                   return response()->json(['error' => 'can not save event'], 401);
                        
            } else {
                return response()->json(['error' => 'can not save event'], 401);
            }
      }else
            return response()->json(['error' =>'You not have User'], 403);

    }
    
    public function generateRandomString($length = 25) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    public function delete_event(Request $request)
    {
        $id=$request->all();
        
        if(Auth::user()->can('delete_event')) {
          
           DB::table('group_events')->where('id', '=',$id['id'])->delete();
           
            return response()->json(['success'], 200);
        } else{
            return response()->json(['error' =>'You not have User'], 403);
        }
    }
    
    public function show_single_event(Request $request)
    {
        $event_id = $request['id'];
        $data = GroupEvent::find($event_id);
        $category = DB::table('event_interests')
                    ->join('categories', 'categories.id', '=', 'event_interests.category_id')
                    ->select('categories.category_name')->where('event_id', '=', $event_id)->get();
        //dd($category);    
        if(!empty($category)){
            foreach($category as $cate){
                $instArr[]= $cate->category_name;
            }
            $data['category_name'] = $instArr;
        }
        $data['event_date'] = date("m/d/Y", strtotime($data->event_date));
        $data['end_date'] = date("m/d/Y", strtotime($data->end_date));
       
        return $data;
    }
    
    public function show_event(Request $request)
    {
        $id=$request->all();
        
        if(Auth::user()->can('delete_event')) {
          
            $Event = GroupEvent::find($id);
           
            return $Event;
        } else{
            return response()->json(['error' =>'You not have User'], 403);
        }
    }
    
    public function delete_member(Request $request)
    {
//        dd($request);
        $aid=$request->all();
        $GID = $aid['group_id'];
        $UID = $aid['user_id'];
        $getMember = DB::table('group_members')->where('group_id', '=',$GID)->where('user_id', '=',$UID)->first();

        if(isset($getMember->id) && $getMember->id !=""){

        

        if(Auth::user()->can('delete_event')) {
          
            DB::table('group_members')->where('id', '=',$getMember->id)->delete();
           
                return response()->json(['success'], 200);
            } else{
                return response()->json(['error' =>'You not have User'], 403);
            }
        }
    }
    
    
    
//    public function get_categories()
//    {
//        $categories = Category::all();
//        
//        if(!empty($categories)){
//            return $categories;
//        }else{
//            return response()->json(['error' =>'You have not any category'], 403);
//        }
//         
//    }



    public function uploadimage()
    {
        
        
        $Group_id=Input::input('group_id');
        
        $file = Input::file('file');
        $size = File::size($file);
        $destinationPath = public_path() . '/groupGallery/';
        $extension = $file->getClientOriginalExtension();
        $filename = str_random(25).'.'.$extension;
        
        $upload_success = Input::file('file')->move($destinationPath, $filename);
        if ($upload_success) {
            $GroupImage = new GroupImage();
                $grpfilesave=array();
                $grpfilesave['group_id']= $Group_id;
                $grpfilesave['image']= $filename;
                $grpfilesave['image_size']= $size;
                $grpfilesave['image_type']= $extension;
                $groupSave=  $GroupImage->saveimage($grpfilesave);
                
            if($groupSave){       
              return response()->json(['filename'=>$filename,'size'=>$size,'lastID'=>$groupSave->id]);
            }else{
                return 'YEP: Problem in file upload';
            }
        } else {
            return 'YEP: Problem in file upload';
        }

    }

    #### delete User avatar
    public function deleteUpload($id)
    {
        
        $filename = $id;
        $temp_dir = public_path() .DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
        $path_final_dir = public_path() .DIRECTORY_SEPARATOR.'groupGallery'.DIRECTORY_SEPARATOR;
        //dd($path_final_dir.$filename);
        if(File::delete($path_final_dir.$filename) || File::delete($temp_dir.$filename))
        {
            $r= DB::table('group_images')->where('image', '=',$filename)->delete();

//            $temp=User::find($id);
//            $temp->update(['avatar_url'=>'']);  ### update avatar_url to null in database
            if($r)
            return 1;
            else
            return 0;                         ### update avatar_url to null in html
        }
        else
        {
            return 0;
        }
    }
    
    public function status(Request $request){
        if(!empty($request)){
            DB::table('groups')
            ->where('id', $request['id'])
            ->update(['status' => $request['status']]);
            
            
            $data = DB::table('users')
                    ->join('groups', 'groups.user_id', '=', 'users.id')
                    ->where('groups.id', '=', $request['id'])
                    ->select('users.first_name','groups.name as group_name','users.email')
                    ->first();
            
            if($request['status']==1){
                $data->status = "Activated"; 
                $data->subject="Account Activated";
            }else{
                $data->status= "Deactivated";
                $data->subject="Account Deactivated";
            }
            $data = (array)$data;
            
            
            $msg = DB::table('email_templates')->where('id', '=', 4)->select('email_body as body','email_subject as subject')->first();
            $msg = (array)$msg;
            
            
            $message['body'] = str_replace(array('{{name}}','{{group_name}}','{{status}}'), array(trim($data['first_name']),$data['group_name'],$data['status']), $msg['body']);
            
            //dd($message->body);
            Mail::queue('emails.status', $message, function($message) use ($data) {
                $message->to($data['email'])->subject($data['subject']);
            });
            
            return response()->json(['success'], 200); 
        }else{
            return "Can not Update";
        }
        
    }
    
    public function member_list(Request $request){
        
        if(!empty($request)){
            $id = $request['id'];
            //$group_member = $id->GroupMember;
            $group_member = DB::table('group_members')->where('group_id', $id)->where('status',2)->get();
            $group_name = DB::table('groups')->select('name')->where('id', $id)->first();
            
            $member=array();
            $num=0;
            foreach($group_member as $single_user){
                
                $user_id = $single_user->user_id;
                $user = DB::table('users')->where('id', $user_id)->first();
//                dd($user[0]->name);
                $single_user->name =  $user->first_name;
                $single_user->city = $user->city;
                //$single_user->url = "bevylife/#/admin/users/".$user_id."/edit";
                $member['members'][$num] = $single_user;
                $num++;
            }
            $member['groupname']=$group_name->name;
            return $member; 
        }else{
            return "Please priovide id";
        }
        
    }
    
    public function member_accept(Request $request){
        if(!empty($request)){
            $id = $request['id'];
            DB::table('group_members')->where('id', $id)->update(['status' =>1]);
        }else{
            return "Please priovide id";
        }
    }
    
    public function member_reject(Request $request){
        if(!empty($request)){
            $id = $request['id'];
            DB::table('group_members')->where('id', $id)->update(['status' =>0]);
        }else{
            return "Please priovide id";
        }
    }
    
    
    public function dashboard(){
        

        
        if (Auth::user()->can('view_user')) {
           
            $total_user = DB::table('users')->count();
            $total_group = DB::table('groups')->count();
            $total_trans = DB::table('strip_transactions')->where('user_id','<>','')->count();
            $total_event = DB::table('group_events')->count();
            $active_user = DB::table('users')->where('status','=','1')->count();
            
            $active_group = DB::table('groups')->where('status','=','1')->count();
            
//            $deactive_group = $total_group-$active_group;
//            
//            $deactive_user = $total_user-$active_user;
            
            $active_user_per = ($active_user/$total_user)*100;
            
            $active_group_per = ($active_group/$total_group)*100;
            
            $data['total_user']= $total_user;
            $data['total_group']= $total_group;
            $data['total_trans']= $total_trans;
            $data['total_event']= $total_event;
            $data['active_user']= $active_user;
            $data['active_group']= $active_group;
            $data['active_user_per']= round($active_user_per);
            $data['active_group_per']= round($active_group_per);
            
            $event = DB::table('group_events')->get();
            $data['event']=$event;
            return $data;
            
        }else
        return response()->json(['error' => 'You not have User'], 403);
        
    }
    
     public function get_event(Request $request){
        
         if (Auth::user()->can('home_user_profile')) {
             $id = $request['id'];
//             $group_id = DB::table('group_members')->where('user_id','=',$id)->get();
//             $all_group = implode(',',$group_id['group_id']);
//             dd($all_group);
             $data =array();
             $group_interest=DB::table('group_events')
                        ->join('groups', 'group_events.group_id', '=', 'groups.id')
                        ->join('group_members', 'group_members.group_id', '=', 'groups.id')
                        ->where('group_members.user_id', '=', $id)
                        ->orderBy('group_events.event_date')
                        ->select('group_events.*')
                        ->get();
             
             $interest_data = array();
             if(!empty($group_interest)){
                $num = 0;
                foreach($group_interest as $singleinterest){
                   $event_id = $singleinterest->id;
                   $user_con = DB::table('event_confirmations')->where('event_id','=',$event_id)->where('user_id','=',$id)->count();
                   if($user_con == 1){
                       $singleinterest->event_con = 1;
                   }else{
                       $singleinterest->event_con = 0;
                   }
                   $interest_data[$num] = $singleinterest;
                   $num++;
                }
             }
             
             $groupdata=DB::table('groups')
                            ->join('group_members', 'group_members.group_id', '=', 'groups.id')
                            ->where('group_members.user_id', '=', $id)
                            ->orderBy('groups.name')
                            ->select('groups.name','groups.id','groups.header','groups.description','groups.avatar_url' )
                            ->get();
             
             $data['events'] =  $interest_data;
             $data['groups'] =  $groupdata;
             
            return $data;
         }
     }
     
     public function event_date (Request $request){
         
          if (Auth::user()->can('view_group')) {
              $event_date = $request['date'];
              $user_id = Auth::user()->id;
              $data =array();
              $group_interest=DB::table('group_events')
                            ->join('groups', 'group_events.group_id', '=', 'groups.id')
                            ->join('group_members', 'group_members.group_id', '=', 'groups.id')
                            ->where('group_members.user_id', '=', $user_id)
                            ->where('group_events.event_date', '=', $event_date)
                            ->orderBy('group_events.event_date')
                            ->select('group_events.*')
                            ->get();
              $data['events'] =  $group_interest;
              return $data;
          }else{
              return "Not an user";
          }
     }
     
     
     public function get_list(){
         if (Auth::user()->can('view_group')) {
             $data= '';
             $groupdata= DB::table('groups')->select('name')->get();
             $catdata= DB::table('categories')->select('category_name')->get();
             $data['group']=$groupdata;
             $data['category'] = $catdata;
         }
//         dd($data);
         return $data;
     }
     
     public function event_con(Request $request){
         $status = $request['status'];
         $event_id = $request['event_id'];
         $user_id = Auth::user()->id;
         
         DB::insert('insert into event_confirmations (user_id, event_id, user_status) values (?, ?,?)', [$user_id,$event_id,$status ]);
         
         return "Success";
     }
     
     public function event_rsvp(Request $request){
        $event_id = $request['id'];
        $event_con = DB::table('event_confirmations')
            ->join('users', 'event_confirmations.user_id', '=', 'users.id')
            ->where('event_confirmations.event_id', '=', $event_id)
            ->whereIn('event_confirmations.user_status', [1,2])
            //show_event->where('event_confirmations.user_status', '=', 2)
            ->select('users.first_name as name','users.id','event_confirmations.user_status')
            ->get();
        
        return $event_con;
     }


     public function confirmRsvp(Request $request){
        
        $id = Auth::user()->id;
        if(!empty($request->all())){

            $request['user_id']=$id;
            $request['user_status']=2;
            EventConfirmation::create($request->all());
            
            return response()->json(['success'], 200); 
        }else{
            return "Can not Update";
        
        }
    }


    public function getRsvp(Request $request){
        
        $id = Auth::user()->id;
        if(!empty($request->all())){

            $present= EventConfirmation::where('user_status',1)
            ->where('event_id',$request['event_id'])
            ->where('user_id',$id)
            ->first();
            if($present)
            return response()->json(['status'=>'success','data'=>$present->id], 200); 
            else
             return response()->json(['status'=>'success','data'=>null], 200);    
        }else{
            return "Can not Update";
        
        }
    }
    
    public function user_event_status(Request $request){
        
        
        $user_id = $request['id'];
        $status = $request['status'];
        $event_id = $request['event_id'];
        $event_con = DB::table('event_confirmations')
                    ->join('users', 'event_confirmations.user_id', '=', 'users.id')
                    ->where('event_confirmations.event_id', '=', $event_id)
                    ->whereIn('event_confirmations.user_status',[1,2] )
                    ->select('users.first_name','users.id')
                    ->get();
        $data['data'] = $event_con;
        if($status==1){
            
            $event = DB::table('group_events')->where('id', '=', $event_id)->select('name')->first();
            
            $user = DB::table('users')->select('first_name','email')->where('id', $user_id)->first();
            
            $msg = DB::table('email_templates')->where('id', '=', 7)->select('email_body as body','email_subject as subject')->first();
            
            $message['body'] = str_replace(array('{{name}}','{{event_name}}'), array($user->first_name,$event->name), $msg->body);
            $data['email']  =  $user->email;
            $data['subject'] =  $msg->subject;

            Mail::queue('emails.sugg', $message, function($message) use ($data) {
                $message->to($data['email'])->subject($data['subject']);
            });
            
            DB::table('event_confirmations')->where('user_id', $user_id)->where('event_id', $event_id)->update(['user_status' =>$status]);
            $data['msg'] = 'Request Accepted';
            return $data;
            
        }else{
            
            $event = DB::table('group_events')->where('id', '=', $event_id)->select('name')->first();
            
            $user = DB::table('users')->select('first_name','email')->where('id', $user_id)->first();
            
            $msg = DB::table('email_templates')->where('id', '=', 6)->select('email_body as body','email_subject as subject')->first();
            
            $message['body'] = str_replace(array('{{name}}','{{event_name}}'), array($user->first_name,$event->name), $msg->body);
            $data['email']  =  $user->email;
            $data['subject'] =  $msg->subject;

            Mail::queue('emails.sugg', $message, function($message) use ($data) {
                $message->to($data['email'])->subject($data['subject']);
            });
            
            DB::table('event_confirmations')->where('user_id', $user_id)->where('event_id', $event_id)->update(['user_status' =>$status]);
            $data['msg'] = 'Request Rejected';
            return $data;
        }
        
        
        
    }
    
    
    public function get_userrsvp(Request $request){
        
        $user_id = $request->id;
        $current_date = date("Y-m-d");
        $past_event = DB::table('groups')
            ->join('group_events','group_id','=','groups.id')    
            ->join('event_confirmations','event_confirmations.id','=','group_events.id')    
            ->where('event_confirmations.user_id', '=', $user_id)
            ->where('group_events.event_date','<',$current_date)    
            ->whereIn('event_confirmations.user_status',[1,2] )
            ->select('group_events.name','group_events.id','group_events.avatar_url','group_events.description','group_events.start_time','group_events.address','event_confirmations.user_status','group_events.event_date')
            ->get();
        $future_event = DB::table('groups')
            ->join('group_events','group_id','=','groups.id')    
            ->join('event_confirmations','event_confirmations.id','=','group_events.id')    
            ->where('event_confirmations.user_id', '=', $user_id)
            ->where('group_events.event_date','>',$current_date)    
            ->whereIn('event_confirmations.user_status',[1,2] )
            ->select('group_events.name','group_events.id','group_events.avatar_url','group_events.description','group_events.start_time','group_events.address','event_confirmations.user_status','group_events.event_date')
            ->get();
        $data['past_event']=$past_event;
        
        $data['future_event']=$future_event;
        return $data;
    }

    public function cancel_rsvp(Request $request){
        
        $id         = $request->id;
        $user_id    = $request->user_id;
        if($id && $user_id){
            DB::table('event_confirmations')
            ->where('id', $id)
            ->where('user_id', $user_id)        
            ->update(['event_confirmations.user_status' => 0]);
        }
        
    }
             
    
}