<?php

namespace App\Http\Controllers;

use App\Role;
use App\role_user;
use App\User;
use App\Group;
use App\Category;
use App\GroupEvent;
use App\CmsTemplate;
use App\CmsTemplateImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Support\Facades\DB;
use Hash;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GroupImage;
use Carbon\Carbon;
use Stripe\Charge;
use Stripe\Stripe;
use Stripe\Error;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
class FindGroupController extends Controller {

    public function __construct() {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        //$this->middleware('jwt.auth', ['except' => ['authenticate','uploadimage','deleteUpload']]);
    }

    public function exportFile(Request $request) {
        ###
        //dd('sadfasdf');
        if ($request['export_type'] == 'pdf') { //export PDF
            $html = '<h1 style="text-align: center">RSVP List</h1>';
            $html .= '<style> table, th, td {text-align: center;} th, td {padding: 5px;} th {color: #43A047;border-color: black;background-color: #C5E1A5} </style> <table border="2" style="width:100%;"> <tr> <th>Name</th></tr>';
//            dd($request);
            foreach ($request['selection'] as $user) {
                $user_name = json_decode($user);
                $name = $user_name->name;
                //$user_name = $user['name'];
                $html .="<tr> <td>$name</td></tr>";
            }
            $html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $headers = array(
                'Content-Type: application/pdf',
            );
            $pdf->loadHTML($html);
            return $pdf->download('permission.pdf', $headers);
        } else {
            Excel::create('user', function ($excel) use ($users) {
                $excel->sheet('Sheet 1', function ($sheet) use ($users) {
                    $sheet->fromArray($users);
                });
            })->download($request['export_type']);
        }
    }

    /*
     *  Search Method
     */

    public function search(Request $request) {
        //dd($request);
        $per_page = \Request::get('per_page') ? : 10;
        ### search
        if ($request['query']) {
            $Group = Group::search($request['query'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Group->count();
            $Group = $Group->slice($page * $per_page, $per_page);
            $Group = new \Illuminate\Pagination\LengthAwarePaginator($Group, $total, $per_page);
            return $Group;
        }
        return 'not found';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $per_page = \Request::get('per_page') ? : 10;


        $groupdata = Group::orderBy('created_at', 'asec')->paginate($per_page);

        //dd($groupdata);
        return $groupdata;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //dd($_GET);
        $Group = Group::find($id);
        if (!$Group)
            return response()->json(['error' => 'not found item'], 404);

        if (!empty($Group->GroupInterest)) {

            $group_interest = DB::table('categories')
                    ->join('group_interests', 'group_interests.category_id', '=', 'categories.id')
                    ->where('group_interests.group_id', '=', $id)
                    ->select('categories.category_name')
                    ->first();

            $Group->category_name = $group_interest->category_name;
        } else {
            $Group->category_name = "N/A";
        }
        $group_create_date=$Group->created_at;
        $Group->create_date=date("m/d/Y", strtotime($group_create_date));
        $group_member = $Group->GroupMember;
        $count_member = DB::table('group_members')->where('group_id', '=', $id)->where('status',1)->count();
        $Group->count_member = $count_member;
        $count_group_member = count($group_member);
        $user_id = array();
        foreach ($group_member as $single_member) {
            $user_id[] = $single_member->user_id;
        }

        $user = implode(",", $user_id);
        if (!empty($user)) {
            $member_details = DB::table('users')->select('id', 'first_name', 'email')->whereIn('id', $user_id)->get();
        } else {
            $member_details = "";
        }
        //dd($member_details);
        $Group->members = $member_details;

        $Group->members_count = $count_group_member;
        $date = date('Y-m-d', strtotime("-1 days"));
        //dd($date);
        $a = $_GET;
        if(!empty($a)){
            //dd($a);
            $Group->events = $Group->GroupEvent()->where('event_date', '>', $date)->where('status', '=', 1)->orderBy('event_date')->limit(5)->get();
        }else{
            //dd($a);
            $Group->events = $Group->GroupEvent()->where('event_date', '>', $date)->where('status', '=', 1)->orderBy('event_date')->get();
        }    
        //dd($Group->events);
        
        foreach($Group->events as $k=>$s_event){
            $count_confirm = DB::table('event_confirmations')->where('event_id','=',$s_event['id'])->count('id');
            $Group->events[$k]->count_confirm=$count_confirm;
            if(empty($Group->events[$k]->max_member)){
                $Group->events[$k]->remaining_mem='N/A';
            }else{
                $Group->events[$k]->remaining_mem=$Group->events[$k]->max_member-$count_confirm;
            }
            
        }
//        $Group->categories=category::all();
        $Group->ImageGallery = GroupImage::where('group_id', $id)->get();

        if ($Group)
            return $Group;
        else
            return response()->json(['error' => 'not found item'], 404);
    }

    public function show_event(Request $request) {
        $id = $request->all();

        $Event = GroupEvent::find($id);
        return $Event;
    }

    public function member_list(Request $request) {

        if (!empty($request)) {
            $id = $request['id'];
            //$group_member = $id->GroupMember;
            $group_member = DB::table('group_members')->where('group_id', $id)->where('status', 2)->get();
            $member = array();
            $num = 0;
            foreach ($group_member as $single_user) {

                $user_id = $single_user->user_id;
                $user = DB::table('users')->where('id', $user_id)->first();
//                dd($user[0]->name);
                $single_user->name = $user->first_name;
                $single_user->city = $user->city;
                $member[$num] = $single_user;
                $num++;
            }
            return $member;
        } else {
            return "Please priovide id";
        }
    }

    /*
     * get all categories based on given params 
     * used for userside, for all group page.
     * created by dev
     * created on 12/16/2016
     */

    public function getAllCatByword(Request $request) {
        // dd($request['arg_token']);//arg_token
        $bsqd = $request['term'];
        $categories = Category::where('category_name', 'like', '%' . $bsqd . '%')->select('categories.category_name')->get()->toArray();
        $Arr = [];
        foreach ($categories as $key => $value) {
            $Arr[] = $value['category_name'];
        }
        return $Arr;
    }

    /*
     * get all categories 
     * used for userside, for home page index to get the image.
     * created by dev
     * created on 12/19/2016
     */

    public function randCategory(Request $request) {
       
        $categories = Category::orderByRaw("RAND()")->limit(7)->get();
        foreach ($categories as $key => $value) {
            $row[$value['category_name']] = $value['avatar_url'];
        }
        
        return $row;
    }

    public function allSetCategory(Request $request) {

        $categories = Category::orderby('category_name')->get();
        $row = [];
        foreach ($categories as $key => $value) {
            $row[$value['category_name']] = $value['avatar_url'];
        }

        return $row;
    }

    public function cmsDetail(Request $request) {

        $categories = CmsTemplate::find(1);
        $cms_images = CmsTemplateImage::select('image')->where('cms_template_id', '=', 1)->first();

        $data = array();
        $data['page_heading'] = $categories->page_heading;
        $data['sub_heading'] = $categories->sub_heading;
        $data['description'] = $categories->description;
        $data['image'] = $cms_images->image;

        return $data;
    }
    
    public function setCurrentLocation(Request $request) {
        
        if ($request['current_location'] != "") {
            $er = $this->getLnt($request['current_location']);
            return response()->json(['status' => 'success', 'data' => $er], 200);
        } else {
            return response()->json(['error'], 401);
        }
    }

    /*
     * get all categories based on given params 
     * used for userside, for all group page always filtered by locaion and area of interest.
     * created by dev
     * created on 12/19/2016
     */

    public function getGroupByInterest(Request $request) {

        $user_id = isset($request['user_id']) ? $request['user_id'] : '';
        $interestfilter = isset($request['interest']) ? $request['interest'] : "1";
        $group_int = isset($request['interest']) ? $request['interest'] : '';
        $current_date = date("Y-m-d");
        if (isset($request['latitude']) && $request['latitude'] != "") {
            $lng = $request['latitude'];
            $lat = $request['longitude'];
            $distance = 75;
        } else {
            $lat = '39.73915';
            $lng = '-104.9847';
            $distance = 75;
        }
        
        $products = DB::table('groups')
                //->select('groups.*',DB::raw('CONCAT(group_members.user_id as combine_user_id')))
                ->select('groups.*', DB::raw('GROUP_CONCAT(group_members.user_id) AS combine_user_id'))
                ->leftJoin('group_members', 'groups.id', '=', 'group_members.group_id')
                ->leftJoin('group_interests', 'groups.id', '=', 'group_interests.group_id')
                ->leftJoin('categories', 'group_interests.category_id', '=', 'categories.id')
                ->join('strip_group_payments', 'strip_group_payments.group_id', '=', 'groups.id')
                ->where('strip_group_payments.membership_end', '>', $current_date)
                ->selectRaw('( 6371 * acos( cos( radians(?) ) *
                                   cos( radians( groups.latitude ) )
                                   * cos( radians( groups.longitude ) - radians(?)
                                   ) + sin( radians(?) ) *
                                   sin( radians( groups.latitude ) ) )
                                 ) AS distance', [$lng, $lat, $lng]);
        $products->where('groups.status', 1);
        if (isset($request['interest']) && $request['interest'] != '') {
            $filtered = $request['interest'];
            $products->where(function ($products) use ($filtered){
                $products->orWhere('categories.category_name', 'like', '%' . $filtered . '%');
                $products->orWhere('groups.name', 'like', '%' . $filtered . '%');
                $products->orWhere('groups.header', 'like', '%' . $filtered . '%');
            });
            
        }
        
        $products->havingRaw("distance < ?", [$distance]);
        $result = $products->orderBy('groups.name', 'asc');
        $result = $products->groupBy('groups.id');
        //$result = $products->simplePaginate($request->records);
        $currentPage = $request->records;
        Paginator::currentPageResolver(function() use ($currentPage) {
            return $currentPage;
        });
        $result = $products->simplePaginate(1000);
        return $result;
    }

    public function test(Request $request) {
        $lat = '77.33257';
        $lng = '28.6455348';
        $distance = 25;

        $products = DB::table('categories')
                ->select('groups.*')
                ->join('group_interests', 'group_interests.category_id', '=', 'categories.id')
                ->join('groups', 'groups.id', '=', 'group_interests.group_id')
                ->selectRaw('( 6371 * acos( cos( radians(?) ) *
                               cos( radians( groups.latitude ) )
                               * cos( radians( groups.longitude ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( groups.latitude ) ) )
                             ) AS distance', [$lng, $lat, $lng])
                ->where('groups.status', 1)
                ->havingRaw("distance < ?", [$distance]);

        $p = $products->simplePaginate(15);
        return $p;
    }

    public function category(Request $request) {
        $categories = Category::select('categories.category_name', 'categories.id')->orderBy('category_name')->get();
        return $categories;

      
    }

    public function category_id(Request $request) {
        $id = $request['id'];
        $categories =DB::select( DB::raw("select * from categories where id not in (select category_id from user_interests where user_id=".$id.")") );
        // $categories = DB::table('categories')
        //     ->join('user_interests', 'user_interests.category_id', '=', 'categories.id')
        //     ->where('user_interests.user_id', '<>', $id)
        //     ->select('categories.category_name','categories.id')
        //     ->toSql();
        return $categories;

      
    }

    public function index_group() {
        $groupdata = Group::orderBy('created_at', 'asec')->first();

        $id = $groupdata['id'];

        $image = GroupImage::select('image')->where('group_id', '=', $id)->get();
        $groupdata['images'] = $image;
        return $groupdata;
    }

    public function get_event(Request $request) {
        $event_id = $request['id'];
        if ($request['user_id']) {
            $user_id = $request['user_id'];

            if ($event_id) {
                $group_interest = DB::table('group_events')
                        ->join('groups', 'group_events.group_id', '=', 'groups.id')
                        ->where('group_events.id', '=', $event_id)
                        ->select('group_events.*', 'groups.name as group_name','groups.user_id', 'groups.id as group_id', 'groups.header as group_header', 'groups.description as group_description', 'groups.avatar_url as group_avatar','groups.created_at as group_created_date')
                        ->first();
                $group_create_date=$group_interest->group_created_date;
                $group_interest->create_date=date("m/d/Y", strtotime($group_create_date));
                $group_id = $group_interest->group_id;
                $count_member = DB::table('group_members')->where('group_id', '=', $group_id)->where('status',1)->count();
                $group_interest->count_member = $count_member;
                $check_member = DB::table('group_members')->where('user_id', '=', $user_id)->where('group_id', '=', $group_id)->first();
                if (!empty($check_member)) {
                    $member_status = 1;
                } else {
                    $member_status = 0;
                }
                $confirm = DB::table('event_confirmations')->where('event_id', '=', $event_id)->whereIn('user_status', [1, 2])->count();

                $images = GroupImage::select('image')->where('group_id', '=', $group_id)->get();
                $user_status = DB::table('event_confirmations')->where('event_id', '=', $event_id)->where('user_id', '=', $user_id)->first();
                
                $group_owner = DB::table('group_events')
                        ->join('groups', 'group_events.group_id', '=', 'groups.id')
                        ->where('group_events.id', '=', $event_id)
                        ->where('groups.user_id', '=', $user_id)
                        ->where('groups.id', '=', $group_id)
                        ->select('groups.user_id')
                        ->first();
                $user_client_id = DB::table('users')->select('paypal_client_id')->where('id', '=', $group_interest->user_id)->first();
                $data['paypal_client_id'] = $user_client_id->paypal_client_id;
                $data['group_event'] = $group_interest;
                $data['images'] = $images;
                $data['confirm'] = $confirm;
                $data['member_status'] = $member_status;
                $data['user_status'] = $user_status;
                
                if(!empty($group_owner->user_id)){
                    $data['group_owner'] = $group_owner->user_id;
                    
                }
                //dd($user_client_id->paypal_client_id);
                return $data;
            }
        } else {
            //$user_id = $request['user_id'];

            if ($event_id) {
                $group_interest = DB::table('group_events')
                        ->join('groups', 'group_events.group_id', '=', 'groups.id')
                        ->where('group_events.id', '=', $event_id)
                        ->select('group_events.*', 'groups.name as group_name', 'groups.id as group_id', 'groups.header as group_header', 'groups.description as group_description', 'groups.avatar_url as group_avatar','groups.created_at as group_created_date')
                        ->first();
                $group_create_date=$group_interest->group_created_date;
                $group_interest->create_date=date("m/d/Y", strtotime($group_create_date));
                $group_id = $group_interest->group_id;
                $count_member = DB::table('group_members')->where('group_id', '=', $group_id)->where('status',1)->count();
                $group_interest->count_member = $count_member;
                $confirm = DB::table('event_confirmations')->where('event_id', '=', $event_id)->whereIn('user_status', [1, 2])->count();

                $images = GroupImage::select('image')->where('group_id', '=', $group_id)->get();
                //$user_status = DB::table('event_confirmations')->where('event_id', '=', $event_id)->where('user_id', '=', $user_id)->first();

                $data['group_event'] = $group_interest;
                $data['images'] = $images;
                $data['confirm'] = $confirm;
                //$data['user_status'] = $user_status;
                return $data;
            }
        }
    }

    public function getEventByDate(Request $request) {

        $per_page = \Request::get('per_page') ? : 10;
        $event_date = $request['date'];
        $groupsId = $request['id'];

        if ($event_date) {

            if ($event_date != "all") {
                $group_interest = DB::table('group_events')
                        ->join('groups', 'group_events.group_id', '=', 'groups.id')
                        ->select('group_events.*', 'groups.id as group_id', 'groups.avatar_url', 'groups.user_id')
                        ->orderBy('event_date')
                        ->where('group_events.event_date', '=', $event_date)
                        ->where('groups.id', '=', $groupsId)
                        ->paginate($per_page);
            } else {
                $group_interest = DB::table('group_events')
                        ->join('groups', 'group_events.group_id', '=', 'groups.id')
                        ->select('group_events.*', 'groups.name as group_name', 'groups.id as group_id', 'groups.header as group_header', 'groups.description as group_description', 'groups.avatar_url', 'groups.user_id')
                        ->orderBy('event_date')
                        ->where('groups.id', '=', $groupsId)
                        ->where('group_events.event_date', '>', date('Y-m-d'))
                        ->paginate($per_page);
            }

            return $group_interest;
        } else {
            return false;
        }
    }

    public function event_list() {

        $groupdata = GroupEvent::orderBy('event_date')->get();

        if ($groupdata) {
            return $groupdata;
        } else
            return "No Event forund!!!";
    }

    public function howitworks() {

        $cms = CmsTemplate::where('id', 2)->first();

        return $cms;
    }

    public function latestnews() {

        $cms = CmsTemplate::where('id', 3)->first();

        return $cms;
    }

    public function doanddonts() {

        $cms = CmsTemplate::where('id', 4)->first();

        return $cms;
    }
    
    public function faqpricing(){
        
        $cms = CmsTemplate::where('id', 6)->first();

        return $cms;
    }

    public function Orgpayment(Request $request) {
        //$request = json_decode($request);
        $request = $request->users;
        $tempgroupid = explode("=", $request['gid']);
        $tempgroupid = $tempgroupid[1];

        $email = explode("=", $request['email']);
        $email = $email[1];
        $ID = User::select('id')->where('email', $email)->orderBy('id', 'DESC')->first();
        $pay_details = json_decode($request['pay']);
        $total_price = $pay_details->amount * 100;
        $pay_id = $pay_details->id;
        $pay_p_id = $pay_details->p_id;
        $month = $pay_details->month;
        $strip_plan_id = $pay_details->strip_plan_id;

        if (empty($ID)) {

            return response()->json(['error' => 'Your signup details are mismatched'], 406);
        }

        if (empty($request)) {
            return response()->json(['error' => 'Error while fetching your data'], 406);
        }

        if ($request['stripeToken'] == "") {
            return response()->json(['error' => 'There is a problem in your card'], 406);
        }

        if (!isset($tempgroupid) || empty($tempgroupid)) {

            return response()->json(['error' => 'You group is not saved successfully, please try again'], 406);
        } else {

            $tempgroup = DB::table('temp_groups')->where('id', $tempgroupid)->first();
            $TempGroupInterest = DB::table('temp_group_interests')->where('temp_group_interests.group_id', $tempgroupid)->get();
        }
        $tempgroup->user_id = $ID['id'];
        $details_store = DB::table('groups')->insert(['user_id' => $tempgroup->user_id, 'name' => $tempgroup->name, 'header' => $tempgroup->header, 'status' => $tempgroup->status, 'city' => $tempgroup->city, 'state' => $tempgroup->state, 'country' => $tempgroup->country, 'postalcode' => $tempgroup->postalcode, 'latitude' => $tempgroup->latitude, 'longitude' => $tempgroup->longitude, 'description' => $tempgroup->description, 'terms_and_condition' => $tempgroup->terms_and_condition, 'avatar_url' => $tempgroup->avatar_url, 'slug' => $tempgroup->slug]);
        $name = $tempgroup->name;
        $user_id = $tempgroup->user_id;
        $header = $tempgroup->header;
        $city = $tempgroup->city;
        $state = $tempgroup->state;
        $country = $tempgroup->country;
        $postalcode = $tempgroup->postalcode;

        $group_id = Group::select('id')->where('name', $name)->where('user_id', $user_id)->where('header', $header)->where('city', $city)->where('state', $state)->where('country', $country)->where('postalcode', $postalcode)->first();
        $group_id = $group_id['id'];


        
        
        $token = $request['stripeToken'];
        $user = Group::find($group_id);
        
        
        try {
            $user->subscription($strip_plan_id)->create($token, [
                'email' => $user->email, 'description' => 'monthly desc'
            ]);
            $user->trial_ends_at = Carbon::now()->addDays(90);
            $user->save();
        } catch (\Stripe\Error\Card $e) {
            DB::table('groups')->where('id', '=', $group_id)->delete();
            return response()->json(['error' => 'Card declined'], 406);
        } catch (\Stripe\Error\InvalidRequest $e) {
            DB::table('groups')->where('id', '=', $group_id)->delete();
            return response()->json(['error' => 'Invalid parameters were supplied to Stripes API'], 406);
        } catch (\Stripe\Error\Authentication $e) {
            DB::table('groups')->where('id', '=', $group_id)->delete();
            return response()->json(['error' => 'Authentication with Stripes API failed'], 406);
        } catch (\Stripe\Error\ApiConnection $e) {
            DB::table('groups')->where('id', '=', $group_id)->delete();
            return response()->json(['error' => 'Network communication with Stripe failed'], 406);
        } catch (\Stripe\Error\Base $e) {
            DB::table('groups')->where('id', '=', $group_id)->delete();
            return response()->json(['error' => 'Display a very generic error to the user'], 406);
        } catch (Exception $e) {
            DB::table('groups')->where('id', '=', $group_id)->delete();
            return response()->json(['error' => 'Something else happened, completely unrelated to Stripe'], 406);
        }
        if (!$user->subscribed()) {
            DB::table('groups')->where('id', '=', $group_id)->delete();
            return response()->json(['error' => 'Error while captureing the payment'], 406);
        } else {
            foreach ($TempGroupInterest as $key => $value) {

                DB::table('group_interests')->insert(['group_id' => $group_id, 'category_id' => $value->category_id]);
            }
            $current_date = date("Y-m-d");
            $month=$month+3;
            $membership_end_date = date('Y-m-d', strtotime('+' . $month . ' months'));
            
            $getStripeconfirm = DB::table('strip_group_payments')->insert([
                    'user_id' => $user_id,
                    'group_id' =>$group_id,
                    'status' => 1,
                    'group_owner' => $user_id,
                    'payment' =>1,
                    'plan_id' => $pay_id,
                    'payment_plan_id' => $pay_p_id,
                    'membership_start' => $current_date,
                    'membership_end' => $membership_end_date,
                    'amount' => $total_price,
                ]);
            
            
            $msg = DB::table('email_templates')->where('id', '=', 8)->select('email_body as body','email_subject as subject')->first();
            $msg = (array)$msg;
        
            $user_data = DB::table('users')->where('id', '=', $user_id)->select('first_name','email')->first();
            $user_data = (array)$user_data;
            
            $plan_data = DB::table('group_payment_plans')->where('id', '=', $pay_id)->select('strip_plan_id as plan')->first();
            
            $data['first_name'] = $user_data['first_name'];
            $data['group_name'] = $name;
            $data['plan'] = $plan_data->plan;
            $data['amount'] = $total_price/100;
            $data['month'] = $month;
            $data['email'] = $user_data['email'];
            
            $data['subject'] = $msg['subject'];
            
            $message['body'] = str_replace(array('{{name}}','{{group_name}}','{{plan}}','{{amount}}','{{month}}'), array(trim($data['first_name']),$data['group_name'],$data['plan'],$data['amount'],$data['month']), $msg['body']);
            
            //dd($message->body);
            Mail::queue('emails.group_con', $message, function($message) use ($data) {
                $message->to($data['email'])->subject($data['subject']);
            });
            
            
                
            return "Success";
        }
    }

    public function membership() {

        $datas = DB::table('group_membership_plans')->get();
        $num = 0;
        foreach ($datas as $single) {
            $id = $single->id;
            $datas[$num]->payments = DB::table('group_payment_plans')->where('p_id', $id)->get();
            $num++;
            //$data
        }
        //dd($datas);
        if (isset($datas)) {
            return $datas;
        } else {
            return "No membership found";
        }
    }

    public function suggestion(Request $request) {

        if (empty($request['sugg'])) {
            return response()->json(['error' => 'Please Post Suggestion'], 404);
        }

        $message['body'] = $request['sugg'];



        $data['email'] = 'info@bevylife.com';

        $data['subject'] = 'Suggestion';




        Mail::queue('emails.sugg', $message, function($message) use ($data) {
            $message->to($data['email'])->subject($data['subject']);
        });
//        
        return response()->json(['success'], 200);
    }

    public function fb_feed() {
        $facebook_token = file_get_contents('https://graph.facebook.com/oauth/access_token?client_id=1334811473265828&client_secret=38a817b36a2f587506a067a162a50f95&grant_type=client_credentials');

        $fb_page_id = "1663461277280424";
        $fb_access_token = json_decode($facebook_token);
        //dd($fb_access_token);
        $fb_access_token = $fb_access_token->access_token;

        $facebook_token = explode('|', $fb_access_token);
        
//        $access_token = $facebook_token[1];
        $fb_access_token = DB::table('api_tokens')->select('tokens')->where('id', 1)->first();
        $access_token=$fb_access_token->tokens;
        
        //$access_token = 'EAASZBAQO0xKQBAOBvOPNAKiSG2S83QdZBbLVaAGD8jtaAEAdSkgpJZBFTpw8AgSfMsDmkOVla2O2ZAX09XdlAZCAkAaKFhKgyZAnvARVc9Srt16tudbGe2AQ618KSA3AHGbARr8AWZClZA1ihgiir39I1bSzyYsTrNREJI4BZBtjpSQZDZD';
        //dd($access_token);
//        $fields = 'user_posts';
        $fields = 'picture,name';
        $limit = 10;


        $json_link = "https://graph.facebook.com/{$fb_page_id}/feed?access_token={$access_token}&fields={$fields}&limit={$limit}";
        //dd($json_link);
        $json = file_get_contents($json_link);
        $obj = json_decode($json, true);
        $feed_item_count = count($obj['data']);
        $data = $obj['data'];
        //dd($data);
        return $data;
    }

    public function insta_feed() {
        $name = 'ABC group';
        $total_price = 60;
        $month = 9;
        
          
        $access_token = '4655774416.1677ed0.2c2185a8fc574354b2b656a17ffa9d30';
        $json_link = "https://api.instagram.com/v1/users/self/media/recent/?access_token={$access_token}";
        $json = file_get_contents($json_link);
        $obj = json_decode($json, true);
        $data = $obj['data'];
        return $data;
    }
    
    public function fb_login(Request $request){
        $email = $request['email'];
        $name = explode(" ",$request['name']);
        //$profile_pic = $request['pic'];
        //$gender = $request['gender'];
        
        $a=1;
        if(count($name)>1){
            $first_name = $name[0];
            $last_name='';
            foreach($name as $single_name){
                if($a==0){
                   $last_name= trim($last_name.' '.$single_name);
                }
                $a=0;
            }
        }else{
            $first_name = $request['name'];
            $last_name = '';
        }
        //dd($last_name);
        
        $user = User::create([
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'password' => bcrypt($first_name),
            'confirmation_code' => 'Login by Facebook',
            'status' => 1,
            'email_confirmation' => 1,
            'payment_status' => 0,
            'username' => trim($request['first_name']) . '_' . rand('1', '999'),
            'createdby' => 'user Itself',
            //'memberships' => $request['userType'],
            //'user_type_id' => $request['userType_chossen'],
        ]);

        return response()->json(['status' => 'success', 'data' => $email, 'message' => 'Congratulations! Your account created successfully.'], 200);
    
    }

    public function get_terms_cond() {

        $cms = CmsTemplate::where('id', 5)->first();

        return $cms;
    }

    public function events_groups() {
        //global $_GET;
        $id = $_GET['id'];
        $edate = $_GET['date'];


        $Group = Group::find($id);
        if (!$Group)
            return response()->json(['error' => 'not found item'], 404);

        if (!empty($Group->GroupInterest)) {

            $group_interest = DB::table('categories')
                    ->join('group_interests', 'group_interests.category_id', '=', 'categories.id')
                    ->where('group_interests.group_id', '=', $id)
                    ->select('categories.category_name')
                    ->first();

            $Group->category_name = $group_interest->category_name;
        } else {
            $Group->category_name = "N/A";
        }

//        dd($group_interest);
        $group_member = $Group->GroupMember;

        $count_group_member = count($group_member);
        $user_id = array();
        foreach ($group_member as $single_member) {
            $user_id[] = $single_member->user_id;
        }

        $user = implode(",", $user_id);
        if (!empty($user)) {
            $member_details = DB::table('users')->select('id', 'first_name', 'email')->whereIn('id', $user_id)->get();
        } else {
            $member_details = "";
        }
        //dd($member_details);
        //$Group->members = $member_details;
        //$Group->members_count = $count_group_member;
        $Group->events = $Group->GroupEvent()->where('event_date', $edate)->limit(5)->get();
        //dd($Group->events);  
//        $Group->categories=category::all();
        //$Group->ImageGallery = GroupImage::where('group_id', $id)->get();

        if ($Group)
            return $Group;
        else
            return response()->json(['error' => 'not found item'], 404);
    }
    
    public function terms_privacy(){
        
        $terms = DB::table('cms_templates')->where('id',7)->first();
        $privacy = DB::table('cms_templates')->where('id',8)->first();
        //dd($privacy);
        if(!empty($privacy) && !empty($terms)){
            $data['terms'] = $terms;
            $data['privacy'] = $privacy;
            return $data;
        }
    }
    
    public function group_check(Request $request){
        
        $group_name = $request['group_name'];
        $group_loc = $request['loc'];
        $group_loc = explode(',',$group_loc);
        if(isset($group_loc[1])){
            $sec_city=$group_loc[1];
        }
        $first_city=$group_loc[0];
        
        if(!empty($sec_city)){
            $terms = DB::table('groups')
                    ->Where('name','=',$group_name);
            $terms->where(function ($terms) use ($sec_city,$first_city){
                 $terms->orWhere('city','like','%'.$sec_city.'%');
                 $terms->orWhere('city','like','%'.$first_city.'%');
             });       
            $terms= $terms->get();
            if(count($terms)!=0){
                return 'true';
            }else{
                return 'false';
            }
        }else{
            $terms = DB::table('groups')
                    ->Where('city','like','%'.$group_loc[0].'%')
                    ->Where('name','=',$group_name)
                    ->get();
            if(count($terms)!=0){
                return 'true';
            }else{
                return 'false';
            }
        }
    }
    public function intersetCategorydelete(Request $request)
    {
        $id = $request->id;
        $data = DB::table('user_interests')->where('id','=',$id)->delete();
        echo $data;
    }
}
