<?php

namespace App\Http\Controllers;

use App\Role;
use App\role_user;
use App\User;
use App\Group;
use Mail;
use App\Category;
use App\GroupEvent;
use App\CmsTemplate;
use App\CmsTemplateImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Support\Facades\DB;
use Hash;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GroupImage;

class RegGroupController extends Controller {

    public function __construct() {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['authenticate', 'uploadimage', 'deleteUpload']]);
    }

    public function exportFile(Request $request) {
//        dd($request['selection']);
        ### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export CSV permission
//        if ($request['export_type'] == 'csv' && !Auth::user()->can('export_csv'))
//            return 'You not have this permission';
//
//        ### Check export EXCEL permission
//        if ($request['export_type'] == 'xls' && !Auth::user()->can('export_xls'))
//            return 'You not have this permission';
        ### record_type 1 equal whole records and 2 equals selected records
//        if ($request['record_type'] == 1) {
//            $users = User::all();
//        } else if ($request['record_type'] == 2) {
////            return $request['selection'];
////            $temp = explode(",", $request['selection']);
//            //        foreach($temp as $val) {
//            //             $users = User::find($val);
////            }
//            $users = User::findMany($request['selection']);
//        }
        ###
        if ($request['export_type'] == 'pdf') { //export PDF
            $html = '<h1 style="text-align: center">RSVP List</h1>';
            $html .= '<style> table, th, td {text-align: center;} th, td {padding: 5px;} th {color: #43A047;border-color: black;background-color: #C5E1A5} </style> <table border="2" style="width:100%;"> <tr> <th>Name</th></tr>';
            $user_name = $request['selection'];
//            dd($user_name);
            foreach ($user_name as $user) {
//                $user_name = json_decode($user);
                $name = $user['first_name'];
                //$user_name = $user['name'];
                $html .="<tr> <td>$name</td></tr>";
            }
            $html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $headers = array(
                'Content-Type: application/pdf',
            );
            $pdf->loadHTML($html);
            return $pdf->download('permission.pdf', $headers);
        } else {
            Excel::create('user', function ($excel) use ($users) {
                $excel->sheet('Sheet 1', function ($sheet) use ($users) {
                    $sheet->fromArray($users);
                });
            })->download($request['export_type']);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $user_id = $request['id'];
        $records = $request['records'];
        $per_page = \Request::get('per_page') ? : 10;
        $current_date = date("Y-m-d");
        //$groupdata=Group::orderBy('created_at', 'asec')->where('status','=',1)->where('user_id','=', $user_id)->paginate($records);
        $groupdata = DB::table('groups')
                ->join('strip_group_payments', 'strip_group_payments.group_id', '=', 'groups.id')
                ->where('strip_group_payments.membership_end', '>', $current_date)
                ->where('groups.user_id', $user_id)
                ->where('groups.status', 1)
                ->select('groups.*', 'strip_group_payments.membership_end')
                ->orderby('groups.name', 'asc')
                ->paginate($records);
        
        $category = array();
        $num = 0;
        $data = '';
        //dd($groupdata);
        foreach ($groupdata as $single_group) {

            $group_id = $single_group->id;

            if (!empty($single_group->GroupInterest)) {
                $group_interest = $single_group->GroupInterest;
                $group_int = $group_interest->category_name;
            } else {
                $group_int = "N/A";
            }
            $count_pending_member = DB::table('group_members')->where('group_id', '=', $group_id)->where('user_id', '<>', $user_id)->whereIn('status', [0, 2])->count();
            $single_group->pending_member = $count_pending_member;
            $single_group->group_int = $group_int;

            $data[$num] = $single_group;

            $num++;
        }
        //dd($data);
        //$groupdata['group'] = $groupdata;
        return $data;
    }

    public function inactive_group(Request $request) {
        $user_id = $request['id'];
        //$records = $request['records'];

        $per_page = \Request::get('per_page') ? : 10;

        $groupdata = Group::orderBy('created_at', 'asec')->where('status', '=', 1)->where('user_id', '=', $user_id)->get();
        $category = array();
        $num = 0;
        $data = '';
        $current_date = date("Y-m-d");
        foreach ($groupdata as $single_group) {
            $group_id = $single_group->id;
            $group_max_id = DB::table('strip_group_payments')->select('strip_group_payments.id')->where('group_id', $group_id)->max('strip_group_payments.id');
            $group_inactive = DB::table('groups')
                    ->join('strip_group_payments', 'strip_group_payments.group_id', '=', 'groups.id')
                    ->join('group_membership_plans', 'group_membership_plans.id', '=', 'strip_group_payments.payment_plan_id')
                    ->where('groups.id', '=', $group_id)
                    ->where('strip_group_payments.membership_end', '<', $current_date)
                    ->where('strip_group_payments.id', $group_max_id)
                    ->distinct('groups.id')
                    ->select('groups.*', 'strip_group_payments.membership_end', 'strip_group_payments.membership_start', 'strip_group_payments.amount', 'group_membership_plans.name as mem_name')
                    ->orderby('strip_group_payments.id', 'desc')
                    ->first();
            //dd($group_inactive);
            //$single_group->pending_member=$count_pending_member;
            $single_group = $group_inactive;
            if (!empty($single_group)) {
                $data[$num] = $single_group;

                $num++;
            }
        }

        //dd($data);

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {
        $id = $request['id'];
        $Group = Group::find($id)->first();
        if (!empty($Group->GroupInterest)) {

            $group_interest = DB::table('categories')
                    ->join('group_interests', 'group_interests.category_id', '=', 'categories.id')
                    ->where('group_interests.group_id', '=', $id)
                    ->select('categories.category_name')
                    ->get();
        }
        if (!empty($group_interest)) {
            foreach ($group_interest as $singleinterest) {
                $instArr[] = $singleinterest->category_name;
            }
            $Group->category_name = $instArr;
        }

//        dd($group_interest);
        $group_member = $pending_request = DB::table('groups')
                ->join('group_members', 'group_members.group_id', '=', 'groups.id')
                ->join('users', 'group_members.user_id', '=', 'users.id')
                ->where('group_members.group_id', '=', $id)
                ->where('group_members.status', '=', 1)
                ->select('users.first_name', 'users.email', 'group_members.status')
                ->get();

        $plan_taken = $pending_request = DB::table('group_membership_plans')
                ->join('group_payment_plans', 'group_payment_plans.p_id', '=', 'group_membership_plans.id')
                ->join('strip_group_payments', 'strip_group_payments.payment_plan_id', '=', 'group_payment_plans.p_id')
                ->where('strip_group_payments.group_id', '=', $id)
                ->orderby('strip_group_payments.id', 'desc')
                ->select('group_membership_plans.name', 'group_payment_plans.type', 'group_payment_plans.amount', 'strip_group_payments.membership_end', 'strip_group_payments.membership_start')
                ->first();
        $current_date = date("Y-m-d");
        if (strtotime($plan_taken->membership_end) > strtotime($current_date)) {
            $plan_taken->membership_status = "Active";
        } else {
            $plan_taken->membership_status = "Inactive";
        }
        $plan_taken->membership_start = date('m-d-Y', strtotime($plan_taken->membership_start));
        $plan_taken->membership_end = date('m-d-Y', strtotime($plan_taken->membership_end));
        $count_group_member = DB::table('group_members')->where('group_id', '=', $id)->where('status', '=', 1)->count();

        $Group->members = $group_member;

        $Group->members_count = $count_group_member;

        $Group->membership = $plan_taken;
//        $pending_request = DB::table('groups')
//                    ->join('group_members', 'group_members.group_id', '=', 'groups.id')
//                    ->join('users', 'group_members.user_id', '=', 'users.id')
//                    ->where('group_members.group_id', '=', $id)
//                    ->where('group_members.status', '=', 2)
//                    ->select('users.name','users.email','group_members.status')
//                    ->get();
//        $count_pending_member = DB::table('group_members')->where('group_id','=',$id)->where('status','=',2)->count();
//        
//        
//        $Group->pending_member = $pending_request;
//        $Group->count_pending_member = $count_pending_member;

        $Group->events = GroupEvent::where('group_id', $id)->get();

//        $Group->categories=category::all();
        $Group->ImageGallery = GroupImage::where('group_id', $id)->get();
        if ($Group) {
            return $Group;
        } else
            return response()->json(['error' => 'not found item'], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {

        $id = $request['id'];
//        dd($request);
        //if(Auth::user()->can('edit_group')) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'description' => 'required',
                    'header' => 'required',
                    'category_name' => 'required',
                    'avatar_url' => 'required',
                        ], $messages = [
                    'category_name.required' => 'The Category name field is required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 406);
        }
        $temp_data = DB::table('groups')->select('city')->where('id', '=', $id)->first();

        $temp_name = $request['name'];
        $temp_loc = $temp_data->city;
        $g_data = DB::table('groups')
                ->Where('city', 'like', '%' . $temp_loc . '%')
                ->Where('name', 'like', '%' . $temp_name . '%')
                ->Where('id', '<>', $id)
                ->get();
        if (count($g_data) > 0) {
            $val['messages'][0] = "Group name already taken, Please change.";
            return response()->json(['error' => $val], 406);
        }
        ###  upload avatar
        if (file_exists("temp/" . $request['avatar_url']) && $request['avatar_url'] != '') {
            File::move("temp/" . $request['avatar_url'], "uploads/" . $request['avatar_url']);
        }

        if (file_exists("temp/" . $request['layout']) && $request['layout'] != '') {
            File::move("temp/" . $request['layout'], "grouplayout/" . $request['layout']);
        }
        ####
        $Group = Group::find($id);

        $category_name = $request['category_name'];
        $category_id = DB::table('categories')->select('id')->where('category_name', '=', $category_name)->first();
        DB::table('group_interests')->where('group_id', $id)->delete();
        foreach ($request['category_name'] as $groupinterest) {

            $category_id = DB::table('categories')->select('id')->where('category_name', '=', $groupinterest)->first();



            DB::insert('insert into group_interests (group_id, category_id) values (?, ?)', [$id, $category_id->id]);
        }

        if ($Group) {
            $Group->update($request->all());
            return response()->json(['success'], 200);
            //return redirect()->action('Index');
        } else
            return response()->json(['error' => 'not found item'], 404);
//        } else
//            return response()->json(['error' =>'You not have User'], 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $temp = $request['id'];

        if (Auth::user()->can('delete_group')) {
//            $temp = explode(",", $id);
            foreach ($temp as $val) {
                /**
                 * in demo version you can't delete default task manager user permission.
                 * in production you should remove it
                 */
//                if($val==1)
//                    return response()->json(['error' =>'You not have permission to delete this item in demo mode'], 403);

                DB::table('groups')->where('id', '=', $val)->delete();
//                $Group = Group::find($val);
                //dd($Group);
//                $Group->delete();
                // Delete group interest table
                $a = DB::table('group_interests')->where('group_id', $val)->get();

                if (!empty($a)) {
                    DB::table('group_interests')->where('group_id', $val)->delete();
                }

                // Delete group events table
                $b = DB::table('group_events')->where('group_id', $val)->get();
                if (!empty($b)) {
                    DB::table('group_events')->where('group_id', $val)->delete();
                }

                // Delete group images table
                $c = DB::table('group_images')->where('group_id', $val)->get();
                if (!empty($c)) {
                    DB::table('group_images')->where('group_id', $val)->delete();
                }

                // Delete group member table
                $d = DB::table('group_members')->where('group_id', $val)->get();
                if (!empty($d)) {
                    DB::table('group_members')->where('group_id', $val)->delete();
                }
            }
            return response()->json(['success'], 200);
        } else
            return response()->json(['error' => 'You not have User'], 403);
    }

    public function create_event(Request $request) {

        //dd($request);
        if ($request) {
            $validator = Validator::make($request->all(), [
                        'name' => 'required',
                        'event_date' => 'required',
                        'description' => 'required',
                        'start_time' => 'required',
                        'end_time' => 'required',
                        'end_date'=>'required',
                        //'location' => 'required',
                        'address' => 'required',
                        //'avatar_url' => 'required',
                        'category_name' => 'required',
                            ], $messages = [
                        'name.required' => 'Event Name is required',
                        'event_date.required' => 'Event date is required',
                        'description.required' => 'Event Description is required',
                        'start_time.required' => 'Event Start Time is required',
                        'end_time.required' => 'Event End Time is required',
                        //'location.required' => 'Event Location is required',
                        'address.required' => 'Event Address is required',
                        //'avatar_url.required' => 'Event image is required',
                        'category_name.required' => 'Event Category is required',
            ]);
            $from = strtotime($request['start_time']);
            $to = strtotime($request['end_time']);

            if ($from > $to) {
                $errMsg['error'] = array('msg' => 'start time should be smaller than end time');
                return response()->json(['error' => $errMsg], 406);
            }
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 406);
            }
 
            if(!empty($request['recur__week_number']) || !empty($request['recur__mon_number'])){
               
                $key=0;
                if(!empty($request['recur__week_number'])){
                    $count = $request['recur__week_number'];
                }else{
                    $count = $request['recur__mon_number'];
                }
                $event_date = date("Y-m-d", strtotime($request['event_date']));
                for($key = 0;$key < $count; $key++){
                    unset($random_name);
                    if(!empty($request['recur__week_number'])){
                        $event[$key]['event_date']=date('Y-m-d',strtotime("+".$key." week", strtotime($event_date)));
                    }else{
                        $event[$key]['event_date']=date('Y-m-d',strtotime("+".$key." month", strtotime($event_date)));
                    }
                    
                    $random_name = $this->generateRandomString();
                    //dd($random_name);
                    if (file_exists("temp/" . $request['avatar_url']) && $request['avatar_url'] != '') {
                        File::copy("temp/" . $request['avatar_url'], "uploads/" . $random_name.".jpg");
                        $event[$key]['avatar_url'] = $random_name.".jpg";
                    }
                    
                    $event[$key]['end_date'] = date("Y-m-d", strtotime($request['end_date']));
                    $event[$key]['group_id'] = $request['group_id'];
                    $event[$key]['name'] = $request['name'];
                    $event[$key]['description'] = $request['description'];
                    //$event['event_date'] = date("Y-m-d", strtotime($request['event_date']));
                    // $event['recurring'] = $request['recurring'];
                    $event[$key]['start_time'] = $request['start_time'];
                    $event[$key]['end_time'] = $request['end_time'];
                    $event[$key]['location'] = $request['location'];
                    $event[$key]['address'] = $request['address'];
                    

                    $event[$key]['category_name'] = $request['category_name'];
                    $event[$key]['cost'] = $request['cost'];
                    $event[$key]['recurring_payment'] = $request['recurring_payment'];
                    $event[$key]['recurring_week'] = $request['recurring_week'];
                    //$event['recurring_day'] = $request['recurring_day'];
                    $event[$key]['recurring_month'] = $request['recurring_month'];
                    //$event['recurring_date'] = $request['recurring_date'];
                    $event[$key]['status'] = $request['status'];
                    $event[$key]['max_member'] = $request['max_member'];
                    $a=1;
                    
                    $group = GroupEvent::create($event[$key]);
                    if (!empty($request['category_name'])) {

                        foreach ($request['category_name'] as $single_category) {
                            $category_id = DB::table('categories')->select('id')->where('category_name', '=', $single_category)->first();
                            DB::insert('insert into event_interests (event_id, category_id) values (?, ?)', [$group->id, $category_id->id]);
                        }
                    }
                }
                
                
            }else{
                 if (file_exists("temp/" . $request['avatar_url']) && $request['avatar_url'] != '') {
                    File::move("temp/" . $request['avatar_url'], "uploads/" . $request['avatar_url']);
                }

                $event['end_date'] = date("Y-m-d", strtotime($request['end_date']));
                $event['group_id'] = $request['group_id'];
                $event['name'] = $request['name'];
                $event['description'] = $request['description'];
                $event['event_date'] = date("Y-m-d", strtotime($request['event_date']));
                //$event['recurring'] = $request['recurring'];
                $event['start_time'] = $request['start_time'];
                $event['end_time'] = $request['end_time'];
                $event['location'] = $request['location'];
                $event['address'] = $request['address'];
                $event['avatar_url'] = $request['avatar_url'];

                $event['category_name'] = $request['category_name'];
                $event['cost'] = $request['cost'];
                $event['recurring_payment'] = $request['recurring_payment'];
                $event['recurring_week'] = $request['recurring_week'];
                //$event['recurring_day'] = $request['recurring_day'];
                $event['recurring_month'] = $request['recurring_month'];
                //$event['recurring_date'] = $request['recurring_date'];
                $event['status'] = $request['status'];
                $event['max_member'] = $request['max_member'];
                
                $group = GroupEvent::create($event);
                if (!empty($request['category_name'])) {

                    foreach ($request['category_name'] as $single_category) {
                        $category_id = DB::table('categories')->select('id')->where('category_name', '=', $single_category)->first();
                        DB::insert('insert into event_interests (event_id, category_id) values (?, ?)', [$group->id, $category_id->id]);
                    }
                }
            }
            
            if ($group)
                return response()->json(['success'], 200);
            else
                return response()->json(['error' => 'can not save event'], 401);
        } else {
            return response()->json(['error' => 'can not save event'], 401);
        }
    }
    
    public function generateRandomString($length = 25) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function delete_event(Request $request) {
        $id = $request->all();

        if (Auth::user()->can('delete_event')) {

            DB::table('group_events')->where('id', '=', $id['id'])->delete();

            return response()->json(['success'], 200);
        } else {
            return response()->json(['error' => 'You not have User'], 403);
        }
    }

    public function delete_member(Request $request) {
//        dd($request);
        $aid = $request->all();
        $GID = $aid['group_id'];
        $UID = $aid['user_id'];
        $getMember = DB::table('group_members')->where('group_id', '=', $GID)->where('user_id', '=', $UID)->first();

        if (isset($getMember->id) && $getMember->id != "") {



            if (Auth::user()->can('delete_event')) {

                DB::table('group_members')->where('id', '=', $getMember->id)->delete();

                return response()->json(['success'], 200);
            } else {
                return response()->json(['error' => 'You not have User'], 403);
            }
        }
    }

//    public function get_categories()
//    {
//        $categories = Category::all();
//        
//        if(!empty($categories)){
//            return $categories;
//        }else{
//            return response()->json(['error' =>'You have not any category'], 403);
//        }
//         
//    }



    public function uploadimage() {

        $Group_id = Input::input('group_id');

        $file = Input::file('file');
        $size = File::size($file);
        $destinationPath = public_path() . '/groupGallery/';
        $extension = $file->getClientOriginalExtension();
        $filename = str_random(25) . '.' . $extension;

        $upload_success = Input::file('file')->move($destinationPath, $filename);

        if ($upload_success) {
            $GroupImage = new GroupImage();
            $grpfilesave = array();
            $grpfilesave['group_id'] = $Group_id;
            $grpfilesave['image'] = $filename;
            $grpfilesave['image_size'] = $size;
            $grpfilesave['image_type'] = $extension;
            $groupSave = $GroupImage->saveimage($grpfilesave);

            if ($groupSave) {
                return response()->json(['filename' => $filename, 'size' => $size, 'lastID' => $groupSave->id]);
            } else {
                return 'YEP: Problem in file upload';
            }
        } else {
            return 'YEP: Problem in file upload';
        }
    }

    #### delete User avatar

    public function deleteUpload($id) {
        $filename = $id;
        $temp_dir = public_path() . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR;
        $path_final_dir = public_path() . DIRECTORY_SEPARATOR . 'groupGallery' . DIRECTORY_SEPARATOR;
        //dd($path_final_dir.$filename);
        if (File::delete($path_final_dir . $filename) || File::delete($temp_dir . $filename)) {
            $r = DB::table('group_images')->where('image', '=', $filename)->delete();

//            $temp=User::find($id);
//            $temp->update(['avatar_url'=>'']);  ### update avatar_url to null in database
            if ($r)
                return 1;
            else
                return 0;### update avatar_url to null in html
        }
        else {
            return 0;
        }
    }

    #### delete image

    public function deleteImage(Request $request) {
        $filename = $request['image'];
        //dd($filename);
        $temp_dir = public_path() . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR;
        $path_final_dir = public_path() . DIRECTORY_SEPARATOR . 'groupGallery' . DIRECTORY_SEPARATOR;
        //dd($path_final_dir.$filename);
        if (File::delete($path_final_dir . $filename) || File::delete($temp_dir . $filename)) {
            $r = DB::table('group_images')->where('image', '=', $filename)->delete();

//            $temp=User::find($id);
//            $temp->update(['avatar_url'=>'']);  ### update avatar_url to null in database
            if ($r)
                return "Success";
            else
                return 0;### update avatar_url to null in html
        }
        else {
            return 0;
        }
    }

    public function status(Request $request) {

        if (!empty($request)) {
            DB::table('groups')
                    ->where('id', $request['id'])
                    ->update(['status' => $request['status']]);

            return response()->json(['success'], 200);
        } else {
            return "Can not Update";
        }
    }

    public function member_list(Request $request) {

        if (!empty($request)) {
            $group_id = $request['id']['id'];
            $user_id = $request['user_id'];

            $members = DB::table('group_members')
                    ->join('users', 'group_members.user_id', '=', 'users.id')
                    ->where('group_members.group_id', '=', $group_id)
                    ->where('users.id', '<>', $user_id)
                    ->whereIn('group_members.status', [0, 2])
                    ->select('users.first_name', 'group_members.id', 'users.avatar_url', 'users.id as user_id', 'group_members.created_at', 'group_members.status', 'group_members.group_id')
                    ->get();
            return $members;
        } else {
            return "Please priovide id";
        }
    }

    public function member_accept(Request $request) {
        if (!empty($request)) {
            $id = $request['id'];
            DB::table('group_members')->where('id', $id)->update(['status' => 1]);
        } else {
            return "Please priovide id";
        }
    }

    public function member_reject(Request $request) {
        if (!empty($request)) {
            $id = $request['id'];
            DB::table('group_members')->where('id', $id)->update(['status' => 0]);
        } else {
            return "Please priovide id";
        }
    }

    public function get_event(Request $request) {

        if (Auth::user()->can('home_user_profile')) {
            $id = $request['id'];
            $record = $request['records'];




            //$single_event['event_response'] = $event_con;
            $current_date = date("Y-m-d");
            $group_event = DB::table('group_events')
                    ->where('group_events.group_id', '=', $id)
                    ->orderBy('group_events.event_date')
                    ->select('group_events.*')
                    ->where('group_events.event_date', '>=', $current_date)
                    ->where('group_events.status', '!=', 2)
                    ->limit($record)
                    ->get();
            $count_event = DB::table('group_events')
                    ->where('group_events.group_id', '=', $id)
                    ->orderBy('group_events.event_date')
                    ->select('group_events.*')
                    ->where('group_events.event_date', '>=', $current_date)
                    ->where('group_events.status', '!=', 2)
                    ->count();
            $data = '';
            $num = 0;

            foreach ($group_event as $single_event) {
                $event_id = $single_event->id;

                $event_con = DB::table('event_confirmations')
                        ->join('users', 'event_confirmations.user_id', '=', 'users.id')
                        ->where('event_confirmations.event_id', '=', $event_id)
                        ->whereIn('event_confirmations.user_status', [1, 2])
                        ->select('users.first_name as name', 'users.id')
                        ->get();
                if (!empty($event_con)) {
                    $single_event->user_rsvp = $event_con;
                }
                $data[$num] = $single_event;
                $num = $num + 1;
            }
            //dd($data);
            //$group_event['group_id'] = $id['id'];

            if (empty($data)) {
                $data = "0";
            }
            $datas['events'] = $data;
            $datas['group_id'] = $id;
            $datas['count'] = $count_event;
            //dd($group_event);
            return $datas;
        }
    }

    public function event_date(Request $request) {

        if (Auth::user()->can('view_group')) {
            $event_date = $request['date'];
            $user_id = Auth::user()->id;
            $data = array();
            $group_interest = DB::table('group_events')
                    ->join('groups', 'group_events.group_id', '=', 'groups.id')
                    ->join('group_members', 'group_members.group_id', '=', 'groups.id')
                    ->where('group_members.user_id', '=', $user_id)
                    ->where('group_events.event_date', '=', $event_date)
                    ->orderBy('group_events.event_date')
                    ->select('group_events.*')
                    ->get();
            $data['events'] = $group_interest;
            return $data;
        } else {
            return "Not an user";
        }
    }

    public function get_lists(Request $request) {

        $id = $request['id'];
        $record = $request['records'];
        $current_date = date("Y-m-d");
        $groupdata = DB::table('groups')
                ->join('group_members', 'group_members.group_id', '=', 'groups.id')
                ->where('group_members.user_id', '=', $id)
                //->where('groups.status', '=', 1)
                ->join('strip_group_payments', 'strip_group_payments.group_id', '=', 'groups.id')
                ->where('strip_group_payments.membership_end', '>', $current_date)
                ->orderBy('groups.name')
                ->select('groups.name', 'groups.id', 'groups.header', 'groups.description', 'groups.avatar_url')
                /* ->limit($record) */
                ->get();

        $ownergroup = DB::table('groups')
                ->where('groups.user_id', '=', $id)
                ->orderBy('groups.name')
                ->select('groups.name', 'groups.id', 'groups.header', 'groups.description', 'groups.avatar_url')
                ->join('strip_group_payments', 'strip_group_payments.group_id', '=', 'groups.id')
                ->where('strip_group_payments.membership_end', '>', $current_date)
                /* ->limit($record) */
                //->where('groups.status', '=', 1)
                ->get();
        $data['member'] = $groupdata;
        $data['owner'] = $ownergroup;
        return $data;
    }

    public function event_con(Request $request) {
        $status = $request['status'];
        $event_id = $request['event_id'];
        $user_id = Auth::user()->id;

        DB::insert('insert into event_confirmations (user_id, event_id, user_status) values (?, ?,?)', [$user_id, $event_id, $status]);

        return "Success";
    }

    public function event_rsvp(Request $request) {
        $event_id = $request['id'];
        $event_con = DB::table('event_confirmations')
                ->join('users', 'event_confirmations.user_id', '=', 'users.id')
                ->where('event_confirmations.event_id', '=', $event_id)
                ->where('event_confirmations.user_status', '=', 1)
                ->select('users.first_name')
                ->get();

        return $event_con;
    }

    public function getmember(Request $request) {
        $group_id = $request['id'];

        $members = DB::table('group_members')
                ->join('users', 'group_members.user_id', '=', 'users.id')
                ->where('group_members.group_id', '=', $group_id)
                ->where('group_members.status', '!=', 0)
                ->select('users.first_name', 'group_members.id', 'users.avatar_url', 'users.id as user_id', 'group_members.created_at', 'group_members.status', 'group_members.group_id')
                ->get();

        return $members;
    }

    public function member_status(Request $request) {
        if (!empty($request)) {
            $id = $request['id'];
            $status = $request['status'];
            DB::table('group_members')->where('id', $id)->update(['status' => $status]);
            return "Success";
        } else {
            return "Please priovide id";
        }
    }

    public function single_event(Request $request) {

        if (Auth::user()->can('home_user_profile')) {
            $id = $request['id'];

            $single_event = GroupEvent::where('id', '=', $id)->first();
            $category = DB::table('event_interests')
                            ->join('categories', 'categories.id', '=', 'event_interests.category_id')
                            ->select('categories.category_name')->where('event_id', '=', $request['id'])->get();

            //$event['event_date'] = date("Y-m-d", strtotime($request['event_date']));
            $single_event['event_date'] = date("m/d/Y", strtotime(str_replace("-", "/", $single_event['event_date'])));
            $single_event['end_date'] = date("m/d/Y", strtotime(str_replace("-", "/", $single_event['end_date'])));
            //dd($category);
            if (!empty($category)) {
                foreach ($category as $cate) {
                    $instArr[] = $cate->category_name;
                }
                $single_event['category_name'] = $instArr;
            }



            //dd($single_event['event_date']);
//            $event_con = DB::table('event_confirmations')
//                ->join('users', 'event_confirmations.user_id', '=', 'users.id')
//                ->where('event_confirmations.event_id', '=', $id)
//                ->where('event_confirmations.user_status', '=', 1)
//                ->select('users.name','users.id')
//                ->get();
//            
//            
//            $single_event['event_response'] = $event_con;

            return $single_event;
        }
    }

    public function update_event(Request $request) {

        if (Auth::user()->can('home_user_profile')) {

            $validator = Validator::make($request->all(), [
                        'name' => 'required',
                        'event_date' => 'required',
                        'description' => 'required',
                        'start_time' => 'required',
                        'end_date'=>'required',
                        'end_time' => 'required',
                        //'location' => 'required',
                        'address' => 'required',
                        //'avatar_url' => 'required',
                        'category_name' => 'required',
                            // 'recurring'=>'required',
                            ], $messages = [
                        'name.required' => 'Event Name is required',
                        'event_date.required' => 'Event date is required',
                        'description.required' => 'Event Description is required',
                        'start_time.required' => 'Event Start Time is required',
                        'end_time.required' => 'Event End Time is required',
                        //'location.required' => 'Event Location is required',
                        'address.required' => 'Event Address is required',
                        //'avatar_url.required' => 'Event Image is required',
                        'category_name.required' => 'Event Category is required',
                            //'recurring.required' => 'Recurring is required',
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 406);
            }

            $event_date = strtotime($request['event_date']);

            $current_str = strtotime("now");
//                dd($event_date);
            if ($current_str > $event_date) {
                return response()->json(['error' => 'Select Valid Date'], 401);
            }
            //dd($current_date);
            if (!$request['recurring']) {
                $request['recurring'] = 0;
            }
            if (!empty($request['category_name'])) {

                DB::table('event_interests')->where('event_id', '=', $request['id'])->delete();
                foreach ($request['category_name'] as $single_category) {
                    $category_id = DB::table('categories')->select('id')->where('category_name', '=', $single_category)->first();
                    //dd();
                    DB::insert('insert into event_interests (event_id, category_id) values (?, ?)', [$request['id'], $category_id->id]);
                }
            }

            if (file_exists("temp/" . $request['avatar_url']) && $request['avatar_url'] != '') {
                File::move("temp/" . $request['avatar_url'], "uploads/" . $request['avatar_url']);
                $event['avatar_url'] = $request['avatar_url'];
            }
            $event['group_id'] = $request['group_id'];
            $event['name'] = $request['name'];
            $event['description'] = $request['description'];
            $event['event_date'] = date('Y-m-d', strtotime($request['event_date']));
            $event['end_date'] = date('Y-m-d', strtotime($request['end_date']));
            //dd($event['event_date']);
            //$event['recurring'] = $request['recurring'];
            $event['start_time'] = $request['start_time'];
            $event['end_time'] = $request['end_time'];
            $event['location'] = $request['location'];
            $event['address'] = $request['address'];
            
            $event['cost'] = $request['cost'];
            $event['recurring_payment'] = $request['recurring_payment'];
            $event['recurring_week'] = $request['recurring_week'];
            $event['recurring_day'] = $request['recurring_day'];
            $event['recurring_month'] = $request['recurring_month'];
            $event['recurring_date'] = $request['recurring_date'];
            $event['max_member'] = $request['max_member'];
            GroupEvent::find($request['id'])->update($event);

            return $event['group_id'];
            //return response()->json(['Success' => ''], 404);
        }
    }

    public function get_images(Request $request) {
        $id = $request['id'];
        $images = GroupImage::where('group_id', '=', $id)->get();

        return $images;
    }

    public function group_join(Request $request) {
        $id = $request['id'];
        $group_id = $request['group_id'];

        $check_group = DB::table('group_members')->where('group_id', $group_id)->where('user_id', $id)->first();
        if (!empty($check_group)) {
            return response()->json(['error' => 'You are already member of this group'], 404);
        }
        DB::insert('insert into group_members (group_id, user_id, status) values (?, ?, ?)', [$group_id, $id, 0]);

        return "Success";
    }

    public function event_change_status(Request $request) {

        //$id = $request['id'];
        $event['status'] = $request['status'];
        GroupEvent::find($request['id'])->update($event);

        return "Success";
    }

    public function send_mail_member(Request $request) {

        $validator = Validator::make($request->all(), [
                    'subject' => 'required',
                    'message' => 'required',
                    'email_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 406);
        }
        $data = $request;
        $email_id = $request['email_id'];
        $event_id = $request['id'];
        //$members = DB::table('event_confirmations')
//                ->join('users', 'event_confirmations.user_id', '=', 'users.id')
//                ->where('event_confirmations.event_id', '=', $event_id)
//                ->where('event_confirmations.user_status', '!=', 0)
//                ->select('users.first_name', 'users.email' )
//                ->get();

        $members = explode(',', $email_id);
        $sender = 'admin@group_organizer.com';
        $data->id = $event_id;
        $url = env('APP_URL');
        if (isset($_SERVER['url'])) {
            $full_url = $_SERVER['url'];
        } else {
            $full_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$url";
        }


        //dd($_SERVER['url']);
        $data->url = $full_url;
        foreach ($members as $single) {
            $single = trim($single);
            Mail::send('emails.event_member', ['data' => $data], function($m) use ($data, $single, $sender) {
                //$m->from($sender);

                $m->to($single)->subject($data->subject);
            });
        }
        return response()->json(['status' => 'success', 'message' => 'Mail sent'], 200);
    }

    public function reg_event(Request $request) {

        if (!empty($request['eid'])) {

            $user_id = Auth::user()->id;
            $group_id = $request['gid'];
            $event_id = $request['eid'];

            //dd($user_id);
            $event_con = DB::table('event_confirmations')->where('event_id', $event_id)->where('user_id', $user_id)->first();
            if (empty($event_con)) {
                DB::table('event_confirmations')->insert(['event_id' => $event_id, 'user_id' => $user_id]);
            }
            $event_mem = DB::table('group_members')->where('group_id', $group_id)->where('user_id', $user_id)->first();
            if (empty($event_mem)) {
                DB::table('group_members')->insert(['group_id' => $group_id, 'user_id' => $user_id, 'status' => 1]);
            }
            return "Success";
        }
    }

    public function duplicate_event(Request $request) {
        
        $event_id = $request['event_id'];

        $events = GroupEvent::find($event_id);
        
        $events['status'] = 0;
        $events['name'] = $events->name."-copy";
        if (!empty($events)) {
            $newEvent = $events->replicate();
            $newEvent->save();
            return response()->json(['status' => 'success', 'event_id' => $newEvent->id], 200);
        } else {
            return "Error";
        }
    }

}
