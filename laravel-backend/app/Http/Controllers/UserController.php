<?php

namespace App\Http\Controllers;

use App\Role;
use App\Group;
use App\role_user;
use App\User;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\DB;
use Hash;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller {

    public function __construct() {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    /*
     * Export Excel Method
     */

    public function exportFile(Request $request) {
        ### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export CSV permission
        if ($request['export_type'] == 'csv' && !Auth::user()->can('export_csv'))
            return 'You not have this permission';

        ### Check export EXCEL permission
        if ($request['export_type'] == 'xls' && !Auth::user()->can('export_xls'))
            return 'You not have this permission';



        ### record_type 1 equal whole records and 2 equals selected records
        if ($request['record_type'] == 1) {
            $users = User::all();
        } else if ($request['record_type'] == 2) {
//            return $request['selection'];
//            $temp = explode(",", $request['selection']);
            //        foreach($temp as $val) {
            //             $users = User::find($val);
//            }
            $users = User::findMany($request['selection']);
        }

        ###
        if ($request['export_type'] == 'pdf') { //export PDF
            $html = '<h1 style="text-align: center">YEP ngLaravel PDF </h1>';
            $html .= '<style> table, th, td {text-align: center;} th, td {padding: 5px;} th {color: #43A047;border-color: black;background-color: #C5E1A5} </style> <table border="2" style="width:100%;"> <tr> <th>Name</th> <th>Email</th> </tr>';
            foreach ($users as $user) {
                $html .="<tr> <td>$user->first_name</td> <td>$user->email</td> </tr>";
            }
            $html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $headers = array(
                'Content-Type: application/pdf',
            );
            $pdf->loadHTML($html);
            return $pdf->download('permission.pdf', $headers);
        } else {
            Excel::create('user', function ($excel) use ($users) {
                $excel->sheet('Sheet 1', function ($sheet) use ($users) {
                    $sheet->fromArray($users);
                });
            })->download($request['export_type']);
        }
    }

    /*
     *  Search Method
     */

    public function search(Request $request) {

        $per_page = \Request::get('per_page') ? : 10;
        ### search
        if ($request['query']) {
            $User = User::search($request['query'], null, false)->where('id', '!=', '1')->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $User->count();
            $User = $User->slice($page * $per_page, $per_page);
            $User = new \Illuminate\Pagination\LengthAwarePaginator($User, $total, $per_page);
            //dd($User);
            $num = 0;
            $count = count($User);
            $User_data = $User;
            foreach ($User as $single_user) {
                //dd($single_user['id']);
                $user_interest = $single_user->UserInterest;
                $user_category = "NA";
                $user_type = $single_user->UserType;
                if($user_type){
                    $usertype = $user_type->usertype;
                }else{
                    $usertype = 'Not Define';
                }
                
                $single_user->member_type = $usertype;


                if (count($user_interest) > 0) {
                    $category = array();
                    foreach ($user_interest as $details) {
                        $category[] .= $details->category_name;
                    }
                    $user_category = implode(",", $category);
                } else {

                    $user_category = "NA";
                }

                $single_user->user_int = $user_category;

                $User[$num] = $single_user;
                $user_category = "";
                $num++;
            }

            return $User;
        }
        return 'not found';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $per_page = \Request::get('per_page') ? : 10;

        if (Auth::user()->can('view_user')) {
            $userdata = User::select('id','first_name','last_name','email','status','user_type_id','created_at')->where('id', '!=', '1')->orderBy('created_at', 'asec')->paginate($per_page);
            $category = array();
            $num = 0;
            $data= array();
            foreach ($userdata as $single_user) {
                $id = $single_user['id'];
                $user_type = $single_user['user_type_id'];
                
                $userdata1 = DB::table('user_interests')
                        ->join('categories', 'categories.id', '=', 'user_interests.category_id')
                        ->where('user_interests.user_id', '=',$id)
                        ->select(array(DB::raw('group_concat(categories.category_name) as interest')))
                        ->first();
                
                $usertype = DB::table('user_types')->select('usertype')->where('id', '=' ,$user_type)->first();
                
                if(empty($userdata1->interest)){
                    $userdata1->interest = 'N/A';
                }
                
                $single_user['user_int'] = $userdata1->interest;
                if(!empty($usertype->usertype)){
                    $single_user['member_type'] = $usertype->usertype;
                }else{
                    $single_user['member_type'] = 'Not Define';
                }
//                $user_interest = $single_user->UserInterest;
//                $user_category = "N/A";
//                $user_type = $single_user->UserType;
//                $usertype = $user_type->usertype;
//                $single_user->member_type = $usertype;
//
//                if (count($user_interest) > 0) {
//                    $category = array();
//                    foreach ($user_interest as $details) {
//                        $category[] .= $details->category_name;
//                    }
//                    $user_category = implode(",", $category);
//                } else {
//
//                    $user_category = "N/A";
//                }
//                $single_user->user_int = $user_category;

                $userdata[$num] = $single_user;
                $user_category = "";
                $num++;
            }
//            dd($userdata);
            return $userdata;
        } else
            return response()->json(['error' => 'You not have User'], 403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//        dd(Auth::user()->id);
        $valid = array();
        $valid2 = array();
        $messages = array();
        $messages2 = array();
        if (Auth::user()->can('add_user')) {
            
            if ($request) {
                $valid = [
                            'first_name' => 'required',
                            'last_name' => 'required',
                            'email' => 'unique:users,email|required|email',
                            'password' => 'required|min:6',
                            'phone' => 'required||min:10',
                            'city' => 'required',
                            'state' => 'required',
                            'country' => 'required',
                            'postalcode' => 'required|numeric',
                            'aboutme' => 'required',
                            'user_type_id' => 'required',
                                ];
                $messages = [
                            'user_type_id.required' => 'The User Type field is required',
                            'postalcode.required'=>'The Postal Code field is required',
                            'aboutme.required'=>'The Bio field is required',
                            'email.unique' =>'Email Address is alrady register',
                           ];
                if($request['user_type_id']!=4){
                    
                    $valid2 =[
                        'userinterest' => 'required',
                        'groupname' => 'required',
                            ];
                  $messages2 = [
                        'userinterest.required' => 'The User Interest field is required',
                        'groupname.required'=>'The Interested Group field is required',
                            ];
                    
                }
                $val =array_merge($valid,$valid2);
                $mess = array_merge($messages,$messages2);
                
                $validator = Validator::make($request->all(),$val,$mess);
//                dd($validator);
                if ($validator->fails()) {

                    return response()->json(['error' => $validator->errors()], 406);
                }


//                dd($validator->fails());
                ###  upload avatar
                if (file_exists("temp/" . $request['avatar_url']) && $request['avatar_url'] != '') {
                    File::move("temp/" . $request['avatar_url'], "uploads/" . $request['avatar_url']);
                }
                ####

                $request['password'] = bcrypt($request['password']);
                 if($request['postalcode'] !=""){
                
                $er= $this->getLnt($request['postalcode']);
                    $request['latitude'] = $er['latitude'];
                    $request['longitude'] = $er['longitude'];
                } 
                // If the user created by admin then created by will be 1, otherwise created by will be 0  
                
                
                if(Auth::user()->id==1){
                    $request['email_confirmation'] = 1;
                    $request['createdby'] = 1;
                    $request['status'] = 1;
                }
                
                
                $user = User::create($request->all());
                $user_id = $user['id'];
                
                if(!empty($request['userinterest'])){
                    $category = $request['userinterest'];
                   
                    foreach ($category as $single_category) {
//                        $category_name = $single_category["category_name"];
                        $category_id=DB::table('categories')->select('id')->where('category_name','=',$single_category)->first();
                        
                        DB::insert('insert into user_interests (user_id, category_id) values (?, ?)', [$user_id, $category_id->id]);
                    }

                    
                }
                
                if(!empty($request['groupname'])){
                    $group_info = $request['groupname'];
                    foreach ($group_info as $single_group) {
//                        $group_id = $single_group["id"];
                        $group_id=DB::table('groups')->select('id')->where('name','=',$single_group)->first();
//                        $group_id = $groups_id['id'];
                        DB::insert('insert into group_members (group_id,user_id) values (?, ?)', [$group_id->id, $user_id]);
                    }
                }
                
                //DB::insert('insert into role_user (user_id, role_id) values (?, ?)', [$user->id, $request['role_id']]);
                return response()->json(['success'], 200);
            } else {
                return response()->json(['error' => 'can not save product'], 401);
            }
        } else
            return response()->json(['error' => 'You not have User'], 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $user = User::find($id);
        $membership_active='';
        $user_interest = DB::table('categories')
            ->join('user_interests', 'user_interests.category_id', '=', 'categories.id')
            ->where('user_interests.user_id', '=', $id)
            ->select('categories.category_name','user_interests.id')
            ->get();
        //dd($user_interest);
        $group_member = DB::table('groups')
            ->join('group_members', 'groups.id', '=', 'group_members.group_id')
            ->where('group_members.user_id', '=', $id)
            ->select('groups.name')
            ->get();
        
        $current_date = date("Y-m-d");        
        //$groupdata=Group::orderBy('created_at', 'asec')->where('status','=',1)->where('user_id','=', $user_id)->paginate($records);
        $membership_active=DB::table('groups')
            ->join('strip_group_payments', 'strip_group_payments.group_id', '=', 'groups.id')
            ->join('group_membership_plans','group_membership_plans.id','=','strip_group_payments.payment_plan_id')    
            ->where('strip_group_payments.membership_end', '>', $current_date)
            ->where('groups.user_id', $id)
            ->select('groups.name','strip_group_payments.membership_start','strip_group_payments.membership_end','strip_group_payments.amount','group_membership_plans.name as mem_name')
            ->orderby('strip_group_payments.id','desc')
            ->get();
        
        if(!empty($membership_active)){
            $user->membership_active =$membership_active;
        }
        
        
        $instArr=[];
        
        foreach ($user_interest as $key => $value) {
            $instArr[$key][]= $value->category_name;
            $instArr[$key][]= $value->id;
        }
        $user->userinterest=$instArr;
        
        $grpArr=[];
        foreach ($group_member as $key => $value) {
            $grpArr[]= $value->name;
        }
        $user->groupname =$grpArr;
        unset($grpArr);
        if ($user)
            return $user;
        else
            return response()->json(['error' => 'not found item'], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $editUser = User::find($id);
        if ($editUser)
            return response()->json(['success' => $editUser], 200);
        else
            return response()->json(['error' => 'not found item'], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        /**
         * in demo version you can't delete default task manager user permission.
         * in production you should remove it
         */
//        if($id==1)
//            return response()->json(['error' => ['data'=>['You not have permission to edit this item in demo mode']]], 403);
//        dd($request);
        if (Auth::user()->can('edit_user')) {
            $valid = array();
            $valid2 = array();
            $messages = array();
            $messages2 = array();
            
            if ($request) {
                $valid = [
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'email' => 'email|required',
                        'password' => 'required|min:6',
                        'phone' => 'required||min:10',
                        'city' => 'required',
                        'state' => 'required',
                        'country' => 'required',
                        'postalcode' => 'required|numeric',
                        'aboutme' => 'required',
                        'user_type_id' => 'required',
                            ];
                $messages = [
                            'user_type_id.required' => 'The User Type field is required',
                            'first_name.required' => 'First Name field is required',
                            'last_name.required' => 'Last Name field is required',
                            'postalcode.required'=>'The Postal Code field is required',
                            'aboutme.required'=>'The Bio field is required',
                           ];
                if($request['user_type_id']!=4){

                    $valid2 =[
                        'userinterest' => 'required',
                        'groupname' => 'required',
                            ];
                  $messages2 = [
                        'userinterest.required' => 'The User Interest field is required',
                        'groupname.required'=>'The Interested Group field is required',
                            ];

                }
                  $val =array_merge($valid,$valid2);
                    $mess = array_merge($messages,$messages2);

                $validator = Validator::make($request->all(),$val,$mess);

                if ($validator->fails()) {

                    return response()->json(['error' => $validator->errors()], 406);
                }
            }    
            ###  upload avatar
            if (file_exists("temp/" . $request['avatar_url']) && $request['avatar_url'] != '') {
                File::move("temp/" . $request['avatar_url'], "uploads/" . $request['avatar_url']);
            }
            ####
            $User = User::find($id);
            //DB::table('role_user')->where('user_id',$User->id)->delete();
            //DB::insert('insert into role_user (user_id, role_id) values (?, ?)', [$User->id, $request['role_id']]);
            if ($request['password'] != '********')
                $request['password'] = bcrypt($request['password']);
            else
                $request['password'] = $User->password;

            if ($request['status'] == True OR $request['status'] == False) {
                $status = $request['status'];
                DB::table('groups')->where('user_id', $id)->update(['status' => $status]);
            }
            //dd("end");
             if($request['postalcode'] !=""){
                
                $er= $this->getLnt($request['postalcode']);
                    $request['latitude'] = $er['latitude'];
                    $request['longitude'] = $er['longitude'];
            }
            DB::table('user_interests')->where('user_id', $id)->delete();
            foreach($request['userinterest'] as $userinterest){
                
                $category_id = DB::table('categories')->select('id')->where('category_name', '=' ,$userinterest)->first();
                
                
               
                DB::table('user_interests')->insert(['user_id' => $id, 'category_id' => $category_id->id]);
            }
            
            if(!empty($request['groupname'])){
                DB::table('group_members')->where('user_id', $id)->delete();
                $group_info = $request['groupname'];
                foreach ($group_info as $single_group) {
//                        $group_id = $single_group["id"];
                    $group_id=DB::table('groups')->select('id')->where('name','=',$single_group)->first();
//                        $group_id = $groups_id['id'];
                    DB::insert('insert into group_members (group_id,user_id) values (?, ?)', [$group_id->id, $id]);
                }
            }

            if ($User) {
                $User->update($request->all());
                return response()->json(['success'], 200);
                //return redirect()->action('Index');
            } else
                return response()->json(['error' => 'not found item'], 404);
        } else
            return response()->json(['error' => 'You not have User'], 403);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function editProfile(Request $request, $id) {
//        if($id==1)
//            return response()->json(['error' => ['data'=>['You not have permission to edit this item in demo mode']]], 403);

        if (Auth::user()->can('edit_profile')) {
            if ($request['changePasswordStatus'] == 'true') {
                $roleValidation = [
//                    'name' => 'required',
                    'email' => 'required|email',
//                    'phone' => 'numeric|min:10',
                    'currentPassword' => 'required',
                    'newPassword' => 'required|confirmed|min:6|max:50|different:currentPassword',
                    'newPassword_confirmation' => 'required_with:newPassword|min:6'
                ];
            } else {
                $roleValidation = [
//                    'name' => 'required|min:4',
                    'email' => 'required|email',
//                    'phone' => 'numeric|min:10',
                ];
            }
            $validator = Validator::make($request->all(), $roleValidation);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 406);
            }
            ###  upload avatar
            if (file_exists("temp/" . $request['avatar_url']) && $request['avatar_url'] != '') {
                File::move("temp/" . $request['avatar_url'], "uploads/" . $request['avatar_url']);
            }
            ####
            $User = User::find($id);
            if ($request['changePasswordStatus'] == 'true') {
                if (Hash::check($request['currentPassword'], $User->password)) {
                    $request['newPassword'] = bcrypt($request['newPassword']);
                } else {
                    return response()->json(['error' => ['data' => ['Current password is incorrect']]], 404);
                }
            } else
                $request['newPassword'] = $User->password;

            $request['password'] = $request['newPassword'];
            $User->update($request->all());
            return response()->json(['success'], 200);
        } else
            return response()->json(['error' => ['data' => ['You don\'t have permission']]], 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Auth::user()->can('delete_user')) {
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                /**
                 * in demo version you can't delete default task manager user permission.
                 * in production you should remove it
                 */
                if ($val == 1)
                    return response()->json(['error' => 'You not have permission to delete this item in demo mode'], 403);

                $User = User::find($val);
                $User->delete();
                DB::table('user_interests')->where('user_id', $val)->delete();
                DB::table('group_members')->where('user_id', $val)->delete();
            }
            return response()->json(['success'], 200);
        } else
            return response()->json(['error' => 'You not have User'], 403);
    }

    public function user_list() {
//        $user_list = User::where('user_type_id','=','3')->orderBy('name')->get();
        $user_list = DB::table('users')
                        ->select(DB::raw('CONCAT(first_name,"(", email,")") AS name, id'))
                        ->where('user_type_id', '=', '3')->orderBy('first_name')->get();
        if (!empty($user_list)) {
            return $user_list;
        } else {
            return response()->json(['error' => 'You have not any Group Owner'], 403);
        }
    }
    
    public function search_usertype(Request $request){
        $per_page = \Request::get('per_page') ? : 10;
        //  dd($request);
        if($request['type']=='Free_Member'){
            $user_type_id=4;
        }else if($request['type']=='Group_Member'){
            $user_type_id=2;
        }else if($request['type']=='Group_Owner'){
            $user_type_id=3;
        }else if($request['type']=='All_Member'){
            $user_type_id=5;
        }
        
        if (Auth::user()->can('view_user')) {
            if($user_type_id!=5){
                $userdata = User::where('user_type_id', '=', $user_type_id)->orderBy('created_at', 'asec')->paginate($per_page);
            }else{
                $userdata = User::where('id', '!=', '1')->orderBy('created_at', 'asec')->paginate($per_page);
            }
            $category = array();
            $num = 0;

            foreach ($userdata as $single_user) {

                //dd($single_user['id']);
                $user_interest = $single_user->UserInterest;
                $user_category = "NA";
                $user_type = $single_user->UserType;
                $usertype = $user_type->usertype;
                $single_user->member_type = $usertype;

                if (count($user_interest) > 0) {
                    $category = array();
                    foreach ($user_interest as $details) {
                        $category[] .= $details->category_name;
                    }
                    $user_category = implode(",", $category);
                } else {

                    $user_category = "NA";
                }
                $single_user->user_int = $user_category;

                $userdata[$num] = $single_user;
                $user_category = "";
                $num++;
            }
            //dd($userdata);
            return $userdata;
        } else
            return response()->json(['error' => 'You not have User'], 403);
    }
    
    public function status(Request $request){
        
        if(!empty($request)){
            DB::table('users')
            ->where('id', $request['id'])
            ->update(['status' => $request['status']]);
            
            return response()->json(['success'], 200); 
        }else{
            return "Can not Update";
        }
        
    }


    public function loginFirst(Request $request){
        
        $id = Auth::user()->id;
        if($id){

        if(!empty($request)){
            DB::table('users')
            ->where('id',$id)
            ->update(['firstLogin' => 1]);
            
            return response()->json(['success'], 200); 
        }else{
            return "Can not Update";
        }
        }
    }


    public function updateInterest(Request $request){
        
        $userId=Auth::user()->id;
        if(!empty($request)){
            
            if($request->interest==null){
                
                 return response()->json(['error' => 'please select at least interest'], 403);     
            }
            //DB::table('user_interests')->where('user_id', $userId)->delete();

            foreach ($request->interest as $key => $value) {
              // echo $value;
               DB::table('user_interests')->insert(['user_id' =>$userId, 'category_id' =>$value]);
            }
            
            
            return response()->json(['status'=>'success','message'=>'You have saved area of interest successfully'], 200); 
        }else{
            return response()->json(['status'=>'error','message'=>'please select at least interest'],403); 
        }
        
    }


    public function organizerEmail(Request $request) {
       
        $id=Auth::user()->id;
        if(!$id){
           return response()->json(['error' => 'user not found'], 401); 
        }
        $user = User::find($id);
        
        $group_member = DB::table('groups')
            ->join('group_members', 'groups.id', '=', 'group_members.group_id')
            ->join('users', 'users.id', '=', 'groups.user_id')
            ->where('group_members.user_id', '=', $id)
            ->distinct('groups.id')
            ->select(DB::raw('CONCAT(users.email," (", groups.name,")") AS email'))
            ->get();
        if($group_member)
        return response()->json(['status'=>'success','data'=>$group_member],200);
        else
            return response()->json(['error' => 'not found item'], 404);
    }

    public function memberType(Request $request) {
       
        $id=Auth::user()->id;
        if(!$id){
           return response()->json(['error' => 'user not found'], 401); 
        }
        $user = User::find($id);
        
        $group_owner = DB::table('groups')
            ->select('groups.id')
            ->where('user_id', '=', $id)
            ->first();
        if($group_owner){
            $ownID=$group_owner->id;
        }else{
            $ownID=null;
        }
        $group_member = DB::table('group_members')
            ->select('group_members.id')
            ->where('user_id', '=', $id)
            ->first();
        if($group_member){
            $memberID=$group_member->id;
        }else{
            $memberID=null;
        }
        return response()->json(['status'=>'success','groupOwner'=>$ownID,'groupMember'=>$memberID],200);

    }


    

}
