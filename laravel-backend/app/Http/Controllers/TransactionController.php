<?php

namespace App\Http\Controllers;

use App\Role;
use App\role_user;
use App\User;
use App\Group;
use App\Transaction;
use App\Category;
use App\GroupEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\DB;
use Hash;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $per_page = \Request::get('per_page') ?: 10;
        
        
        if(Auth::user()->can('view_transaction')){
            
            $transaction=  DB::table('strip_group_payments')
                    ->join('users', 'users.id', '=', 'strip_group_payments.user_id')
                    ->join('group_payment_plans','group_payment_plans.id','=','strip_group_payments.plan_id')
                    ->join('group_membership_plans','group_membership_plans.id','=','strip_group_payments.payment_plan_id')
                    ->select('users.first_name','users.user_type_id', 'strip_group_payments.*','group_payment_plans.type','group_membership_plans.name as plan_name')
                    ->orderby('strip_group_payments.id','desc')
                    ->paginate($per_page);
//            dd($transaction);        
            return $transaction;
        }
        else
            return response()->json(['error' =>'You have not any permission'], 403);
    }
    
    public function search(Request $request) {

        $per_page = \Request::get('per_page') ? : 10;
        ### search
        if ($request['text']) {
            $keyword = $request['text'];
            if($request['user_type_name']=='Free_Member'){
                $user_type_id=4;
            }else if($request['user_type_name']=='Group_Member'){
                $user_type_id=2;
            }else if($request['user_type_name']=='Group_Owner'){
                $user_type_id=3;
            }else if($request['user_type_name']=='All_Member'){
                $user_type_id=5;
            }
            if($request['user_type_name']!='All_Member'){
                
                    $transaction = DB::table('strip_group_payments')
                            ->join('users', 'users.id', '=', 'strip_group_payments.user_id')
                            ->join('group_payment_plans','group_payment_plans.id','=','strip_group_payments.plan_id')
                            ->join('group_membership_plans','group_membership_plans.id','=','strip_group_payments.payment_plan_id')
                            ->where("users.user_type_id", "=",$user_type_id)
                            ->orWhere("users.first_name", "LIKE","%{$keyword}%")
                            ->orWhere("users.last_name", "LIKE","%{$keyword}%")
                            ->select('users.first_name','users.user_type_id', 'strip_group_payments.*','group_payment_plans.type','group_membership_plans.name as plan_name')
                            //->select('users.*', 'transactions.*')
                            ->orderby('strip_group_payments.id','desc')
                            ->get();

                    return $transaction;
            }else{
            
                
                $transaction = DB::table('strip_group_payments')
                        ->join('users', 'users.id', '=', 'strip_group_payments.user_id')
                        ->join('group_payment_plans','group_payment_plans.id','=','strip_group_payments.plan_id')
                        ->join('group_membership_plans','group_membership_plans.id','=','strip_group_payments.payment_plan_id')
                        ->orWhere("users.first_name", "LIKE","%{$keyword}%")
                        ->orWhere("users.last_name", "LIKE","%{$keyword}%")
                        ->select('users.first_name','users.user_type_id', 'strip_group_payments.*','group_payment_plans.type','group_membership_plans.name as plan_name')
                        //->select('users.*', 'transactions.*')
                        ->get();

                return $transaction;
            }    
        }
        $transaction=  DB::table('strip_group_payments')
                ->join('users', 'users.id', '=', 'strip_group_payments.user_id')
                ->join('group_payment_plans','group_payment_plans.id','=','strip_group_payments.plan_id')
                ->join('group_membership_plans','group_membership_plans.id','=','strip_group_payments.payment_plan_id')
                ->select('users.first_name','users.user_type_id', 'strip_group_payments.*','group_payment_plans.type','group_membership_plans.name as plan_name')
                ->get();

        return $transaction;
    }
    
    
    public function search_usertype(Request $request){
        $per_page = \Request::get('per_page') ? : 10;
        
        if($request['type']=='Free_Member'){
            $user_type_id=4;
        }else if($request['type']=='Group_Member'){
            $user_type_id=2;
        }else if($request['type']=='Group_Owner'){
            $user_type_id=3;
        }else if($request['type']=='All_Member'){
            $user_type_id=5;
        }
        
        if ($user_type_id==5) {
            
            
            $transaction=  DB::table('strip_group_payments')
                    ->join('users', 'users.id', '=', 'strip_group_payments.user_id')
                    ->join('group_payment_plans','group_payment_plans.id','=','strip_group_payments.plan_id')
                    ->join('group_membership_plans','group_membership_plans.id','=','strip_group_payments.payment_plan_id')
                    ->select('users.first_name','users.user_type_id', 'strip_group_payments.*','group_payment_plans.type','group_membership_plans.name as plan_name')
                    ->orderby('strip_group_payments.id','desc')
                    ->paginate($per_page);
            
            return $transaction;
        }else{
            $transaction=  DB::table('strip_group_payments')
                    ->join('users', 'users.id', '=', 'strip_group_payments.user_id')
                    ->join('group_payment_plans','group_payment_plans.id','=','strip_group_payments.plan_id')
                    ->join('group_membership_plans','group_membership_plans.id','=','strip_group_payments.payment_plan_id')
                    ->select('users.first_name','users.user_type_id', 'strip_group_payments.*','group_payment_plans.type','group_membership_plans.name as plan_name')
                    ->where('users.user_type_id','=',$user_type_id)
                    ->orderby('strip_group_payments.id','desc')
                    ->paginate($per_page);

            return $transaction;
        }
    }
}    