<?php

namespace App\Http\Controllers;

use App\Role;
use App\role_user;
use App\User;
use Mail;
use App\Group;
use App\Category;
use App\GroupEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\DB;
use Hash;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserMailController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    public function send(Request $request){
       // dd($request->all());
       if($request) {
            
            $validator = Validator::make($request->all(), [
                'subject' => 'required',
                'message' => 'required',
                
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 406);
            }
            
            $data=$request;
            $sender = 'admin@group_organizer.com';
            $count_request = count($data['email']);
                        
                if($count_request==1){
                    $data->name = User::where('email',$request->email)->first()->first_name;
                    $EmailTemplate = \App\EmailTemplate::find(3);
                    $data->email_subject = $EmailTemplate->email_subject;
                    $data->description = $EmailTemplate->description;
                   
                    Mail::send('emails.mail_template', ['data' => $data], function($m) use ($data,$sender) {
                       $m->from($sender);
                       
                       $m->to($data['email'])->subject($data->subject);
               });
                    
                    return response()->json(['status'=>'success','message'=>'Mail sent'], 200);

              } else {

                  
                 foreach($data['email'] as $single_request){
                    
                  Mail::send('emails.contactEmail', ['data' => $data], function($m) use ($data,$single_request,$sender) {
                             $m->from($sender);

                             $m->to($single_request)->subject($data->subject);
                     });
                 }
                    return response()->json(['status'=>'success','message'=>'Mail sent'], 200);
              }

         }else{
            return response()->json(['error' => 'You not have User'], 403);
             
         }
    }
}
