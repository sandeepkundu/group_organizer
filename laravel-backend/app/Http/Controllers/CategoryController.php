<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
        
    }


    public function index()
    {
        
        $categories = Category::orderBy('created_at','desc')->get();
        return $categories;
    }


    public function get_cat(Request $request)
    {
        $per_page=$request['records'];
        $data = Category::orderBy('created_at','desc')->paginate($per_page);
        return $data;
    }


    public function store(Request $request)
    {
//        dd($request);
//        if(Auth::user()->can('add_category')) {
            if ($request) {
                $validator = Validator::make($request->all(), [
                    'category_name' => 'required|min:3',
                    'avatar_url' => 'required',
                ],
                 $messages = [
                'avatar_url.required'   => 'Please Upload Category Image',
                ]);
                if ($validator->fails()) {
                    return response()->json(['error' => $validator->errors()], 406);
                }
                
                if (file_exists("temp/" . $request['avatar_url']) && $request['avatar_url'] != '') {
                   
                    File::move("temp/" . $request['avatar_url'], "uploads/" . $request['avatar_url']);
                }
                Category::create($request->all());
                return response()->json(['success'], 200);
            } else {
                return response()->json(['error' => 'can not save product'], 401);
            }
        

    }


    public function show($id)
    {
       $category = Category::find($id);
       return $category;
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request)
    {
        
        if(Auth::user()->can('edit_category')) {
//            dd($request);
            $validator = Validator::make($request->all(), [
                    'category_name' => 'required|min:3',
                    'avatar_url' => 'required',
                ],
                 $messages = [
                'avatar_url.required'   => 'Please Upload Category Image',
                ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 406);
            }
            if (file_exists("temp/" . $request['avatar_url']) && $request['avatar_url'] != '') {
                   
                File::move("temp/" . $request['avatar_url'], "uploads/" . $request['avatar_url']);
            }
            $id = $request['id'];
            $category = Category::find($id);
            
            if ($category) {
//                DB::table('categories')->where('id', $id)->delete();
//                $category_name=$request['category_name'];
//                $avatar=$request['avatar_url'];
//                dd($category_name);
//                DB::insert('insert into categories (category_name,avatar_url) values (?, ?)', [$category_name, $avatar]);
                $category->update($request->all());
                return response()->json(['success'], 200);
            } else
                return response()->json(['error' => 'not found item'], 404);
        } else{
            return response()->json(['error' =>'You not have permission'], 403);
        }
    }


    public function destroy(Request $request)
    {
        
        if(Auth::user()->can('delete_category')) {

                $Customer = Category::find($request['id']);
                $Customer->delete();
                 return response()->json(['success'], 200);
        } else
            return response()->json(['error' =>'You not have permission'], 403);
    } 

}
