<?php

namespace App;


use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Nicolaslopezj\Searchable\SearchableTrait;
use PhpSoft\Users\Models\UserTrait;
//use Nicolaslopezj\Searchable\SearchableTrait;


    class CmsTemplateImage extends Model{
    
         
        protected $fillable= ['cms_template_id','image'];
        
        protected $searchable = [
            'columns' => [
                'cms_template_images.cms_template_id' => 1,
            ],
        ];
        
        protected $import_fields = [
            'cms_template_id',
            'image',
        ];
        
        public function CmsTemplate()
        {
            return $this->belongsTo('App\CmsTemplate');

        }
    }

?>