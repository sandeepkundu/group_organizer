<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Nicolaslopezj\Searchable\SearchableTrait;
use PhpSoft\Users\Models\UserTrait;

class User extends Model implements AuthenticatableContract,

                                    //AuthorizableContract,
                                    CanResetPasswordContract
                                    
                                    
{
    use SearchableTrait;     
    protected $searchable = [
        'columns' => [
            'users.first_name' => 200,
            'users.last_name' => 200,
            'users.email' => 200,
            'users.city' => 20,
            'users.country' => 20,
            'users.state' => 20,
            'users.postalcode' => 5,
        ],
    ];
    use Authenticatable, CanResetPassword,UserTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'gender',
        'avatar_url',
        'status',
        'firstLogin',
        'phone',
        'city',
        'state',
        'postalcode',
        'country',
        'user_type_id',
        'createdby',
        'aboutme',
        'confirmation_code',
        'memberships',
        'email_confirmation',
        'payment_status',
        'latitude',
        'longitude',
        'username',
        'paypal_client_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password'];

    public function Task()
    {
        return $this->hasMany('App\Task');
    }
    
    public function Transuser()
    {
        return $this->hasMany('App\Transaction', 'user_id', 'id');
    }
    
    public function Comment()
    {
        return $this->hasMany('App\Comment');
                 
    }
    
    public function UserInterest()
    {
        //return $this->hasMany('App\UserInterest');
        return $this->hasMany('App\UserInterest')
        ->join('categories', 'categories.id','=', 'user_interests.category_id');
//        ->join('users', 'users.id','=', 'user_interests.user_id');
        
    }
    public function UserType()
    {
        return $this->belongsTo('App\UserType');

    }
    public function getEmployeeshifts() {
            $usertype = DB::table('usertype')
                                ->select('usertype')
                                ->get();

             return Response::json($usertype);
         }  
    
   #### import fields
    public  $import_fields=['first_name',
        'last_name',
        'email',
        'password',
        'gender',
        'avatar_url',
        'status',
        'phone',
        'city',
        'state',
        'postalcode',
        'country',
        'user_type_id',
        'createdby',
        'aboutme',
            ];


        
}
