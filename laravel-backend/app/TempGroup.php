<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Nicolaslopezj\Searchable\SearchableTrait;
use PhpSoft\Users\Models\UserTrait;

class TempGroup extends Model implements AuthenticatableContract,
                                    //AuthorizableContract,
                                    CanResetPasswordContract
{
    
    use Authenticatable, CanResetPassword,UserTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'temp_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'header',
        'avatar_url',
        'status',
        'city',
        'state',
        'postalcode',
        'latitude',
        'longitude',
        'country',
        'user_id',
        'description',
        'terms_and_condition',
        'slug',
    ];

    
    public function GroupInterest()
    {
        return $this->hasOne('App\GroupInterest')
        ->join('categories', 'categories.id','=', 'group_interests.category_id');
        
    }

    public  $import_fields=[
        'name',
        'avatar_url',
        'status',
        'city',
        'header',
        'state',
        'postalcode',
        'country',
        'user_id',
        'description',
        'terms_and_condition',
            ];
}
