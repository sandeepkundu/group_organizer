<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Nicolaslopezj\Searchable\SearchableTrait;
use PhpSoft\Users\Models\UserTrait;
//use Nicolaslopezj\Searchable\SearchableTrait;


    class GroupEvent extends Model{
    
        public function Group()
        {
            return $this->belongsTo('App\Group');
        }
        

        protected $fillable= ['id','group_id','max_member','name','event_date','end_date','description','recurring','start_time','end_time','location','address','avatar_url','cost','recurring_payment','recurring_week','recurring_day','recurring_month','recurring_date','status'];
    }

?>