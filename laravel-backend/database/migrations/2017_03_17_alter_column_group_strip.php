<?php

namespace {

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * @codeCoverageIgnore
     */
    class ColumnGroupStrip extends Migration
    {
        /**
         * Run the migraions.
         *
         * @return void
         */
        public function up()
        {
            Schema::table('strip_group_payments', function (Blueprint $table) {
                $table->date('membership_start',50)->change();
                $table->date('membership_end',50)->change();
                
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {

        }
    }
}
