<?php

namespace {

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;

    /**
     * @codeCoverageIgnore
     */
    class InsertPermissionTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return  void
         */
        public function up()
        {
                DB::table('permissions')->insert([
                    array('name' => 'add_event','display_name' => 'add_event'),
                    array('name' => 'delete_event','display_name' => 'delete_event'),
                ]);
                DB::table('permission_role')->insert([
                    array('permission_id' => 52,'role_id' => 1),
                    array('permission_id' => 53,'role_id' => 1),
                ]);
        }
        
        public function down()
        {
           
        }
    }
}