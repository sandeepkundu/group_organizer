<?php

namespace {

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * @codeCoverageIgnore
     */
    class UsersAddlatlong extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
                Schema::table('users', function (Blueprint $table) {
                
                $table->string('latitude',255)->nullable()->after('postalcode');
                $table->string('longitude',255)->nullable()->after('latitude');
            });


            Schema::table('groups', function (Blueprint $table) {
                
                $table->string('latitude',255)->nullable()->after('postalcode');
                $table->string('longitude',255)->nullable()->after('latitude');
            });    
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {

        }
    }
}
