<?php

namespace {

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;

    /**
     * @codeCoverageIgnore
     */
    class PermissionRoleTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return  void
         */
        public function up()
        {
                DB::table('permissions')->insert([
                    array('name' => 'edit_event','display_name' => 'add_event'),
                    array('name' => 'view_event','display_name' => 'delete_event'),
                ]);
                DB::table('permission_role')->insert([
                    array('permission_id' => 54,'role_id' => 1),
                    array('permission_id' => 55,'role_id' => 1)
                ]);
        }
        
        public function down()
        {
           
        }
    }
}