<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableEventPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->bigInteger('group_id');
            $table->string('intent',255);
            $table->string('payer_id',255);
            $table->string('payment_id',255);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
        
    }

}