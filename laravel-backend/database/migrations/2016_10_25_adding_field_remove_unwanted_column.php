<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FieldRemoveUnwantedColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('avatar_url');
            $table->dropColumn('remember_token');
            $table->dropColumn('deleted_at');
            $table->dropColumn('site');
            $table->dropColumn('department');
            $table->dropColumn('title');
            $table->dropColumn('timezone');
            $table->dropColumn('language');
            $table->dropColumn('confirmation_code');
            $table->string('city',100);
            $table->string('state',100);
            $table->string('postalcode',100);
            $table->string('country',100);
            $table->string('usertype',100);
            $table->string('aboutme',100);
            $table->string('createdby',100);
            
     

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
