<?php

namespace {

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * @codeCoverageIgnore
     */
    class ColumnStripPayment extends Migration
    {
        /**
         * Run the migraions.
         *
         * @return void
         */
        public function up()
        {
            Schema::table('strip_group_payments', function (Blueprint $table) {
                $table->string('plan_id',50)->after('payment');
                $table->string('payment_plan_id',50)->after('plan_id');
                $table->string('membership_start',50)->after('payment_plan_id');
                $table->string('membership_end',50)->after('membership_start');
                
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {

        }
    }
}
