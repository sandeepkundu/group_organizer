<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GroupsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name',50);
            //$table->string('email')->unique();
            $table->binary('status');
            $table->string('city',100);
            $table->string('state',100);
            $table->string('country',100);
            $table->string('postalcode',50);
            $table->longText('description');
            $table->longText('terms_and_condition');
            $table->string('avatar_url', 100);
            $table->string('slug', 100);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
