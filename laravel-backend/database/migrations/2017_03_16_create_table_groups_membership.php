<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableGroupsMembership extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Group_Membership_Plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',50);
            $table->string('sub_title',50);
            $table->string('members',50);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
        
    }

}

