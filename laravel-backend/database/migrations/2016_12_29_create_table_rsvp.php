<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableRsvp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_confirmations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id');
            $table->integer('event_id');
            $table->integer('user_id');
            $table->integer('user_status');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
