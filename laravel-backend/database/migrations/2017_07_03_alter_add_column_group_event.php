<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnGroupEvent extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('group_events', function(Blueprint $table)
		{
			$table->dropColumn('recurring');
                        $table->string('cost',255)->after('address');
                        $table->string('recurring_payment',255)->nullable()->after('cost');
                        $table->string('recurring_week',255)->nullable()->after('recurring_payment');
                        $table->string('recurring_day',255)->nullable()->after('recurring_week');
                        $table->string('recurring_month',255)->nullable()->after('recurring_day');
                        $table->string('recurring_date',255)->nullable()->after('recurring_month');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}
