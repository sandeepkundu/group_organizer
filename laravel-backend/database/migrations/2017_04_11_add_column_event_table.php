<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ColumnEventTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('group_events', function(Blueprint $table)
		{
			$table->time('start_time')->after('recurring');
                        $table->time('end_time')->after('start_time');
                        $table->string('location',255)->after('end_time');
                        $table->string('address',255)->after('location');
                        
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}
