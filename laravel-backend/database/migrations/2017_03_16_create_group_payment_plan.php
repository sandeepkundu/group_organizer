<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GroupPaymentPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_payment_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('p_id');
            $table->string('type',50);
            $table->string('amount',50);
            $table->string('discount',50);
            $table->string('month',10);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
        
    }

}

