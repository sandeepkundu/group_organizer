<?php

namespace {

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * @codeCoverageIgnore
     */
    class AddColumnCmsTemplates extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
                Schema::table('cms_templates', function (Blueprint $table) {
                
                $table->string('page_title',100)->after('id');
            });
   
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {

        }
    }
}
