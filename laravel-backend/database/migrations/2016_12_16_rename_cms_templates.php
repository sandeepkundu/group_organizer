
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_templates', function (Blueprint $table) {
            $table->renameColumn('heading', 'page_heading');
        });
        Schema::table('cms_templates', function (Blueprint $table) {
            $table->dropColumn('page_title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
