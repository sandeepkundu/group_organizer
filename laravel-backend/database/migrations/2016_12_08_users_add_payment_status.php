<?php

namespace {

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * @codeCoverageIgnore
     */
    class AddPaymentStatus extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table('users', function (Blueprint $table) {
                
                $table->string('slug',100)->nullable()->after('status');
                $table->string('confirmation_code',255)->nullable()->after('slug');
                $table->integer('email_confirmation')->default(0)->after('confirmation_code');
                $table->integer('payment_status')->default(0)->after('email_confirmation');
                $table->string('username',255)->nullable()->after('payment_status');
                $table->string('memberships',255)->nullable()->after('username');
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {

        }
    }
}
