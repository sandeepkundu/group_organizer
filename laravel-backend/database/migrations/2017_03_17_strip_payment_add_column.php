<?php

namespace {

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * @codeCoverageIgnore
     */
    class PaymentAddColumn extends Migration
    {
        /**
         * Run the migraions.
         *
         * @return void
         */
        public function up()
        {
            Schema::table('strip_group_payments', function (Blueprint $table) {
                $table->string('amount',50)->after('membership_end');
                
            });
        }

        
    }
}
