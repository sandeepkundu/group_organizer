<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StripTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('strip_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('strip_id');
            $table->string('object');
            $table->string('amount');
            $table->string('balance_transaction');
            $table->string('captured');
            $table->string('currency');
            $table->string('description');
            $table->string('receipt_email');
            $table->string('receipt_number');
            $table->string('status');
            $table->string('invoice');
            $table->string('livemode');
            $table->string('card_detail');
            $table->string('jsonresponse');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::drop('strip_transactions');
    }
}
