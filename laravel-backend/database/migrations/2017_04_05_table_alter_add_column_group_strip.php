<?php

namespace {

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * @codeCoverageIgnore
     */
    class AlterAddColumnGroupStrip extends Migration
    {
        /**
         * Run the migraions.
         *
         * @return void
         */
        public function up()
        {
            Schema::table('group_stripe_customer', function (Blueprint $table) {
                $table->bigInteger('amount')->after('group_id');
                $table->date('membership_start')->after('amount');
                $table->date('membership_end')->after('membership_start');
                $table->string('plan_id',50)->after('membership_end');
                $table->string('payment_plan_id',50)->after('plan_id');
                $table->string('customer_id', 50)->change();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {

        }
    }
}
