<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserMailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_mail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id',100);
            $table->string('user_email',50);
            $table->longText('subject');
            $table->longText('message');
            $table->string('status',10);
            $table->timestamp('date');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
