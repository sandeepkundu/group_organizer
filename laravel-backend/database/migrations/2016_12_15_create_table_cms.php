<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableCms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('heading');
            $table->longText('sub_heading');
            $table->longText('description');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
        Schema::create('cms_template_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cms_template_id');
            $table->string('image',255);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
