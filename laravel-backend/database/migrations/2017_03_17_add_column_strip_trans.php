<?php

namespace {

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * @codeCoverageIgnore
     */
    class ColumnStripTrans extends Migration
    {
        /**
         * Run the migraions.
         *
         * @return void
         */
        public function up()
        {
            Schema::table('strip_transactions', function (Blueprint $table) {
                $table->string('user_id',50)->after('id');
                
            });
        }

        
    }
}
