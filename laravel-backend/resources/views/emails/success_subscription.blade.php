
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta content="text/html; charset=US-ASCII" http-equiv="Content-Type">
  <!-- Facebook sharing information tags -->
  <meta property="og:title" content="%%subject%%">
  <title> Bevylife : {{$data['subject']}} </title>
</head>

<body style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0;">
  <style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Bitter:400,700);@import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);#outlook a{padding:0}blockquote .original-only,.WordSection1 .original-only{display:none !important}@media only screen and (max-width: 480px){body,table,td,p,a,li,blockquote{-webkit-text-size-adjust:none !important}body{width:100% !important;min-width:100% !important}#template-container{margin-top:0px !important;max-width:600px !important;width:100% !important}#header{padding:30px 20px 30px 20px !important}#content{padding:0 20px 20px !important}#content img.full-width{width:100% !important}#signature-row{width:100% !important}#signature-row .signature-box{display:block !important;width:100% !important;margin-top:20px !important}}@media only screen and (max-width: 480px){#template-container{margin-top:0px !important;max-width:600px !important;width:100% !important}#header{padding:30px 20px 30px 20px !important}#content{padding:20px 20px 20px !important}#content .full-width{width:100% !important}table.collapse{width:100% !important}table.collapse td{width:100% !important;display:block !important}#zenpayroll-banner{padding-left:20px;padding-right:20px}#zenpayroll-banner td:nth-child(1){text-align:center}#zenpayroll-banner td:nth-child(2) p{margin-left:20px}#signature-row{width:100% !important}#signature-row .signature-box{display:block !important;width:100% !important;margin-top:20px !important}}
  </style>
 <style>
    .invoice-box{
        max-width:800px;
        margin:auto;
        padding:30px;
        border:1px solid #eee;
        box-shadow:0 0 10px rgba(0, 0, 0, .15);
        font-size:16px;
        line-height:24px;
        font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color:#555;
    }
    
    .invoice-box table{
        width:100%;
        line-height:inherit;
        text-align:left;
    }
    
    .invoice-box table td{
        padding:5px;
        vertical-align:top;
    }
    
    .invoice-box table tr td:nth-child(2){
        text-align:right;
    }
    
    .invoice-box table tr.top table td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.top table td.title{
        font-size:45px;
        line-height:45px;
        color:#333;
    }
    
    .invoice-box table tr.information table td{
        padding-bottom:40px;
    }
    
    .invoice-box table tr.heading td{
        background:#eee;
        border-bottom:1px solid #ddd;
        font-weight:bold;
    }
    
    .invoice-box table tr.details td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom:1px solid #eee;
    }
    
    .invoice-box table tr.item.last td{
        border-bottom:none;
    }
    
    .invoice-box table tr.total td:nth-child(2){
        border-top:2px solid #eee;
        font-weight:bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td{
            width:100%;
            display:block;
            text-align:center;
        }
        
        .invoice-box table tr.information table td{
            width:100%;
            display:block;
            text-align:center;
        }
    }
    </style>
  <table id="body-table" style="border-collapse: collapse; margin-left: 0; margin-top: 0; margin-right: 0; margin-bottom: 0; padding-left: 0; padding-bottom: 0; padding-right: 0; padding-top: 0; background-color: #f2f2f2; height: 100% !important; width: 100% !important;"
    bgcolor="#f2f2f2">
    <tbody>
      <tr>
        <td align="center" style="border-collapse: collapse;">
          <p id="template-logo" style="text-align: center; margin-right: 0; margin-top: 30px; margin-bottom: 30px; margin-left: 0;" align="center">
            <a href="https://www.bevylife.com" style="color:#59595b">
              <img src="https://www.bevylife.com/assets/home/img/logo.png" id="logo" alt="" style="height: auto; line-height: 100%; outline: none; text-decoration: none; border-bottom-style: none; border-right-style: none; border-top-style: none; border-left-style: none; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0;"
                width="120">
            </a>
          </p>
          <!-- BEGIN
            TEMPLATE // -->
          <table id="template-container" style="border-collapse: collapse; margin-top: 0px; width: 600px; background-color: #FFFFFF; border-top-color: #f00740; border-top-width: 10px; border-top-style: solid;" bgcolor="#FFFFFF" width="600">
            <tbody>
              <tr>
                <td id="content" style="border-collapse: collapse; padding-right: 40px; padding-top: 20px; padding-bottom: 20px; padding-left: 40px;">
                  <div>
                    <a style="color:#59595b">

                    </a>
                  </div>
                  <p class="heading" style="font-family: Helvetica, Arial, sans-serif; font-weight: 700; font-size: 16px; color: #59595b; margin-bottom: 20px; text-align: left;" align="center">Hi {{$data['UserName']}} ,</p>
                  <p class="heading" style="font-family: Helvetica, Arial, sans-serif; font-weight: 200; font-size: 16px; color: #59595b; margin-bottom: 20px; text-align: justify;" align="center">{{'Your Payment Accept'}}<br>
                    <!-- <table style="width:100%" border=2>
                            <tr>
                              <th>Plan</th>
                              <th>Amount</th> 
                              <th>Date</th> 
                              
                            </tr>
                            <tr>
                              <td>{{$data['plan']}}</td>
                              <td>{{$data['amount']}}</td>
                            
                             
                            </tr>
                           
                            
                          </table> -->
        
        <table style="min-width: 500px !important;">
            <tr class="top">
                <td style="min-width: 500px !important;">
                    <table>
                        <tr>
                            
                            
                            <td style="width:80%;text-align: right;">
                                
                                Start Date:  {{$data['startdate']}}<br>
                                End Date: {{$data['enddate']}}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
       
            
        <!--     <tr class="heading">
                <td>
                    Payment Method
                </td>
                
                <td>
                    Check #
                </td>
            </tr> -->
            
          <!--   <tr class="details">
                <td>
                    Check
                </td>
                
                <td>
                    1000
                </td>
            </tr> -->
            
            <tr>
                <td style="border: 1px solid #9cbdcc; background-color: gray">
                    Item
                </td>
                
                <td style="border: 1px solid #9cbdcc; background-color: gray">
                    Price
                </td>
            </tr>
            
            <tr class="item">
                <td>{{$data['plan']}}</td>
                <td>${{$data['amount']}}</td>
            </tr>
            
          <!--   <tr class="item">
                <td>
                    Hosting (3 months)
                </td>
                
                <td>
                    $75.00
                </td>
            </tr>
            
            <tr class="item last">
                <td>
                    Domain name (1 year)
                </td>
                
                <td>
                    $10.00
                </td>
            </tr> -->
            
            <tr class="total">
            <td></td>

                <td> ${{$data['amount']}}</td>
            </tr>
        </table>


                  </p>
                  <div class="cta margin-top-30px margin-bottom-50px" style="text-align: center; margin-top: 30px !important; margin-bottom: 50px !important;" align="center">
                   
                  </div>
                  <table id="have-questions" class="full-width margin-top-20px" style="border-collapse: collapse; background-color:#74CECE; margin-top: 20px !important;" width="520">
                    <tr>
                      <td style="border-collapse: collapse; text-align: center;">
                        <p class="title margin-bottom-10px" style="color: #fff; font-family: 'Open Sans', Helvetica, Arial, sans-serif; font-weight: 700; font-size: 14px; text-align: center; margin-bottom: 10px !important;" align="center"><strong>HAVE QUESTIONS?</strong>
                        </p>
                        <p class="call-us margin-top-none" style="margin-bottom: 20px; color: #FFFFFF; font-family: 'Open Sans', Helvetica, Arial, sans-serif; font-size: 14px; text-align: center; margin-top: 0 !important;" align="center">Drop us a line at 
                          <a href="mailto:support@bevylife.com" style="color:#59595b;color:#FFFFFF">support@bevylife.com</a>. <br>We look forward to hearing from you.
                        </p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <!-- // END TEMPLATE -->
          <div id="template-footer" style="padding-top: 30px; padding-right: 20px; padding-bottom: 50px; padding-left: 20px;">
            <div id="footer-copyright" style="font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #59595b; font-size: 14px;">&#169; 2017 The Bevy Corporation</div>
            <div id="footer-address" style="font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #59595b; font-size: 14px;">3845 Tennyson Street, Suite 121, Denver, CO 80212</div>
            <div id="footer-links" class="margin-top-10px" style="font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #59595b; font-size: 14px; margin-top: 10px !important;">
              <a href="https://www.bevylife.com/#/home/index/policy" style="color:#59595b">Privacy Policy</a>
            </div>
            <div class="padding-top-20px" style="font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #59595b; font-size: 14px; padding-top: 20px !important;">
              <a href="https://www.facebook.com/bevylife" style="color:#59595b;text-decoration:none;">
                <img src="<?php echo url('/');?>/share/fb.png" width="30" height="30" hspace="7">
              </a>
              <a href="https://www.instagram.com/bevylife/" style="color:#59595b; text-decoration:none;">
                  <img src="<?php echo url('/');?>/share/in.png" style="height: auto; line-height: 100%; outline: none; text-decoration: none; border-bottom-style: none; border-right-style: none; border-top-style: none; border-left-style: none; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0;"
                  width="30" height="30" hspace="7">
              </a>
            </div>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
</body>

</html>