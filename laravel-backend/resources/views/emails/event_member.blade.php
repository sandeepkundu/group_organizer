<?php
  $email_template = App\EmailTemplate::find(5);

?>

<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">


<body style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;
 background-color: #fff; height: 100%; margin: 0; padding: 0; width: 100%">
  <center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" id="bodyTable" style="border-collapse: collapse; mso-table-lspace: 0;
 mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust:
 100%; background-color: #fff; height: 100%; margin: 0; padding: 0; width:
 100%" width="100%">
      <tr>
        <td align="center" id="bodyCell" style="mso-line-height-rule: exactly;
 -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-top: 0;
 height: 100%; margin: 0; padding: 0; width: 100%" valign="top">
          <!-- BEGIN TEMPLATE // -->
          <!--[if gte mso 9]>
              <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                <tr>
                  <td align="center" valign="top" width="600" style="width:600px;">
                  <![endif]-->
          <table border="0" cellpadding="0" cellspacing="0" class="templateContainer" style="border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;
 -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; max-width:
 600px; border: 0" width="100%">
            <tr>
              <td id="templatePreheader" style="mso-line-height-rule: exactly;
 -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #fff;
 border-top: 0; border-bottom: 0; padding-top: 16px; padding-bottom: 8px" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" class="mcnTextBlock" style="border-collapse: collapse; mso-table-lspace: 0;
 mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;
 min-width:100%;" width="100%">
                  <tbody class="mcnTextBlockOuter">
                    <tr>
                      <td class="mcnTextBlockInner" style="mso-line-height-rule: exactly;
 -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%" valign="top">
                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnTextContentContainer" style="border-collapse: collapse; mso-table-lspace: 0;
 mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust:
 100%; min-width:100%;" width="100%">
                          <tbody>
                            <tr>
                              <td class="mcnTextContent" style='mso-line-height-rule: exactly;
 -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word;
 color: #2a2a2a; font-family: "Asap", Helvetica, sans-serif; font-size: 12px;
 line-height: 150%; text-align: left; padding-top:9px; padding-right: 18px;
 padding-bottom: 9px; padding-left: 18px;' valign="top">
                                <a style="mso-line-height-rule: exactly;
 -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #2a2a2a;
 font-weight: normal; text-decoration: none" target="_blank" title="{{ $email_template->description }}">
                                  <img align="none" alt="{{ $email_template->description }}" height="32" style="-ms-interpolation-mode: bicubic; border: 0; outline: none;
 text-decoration: none; height: auto; width: 107px; height: 32px; margin: 0px;" width="107" />
                                </a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
              <p id="template-logo" style="text-align: center; margin-right: 0; margin-top: 30px; margin-bottom: 30px; margin-left: 0;" align="center">
            <a href="https://www.bevylife.com" style="color:#59595b">
              <img src="https://www.bevylife.com/assets/home/img/logo.png" id="logo" alt="" style="height: auto; line-height: 100%; outline: none; text-decoration: none; border-bottom-style: none; border-right-style: none; border-top-style: none; border-left-style: none; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0;"
                width="120">
            </a>
          </p>
            
            <tr>
              <td id="templateBody" style="mso-line-height-rule: exactly;
 -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #f7f7ff;
 border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 0" valign="top">
                
                <table border="0" cellpadding="0" cellspacing="0" class="mcnTextBlock" style="border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace:
 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;
 min-width:100%;" width="100%">
                  <tbody class="mcnTextBlockOuter">
                    <tr>
                      <td class="mcnTextBlockInner" style="mso-line-height-rule: exactly;
 -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%" valign="top">
                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnTextContentContainer" style="border-collapse: collapse; mso-table-lspace: 0;
 mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust:
 100%; min-width:100%;" width="100%">
                          <tbody>
                            <tr>
                              <td class="mcnTextContent" style='mso-line-height-rule: exactly;
 -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word;
 color: #2a2a2a; font-family: "Asap", Helvetica, sans-serif; font-size: 16px;
 line-height: 150%; text-align: left; padding-top:9px; padding-right: 18px;
 padding-bottom: 9px; padding-left: 18px;' valign="top">
                                    <p class="heading" style="font-family: Helvetica, Arial, sans-serif; font-weight: 700; font-size: 16px; color: #59595b; margin-bottom: 20px; text-align: left;" align="center">Hi,</p>
                                    <p class="heading" style="font-family: Helvetica, Arial, sans-serif; font-weight: 200; font-size: 16px; color: #59595b; margin-bottom: 20px; text-align: justify;" align="center">{{$data->message}}</p>
                                    <p class="heading" style="font-family: Helvetica, Arial, sans-serif; font-weight: 200; font-size: 16px; color: #59595b; margin-bottom: 20px; text-align: justify;" align="center">Please take a look at our event {{$data->url}}/#/home/single-event/{{$data->id}}</p>
                                    <div class="cta margin-top-30px margin-bottom-50px" style="text-align: center; margin-top: 30px !important; margin-bottom: 50px !important;" align="center">
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
                
                <table id="have-questions" class="full-width margin-top-20px" style="border-collapse: collapse; background-color:#74CECE; margin-top: 20px !important;" width="600">
                    <tr>
                      <td style="border-collapse: collapse; text-align: center;">
                        <p class="title margin-bottom-10px" style="color: #fff; font-family: 'Open Sans', Helvetica, Arial, sans-serif; font-weight: 700; font-size: 14px; text-align: center; margin-bottom: 10px !important;" align="center"><strong>HAVE QUESTIONS?</strong>
                        </p>
                        <p class="call-us margin-top-none" style="margin-bottom: 20px; color: #FFFFFF; font-family: 'Open Sans', Helvetica, Arial, sans-serif; font-size: 14px; text-align: center; margin-top: 0 !important;" align="center">Drop us a line at 
                          <a href="mailto:support@bevylife.com" style="color:#59595b;color:#FFFFFF">support@bevylife.com</a>. <br>We look forward to hearing from you.
                        </p>
                      </td>
                    </tr>
                  </table>
              </td>
            </tr>
            
          </table>
          <div id="template-footer" style="padding-top: 30px; padding-right: 20px; padding-bottom: 50px; padding-left: 20px;">
            <div id="footer-copyright" style="font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #59595b; font-size: 14px;">&#169; 2017 The Bevy Corporation</div>
            <div id="footer-address" style="font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #59595b; font-size: 14px;">3845 Tennyson Street, Suite 121, Denver, CO 80212</div>
            <div id="footer-links" class="margin-top-10px" style="font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #59595b; font-size: 14px; margin-top: 10px !important;">
              <a href="https://www.bevylife.com/#/home/index/policy" style="color:#59595b">Privacy Policy</a>
            </div>
            <div class="padding-top-20px" style="font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #59595b; font-size: 14px; padding-top: 20px !important;">
              <a href="https://www.facebook.com/bevylife" style="color:#59595b;text-decoration:none;">
                <img src="<?php echo url('/');?>/share/fb.png" width="30" height="30" hspace="7">
              </a>
              <a href="https://www.instagram.com/bevylife/" style="color:#59595b; text-decoration:none;">
                  <img src="<?php echo url('/');?>/share/in.png" style="height: auto; line-height: 100%; outline: none; text-decoration: none; border-bottom-style: none; border-right-style: none; border-top-style: none; border-left-style: none; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0;"
                  width="30" height="30" hspace="7">
              </a>
            </div>
          </div>
          <!--[if gte mso 9]>
                  </td>
                </tr>
              </table>
            <![endif]-->
          <!-- // END TEMPLATE -->
        </td>
      </tr>
    </table>
  </center>
</body>

</html>