<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta content="text/html; charset=US-ASCII" http-equiv="Content-Type">
  <!-- Facebook sharing information tags -->
  <meta property="og:title" content="%%subject%%">
  <title>Bevylife</title>
</head>

<body style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0;">
  <style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Bitter:400,700);
  </style>
  <table id="body-table" style="border-collapse: collapse; margin-left: 0; margin-top: 0; margin-right: 0; margin-bottom: 0; padding-left: 0; padding-bottom: 0; padding-right: 0; padding-top: 0; background-color: #f2f2f2; height: 100% !important; width: 100% !important;"
    bgcolor="#f2f2f2">
    <tbody>
      <tr>
        <td align="center" style="border-collapse: collapse;">
          <p id="template-logo" style="text-align: center; margin-right: 0; margin-top: 30px; margin-bottom: 30px; margin-left: 0;" align="center">
           <a href="https://www.bevylife.com" style="color:#59595b">
              <img src="https://www.bevylife.com/assets/home/img/logo.png" id="logo" alt="" style="height: auto; line-height: 100%; outline: none; text-decoration: none; border-bottom-style: none; border-right-style: none; border-top-style: none; border-left-style: none; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0;"
                width="120">
            </a>
          </p>
          
          <!-- BEGIN
            TEMPLATE // -->
          <table id="template-container" style="border-collapse: collapse; margin-top: 0px; width: 600px; background-color: #FFFFFF; border-top-color: #363636; border-top-width: 10px; border-top-style: solid;" bgcolor="#FFFFFF" width="600">
            <tbody>
              <tr>
                <td id="content" style="border-collapse: collapse; padding-right: 40px; padding-top: 20px; padding-bottom: 20px; padding-left: 40px;">
                  

                  <div class="message-body">
                      <?php echo"$email_body"; ?>
                  </div>
                  </p>
                  <div class="cta margin-top-30px margin-bottom-50px" style="text-align: center; margin-top: 30px !important; margin-bottom: 50px !important;" align="center">

                    <a href="{{ url('../../#/register/verify/' . @$confirmation_code) }}" style="color:#59595b;background-color:#c5de8a;border-radius:25px;color:#000000;display:inline-block;font-family: 'Open Sans', Helvetica, Arial, sans-serif;font-size:14px;letter-spacing:1px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:300px;-webkit-text-size-adjust:none;">CONFIRM EMAIL ADDRESS</a>
                    
                  </div>
                  <p>
                    Cheers,
                  </p>
                  <p>
                    The Bevylife Team
                  </p>
                  <table id="have-questions" class="full-width margin-top-20px" style="border-collapse: collapse; background-color:#74CECE; margin-top: 20px !important;" width="520">
                    <tr>
                      <td style="border-collapse: collapse; text-align: center;">
                        <p class="title margin-bottom-10px" style="color: #fff; font-family: 'Open Sans', Helvetica, Arial, sans-serif; font-weight: 700; font-size: 14px; text-align: center; margin-bottom: 10px !important;" align="center"><strong>HAVE QUESTIONS?</strong>
                        </p>
                        <p class="call-us margin-top-none" style="margin-bottom: 20px; color: #FFFFFF; font-family: 'Open Sans', Helvetica, Arial, sans-serif; font-size: 14px; text-align: center; margin-top: 0 !important;" align="center">Drop us a line at 
                          <a href="mailto:support@bevylife.com" style="color:#59595b;color:#FFFFFF">support@bevylife.com</a>. <br>We look forward to hearing from you.
                        </p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <!-- // END TEMPLATE -->
          <div id="template-footer" style="padding-top: 30px; padding-right: 20px; padding-bottom: 50px; padding-left: 20px;">
            <div id="footer-copyright" style="font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #59595b; font-size: 14px;">&#169; 2017 The Bevy Corporation</div>
            <div id="footer-address" style="font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #59595b; font-size: 14px;">3845 Tennyson Street, Suite 121, Denver, CO 80212</div>
            <div id="footer-links" class="margin-top-10px" style="font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #59595b; font-size: 14px; margin-top: 10px !important;">
              <a href="https://www.bevylife.com/#/home/index/policy" style="color:#59595b">Privacy Policy</a>
            </div>
            <div class="padding-top-20px" style="font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #59595b; font-size: 14px; padding-top: 20px !important;">
              <a href="https://www.facebook.com/bevylife" style="color:#59595b;text-decoration:none;">
                <img src="<?php echo url('/');?>/share/fb.png" width="30" height="30" hspace="7">
              </a>
              <a href="https://www.instagram.com/bevylife/" style="color:#59595b; text-decoration:none;">
                  <img src="<?php echo url('/');?>/share/in.png" style="height: auto; line-height: 100%; outline: none; text-decoration: none; border-bottom-style: none; border-right-style: none; border-top-style: none; border-left-style: none; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0;"
                  width="30" height="30" hspace="7">
              </a>
            </div>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
</body>

</html>