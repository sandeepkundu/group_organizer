'use strict';

var app = angular.module('ng-laravel',[]);
app.controller('CategoryhomeCtrl',function($scope,$auth,$state,Restangular,$rootScope,$http,$translate,$location,SweetAlert,$window){
   
     $window.scrollTo(0, 0);
    /* show loading on page change */
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if (toState.resolve) {
            $scope.loader = true;
        }
    });
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        if (toState.resolve) {
            $scope.loader = false;
        }
    });
 if($auth.isAuthenticated().$$state.value==true){
        $scope.profile = $auth.getProfile().$$state.value;
    }
    if($scope.profile){
        if($scope.profile.firstLogin==0){
            $scope.firstTime=1;
        }
    }
    Restangular.all('user').get($scope.profile.id).then(function(data) {

                    //console.log(data.userinterest);
                    $scope.interest=data.userinterest;

                    //$scope.interest=data.userinterest.join(", ");

                }, function(response) {
                SweetAlert.swal({ title: "Error", text:'UnAutorised user', type: "error", html:true});
            });  

    //$scope.user='{}';
    var myarray = []; 
    $scope.getCategory=function (){
       
       $http({
            method: 'POST',
            url: 'laravel-backend/public/api/groups/id_category',
            data:{'id':$scope.profile.id}
        }).then(function successCallback(response) {
           //console.log(response.data);
            var a = [];
                for(var i in response.data){
                    a.push(response.data[i]);
                }
              $scope.categories =a;

        }, function errorCallback(response) {
            
        }); 
    };
// $scope.user={};
// $scope.user.interest=['Football'];
//     $scope.showCategory = function() {
//         var selected = $filter('filter')($scope.categories, {value: $scope.user.status});
//         return ($scope.user.status && selected.length) ? selected[0].text : 'Not set';
//     };


$scope.form = {

        submit: function (form,ndata) {
            var firstError = null;
            if (form.$invalid) {

                var field = null, firstError = null;
                for (field in form) {
                    if (field[0] != '$') {
                        if (firstError === null && !form[field].$valid) {
                            firstError = form[field].$name;
                        }

                        if (form[field].$pristine) {
                            form[field].$dirty = true;
                        }
                    }
                }

                angular.element('.ng-invalid[name=' + firstError + ']').focus();
                SweetAlert.swal("Error", "Please enter an Area of Interest", "error");
                return;

            } else {

                //console.log(ndata);   
                $scope.loader = true;
                 $('button').attr('disabled',true);
                
                Restangular.all('user/user-categories').customPOST(ndata).then(function(data) {
                SweetAlert.swal({ title: "Success", text: data.message, type: "success"});
                
                $scope.interest='';
                $scope.loader =false;
                 $('button').attr('disabled',false);
                 location.reload();
                 //$location.path('/registered/find-group');
            }, function(response) {
                //console.log(response);
                SweetAlert.swal({ title: "Error", text:response.data.error, type: "error", html:true});
                $scope.loader =false;
                 $('button').attr('disabled',false);
            });         
                
            }

        }
    };

$scope.category_delete = function (id){
    
        swal({
            title: "Are you sure?",
            text: "It will delete the Category",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#CAE197",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            html: false
        }, function () {
            $http({
                method: 'POST',
                url: 'laravel-backend/public/api/user/category_delete',
                data:{'id':id}
            }).then(function successCallback(response) {
               $('.interstname_'+id).remove();
               swal("Deleted!", "Selected Category Delete Successfully!", "success");
            }, function errorCallback(response) {

            }); 
        });
       
      
    };

});