'use strict';


app.controller('UserrsvpCtrl', function ($scope,  $auth, $http, $translate, SweetAlert) {
    
    $('#loader').hide();
    
    $scope.profile = $auth.getProfile().$$state.value;
    $scope.rsvps = {};
    $scope.get_events = function () {
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/get_userrsvp',
            data: {'id': $scope.profile.id}
        }).then(function successCallback(response) {
            $scope.rsvps.past = response.data.past_event;
            $scope.rsvps.future = response.data.future_event;
            //$scope.event_response = $scope.event.event_response
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    $scope.get_events();
    
    $scope.cancel_rsvp = function(id){
        
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/cancel_rsvp',
            data: {'id': id,'user_id':$scope.profile.id}
        }).then(function successCallback(response) {
            SweetAlert.swal({ title: "Success", text:'RSVP Canceled', type: "success"});
            $scope.get_events();
            //$scope.event_response = $scope.event.event_response
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
        
    }
});