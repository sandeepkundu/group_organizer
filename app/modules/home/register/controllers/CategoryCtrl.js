'use strict';

var app = angular.module('ng-laravel',[]);
app.controller('CategoryhomeCtrl',function($scope,$auth,$state,Restangular,$rootScope,$http,$translate,SweetAlert,$window){
    /*
     * Define initial value
     */
     $window.scrollTo(0, 0);
    /* show loading on page change */
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if (toState.resolve) {
            $scope.loader = true;
        }
    });
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        if (toState.resolve) {
            $scope.loader = false;
        }
    });
 if($auth.isAuthenticated().$$state.value==true){
        $scope.profile = $auth.getProfile().$$state.value;
    }
    Restangular.all('user').get($scope.profile.id).then(function(data) {

                    //console.log(data.userinterest);


                    $scope.interest=data.userinterest.join(", ");

                }, function(response) {
                SweetAlert.swal({ title: "Error", text:'UnAutorised user', type: "error", html:true});
            });  

    //$scope.user='{}';
    var myarray = []; 
    $scope.getCategory=function (){
       
       $http({
            method: 'GET',
            url: 'laravel-backend/public/api/groups/category',
            
        }).then(function successCallback(response) {
           //console.log(response.data);
            var a = [];
                for(var i in response.data){
                    a.push(response.data[i]);
                }
              $scope.categories =a;

        }, function errorCallback(response) {
            
        }); 
    };
// $scope.user={};
// $scope.user.interest=['Football'];
//     $scope.showCategory = function() {
//         var selected = $filter('filter')($scope.categories, {value: $scope.user.status});
//         return ($scope.user.status && selected.length) ? selected[0].text : 'Not set';
//     };


$scope.form = {

        submit: function (form,ndata) {
            var firstError = null;
            if (form.$invalid) {

                var field = null, firstError = null;
                for (field in form) {
                    if (field[0] != '$') {
                        if (firstError === null && !form[field].$valid) {
                            firstError = form[field].$name;
                        }

                        if (form[field].$pristine) {
                            form[field].$dirty = true;
                        }
                    }
                }

                angular.element('.ng-invalid[name=' + firstError + ']').focus();
                SweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
                return;

            } else {

                //console.log(ndata);   
                $scope.loader = true;
                 $('button').attr('disabled',true);
                
                Restangular.all('user/user-categories').customPOST(ndata).then(function(data) {
                SweetAlert.swal({ title: "Success", text: data.message, type: "success"});
                $scope.interest='';
                $scope.loader =false;
                 $('button').attr('disabled',false);

            }, function(response) {
                //console.log(response);
                SweetAlert.swal({ title: "Error", text:response.data.error, type: "error", html:true});
                $scope.loader =false;
                 $('button').attr('disabled',false);
            });         
                
            }

        }
    };



});