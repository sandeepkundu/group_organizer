'use strict';

var app = angular.module('ng-laravel', ['oitozero.ngSweetAlert', 'dropzone']);

app.directive("datepicker", function () {
  return {
    restrict: "A",
    require: "ngModel",
    inline:true,
    link: function (scope, elem, attrs, ngModelCtrl) {
      var updateModel = function (dateText) {
        scope.$apply(function () {
          ngModelCtrl.$setViewValue(dateText);
        });
      };
      var options = {
        dateFormat: "yy/mm/dd",
        onSelect: function (dateText) {
          updateModel(dateText);
        }
      };
      elem.datepicker(options);
    }
  }
});

app.controller('UserJoinedCtrl', function ($scope, $stateParams, $window,$auth,resolvedItems,$location, $state, Restangular, $rootScope, $http, $translate, SweetAlert) {
   $window.scrollTo(0, 0);
    var _groupService = Restangular.all('group');
   if($auth.isAuthenticated().$$state.value==true){
    $scope.profile = $auth.getProfile().$$state.value;
    }
    $scope.event_record = 0;
    $scope.records = 0;
    $scope.single_record = 0;
    
    $scope.id = $scope.profile.id;
    if($scope.profile.user_type_id==3){
        
    }
    $scope.categories = resolvedItems;
    $('#loader').hide();
    
    $scope.get_events = function (id) {
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/get_event',
            data: {'id': id},
        }).then(function successCallback(response) {
            $scope.event = response.data;
            
            //$scope.event_response = $scope.event.event_response
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    $scope.reload = function(){
        location.reload();
    }
    
    
    $scope.event_rsvp = function (id) {
        
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/event_rsvp',
            data: {'id': id},
        }).then(function successCallback(response) {
            $scope.event_rs = response.data;

        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
        
    };
    
    
    $scope.user_event_status = function (status,id,event_id){
       // alert(event_id);
        
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/user_event_status',
            data: {'id':id,'status':status,'event_id':event_id},
        }).then(function successCallback(response) {
            //SweetAlert.swal({ title: "Success", text:response.data.msg, type: "success"});
            //$scope.event.user_rsvp=response.data.data;
            $('#'+id+'_'+event_id).hide();
            $('#'+id+'__'+event_id).hide();
            if(status==1){
                $('#'+id).html('<span class="label label-table label-success"><b>Accepted</b></span>');
            }else{
                $('#'+id).html('<span class="label label-table label-danger"><b>Rejected</b></span>');
            }
            
        }, function errorCallback(response) {
            SweetAlert.swal({ title: "Error", text: displayError(response) , type: "error", html:true});
        });
    }
    
    $scope.export = function(selection,export_type){
        var recordType = 1;
        _groupService.withHttpConfig({responseType: 'blob'}).customGET('export/file',{record_type:recordType,export_type:export_type,'selection[]':selection}).then(function(response) {
            var url = (window.URL || window.webkitURL).createObjectURL(response);
            var anchor = document.createElement("a");
            document.body.appendChild(anchor);//required in FF, optional for Chrome
            anchor.download = "exportfile."+export_type;
            anchor.href = url;
            anchor.click();
        })
    };
    
    $scope.get_event = function () {
//        console.log($scope.profile.id);
        $scope.single_record = $scope.single_record + 4;

        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/groups/get_list',
            data: {'id': $scope.id,'records':$scope.single_record},
        }).then(function successCallback(response) {
           $scope.groups = response.data.member;
           $scope.ownergroups = response.data.owner;

           
           $('#loader').hide();

        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    $scope.get_event();
    
    $scope.event_date = function (date) {
//        var date = '2017-03-15';
        $http({
            method: 'GET',
            url: 'laravel-backend/public/api/event_date?date='+date,
           
        }).then(function successCallback(response) {
            $scope.data = response.data;
           
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    
    $scope.member_status = function(status,id) {
       
       $http({
                method: 'POST',
                url: 'laravel-backend/public/api/member/status',
                data: {'id':id,'status':status}
              }).then(function successCallback(response) {
                    SweetAlert.swal({ title: "Success", text:'Member Status Changed', type: "success"}); 
                    location.reload();
                }, function errorCallback(response) {
                  // called asynchronously if an error occurs
                  // or server returns response with an error status.
                });
     };
     
     $scope.group_dash = function (id) {
         $('#loader').show();
         $scope.members = '';
         $scope.images = '';
         $scope.events = '';
         $scope.pending_groups = '';
         //$scope.edit_group = '';
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/show',
            data: {'id':id},
        }).then(function successCallback(response) {
            $scope.edit_group = response.data;
            $('#loader').hide();
            setTimeout(function(){
                $('#managetab').trigger("click");
        },500)

            $('#groups_details').tab('show');
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    $scope.get_members = function (id) {
        $('#loader').show();
        //$scope.members = '';
        $scope.images = '';
        $scope.events = '';
        $scope.pending_groups = '';
        $scope.edit_group = '';
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/members',
            data: {'id': id},
        }).then(function successCallback(response) {
            $scope.members = response.data;
            $('#loader').hide();
             setTimeout(function(){
                $('#memberstab').trigger("click");
        },500)
//            console.log($scope.members);
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    
    $scope.get_images = function (id) {
        $('#loader').show();
        $scope.members = '';
        //$scope.images = '';
        $scope.events = '';
        $scope.pending_groups = '';
        $scope.edit_group = '';
        $scope.group_id = id;
        //console.log($scope.group_id);
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/get_images',
            data: {'id': id},
        }).then(function successCallback(response) {
            $scope.images = response.data;
            $('#loader').hide();
            setTimeout(function(){
                $('#imgstab').trigger("click");
        },500)
//            console.log($scope.members);
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    
    $scope.get_events = function (group_id) {
        $('#loader').show();
       
        $scope.event_record = $scope.event_record + 4;
        $scope.members = '';
        $scope.images = '';
        //$scope.events = '';
        $scope.pending_groups = '';
        $scope.edit_group = '';
        //console.log(id);
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/events',
            data: {'id': group_id,'records':$scope.event_record},
        }).then(function successCallback(response) {
            $scope.events = response.data.events;
            $scope.grp_id = response.data.group_id;
            //alert($scope.events.length);
            $('#loader').hide();
            setTimeout(function(){
                $('#eventstab').trigger("click");
        },500)
            
//            console.log($scope.groups_list);
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });

    };
    
    $scope.pending_members = function (group_id) {
        $('#loader').show();
        $scope.members = '';
        $scope.images = '';
        $scope.events = '';
        //$scope.pending_groups = '';
        $scope.edit_group = '';
        //console.log(id);
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/pending_member',
            data: {'id': group_id,'user_id': $scope.id},
        }).then(function successCallback(response) {
            $scope.pending_groups = response.data;
            $('#loader').hide();
             setTimeout(function(){
                $('#joiningtab').trigger("click");
        },500);
//            console.log($scope.groups_list);
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    $scope.get_group = function () {
        
        $('#loader').show();
         //$('#load_more').show();
        $scope.records = $scope.records + 4;
        
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/get',
            data: {'id': $scope.id,'records':$scope.records},
        }).then(function successCallback(response) {
            $scope.groups_list = response.data;
            //console.log($scope.groups_list[0]);
            $('#loader').hide();
//            console.log($scope.groups_list);
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    $scope.get_group();
    
    
    $scope.get_inactivegroup = function () {
        
        $('#loader').show();
         //$('#load_more').show();
        $scope.records = $scope.records + 4;
        
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/inactive',
            data: {'id': $scope.id,'records':$scope.records},
        }).then(function successCallback(response) {
            
            $scope.inactive_list = response.data;
            $('#loader').hide();
            
//            console.log($scope.groups_list);
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    $scope.get_inactivegroup();
    
    $scope.event_con = function (status,event_id) {
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/event/con',
           data: {'status':status,'event_id':event_id },
        }).then(function successCallback(response) {
            SweetAlert.swal({ title: "Success", text:'Conformation Accepted', type: "success"}); 
            location.reload();
           
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    $scope.group_pay = function (group_id) {
        
        $location.path('/registered/orgpayment/g_id:' + group_id.id);

    };
    
    // delete images from group
    $scope.removeimage = function (image) {
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this Image!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#CAE197",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false,
          html: false
            }, function(){
            
           $http({
                method: 'POST',
                url: 'laravel-backend/public/api/grpgroup/deleteimage',
               data: {'image':image},
            }).then(function successCallback(response) {
                SweetAlert.swal({ title: "Success", text:'Conformation Accepted', type: "success"}); 
                location.reload();

            }, function errorCallback(response) {
                SweetAlert.swal({ title: "Error", text:'Can not delete', type: "warning"}); 
            });

        });
    };
    
    /*
     * Dropzone file uploader for group image
     */
    $scope.dropzoneConfig = {
        options: {// passed into the Dropzone constructor
            url: '../laravel-backend/public/api/uploadimage',
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 1, // MB
            acceptedFiles: 'image/jpeg,image/png,image/gif',
            maxFiles: 1,
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            addRemoveLinks: true,
            dictDefaultMessage: '<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
            dictResponseError: 'Error while uploading file!',
        },
        'eventHandlers': {
            'removedfile': function (file, response) {
                $http({
                    method: "POST",
                    url: "../laravel-backend/public/api/deleteimage/" + $scope.edit_group.avatar_url
                }).then(function mySucces(response) {
                    $scope.deleteMessage = response.data;
                    $scope.edit_group.avatar_url = '';
                });
            },
            'success': function (file, response) {
                $scope.edit_group.avatar_url = response.filename;
            }
        }
    };
    
    /*
     * Dropzone file uploader For layout image
     */
    $scope.dropzoneConfig2 = {
        options: {// passed into the Dropzone constructor
            url: '../laravel-backend/public/api/uploadlayout',
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 1, // MB
            acceptedFiles: 'image/jpeg,image/png,image/gif',
            maxFiles: 1,
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            addRemoveLinks: true,
            dictDefaultMessage: '<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
            dictResponseError: 'Error while uploading file!',
        },
        'eventHandlers': {
            'removedfile': function (file, response) {
                $http({
                    method: "POST",
                    url: "../laravel-backend/public/api/deletelayout/" + $scope.edit_group.layout
                }).then(function mySucces(response) {
                    $scope.deleteMessage = response.data;
                    $scope.edit_group.layout = '';
                });
            },
            'success': function (file, response) {
                $scope.edit_group.layout = response.filename;
            }
        }
    };
    
    $scope.update = function (group) {
        console.log(group);
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/update',
            data: {'id': group.id , 'name':group.name,'avatar_url':group.avatar_url ,'layout':group.layout , 'description':group.description , 'category_name':group.category_name , 'header':group.header},
        }).then(function successCallback(response) {
               SweetAlert.swal({ title: "Success", text:'Group updated', type: "success"}); 
              
        }, function errorCallback(response) {
               SweetAlert.swal({ title: "Error", text: displayError(response) , type: "error", html:true});
        });
    }
    function displayError(param) {
        var str = '';
        angular.forEach(param.data.error, function (value, key) {
            angular.forEach(value, function (v, k) {
                str += v +'<br>';
            })
        });
        return str;
    }
    
    
    //For mutiple images
    $scope.dropzoneOption1 = {
        url: "laravel-backend/public/api/grpgroup/groupimages",
        paramName: "file", // The name that will be used to transfer the file
        sending: function (file, xhr, formData) {
            formData.append("group_id",$scope.group_id.id );
        },
        acceptedFiles: 'image/jpeg,image/png,image/gif',
        maxFilesize: 50, // MB
        maxFiles: 10,
        maxfilesexceeded: function (file) {
            this.removeAllFiles();
            this.addFile(file);
        },
        addRemoveLinks: true,
        dictDefaultMessage:
                '<span class="bigger-150 bolder"><i class=" fa fa-caret-right red"></i> Drop files</span> to upload \
            <span class="smaller-80 grey">(or click)</span> <br /> \
            <i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
        dictResponseError: 'Error while uploading file!',
        init: function () {

            this.on("removedfile", function (file) {

                $.ajax({
                    type: 'POST',
                    url: 'laravel-backend/public/api/grpgroup/delgro/' + $scope.currImgID,
                    //data: {name:$scope.currImgID},
                    dataType: 'html',
                    success: function (data) {
                        var rep = JSON.parse(data);
                        if (rep.code == 200)
                        {

                        }

                    }
                });

            });
            this.on("success", function(file){
                if(file.width < 400 ||  file.height<400){
                    $rootScope.$broadcast('group.imageError');
                    this.removeAllFiles();
                }
            })
        },
        error: function (file, response) {

        },
        success: function (file, done) {
//        console.log(file.name);
            //console.log(done);
            $scope.currImgID = done.filename;
        }
    };

    $scope.clearall = function(){
        $scope.members = '';
        $scope.images = '';
        $scope.events = '';
        $scope.pending_groups = '';
        $scope.edit_group = '';
    }
                                         


});