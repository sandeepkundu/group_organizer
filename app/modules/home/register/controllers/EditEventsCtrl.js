'use strict';

var app = angular.module('ng-laravel', ['dropzone', 'xeditable', 'ui.bootstrap', 'checklist-model', 'bootstrap-image-gallery', 'oitozero.ngSweetAlert']);
app.directive('jqdatepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            element.datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function (date) {
                    scope.event.event_date = date;
                    scope.$apply();
                }
            });

            element.css('position', 'relative');
            element.css('z-index', 9999);
        }
    };
});
app.controller('EditEventsCtrl',function($scope,$auth,$state,$window,resolvedItems,Restangular,$rootScope,$http,$translate,SweetAlert){
    
    var _groupService = Restangular.all('group');
    //alert($state.params.id);
    $scope.event_id = $state.params.id;
    
    if($auth.isAuthenticated().$$state.value==true){
        $scope.profile = $auth.getProfile().$$state.value;
    }
    $window.scrollTo(0, 0);
    $scope.categories = resolvedItems;
    $scope.get_events = function () {
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/get_event',
            data: {'id': $scope.event_id},
        }).then(function successCallback(response) {
            $scope.event = response.data;
            //$scope.event_response = $scope.event.event_response
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    var date = new Date();
    var aser = date.setDate(date.getDate() + 1);
    $('.datepicker').datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true,
        startDate: new Date(aser)
    });
    $('.datepicker1').datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true,
        startDate: new Date(aser)
    });
    
    $scope.get_events();
    $scope.event_rsvp = function (id) {
        
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/event_rsvp',
            data: {'id': id},
        }).then(function successCallback(response) {
            $scope.event_rs = response.data;

        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
        
    };
    
    $scope.updateevent = function (event) {
        var startdate=event.event_date;
        var enddate=event.end_date;
        var shours = Number(event.start_time.match(/^(\d+)/)[1]);
        var sminutes = Number(event.start_time.match(/:(\d+)/)[1]);
        var sAMPM = event.start_time.match(/\s(.*)$/)[1].toLowerCase();
        
        var ehours = Number(event.end_time.match(/^(\d+)/)[1]);
        var eminutes = Number(event.end_time.match(/:(\d+)/)[1]);
        var eAMPM = event.end_time.match(/\s(.*)$/)[1].toLowerCase();
        //alert(sAMPM);
        if(sAMPM=='pm' && eAMPM=='am'){
            SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
            return false;
        }
        if (enddate < startdate) {
            SweetAlert.swal({title: "Error", text: 'Enter Valid End Date', type: "error", html: true});
            return false;
        }
        
        if((sAMPM=='pm' && eAMPM=='pm')){
            if(shours>ehours && shours!=12){
               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }else if(shours==ehours && sminutes>eminutes){
               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }else if(shours==ehours && sminutes==eminutes){
               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }
        }
        if((sAMPM=='am' && eAMPM=='am')){
            if(shours>ehours && shours!=12){
               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }else if(shours==ehours && sminutes>eminutes){
               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }else if(shours==ehours && sminutes==eminutes){
               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }
        }
        
//        if(event.start_time>event.end_time){
//            SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
//            return false;
//        }
        if (event.max_member){
            if (String(event.max_member).match(/[a-z]/i)) {
                SweetAlert.swal({title: "Error", text: 'Enter a valid Max Member', type: "error", html: true});
                return false;
             }
        }
        if (event.cost){
            if (event.cost.match(/[a-z]/i)) {
                SweetAlert.swal({title: "Error", text: 'Enter a valid Cost', type: "error", html: true});
                return false;
             }
        }
        if(event.cost && !$scope.profile.paypal_client_id){
            SweetAlert.swal({title: "Error", text: 'Please Provide your Paypal Client ID in profile page', type: "error", html: true});
            return false;
        }
        
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/event_update',
            data: event,
        }).then(function successCallback(response) {
            SweetAlert.swal({ title: "Success", text:'Event Updated', type: "success"});
            //$location.path('/registered/dashboard');
            window.location.href='#/registered/dashboard/'+response.data;
        }, function errorCallback(response) {
            SweetAlert.swal({ title: "Error", text: displayError(response) , type: "error", html:true});
        });
        
    };
    
    $scope.user_event_status = function (status,id){
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/user_event_status',
            data: {'id':id,'status':status,'event_id':$scope.event_id},
        }).then(function successCallback(response) {
            SweetAlert.swal({ title: "Success", text:response.data, type: "success"});
            $('#'+id).hide();
            //$location.path('/registered/dashboard');  
        }, function errorCallback(response) {
            SweetAlert.swal({ title: "Error", text: displayError(response) , type: "error", html:true});
        });
    }
    
    function displayError(param) {
        var str = '';
        angular.forEach(param.data.error, function (value, key) {
            angular.forEach(value, function (v, k) {
                str += '<span style="color:red;">'+ v +'</span><br>';
            })
        });
        return str;
    }
    
    $scope.export = function(selection,export_type){
        var recordType = 1;
        _groupService.withHttpConfig({responseType: 'blob'}).customGET('export/file',{record_type:recordType,export_type:export_type,'selection[]':selection}).then(function(response) {
            var url = (window.URL || window.webkitURL).createObjectURL(response);
            var anchor = document.createElement("a");
            document.body.appendChild(anchor);//required in FF, optional for Chrome
            anchor.download = "exportfile."+export_type;
            anchor.href = url;
            anchor.click();
        })
    };
    
    /*
     * Dropzone file uploader initial
     */
    $scope.dropzoneConfig = {
        options: {// passed into the Dropzone constructor
            url: 'laravel-backend/public/api/uploadimage',
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: .5, // MB
            acceptedFiles: 'image/jpeg,image/png,image/gif',
            maxFiles: 1,
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            addRemoveLinks: true,
            dictDefaultMessage: '<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
            dictResponseError: 'Error while uploading file!',
        },
        'eventHandlers': {
            'removedfile': function (file, response) {
                $http({
                    method: "POST",
                    url: "laravel-backend/public/api/deleteimage/" + $scope.event.avatar_url
                }).then(function mySucces(response) {
                    $scope.deleteMessage = response.data;
                    $scope.event.avatar_url = '';
                });
            },
            'success': function (file, response) {
                $scope.event.avatar_url = response.filename;
            }
        }
    };
    
});