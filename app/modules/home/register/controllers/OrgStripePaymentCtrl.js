'use strict';

var app = angular.module('ng-laravel', ['oitozero.ngSweetAlert', 'dropzone']);
app.controller('OrgStripePaymentCtrl', function ($scope, $stateParams, $auth, $state, $window,Restangular, $rootScope, $http, $translate, SweetAlert) {
    /*
     * Define initial value
     */
    /* show loading on page change */


    $window.scrollTo(0, 0);
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        if (toState.resolve) {
            $scope.loader = true;
        }
    });
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        if (toState.resolve) {
            $scope.loader = false;
        }
    });

    $scope.membership = function () {

        $http({
            method: 'GET',
            url: 'laravel-backend/public/api/membership',
        }).then(function successCallback(response) {
            $scope.membership = response.data;
        }, function errorCallback(response) {
            SweetAlert.swal({title: "Error", text: "No Membership found", type: "error", html: true});
        });
    };

    $scope.membership();

    if ($auth.isAuthenticated().$$state.value == true) {
        $scope.profile = $auth.getProfile().$$state.value;
    }

    $scope.users = {tempGroupID: $stateParams.id};
    $scope.form = {
        submit: function (form) {

            if (!$scope.users.pay) {
                SweetAlert.swal("", 'Please select membership plan', "error");
                $scope.loader = false;
                $scope.isDisabled = false;
                return false;
            }
            var firstError = null;
            if (form.$invalid) {

                var field = null, firstError = null;
                for (field in form) {
                    if (field[0] != '$') {
                        if (firstError === null && !form[field].$valid) {
                            firstError = form[field].$name;
                        }

                        if (form[field].$pristine) {
                            form[field].$dirty = true;
                        }
                    }
                }

                angular.element('.ng-invalid[name=' + firstError + ']').focus();
                SweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
                return;

            } else {

                var ndata = {number: $scope.users.number,name : $scope.users.card_name, exp_month: $scope.users.exp_month, exp_year: $scope.users.exp_year, cvc: $scope.users.cvc};
                $scope.loader = true;
                $('button').attr('disabled', true);
                Stripe.card.createToken(ndata, stripeResponseHandler);

            }

        }
    };



    function stripeResponseHandler(status, response) {


        //console.log(response);

        if (response.error) { // Problem!

            SweetAlert.swal(response.error.code, response.error.message, "error");
            $scope.loader = false;
            $('button').attr('disabled', false);
            return false;

        } else { // Token was created!

            $scope.users.stripeToken = response.id;
            //alert($scope.users.stripeToken);
            if ($scope.profile) {
                if (window.location.href.indexOf("g_id:") > -1) {
                    var g_id = $stateParams.id;
                    var res = g_id.replace("g_id:", "");
                    //alert(res);

                    Restangular.all('regorgStripPayment').customPOST($scope.users).then(function (data) {
                        SweetAlert.swal({title: "Success", text: "Payment Accepted", type: "success"});
                        $state.go('registered.dashboard');
                    }, function (response) {

                        $scope.loader = false;
                        $('button').attr('disabled', false);

                        SweetAlert.swal({title: "Error", text:response.data.error, type: "error", html: true});
                        $scope.isDisabled = false;
                    });
                    return true;

                } else {

                    Restangular.all('orgStripPayment').customPOST($scope.users).then(function (data) {
                        SweetAlert.swal({title: "Success", text: "Payment Accepted", type: "success"});
                        $state.go('registered.dashboard');
                    }, function (response) {

                        $scope.loader = false;
                        $('button').attr('disabled', false);

                        SweetAlert.swal({title: "Error", text:response.data.error, type: "error", html: true});
                        $scope.isDisabled = false;
                    });
                    return true;

                }


            } else {
                $scope.users.gid = $stateParams.gid;
                $scope.users.email = $stateParams.id;
                $http({
                    method: 'POST',
                    url: 'laravel-backend/public/api/groups/orgStripPaymentfind',
                    data: {'users': $scope.users},
                }).then(function successCallback(response) {
                    //$scope.groups = response.data;
                    SweetAlert.swal({title: "Success", text: "Payment Accepted", type: "success"});
                    $('#loader').hide();
                    //$scope.loader =false;
                    window.location.href = '#/home/login';
                    //$state.go('home.login');
                }, function errorCallback(response) {
                        $scope.loader = false;
                        $('button').attr('disabled', false);

                        SweetAlert.swal({title: "Error", text:response.data.error, type: "error", html: true});
                        $scope.isDisabled = false;
                });



                //Restangular.all('orgStripPaymentfind').customPOST($scope.details).then(function(data) {
//                SweetAlert.swal({ title: "Success", text: data.message, type: "success"});
//                //$state.go('registered.dashboard'); 
//            }, function(response) {
//                    
//                    $scope.loader =false;
//                 $('button').attr('disabled',false);
//
//                SweetAlert.swal({ title: "Error", text: displayError(response) , type: "error", html:true});
//                $scope.isDisabled = false;
//            });
                return true;
            }
//            Restangular.all('orgStripPayment').customPOST($scope.users).then(function(data) {
//                SweetAlert.swal({ title: "Success", text: data.message, type: "success"});
//              $state.go('registered.dashboard'); 
//            }, function(response) {
//                    
//                    $scope.loader =false;
//                 $('button').attr('disabled',false);
//
//                SweetAlert.swal({ title: "Error", text: displayError(response) , type: "error", html:true});
//                $scope.isDisabled = false;
//            });

            return true;
        }
    }
    ;


/**
     * Foreach Function for show validation error
     */
    function displayError(param) {
        var str = '';
        angular.forEach(param.data.error, function (value, key) {
            angular.forEach(value, function (v, k) {
                str += v + '<br>';
            })
        });
        return str;
    }

});