'use strict';

var app = angular.module('ng-laravel', ['dropzone', 'xeditable', 'ui.bootstrap', 'checklist-model', 'bootstrap-image-gallery', 'oitozero.ngSweetAlert']);
app.directive('jqdatepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            element.datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function (date) {
                    scope.event.event_date = date;
                    alert(date);
                    scope.$apply();
                }
            });

            element.css('position', 'relative');
            element.css('z-index', 9999);
        }
    };
});
app.controller('CreateEventsCtrl', function ($scope, $auth, $state, $window,resolvedItems, $location, Restangular, $rootScope, $http, $translate, SweetAlert) {
    
    
    if($auth.isAuthenticated().$$state.value==true){
        $scope.profile = $auth.getProfile().$$state.value;
    }
    
    var _groupService = Restangular.all('group');
    $window.scrollTo(0, 0);
    var date = new Date();
    var aser = date.setDate(date.getDate() + 1);
// add a day
    
    $('.datepicker').datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true,
        startDate: new Date(aser)
    });
   
    $scope.categories = resolvedItems;
 
    $scope.group_id = $state.params.id;
    $scope.event = {};  
    $scope.event.start_time = '1:00 PM';
    $scope.event.end_time = '1:00 PM';
//    $scope.$watch(event.event_date, function (temp_date) {
//        ///if(typeof temp_date !='undefined'){
//            $scope.event.temp=temp_date;
//            console.log($scope.event.temp);
//        //}
//        
//    });
    
    $scope.temp_date = function (){
        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        $scope.temp = new Date($scope.event.event_date);
        var weekday = $scope.temp.getDay();
        $scope.week_day = days[weekday];
        $('.datepicker1').datepicker('remove');
        //alert($scope.week_day);
        //$scope. = new Date($scope.event.event_date);
        
        $('.datepicker1').datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true,
            startDate: $scope.temp,
            
        });
        console.log($scope.temp);
    }
    
    $scope.createevent = function (event,status) {
        var startdate=event.event_date;
        var enddate=event.end_date;
        var shours = Number(event.start_time.match(/^(\d+)/)[1]);
        var sminutes = Number(event.start_time.match(/:(\d+)/)[1]);
        var sAMPM = event.start_time.match(/\s(.*)$/)[1].toLowerCase();
        
        var ehours = Number(event.end_time.match(/^(\d+)/)[1]);
        var eminutes = Number(event.end_time.match(/:(\d+)/)[1]);
        var eAMPM = event.end_time.match(/\s(.*)$/)[1].toLowerCase();
        //alert(sAMPM);
        if(sAMPM=='pm' && eAMPM=='am'){
            SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
            return false;
        }
        if(enddate<startdate){
            SweetAlert.swal({title: "Error", text: 'Enter Valid End Date', type: "error", html: true});
            return false;
        }
        
        if((sAMPM=='pm' && eAMPM=='pm')){
            if(shours>ehours && shours!=12){
               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }else if(shours==ehours && sminutes>eminutes){
               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }else if(shours==ehours && sminutes==eminutes){
               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }
        }
        if((sAMPM=='am' && eAMPM=='am')){
            if(shours>ehours && shours!=12){
               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }else if(shours==ehours && sminutes>eminutes){
               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }else if(shours==ehours && sminutes==eminutes){
               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }
        }
//        if(Date.parse(event.start_time)>Date.parse(event.end_time)){
//            SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
//            return false;
//        }
        if(event.max_member){
            if (event.max_member.match(/[a-z]/i)) {
                SweetAlert.swal({title: "Error", text: 'Enter a valid Max Member', type: "error", html: true});
                return false;
             }
        }
        if (event.cost){
            if (event.cost.match(/[a-z]/i)) {
                SweetAlert.swal({title: "Error", text: 'Enter a valid Cost', type: "error", html: true});
                return false;
             }
        }
        
        if(event.recurring_week && !event.recur__week_number){
            SweetAlert.swal({title: "Error", text: 'Enter a valid Week Number', type: "error", html: true});
            return false;
        }
        if(event.recurring_month && !event.recur__mon_number){
            SweetAlert.swal({title: "Error", text: 'Enter a valid Month Number', type: "error", html: true});
            return false;
        }
        if(startdate!= null){var temp_date = event.event_date.split('/');}
        
        if (event.recur__mon_number){
            if (event.recur__mon_number.match(/[a-z]/i)) {
                SweetAlert.swal({title: "Error", text: 'Enter a valid Month Number', type: "error", html: true});
                return false;
             }
             if(temp_date[1]==30 || temp_date[1]==31){
                SweetAlert.swal({title: "Error", text: 'Enter a Valid Month Number', type: "error", html: true});
                return false;
            }
        }
        if (event.recur__week_number){
            if (event.recur__week_number.match(/[a-z]/i)) {
                SweetAlert.swal({title: "Error", text: 'Enter a Valid Week Number', type: "error", html: true});
                return false;
             }
             if(temp_date[1]==30 || temp_date[1]==31){
                SweetAlert.swal({title: "Error", text: 'Enter a Valid Week Number', type: "error", html: true});
                return false;
            }
        }
        if (event.recur__mon_number){
            if (event.recur__mon_number>99){
             SweetAlert.swal({title: "Error", text: 'Max 99 Month allowed', type: "error", html: true});
             return false;
             }
        }
        if (event.recur__week_number){
            if (event.recur__week_number>99){
                SweetAlert.swal({title: "Error", text: 'Max 99 Week allowed', type: "error", html: true});
                return false;
            }
        }
        
//        if(event.cost && !$scope.profile.paypal_client_id){
//            SweetAlert.swal({title: "Error", text: 'Please Provide your Paypal Client ID in profile page', type: "error", html: true});
//            return false;
//        }
        
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/event_create',
            data: {'group_id': $scope.group_id, 'name': event.name,'max_member':event.max_member,'category_name':event.category_name ,'description': event.description, 'status':status,'recurring': event.recurring, 'event_date': event.event_date,'start_time': event.start_time,'end_time': event.end_time,'location': event.location,'address': event.address,'avatar_url':event.avatar_url,'cost':event.cost,'recurring_payment':event.recurring_payment,'recurring_week':event.recurring_week,'recurring_month':event.recurring_month,'recur__mon_number':event.recur__mon_number,'recur__week_number':event.recur__week_number,'end_date':event.end_date},
        }).then(function successCallback(response) {
            SweetAlert.swal({title: "Success", text: 'Event Created', type: "success"});
            $location.path('/registered/dashboard');
        }, function errorCallback(response) {
            //console.log(response);
            SweetAlert.swal({title: "Error", text: displayError(response), type: "error", html: true});
        });

    };

    function displayError(param) {
        var str = '';
        angular.forEach(param.data.error, function (value, key) {
            angular.forEach(value, function (v, k) {
                str += '<span style="color:red;">' + v + '</span><br>';
            })
        });
        return str;
    }
    
    /*
     * Dropzone file uploader initial
     */
    $scope.event.avatar_url = '';
    $scope.dropzoneConfig = {
        options: {// passed into the Dropzone constructor
            url: 'laravel-backend/public/api/uploadimage',
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: .5, // MB
            acceptedFiles: 'image/jpeg,image/png,image/gif',
            maxFiles: 1,
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            addRemoveLinks: true,
            dictDefaultMessage: '<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
            dictResponseError: 'Error while uploading file!',
        },
        'eventHandlers': {
            'removedfile': function (file, response) {
                $http({
                    method: "POST",
                    url: "laravel-backend/public/api/deleteimage/" + $scope.event.avatar_url
                }).then(function mySucces(response) {
                    $scope.deleteMessage = response.data;
                    $scope.event.avatar_url = '';
                });
            },
            'success': function (file, response) {
                $scope.event.avatar_url = response.filename;
            }
        }
    };


});
