'use strict';

var app = angular.module('ng-laravel', ['oitozero.ngSweetAlert', 'dropzone']);
app.controller('EditProfilesCtrl', function ($scope, $auth, $state, Restangular, $rootScope, $http, $window, $translate, SweetAlert) {
    /*
     * Define initial value
     */
    $window.scrollTo(0, 0);
    /* show loading on page change */
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        if (toState.resolve) {
            $scope.loader = true;
        }
    });
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        if (toState.resolve) {
            $scope.loader = false;
        }
    });

    if ($auth.isAuthenticated().$$state.value == true) {
        $scope.profile = $auth.getProfile().$$state.value;
        console.log($scope.profile);
    }

    if ($scope.profile) {
        if ($scope.profile.firstLogin == 0) {
            $scope.firstTime = 1;
        }

        Restangular.all('organizer_mail').customPOST($scope.profile.id).then(function (obj) {

            $scope.setOrgEmail = obj.data;

        });

    }
    $scope.statuses = ["Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czeck Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia, The", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcaim Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romainia", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Spratly Islands", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "USA", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"];



    $scope.updateState = function () {

        var indexCountry = $scope.statuses.indexOf($scope.profile.country);

        $scope.state = BlockID(indexCountry + 1).split("|");

        if ($scope.state.length >= 1) {
            $scope.state = $scope.state;
            $scope.profile.state = $scope.state['0'];
        }
        if (indexCountry == 0) {
            $scope.state = new Array("");
        }
        //$scope.state = ['Afghanistan','Afghanistans','Afghanistanasd'];
    }
    $scope.$watch('statuses', function () {

        var indexCountry = $scope.statuses.indexOf($scope.profile.country);

        $scope.state = BlockID(indexCountry + 1).split("|");

        if ($scope.state.length >= 1) {
            $scope.state = $scope.state;
        }
        if (indexCountry == 0) {
            $scope.state = new Array("");
        }

    });


    $scope.update_email = function (email,id) {
        
        if (!email) {
            SweetAlert.swal({title: "Error", text: 'Enter Valid Email', type: "error", html: true});
            return false;
        }
        
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/users/update_email',
            data: {'email': email,'id':id}
        }).then(function successCallback(response) {
            SweetAlert.swal({title: "Success", text: 'An verification mail sent to mail ID, please confirm', type: "success"});
            $('#updateemail').modal('hide');
            $state.go('home.login');
        }, function errorCallback(response) {
            SweetAlert.swal({title: "Error", text: displayError(response) , type: "error"});
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });


    };

    $scope.form = {
        submit: function (form) {
            var firstError = null;
            if (form.$invalid) {

                var field = null, firstError = null;
                for (field in form) {
                    if (field[0] != '$') {
                        if (firstError === null && !form[field].$valid) {
                            firstError = form[field].$name;
                        }

                        if (form[field].$pristine) {
                            form[field].$dirty = true;
                        }
                    }
                }

                angular.element('.ng-invalid[name=' + firstError + ']').focus();
                return;

            } else {
                $scope.isDisabled = true;

                Restangular.all('editprofile').customPOST($scope.profile).then(function (data) {
                    $scope.isDisabled = false;
                    $scope.profile.avatar_url = $scope.profile.avatar_url + '?date=' + Math.random();
                    SweetAlert.swal({title: "Success", text: data.message, type: "success"});

                }, function (response) {
                    SweetAlert.swal({title: "Error", text: displayError(response), type: "error", html: true});
                    $scope.isDisabled = false;
                });
            }

        }
    };


    /*
     * Dropzone file uploader initial
     */
    $scope.dropzoneConfig = {
        options: {// passed into the Dropzone constructor
            url: 'laravel-backend/public/api/uploadimage',
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: .5, // MB
            acceptedFiles: 'image/jpeg,image/png,image/gif',
            maxFiles: 1,
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            addRemoveLinks: true,
            dictDefaultMessage: '<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
            dictResponseError: 'Error while uploading file!',
        },
        'eventHandlers': {
            'removedfile': function (file, response) {
                $http({
                    method: "POST",
                    url: "laravel-backend/public/api/deleteimage/" + $scope.profile.avatar_url
                }).then(function mySucces(response) {
                    $scope.deleteMessage = response.data;
                    $scope.profile.avatar_url = '';
                });
            },
            'success': function (file, response) {
                $scope.profile.avatar_url = response.filename;
            }
        }
    };


    /* Get user profile info */

    function displayError(param) {
        var str = '';
        angular.forEach(param.data.error, function (value, key) {
            angular.forEach(value, function (v, k) {
                str += v +'\n';
            })
        });
        return str;
    }

    $scope.updateStatus = function (status) {

        Restangular.all('user/status').customPOST({'id': $scope.profile.id, 'status': 0}).then(function (data) {

            SweetAlert.swal({title: "Success", text: 'You are deactivated now', type: "success"});
            $auth.signout();
            $state.go('home.index');
        }, function (response) {
            SweetAlert.swal({title: "Error", text: displayError(response), type: "error", html: true});
            $scope.isDisabled = false;
        });
    }

    $scope.communicate_mail = function () {

        if ($scope.users) {
            if (!$scope.users.subject) {
                SweetAlert.swal({title: "Error", text: 'subject is empty', type: "error", html: true});
                exit;
            }
        }
        Restangular.all('user_mail/send').customPOST($scope.users).then(function (data) {
            SweetAlert.swal({title: "Success", text: data.message, type: "success"});

        }, function (response) {
            SweetAlert.swal({title: "Error", text: displayError(response), type: "error", html: true});
            $scope.isDisabled = false;
        });
    }

    $scope.passwords = {
        submit: function (form) {
            var firstError = null;
            if (form.$invalid) {

                var field = null, firstError = null;
                for (field in form) {
                    if (field[0] != '$') {
                        if (firstError === null && !form[field].$valid) {
                            firstError = form[field].$name;
                        }

                        if (form[field].$pristine) {
                            form[field].$dirty = true;
                        }
                    }
                }

                angular.element('.ng-invalid[name=' + firstError + ']').focus();
                return;

            } else {
                //console.log($scope.users);
                $scope.users.id = $scope.profile.id;
                Restangular.all('changePassword').customPOST($scope.users).then(function (data) {
                    SweetAlert.swal({title: "Success", text: data.message, type: "success"});

                }, function (response) {
                    SweetAlert.swal({title: "Error", text: displayError(response), type: "error", html: true});
                    $scope.isDisabled = false;
                });
            }

        }
    };


}).directive('compareTo', function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function (scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function (modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function () {
                ngModel.$validate();
            });
        }
    };
});