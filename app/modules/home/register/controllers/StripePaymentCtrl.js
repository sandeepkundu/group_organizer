'use strict';

var app = angular.module('ng-laravel',['oitozero.ngSweetAlert','dropzone']);
app.controller('StripePaymentCtrl',function($scope,$stateParams,$auth,$state,$window,Restangular,$rootScope,$http,$translate,SweetAlert){
    /*
     * Define initial value
     */
     $window.scrollTo(0, 0);
    /* show loading on page change */
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if (toState.resolve) {
            $scope.loader = true;
        }
    });
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        if (toState.resolve) {
            $scope.loader = false;
        }
    });

    $scope.total_price='$100';

    if($auth.isAuthenticated().$$state.value==true){
    $scope.profile = $auth.getProfile().$$state.value;
    }
    $scope.users={GroupID:$stateParams.id};
$scope.form = {

        submit: function (form) {
            var firstError = null;
            if (form.$invalid) {

                var field = null, firstError = null;
                for (field in form) {
                    if (field[0] != '$') {
                        if (firstError === null && !form[field].$valid) {
                            firstError = form[field].$name;
                        }

                        if (form[field].$pristine) {
                            form[field].$dirty = true;
                        }
                    }
                }

                angular.element('.ng-invalid[name=' + firstError + ']').focus();
                SweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
                return;

            } else {
                
                var ndata={number:$scope.users.number,name : $scope.users.card_name,exp_month:$scope.users.exp_month,exp_year:$scope.users.exp_year,cvc:$scope.users.cvc};
                 $scope.loader = true;
                 $('button').attr('disabled',true);
                Stripe.card.createToken(ndata, stripeResponseHandler);

            }

        }
    };



 function stripeResponseHandler(status, response) {
   

         //console.log(response);
        
          if (response.error) { // Problem!

            SweetAlert.swal(response.error.code, response.error.message, "error");
                    $scope.loader =false;
                 $('button').attr('disabled',false);
            return false;

          } else { // Token was created!

            $scope.users.stripeToken = response.id;

            Restangular.all('strip-payment').customPOST($scope.users).then(function(data) {
                SweetAlert.swal({ title: "Success", text: data.message, type: "success"});
              $state.go('registered.dashboard'); 
            }, function(response) {
                    
                    $scope.loader =false;
                 $('button').attr('disabled',false);

                SweetAlert.swal({ title: "Error", text: displayError(response) , type: "error", html:true});
                $scope.isDisabled = false;
            });

            return true;
          }
        };
    



});