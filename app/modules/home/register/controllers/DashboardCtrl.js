
'use strict';

var app = angular.module('ng-laravel', ['oitozero.ngSweetAlert', 'dropzone']);

app.directive("datepicker", function () {
  return {
    restrict: "A",
    require: "ngModel",
    inline:true,
    link: function (scope, elem, attrs, ngModelCtrl) {
      var updateModel = function (dateText) {
        scope.$apply(function () {
          ngModelCtrl.$setViewValue(dateText);
        });
      };
      var options = {
        dateFormat: "yy/mm/dd",
        onSelect: function (dateText) {
          updateModel(dateText);
        }
      };
      elem.datepicker(options);
    }
  }
});

app.controller('DashboardCtrl', function ($scope, $stateParams, $auth,resolvedItems,$location, $state,$window, Restangular, $rootScope, $http, $translate, SweetAlert) {
    //$('#loader_mail').hide();
    
    
    $scope.mail={};
    $scope.mail.sub='';
    $scope.mail.body='';
    $scope.mail.email_id='';
    $scope.card={};
    $scope.card.number='';
    $scope.card.exp_month='';
    $scope.card.exp_year='';
    $scope.card.cvc='';
    $window.scrollTo(0, 0);
    $scope.date = new Date();
    var url =$location.$$absUrl;
    var n = url.search("stripeToken");
    //var n = a;
    $scope.update_account = function(card){
        if(card.number.length==0 || card.number.length!=16){
            SweetAlert.swal({ title: "Error", text:'Please Enter Valid Card Number', type: "error"});
            $scope.card.number='';
            return false;
        }
        if(card.exp_month.length==0 || card.exp_month=='MM'){
            SweetAlert.swal({ title: "Error", text:'Please Enter Valid Month', type: "error"});
            $scope.card.exp_month='';
            return false;
        }
        if(card.exp_year.length==0 || card.exp_year=='YY'){
            SweetAlert.swal({ title: "Error", text:'Please Enter Valid Year', type: "error"});
            $scope.card.exp_year='';
            return false;
        }
        if(card.cvc.length==0){
            SweetAlert.swal({ title: "Error", text:'Please Enter valid CVC', type: "error"});
            $scope.card.cvc='';
            return false;
        }
        $('#update_card_li').prop('disabled', true);
        $('#update_card_li').attr('disabled', 'disabled');
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/update_stripe',
            data: {'card':card,'group_id':$stateParams.id},
        }).then(function successCallback(response) {
            $('#update_card_detail').modal('hide');
            SweetAlert.swal({ title: "Success", text:'Card Updated', type: "success"}); 
            
            //$scope.event_response = $scope.event.event_response
        }, function errorCallback(response) {
           SweetAlert.swal({ title: "Error", text:'Error Occur While Updating Your Card', type: "error"}); 
            // or server returns response with an error status.
        });
    };
    //console.log($stateParams);
    var _groupService = Restangular.all('group');
    
    if($auth.isAuthenticated().$$state.value==true){
        $scope.profile = $auth.getProfile().$$state.value;
    }
    
    $scope.event_record = 0;
    $scope.grecords = 0;
    $scope.single_record = 0;
    
    $scope.id = $scope.profile.id;
    $scope.user_email = $scope.profile.email;
    
    if($scope.profile.user_type_id==3){
        
    }
    $scope.categories = resolvedItems;
    $('#loader').hide();
    
    var base_url =  window.location.origin;
    var grp_id = $stateParams.id;
    if(base_url=='http://localhost'){
        $scope.iframe_url= '<iframe width="560" height="315" src="http://localhost/bevylife/#/calendar/'+grp_id+'" frameborder="0" allowfullscreen></iframe>';
    }
    if(base_url=='http://dev.bevylife.com'){
        $scope.iframe_url= '<iframe width="560" height="315" src="http://dev.bevylife.com/#/calendar/'+grp_id+'" frameborder="0" allowfullscreen></iframe>';
    }
    if(base_url=='http://www.bevylife.com'){
        $scope.iframe_url= '<iframe width="560" height="315" src="http://www.bevylife.com/#/calendar/'+grp_id+' frameborder="0" allowfullscreen></iframe>';
    }
    
    $scope.copy = function(){
        document.querySelector("#text_iframe").select();
        document.execCommand('copy');
        $('#succ_msg').html('<span style="color:green;">Copied to clipboard</span>');
    };
    
    $scope.get_events = function (id) {
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/get_event',
            data: {'id': id},
        }).then(function successCallback(response) {
            $scope.event = response.data;
            
            //$scope.event_response = $scope.event.event_response
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    $scope.reload = function(){
        //alert('In');
        //location.reload();
    }
    
//    For inactive the group

    $scope.status_inactive = function(id,status){
        
        swal({
            title: "Are you sure?",
            text: "You will not be able to access this Group!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#CAE197",
            confirmButtonText: "Yes, Do it!",
            closeOnConfirm: true,
            html: false
        }, function () {
                //$('#loader').show();
                $('#inactive_group').hide();
                $('#status_loader').html('<img  src="assets/img/preloader/material.gif" alt="preloader">');
                $('#status_loader').show();
                $http({
                    method: 'POST',
                    url: 'laravel-backend/public/api/group/status',
                    data: {'id': id,'status':status},
                }).then(function successCallback(response) {
                    $('#status_loader').hide();
                    SweetAlert.swal({ title: "Success", text:'Group Inactivated', type: "success"}); 
                    $location.path('/registered/dashboard');

                }, function errorCallback(response) {
                    SweetAlert.swal({ title: "Error", text:"Group Can't Inactivated", type: "error"}); 
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
            });      
        
    };
    
    $scope.duplicate_event = function(id){
            
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/duplicate_event',
            data: {'event_id': id},
        }).then(function successCallback(response) {
            //SweetAlert.swal({ title: "Success", text:'Event Duplicated', type: "success", 
            $scope.new_event_id = response.data.event_id; 
            
            swal({
                title: "Success",
                text: "Event Duplicated",
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#CAE197",
                confirmButtonText: "Ok",
                closeOnConfirm: true,
                html: false
            }, function () {
                    $state.go('registered.groupevent', {id: $scope.new_event_id});
            });
            
        }, function errorCallback(response) {
            SweetAlert.swal({ title: "Error", text:'Error Occur While Duplicating', type: "error"});
            
        });
   
    };
    
    $scope.set = function(id){
        alert(id)
        $state.go('registered.groupevent', {id: id});
        //$location.path('/registered/event/'+id+'/edit');
    }
    
    $scope.send_mail_member=function(mail,id){
        
        if(mail.sub.length==0){
            SweetAlert.swal({ title: "Error", text:'Please Enter Subject', type: "error"});
            return false;
        }
        if(mail.body.length==0){
            SweetAlert.swal({ title: "Error", text:'Please Enter Message', type: "error"});
            return false;
        }
        if(mail.email_id.length==0){
            SweetAlert.swal({ title: "Error", text:'Please Enter Email ID', type: "error"});
            return false;
        }
        $('#button_mail').prop('disabled', true);
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/send_mail_member',
            data: {'subject': mail.sub,'message':mail.body,'id':id,'email_id':mail.email_id},
        }).then(function successCallback(response) {
            $scope.mail.sub='';
            $scope.mail.body='';
            $scope.mail.email_id='';
            swal({
                title: "Success",
                //text: "You will not be able to recover this Image!",
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#5bc0de",
                confirmButtonText: "OK",
                closeOnConfirm: true,
                html: true
                  }, function(){
                      $('#mailmodal_'+id).modal('toggle');
                      
            });    
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
       
        
    }
    
    $scope.change_status = function(id,status){
         swal({
          title: "Are you sure?",
          //text: "You will not be able to recover this Image!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#CAE197",
          confirmButtonText: "Yes, Sure!",
          closeOnConfirm: false,
          html: true
            }, function(){
            $http({
                method: 'POST',
                url: 'laravel-backend/public/api/group/event_change_status',
                data: {'id': id,'status':status},
            }).then(function successCallback(response) {
                swal({
                    title: "Success",
                    //text: "You will not be able to recover this Image!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#CAE197",
                    confirmButtonText: "Ok",
                    closeOnConfirm: true,
                    html: false
                      }, function(){
                      $scope.get_events($scope.group_id.id);
                });  
            }, function errorCallback(response) {
                SweetAlert.swal({ title: "Success", text:'Deleted', type: "success"}); 
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
         });   
    }

    
    
    $scope.event_rsvp = function (id) {
        
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/event_rsvp',
            data: {'id': id},
        }).then(function successCallback(response) {
            $scope.event_rs = response.data;

        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
        
    };
    
    
    $scope.user_event_status = function (status,id,event_id){
       // alert(event_id);
        
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/user_event_status',
            data: {'id':id,'status':status,'event_id':event_id},
        }).then(function successCallback(response) {
            //SweetAlert.swal({ title: "Success", text:response.data.msg, type: "success"});
            //$scope.event.user_rsvp=response.data.data;
            $('#'+id+'_'+event_id).hide();
            $('#'+id+'__'+event_id).hide();
            if(status==1){
                $('#'+id).html('<span class="label label-table label-success"><b>Accepted</b></span>');
            }else{
                $('#'+id).html('<span class="label label-table label-danger"><b>Rejected</b></span>');
            }
            
        }, function errorCallback(response) {
            SweetAlert.swal({ title: "Error", text: displayError(response) , type: "error", html:true});
        });
    }
    
    $scope.export = function(selection,export_type){
        var recordType = 1;
        _groupService.withHttpConfig({responseType: 'blob'}).customGET('export/file',{record_type:recordType,export_type:export_type,'selection[]':selection}).then(function(response) {
            var url = (window.URL || window.webkitURL).createObjectURL(response);
            var anchor = document.createElement("a");
            document.body.appendChild(anchor);//required in FF, optional for Chrome
            anchor.download = "exportfile."+export_type;
            anchor.href = url;
            anchor.click();
        })
    };
    
    $scope.get_event = function () {
//        console.log($scope.profile.id);
        $scope.single_record = $scope.single_record + 4;

        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/groups/get_list',
            data: {'id': $scope.id,'records':$scope.single_record},
        }).then(function successCallback(response) {
           $scope.groups = response.data;
           
           $('#loader').hide();

        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    $scope.get_event();
    
    $scope.event_date = function (date) {
//        var date = '2017-03-15';
        $http({
            method: 'GET',
            url: 'laravel-backend/public/api/event_date?date='+date,
           
        }).then(function successCallback(response) {
            $scope.data = response.data;
           
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    
    $scope.member_status = function(status,id) {
       
       $http({
                method: 'POST',
                url: 'laravel-backend/public/api/member/status',
                data: {'id':id,'status':status}
              }).then(function successCallback(response) {
                    SweetAlert.swal({ title: "Success", text:'Member Status Changed', type: "success"}); 
                    location.reload();
                }, function errorCallback(response) {
                  // called asynchronously if an error occurs
                  // or server returns response with an error status.
                });
     };
     
     $scope.reload = function (){
         window.location.href='#/registered/group_dashboard/'+$stateParams.id;
     }
     //$scope.reload();
     $scope.group_id = {};
     $scope.group_id.id = $stateParams.id;
     $scope.group_dash = function () {
         $('#loader').show();
         $scope.members = '';
         $scope.images = '';
         $scope.events = '';
         $scope.pending_groups = '';
         //$scope.edit_group = '';
        //alert($stateParams.id);
         $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/show',
            data: {'id':$scope.group_id},
        }).then(function successCallback(response) {
            $scope.edit_group = response.data;
            $scope.groupname = response.data.name;
            $scope.edit_group_data = response.data;
            $('#loader').hide();
            setTimeout(function(){
                $('#managetab').trigger("click");
        },500)

            $('#groups_details').tab('show');
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    $scope.group_dash();
    
    $scope.get_members = function (id) {
        $('#loader').show();
        //$scope.members = '';
        $scope.images = '';
        $scope.events = '';
        $scope.pending_groups = '';
        $scope.edit_group = '';
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/members',
            data: {'id': id},
        }).then(function successCallback(response) {
            $scope.members = response.data;
            $('#loader').hide();
             setTimeout(function(){
                $('#memberstab').trigger("click");
        },500)
//            console.log($scope.members);
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    
    $scope.get_images = function (id) {
        $('#loader').show();
        $scope.members = '';
        //$scope.images = '';
        $scope.events = '';
        $scope.pending_groups = '';
        $scope.edit_group = '';
        $scope.group_id = id;
        //console.log($scope.group_id);
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/get_images',
            data: {'id': id},
        }).then(function successCallback(response) {
            $scope.images = response.data;
            $('#loader').hide();
            setTimeout(function(){
                $('#imgstab').trigger("click");
        },500)
//            console.log($scope.members);
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    $scope.grp_id={};
    
    $scope.get_events = function (group_id) {
        $('#loader').show();
        $scope.event_record = $scope.event_record + 4;
        $scope.members = '';
        $scope.images = '';
        //$scope.events = '';
        $scope.pending_groups = '';
        $scope.edit_group = '';
        //console.log(id);
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/events',
            data: {'id': group_id,'records':$scope.event_record},
        }).then(function successCallback(response) {
            $scope.events = response.data.events;
            $scope.grp_id.id = response.data.group_id;
            $scope.e_count = response.data.count;
            if($scope.events==0){
                //$scope.events=;
            }
            //alert($scope.events.length);
            $('#loader').hide();
            setTimeout(function(){
                $('#eventstab').trigger("click");
        },500)
            
//            console.log($scope.groups_list);
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });

    };
    
    $scope.pending_members = function (group_id) {
        $('#loader').show();
        $scope.members = '';
        $scope.images = '';
        $scope.events = '';
        //$scope.pending_groups = '';
        $scope.edit_group = '';
        //console.log(id);
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/pending_member',
            data: {'id': group_id,'user_id': $scope.id},
        }).then(function successCallback(response) {
            $scope.pending_groups = response.data;
            $('#loader').hide();
             setTimeout(function(){
                $('#joiningtab').trigger("click");
        },500);
//            console.log($scope.groups_list);
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    $scope.event_con = function (status,event_id) {
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/event/con',
           data: {'status':status,'event_id':event_id },
        }).then(function successCallback(response) {
            SweetAlert.swal({ title: "Success", text:'Conformation Accepted', type: "success"}); 
            location.reload();
           
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    $scope.group_pay = function (group_id) {
        
        $location.path('/registered/orgpayment/g_id:' + group_id.id);

    };
    
    // delete images from group
    $scope.removeimage = function (image) {
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this Image!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#CAE197",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false,
          html: false
            }, function(){
            
           $http({
                method: 'POST',
                url: 'laravel-backend/public/api/grpgroup/deleteimage',
               data: {'image':image},
            }).then(function successCallback(response) {
                SweetAlert.swal({ title: "Success", text:'Image Deleted', type: "success"}); 
                location.reload();

            }, function errorCallback(response) {
                SweetAlert.swal({ title: "Error", text:'Can not delete', type: "warning"}); 
            });

        });
    };
    
    /*
     * Dropzone file uploader for group image
     */
    $scope.dropzoneConfig = {
        options: {// passed into the Dropzone constructor
            url: '../laravel-backend/public/api/uploadimage',
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 1, // MB
            acceptedFiles: 'image/jpeg,image/png,image/gif',
            maxFiles: 1,
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            addRemoveLinks: true,
            dictDefaultMessage: '<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
            dictResponseError: 'Error while uploading file!',
        },
        'eventHandlers': {
            'removedfile': function (file, response) {
                $http({
                    method: "POST",
                    url: "../laravel-backend/public/api/deleteimage/" + $scope.edit_group.avatar_url
                }).then(function mySucces(response) {
                    $scope.deleteMessage = response.data;
                    $scope.edit_group.avatar_url = '';
                });
            },
            'success': function (file, response) {
                $scope.edit_group.avatar_url = response.filename;
            }
        }
    };
    
    /*
     * Dropzone file uploader For layout image
     */
    $scope.dropzoneConfig2 = {
        options: {// passed into the Dropzone constructor
            url: '../laravel-backend/public/api/uploadlayout',
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 1, // MB
            acceptedFiles: 'image/jpeg,image/png,image/gif',
            maxFiles: 1,
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            addRemoveLinks: true,
            dictDefaultMessage: '<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
            dictResponseError: 'Error while uploading file!',
        },
        'eventHandlers': {
            'removedfile': function (file, response) {
                $http({
                    method: "POST",
                    url: "../laravel-backend/public/api/deletelayout/" + $scope.edit_group.layout
                }).then(function mySucces(response) {
                    $scope.deleteMessage = response.data;
                    $scope.edit_group.layout = '';
                });
            },
            'success': function (file, response) {
                $scope.edit_group.layout = response.filename;
            }
        }
    };
    
    $scope.update = function (group) {
        //console.log(group);
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/reggroup/update',
            data: {'id': group.id , 'name':group.name,'avatar_url':group.avatar_url ,'layout':group.layout , 'description':group.description , 'category_name':group.category_name , 'header':group.header},
        }).then(function successCallback(response) {
               SweetAlert.swal({ title: "Success", text:'Group updated', type: "success"}); 
              
        }, function errorCallback(response) {
               SweetAlert.swal({ title: "Error", text: displayError(response) , type: "error", html:true});
        });
    }
    function displayError(param) {
        var str = '';
        angular.forEach(param.data.error, function (value, key) {
            angular.forEach(value, function (v, k) {
                str += v +'<br>';
            })
        });
        return str;
    }
    
    
    //For mutiple images
    $scope.dropzoneOption1 = {
        url: "laravel-backend/public/api/grpgroup/groupimages",
        paramName: "file", // The name that will be used to transfer the file
        sending: function (file, xhr, formData) {
            formData.append("group_id",$scope.group_id);
        },
        acceptedFiles: 'image/jpeg,image/png,image/gif',
        maxFilesize: 50, // MB
        maxFiles: 10,
        maxfilesexceeded: function (file) {
            this.removeAllFiles();
            this.addFile(file);
        },
        addRemoveLinks: true,
        dictDefaultMessage:
                '<span class="bigger-150 bolder"><i class=" fa fa-caret-right red"></i> Drop files</span> to upload \
            <span class="smaller-80 grey">(or click)</span> <br /> \
            <i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
        dictResponseError: 'Error while uploading file!',
        init: function () {

            this.on("removedfile", function (file) {

                $.ajax({
                    type: 'POST',
                    url: 'laravel-backend/public/api/grpgroup/delgro/' + $scope.currImgID,
                    //data: {name:$scope.currImgID},
                    dataType: 'html',
                    success: function (data) {
                        var rep = JSON.parse(data);
                        if (rep.code == 200)
                        {

                        }

                    }
                });

            });
            this.on("success", function(file){
                if(file.width < 400 ||  file.height<400){
                    $rootScope.$broadcast('group.imageError');
                    this.removeAllFiles();
                }
            })
        },
        error: function (file, response) {

        },
        success: function (file, done) {
//        console.log(file.name);
            //console.log(done);
            $scope.currImgID = done.filename;
        }
    };

    $scope.clearall = function(){
        //$scope.members = '';
        $scope.images = '';
        $scope.events = '';
        $scope.pending_groups = '';
        $scope.edit_group = $scope.edit_group_data;
    }
                                         
    if($stateParams.id){
        //$scope.event_get_id.id = $stateParams.id;
        //$scope.get_events($stateParams);
    }

});