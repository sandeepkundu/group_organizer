"use strict";

var app = angular.module('ng-laravel');
app.controller('policyCtrl',function($scope,$auth,$state,$rootScope,$http,$translate,$window){
    
    
    $scope.terms_privacy = function () {
        
        $http({
            method: 'GET',
            url: 'laravel-backend/public/api/terms_privacy',
        }).then(function successCallback(response) {
            //$('#sugg').modal('hide');
            //$('#sugg').hide();
            $scope.terms = response.data.terms;
            $('#terms_desc').html(response.data.terms.description);
            $('#privacy_desc').html(response.data.privacy.description);
            $scope.privacy = response.data.privacy;
           // SweetAlert.swal({ title: "Success", text:'Thank you, your suggestion has been sent!\n\Feedback from our users is very important to us, and we take ample time to review every idea that is submitted. Please know that your suggestion will be carefully considered. One of our team members will get back to you only if we need further clarification or information.', type: "success"});
        }, function errorCallback(response) {
            SweetAlert.swal({ title: "Error", text: "Terms of Use and Privacy Policy not found.", type: "error"});
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    $scope.terms_privacy();
   

});