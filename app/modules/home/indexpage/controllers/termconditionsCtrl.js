'use strict';

var app = angular.module('ng-laravel');

app.controller('termconditionsCtrl',function($scope,$stateParams,$state,$window,$rootScope,$http){
    $window.scrollTo(0, 0);
    $http({
        method: 'GET',
        url: 'laravel-backend/public/api/get_terms_cond',
    }).then(function successCallback(response) {
        $scope.tems_cond = response.data;
        $('#description').html(response.data.description);
        //$scope.event_response = $scope.event.event_response
    }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
    });

 });  