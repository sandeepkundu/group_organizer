"use strict";

var app = angular.module('ng-laravel', ['xeditable', 'ui.bootstrap', 'ngStorage']);

app.service('setParam', function () {
    return {};
});

app.controller('findGroupCtrl', function ($scope, $auth, findGroupService, $cookieStore, $localStorage, $window, Restangular, SweetAlert, $http, setParam) {

    var t_time = new Date();
    var curr_time = t_time.setHours(t_time.getHours());
    var cookie_store = $cookieStore.get('time');
    if (cookie_store < curr_time) {
        $cookieStore.remove('key');
        $cookieStore.remove('lat');
        $cookieStore.remove('lng');

//            $cookieStore.$reset();
    }


    $window.scrollTo(0, 0);
    $scope.noMore = 0;
    $scope.findgroups = [];

    if ($auth.isAuthenticated().$$state.value == true) {
        $scope.profile = $auth.getProfile().$$state.value;
    }
    if ($scope.profile != undefined) {
        $scope.getloggindfirst = 0;
        if ($scope.profile.firstLogin == 0) {
            $scope.firstTime = 1;
            Restangular.all('loginFirst').post($scope.profile.id);
        }
    } else {
        $scope.getloggindfirst = 1;
    }
    
    $scope.loadersload = function (e) {
        $('.calender-btn').text('wait...');
    }

    $scope.pageChanged = function (per_page) {
        findGroupService.pageChange($scope.pagination.current_page, per_page.id).then(function (data) {
            $scope.findgroups = data;
            $scope.pagination = $scope.findgroups.metadata;
            $scope.maxSize = 5;
        });
    };

    var key = $cookieStore.get('key');
    if (!key) {
        getCurrentlocation();
    } else {
        getlatlong();    
            
    }
    
    function getlatlong(){
        $scope.location={'current_location':$cookieStore.get('key')};
            //$scope.location.current_location = $cookieStore.get('key');
        $scope.loader = true;
        Restangular.all('setCurrentLocationfind').post($scope.location).then(function (Obj) {
            
            if(!Obj.data.latitude || !Obj.data.longitude){
                console.log('Errro While getting Lat and lng, trying again');
                getlatlong();
            }
            if(Obj.data.latitude && Obj.data.longitude){
                makeGlobalLocation(Obj.data.latitude, Obj.data.longitude);
            }
            


            if(setParam.tempval){
               var usera = {interest: setParam.tempval,
                records:1   
               }; 
               $scope.getGroupByInterest(usera);
            }else{
                var usera = {interest: null,
                latitude: Obj.data.latitude,
                longitude: Obj.data.longitude};
                $scope.findgroups = [];
                $scope.getGroupByInterest(usera);
            }
            $scope.loader = false;
        }, function (response) {
            $scope.loader = false;
        });
    }
    
    $scope.checkName = function(){

        if (typeof $scope.user.current_location == undefined)
            return false;
        //loader();
        $scope.loader = true;
        Restangular.all('setCurrentLocation').post($scope.user).then(function (Obj) {

            makeGlobalLocation(Obj.data.latitude, Obj.data.longitude);

            var usera = {interest: null,
                latitude: Obj.data.latitude,
                longitude: Obj.data.longitude};
            $scope.findgroups = [];
            $scope.getGroupByInterest(usera);
            $scope.loader = false;
        }, function (response) {
            $scope.loader = false;
        });
    };

    $scope.user = {
        C_location: ""
    }
    $scope.makeItPossi = function (user_id, data) {
        if (user_id == $scope.profile.id) {
            return 1;
        }
        if (data == null) {
            return 0;
        }

        if (data == $scope.profile.id) {
            return 1;
        }
        var s = data.includes($scope.profile.id);
        if (s == true) {
            return 1;
        } else {
            return 0;
        }
    };
// Local Validation
    $scope.checkEmpty = function (data) {

        if (data == '') {
            return "This field is required!";
        }
    };




    $scope.getGroupByInterest = function (user, records, resetInt) {
        //alert(user.interest);
        if (resetInt != undefined) {
            if (resetInt == 1) {
                $scope.findgroups = [];
            }
        }
        if (records == undefined) {
            records = 0;
        }
        if (user == undefined) {
            user = {};
            user.interest = '';
        }
        user.records = records + 1;
        $scope.records = user.records;

        if (user.latitude == undefined)
            user.latitude = "";
        //console.log(user.latitude); 
        if (user.interest == "")
            user.interest = null;
        if (user.latitude == "") {
            user.longitude = $cookieStore.get('lng')
            user.latitude = $cookieStore.get('lat')
        }

        // $scope.totalrecord = user.records;
        //alert($scope.totalrecord);
        // Set default records per page by 1 you can reduce and add.
        if ($scope.profile) {
            user.user_id = $scope.profile.id;

            $('.load-more').text('Loading...');
            Restangular.all('groups/getGroupByInterest').customPOST(user).then(function (Obj) {

                if (Obj.data.length == 0) {
                    $scope.noMore = 1;
                    $('.load-more').hide();
                } else {
                    $scope.noMore = 0;
                }
                $scope.findgroups = $scope.findgroups.concat(Obj.data);
                $scope.pagination = Obj;
                $('.load-more').text('Load More');
                $scope.perpage = Obj.current_page;

            });
        } else {
            $('.load-more').text('Loading...');
            //$scope.findgroups;

            Restangular.all('groups/getGroupByInterest').customPOST(user).then(function (Obj) {
                //alert(Obj.data.length);
                if (Obj.data.length == 0) {
                    $scope.noMore = 1;
                    $('.load-more').hide();
                } else {
                    $scope.noMore = 0;
                }
                $scope.findgroups = $scope.findgroups.concat(Obj.data);
                $scope.pagination = Obj;
                $('.load-more').text('Load More');
                $scope.perpage = Obj.current_page;

            });
        }

    };

    $scope.group_assign = function (group_id) {


        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/groups/group_join',
            data: {'id': $scope.profile.id, 'group_id': group_id}
        }).then(function successCallback(response) {
            SweetAlert.swal({title: "Success", type: "success"});
            $('#join_'+group_id).hide();
          
        }, function errorCallback(response) {
            SweetAlert.swal({title: "Error", text: response.data.error, type: "error"});
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };

    $scope.setparam = setParam;


    if ($scope.setparam.tempval && !key) {
        $scope.grpinst = {};
        $scope.grpinst.interest = $scope.setparam.tempval;
        $scope.get_int = function () {

            var user = {interest: $scope.setparam.tempval,
                latitude: $localStorage.lat,
                longitude: $localStorage.lng};
            
                $scope.getGroupByInterest(user);
        }

        $scope.get_int();
    } else if(!key){

        $scope.get_int = function () {

            var user = {interest: null,
                latitude: $localStorage.lat,
                longitude: $localStorage.lng};
            
            $scope.getGroupByInterest(user);
        }

        $scope.get_int();

    }


    function getCurrentlocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                makeGlobalLocation(position.coords.latitude, position.coords.longitude);
            });

        }

    }

    function makeGlobalLocation(lat, lng) {
        if(!lat || !lng){
        }
        var latlng = new google.maps.LatLng(lat, lng);
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng': latlng}, function (results, status) {
            if (status == 'ZERO_RESULTS') {
                $('#errormsg').html('Invalid Location');
            } else {
                $('#errormsg').html('');
            }

            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1] || results[0]) {

                    for (var ii = 0; ii < results[0].address_components.length; ii++) {
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_3,political") {
                            addr.city = results[0].address_components[ii].long_name;
                        } else if (types == "administrative_area_level_2,political") {
                            addr.city = results[0].address_components[ii].long_name;

                        } else if (types == "country") {
                            addr.city = results[0].address_components[ii].long_name;
                        }

                        if (types == "postal_code" || types == "postal_code_prefix,postal_code") {
                            addr.postalcode = results[0].address_components[ii].long_name;
                        }
                    }
                    $scope.$apply(function () {
                        //console.log(addr['city']+', '+addr['postalcode']);
                        $scope.current_location = addr['city'] + ', ' + addr['postalcode'];
                        var city_postalcode = addr['city'] + ', ' + addr['postalcode'];
                        var lat = lat;
                        var lng = lng;
                        $cookieStore.put('key', city_postalcode);
                        $cookieStore.put('lat', lat);
                        $cookieStore.put('lng', lng);
                        var tmp_time = new Date();

                        var time = tmp_time.setHours(tmp_time.getHours() + 2);
                        $cookieStore.put('time', time);

                    });
                } else {
                    console.log('Location not found');
                }
            }
        });

    }
});





