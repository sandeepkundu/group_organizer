'use strict';

var app = angular.module('ng-laravel');

app.controller('homeloginCtrl',function($scope,$stateParams,$state,$window,$rootScope,AUTH_EVENTS,UserAuthService,SweetAlert){
    
    $window.scrollTo(0, 0);
    //console.log($stateParams);
    
    
    $scope.user = {
        email: '',
        password: ''
      };
    
    $scope.homesignin = function (credentials) {
    UserAuthService.login(credentials).then(function (user) {
                
             if(401 == user.status){
                        SweetAlert.swal({ title: "Error", text:user.data.error, type: "error", html:true});
                        $state.go('home.login');
                        console.log(AUTH_EVENTS.loginFailed);
                        $rootScope.$broadcast(AUTH_EVENTS.loginFailed);

                }else{

                    // $scope.logis = UserAuthService.isAuthenticated().then(function (e) {


                    // });
                    // UserAuthService.isAuthorized(user.permissions).then(function(use){

                    //    console.log(use); 
                    // });
                    $scope.authenticated=true;
                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                    $scope.setCurrentUser(user);
                    $state.go('registered');
                }

        });
      };


    $scope.setCurrentUser = function (user) {
    $scope.currentUser = user;
console.log('dddd');
    console.log(user);
  };
 });  