"use strict";

var app = angular.module('ng-laravel',['xeditable','ui.bootstrap','ngStorage']);
app.service('setParam', function () {
    return {};
});
app.controller('indexUpCtrl',function($scope,$rootScope,$window,$cookieStore,$location,$localStorage,Restangular,$auth,SweetAlert,$http,setParam){

        var t_time=new Date();
        var curr_time = t_time.setHours(t_time.getHours());
        var cookie_store = $cookieStore.get('time');
        if(cookie_store<curr_time){
            $cookieStore.remove('key');
            $cookieStore.remove('lat');
            $cookieStore.remove('lng');
            
//            $cookieStore.$reset();
        }
    
        $window.scrollTo(0, 0);

        
        $http({
            method: 'GET',
            url: 'laravel-backend/public/api/randCategory',
        }).then(function successCallback(response) {
           $scope.categoryImg = response.data;
        }, function errorCallback(response) {
        });


        $http({
            method: 'GET',
            url: 'laravel-backend/public/api/cmsDetail',
        }).then(function successCallback(response) {
           $scope.cmsTemplate = response.data;
           $('#description').html(response.data.description);
        }, function errorCallback(response) {
        });
        
        $http({
            method: 'GET',
            url: 'laravel-backend/public/api/fb_feed',
            }).then(function successCallback(response) {
               $scope.fb_feeds = response.data;
               
            }, function errorCallback(response) {
        });
        
        $http({
            method: 'GET',
            url: 'laravel-backend/public/api/insta_feed',
            }).then(function successCallback(response) {
               $scope.insta_feeds = response.data;
               
            }, function errorCallback(response) {
        });

        $scope.suggestion = function(sugg){
            //alert(sugg);
        }

    var cache = {};
        $( "#tags" ).autocomplete({
           minLength: 3,
            source: function (request, response) {
                var term = request.term;
                if (term in cache) {
                    response(cache[ term ]);
                    return;
                }

                $.getJSON('laravel-backend/public/api/groups/getAllCat', request, function (data, status, xhr) {
                    cache[ term ] = data;
                    response(data);
                });
            }
        });

    /*
     * Get all users
     * Get from resolvedItems function in this page route (config.router.js)
     */
//    $scope.findgroups = resolvedItems;
//    $scope.pagination = $scope.findgroups.metadata;
//    $scope.maxSize = 5;

    $scope.pageChanged = function(per_page) {
        UserService.pageChange($scope.pagination.current_page,per_page.id).then(function(data){
            $scope.users = data;
            $scope.pagination = $scope.users.metadata;
            $scope.maxSize = 5;
        });
    };
  
    var key = $cookieStore.get('key');
    if(!key){
      getCurrentlocation();
    }else{
        
     $scope.current_location = $cookieStore.get('key'); 
     }


    $scope.user = {
        C_location:""
    }
   
// Local Validation
    $scope.checkEmpty = function(data) {
      
        if (data == '') {
            return "This field is required!";
        }
    };

    $scope.checkName = function() {

    if(typeof $scope.user.C_location==undefined)
      return false;
     //$scope.loader = true;
     //alert($scope.user.C_location);
     Restangular.all('setCurrentLocation').post($scope.user).then(function(Obj) {
       
       makeGlobalLocation(Obj.data.latitude,Obj.data.longitude);
       
        $scope.findSetCat(null);
         //$scope.loader = false;
        
        }, function(response) {
          SweetAlert(response);
          
//          console.log(response);
        //$scope.loader = false;  
        });
        
        //$location.path('/home/find-group');
  };


    $scope.getGroupByInterest = function(user){
        //alert('asdfasdf');
        if(user ==undefined)
          user ={};

        if(user.interest =="")
          return false;
        
        var lng = $cookieStore.get('lng');
        if(lng){
            
        user.longitude = lng;
        user.latitude = $cookieStore.get('lat');
        }
        Restangular.all('groups/getGroupByInterests').customPOST(user).then(function(data) {
        
        $scope.findgroups = data.data;
        $scope.pagination = $scope.findgroups.metadata;

        }, function(response) {
            $scope.findgroups =response.data;
        $scope.pagination = $scope.findgroups.metadata;
        });
    };




    function getCurrentlocation (){
       
        
        if (navigator.geolocation) {
            
             navigator.geolocation.getCurrentPosition(function(position){
//                  alert(position);
                  
                    //alert('asdfasdf');
                    console.log(position.coords.latitude);
                    console.log(position.coords.longitude);
                    makeGlobalLocation (position.coords.latitude, position.coords.longitude);
                 },function(failure) {
                        $.ajax({
                            url: 'https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyCpx6aa6Mj4MM16B9ray6IFcnbvkBXOUuk',
                            cache: false,
                            type: 'POST',
                            dataType: 'json',
                            success: function (data) {
//                                alert(data.location.lat);
//                                alert(data.location.lng);
                                makeGlobalLocation(data.location.lat, data.location.lng);
                                //console.log(data.location);
                            }
                     });    
                  });
                

        }else{
            $.ajax({
                            url: 'https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyCpx6aa6Mj4MM16B9ray6IFcnbvkBXOUuk',
                            cache: false,
                            type: 'POST',
                            dataType: 'json',
                            success: function (data) {
//                                alert(data.location.lat);
//                                alert(data.location.lng);
                                makeGlobalLocation(data.location.lat, data.location.lng);
                                //console.log(data.location);
                            }
                     });
        }
        
        //salert('asdfasdfasdf');

      }

        function makeGlobalLocation (lat ,lng){
            
          var latlng = new google.maps.LatLng(lat ,lng);
//          console.log(latlng);
        
              var addr = {};
                  var geocoder = new google.maps.Geocoder();
                      geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                          
                          if(status=='ZERO_RESULTS'){
                                $('#errormsg').html('Invalid Location');
                            }else{
                                $('#errormsg').html('');
                            }
                          if (status == google.maps.GeocoderStatus.OK) {
                              if (results[1]) {

                              for (var ii = 0; ii < results[0].address_components.length; ii++) {
                                  var types = results[0].address_components[ii].types.join(",");
                                  if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_3,political") {
                                      addr.city = results[0].address_components[ii].long_name;
                                  }else if(types == "administrative_area_level_2,political"){
                                      addr.city = results[0].address_components[ii].long_name;

                                  }else if(types == "country"){
                                      addr.city = results[0].address_components[ii].long_name;
                                  }

                                  if (types == "postal_code" || types == "postal_code_prefix,postal_code") {
                                      addr.postalcode = results[0].address_components[ii].long_name;
                                  }
                              }
                              $scope.$apply(function() {
                                  
                                      //console.log(addr['city']+', '+addr['postalcode']);
                                      $scope.current_location=addr['city']+', '+addr['postalcode'];
                                      var city_postalcode=addr['city']+', '+addr['postalcode'];
                                      var lat=lat;
                                      var lng=lng;
                                      $cookieStore.put('key',city_postalcode);
                                      $cookieStore.put('lat',lat);
                                      $cookieStore.put('lng',lng);
                                      var tmp_time=new Date();
                                      
                                      var time = tmp_time.setHours(tmp_time.getHours() + 2);
                                      $cookieStore.put('time',time);
                                        //alert($cookieStore.lat=lat);
                                      
                                      
                              });
                              } else {
                                  console.log('Location not found');
                              }
                          }
                      });

        }
        
    
    $rootScope.get_interest = function(category){
        $scope.setparam=setParam;
        $scope.setparam.tempval=category.interest;
        $location.path('/home/find-group');
     };
     
     $scope.profile = $auth.getProfile().$$state.value;
     

  $rootScope.findSetCat = function(category){
       //console.log(category);
       
        $scope.setparam=setParam;
        $scope.setparam.tempval=category;
        if($scope.profile=='undefined'){
            $location.path('/registered/find-group');
        }else{
            if($scope.profile){
                if($scope.profile.id!=1){
                        $location.path('/registered/find-group');
                }else{
                         $location.path('/home/find-group');
                }
            }else{
                $location.path('/home/find-group');
            }

        }
     };   
     
     
      
    $(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#backtop').fadeIn(200);    // Fade in the arrow
    } else {
        $('#backtop').fadeOut(200);   // Else fade out the arrow
    }
});
$('#backtop').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});
    
});






