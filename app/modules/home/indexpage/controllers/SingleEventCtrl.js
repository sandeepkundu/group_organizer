"use strict";

var app = angular.module('ng-laravel',['xeditable','ui.bootstrap','ngStorage','oitozero.ngSweetAlert']);
app.controller('SingleEventCtrl',function($scope,$stateParams,findGroupService,$cookieStore,$auth,$localStorage,$window,Restangular,SweetAlert,$http){
 $window.scrollTo(0, 0);
 if($auth.isAuthenticated().$$state.value==true){
        $scope.profile = $auth.getProfile().$$state.value;
    }
//$scope.dat = 20;
$('.paypal_button').hide();
if($scope.profile != undefined){
  $scope.getloggindfirst=0;
  if($scope.profile.firstLogin ==0){
    $scope.firstTime=1;
  Restangular.all('loginFirst').post($scope.profile.id);
}
}else{
  $scope.getloggindfirst=1;
}

    if($scope.profile){
        Restangular.all('group/get_rsvp').post({'event_id':$stateParams.id}).then(function(Obj) {
        if(Obj.data !=null)
        $scope.finddata=1;
        else
            $scope.finddata=0;
        //alert($scope.finddata);
        }, function(response) {
          console.log(response);  
        });
        
        $scope.get_event = function(){
            $http({
                method: 'POST',
                url: 'laravel-backend/public/api/event/get',
                data: {'id': $stateParams.id,'user_id':$scope.profile.id},
            }).then(function successCallback(response) {
                $scope.data = response.data.group_event; 
                $scope.images = response.data.images;
                $scope.confirm = response.data.confirm;
                $scope.user_status = response.data.user_status;
                $scope.group_owner = response.data.group_owner;
                $scope.paypal_client_id = response.data.paypal_client_id;
                if($scope.data.max_member){
                    $scope.max_limit=$scope.data.max_member-$scope.confirm;
                }else{
                    $scope.max_limit=1;
                }
                if($scope.getloggindfirst == 0 && !$scope.user_status && $scope.max_limit!=0 && $scope.data.cost){
                    $('.paypal_button').show();
                        // Render the PayPal button

                    paypal.Button.render({

                        // Set your environment

                        env: 'production', // sandbox | production

                        // Specify the style of the button

                        style: {
                            label: 'paypal',
                            size:  'medium',    // small | medium | large | responsive
                            shape: 'rect',     // pill | rect
                            color: 'blue',     // gold | blue | silver | black
                            tagline: false    
                        },

                        // PayPal Client IDs - replace with your own
                        // Create a PayPal app: https://developer.paypal.com/developer/applications/create

                        client: {
                        sandbox:    $scope.paypal_client_id,
                        production: $scope.paypal_client_id,
                    },

                    payment: function(data, actions) {
                        return actions.payment.create({
                             payment: {
                                transactions: [
                                    {
                                        amount: { total: $scope.data.cost, currency: 'USD' }
                                    }
                                ]
                            }
                        });
                    },

                    onAuthorize: function(payment_data, actions) {
                        return actions.payment.execute().then(function() {
                            $scope.makeconfirm();
                            $http({
                                method: 'POST',
                                url: 'laravel-backend/public/api/event/payment',
                                data: {'event_id': $stateParams.id,'user_id':$scope.profile.id,'payment_detail': payment_data},
                            }).then(function successCallback(response) {
                                
                                SweetAlert.swal({ title: "Success", text:'Payment Done', type: "success"});
                            }, function errorCallback(response) {
                                
                            });
                        });
                    }

                    }, '#paypal-button-container');
                }
                
                
                
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        };
        $scope.get_event();
        
    }else{
        
        $scope.get_event = function(){
            $http({
                method: 'POST',
                url: 'laravel-backend/public/api/event/get',
                data: {'id': $stateParams.id},
            }).then(function successCallback(response) {
                $scope.data = response.data.group_event; 
                $scope.images = response.data.images;
                $scope.confirm = response.data.confirm;
                $scope.user_status = response.data.user_status;
                
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        };
        $scope.get_event();
    }
    
    $scope.paypal_payment = function(){
            //SweetAlert.swal({ title: "Success", text:'send to group Organizer', type: "success"});
    }    
        
    $scope.makeconfirm=function(){

    Restangular.all('group/confirm_rsvp').post({'event_id':$stateParams.id}).then(function(Obj) {

            SweetAlert.swal({ title: "Success", text:'send to group Organizer', type: "success"});
            $('.rsvp-btn').hide();
            $scope.get_event();
            //location.reload();
            }, function(response) {
              SweetAlert.swal({ title: "Error", text:'login First' , type: "error", html:true});

            });

    }   

    
        
    var cache = {};
    $( "#tags" ).autocomplete({
       minLength: 3,
        source: function (request, response) {
            var term = request.term;
            if (term in cache) {
                response(cache[ term ]);
                return;
            }

            $.getJSON('laravel-backend/public/api/groups/getAllCat', request, function (data, status, xhr) {
                cache[ term ] = data;
                response(data);
            });
        }
    });

    

$scope.pageChanged = function(per_page) {
        UserService.pageChange($scope.pagination.current_page,per_page.id).then(function(data){
            $scope.users = data;
            $scope.pagination = $scope.users.metadata;
            $scope.maxSize = 5;
        });
    };
    /*
     * Get all Task and refresh cache.
     * At first check cache, if exist, we return data from cache and if don't exist return from API
     */
    // findGroupService.list().then(function(data){
    //     $scope.findgroups = data;
    //     $scope.pagination = $scope.findgroups.metadata;
    // });

    var key = $cookieStore.get('key');
    if(!key){
      getCurrentlocation();
    }else{
        
     $scope.current_location = $cookieStore.get('key'); 
     }


 $scope.user = {
        C_location:""
    }
   
// Local Validation
    $scope.checkEmpty = function(data) {
      
        if (data == '') {
            return "This filed is required!";
        }
    };

$scope.checkName = function() {

    if(typeof $scope.user.C_location==undefined)
      return false;
    //loader();
     Restangular.all('setCurrentLocation').post($scope.user).then(function(Obj) {
       
        makeGlobalLocation(Obj.data.latitude,Obj.data.longitude)
        }, function(response) {
          SweetAlert(response);
          console.log(response);  
        });
  };


    $scope.getGroupByInterest = function(user){
        
        if(user.interest =="")
          return false;
        
        var lng = $cookieStore.get('lng');
        if(lng){
            
        user.longitude = lng;
        user.latitude = $cookieStore.get('lat');
        }
        Restangular.all('groups/getGroupByInterest').customPOST(user).then(function(data) {
        
        $scope.findgroups = data.data;
        $scope.pagination = $scope.findgroups.metadata;

        }, function(response) {
            $scope.findgroups =response.data;
        $scope.pagination = $scope.findgroups.metadata;
        });
    };




    function getCurrentlocation (){
    if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition(function(position){
                
                makeGlobalLocation (position.coords.latitude, position.coords.longitude);
             });

    }

    }

    function makeGlobalLocation (lat ,lng){
    
    var latlng = new google.maps.LatLng(lat ,lng);
   // console.log(latlng);
        var addr = {};
            var geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            
                        for (var ii = 0; ii < results[0].address_components.length; ii++) {
                            var types = results[0].address_components[ii].types.join(",");
                            if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_3,political") {
                                addr.city = results[0].address_components[ii].long_name;
                            }else if(types == "administrative_area_level_2,political"){
                                addr.city = results[0].address_components[ii].long_name;
                                
                            }else if(types == "country"){
                                addr.city = results[0].address_components[ii].long_name;
                            }

                            if (types == "postal_code" || types == "postal_code_prefix,postal_code") {
                                addr.postalcode = results[0].address_components[ii].long_name;
                            }
                        }
                        $scope.$apply(function() {
                                //console.log(addr['city']+', '+addr['postalcode']);
                                $scope.current_location=addr['city']+', '+addr['postalcode'];
                                var city_postalcode=addr['city']+', '+addr['postalcode'];
                                var lat=lat;
                                var lng=lng;
                                $cookieStore.put('key',city_postalcode);
                                $cookieStore.put('lat',lat);
                                $cookieStore.put('lng',lng);
                                
                        });
                        } else {
                            console.log('Location not found');
                        }
                    }
                });

  } 
});





