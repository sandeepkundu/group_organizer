"use strict";


var app = angular.module('ng-laravel',['ui.bootstrap']);
app.controller('latestnewsCtrl',function($scope,$stateParams,$http,$window){
    $window.scrollTo(0, 0);
    $scope.latestnews = function () {
        $http({
            method: 'GET',
            url: 'laravel-backend/public/api/latestnews',
        }).then(function successCallback(response) {
            $scope.cmsTemplate = response.data;
            $('#description').html(response.data.description);
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
 
 
    $scope.latestnews();
});