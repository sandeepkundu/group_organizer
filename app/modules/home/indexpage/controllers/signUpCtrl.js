'use strict';

var app = angular.module('ng-laravel');
app.controller('signUpCtrl', function ($scope, $state, $stateParams, $window,$location, Restangular, SweetAlert, homeSignUpService) {

    /*
     * Define initial value
     */
    $window.scrollTo(0, 0);
    $scope.user = {};
    $scope.isDisabled = false;
    $scope.response = null;
    $scope.widgetId = null;
    //$scope.key =  '6LeD-gYUAAAAAJHSP8johePoD4YQ54fI4lKkQ5aK';
    if($stateParams.eid!=undefined){
        $scope.user.event_id = $stateParams.eid;
        $scope.user.group_id = $stateParams.gid;
        //alert($scope.user.group_id);
        //alert($scope.user.group_id);
//        $('#eventlink').html('<a href="#/home/login?eid=' + $scope.user.event_id + '">Sign In</a>');
        $('#setorgLink').html('<a href="#/home/login?eid=' + $scope.user.event_id + '&gid=' + $scope.user.group_id + '"><b>Login</b></a>');
    }

    /*
     * User Registration
     */
    $scope.master = function () {
        if ($scope.response == true)
            $scope.response = false;
        else
            $scope.response = true;
    };

    $scope.signup = function (user) {
        $scope.user.name = $scope.user.first_name + ' ' + $scope.user.last_name;
        //alert($scope.user.name);
        if($scope.user.password!=$scope.user.passwordconf){
           SweetAlert.swal({title: "Error", text: "Please Enter Same Password", type: "error", html: true}); 
           return false;
        }
       
        $scope.isDisabled = true;
        Restangular.all('signup').customPOST(user).then(function (data) {
            SweetAlert.swal({title: "Success", text: data.message, type: "success"});
            $scope.isDisabled = false;
            $scope.user = {};
            $state.go('home.getConfirmationMail', {"usermail": data.data});
        }, function (response) {
            SweetAlert.swal({title: "Error", text: displayError(response), type: "error", html: true});
            $scope.isDisabled = false;
        });
    };


    /**
     * Foreach Function for show validation error
     */
    function displayError(param) {
        var str = '';
        angular.forEach(param.data.error, function (value, key) {
            angular.forEach(value, function (v, k) {
                str += v + '<br>';
            })
        });
        return str;
    }
});  