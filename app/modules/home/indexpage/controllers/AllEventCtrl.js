"use strict";

var app = angular.module('ng-laravel', ['ui.bootstrap', 'ngStorage', 'xeditable']);
app.controller('AllEventCtrl', function ($scope, $stateParams, $auth, eventService, $cookieStore, $localStorage, Restangular, SweetAlert, $http, $window) {
    $window.scrollTo(0, 0);
    if ($auth.isAuthenticated().$$state.value == true) {
        $scope.profile = $auth.getProfile().$$state.value;
    }

    if ($scope.profile != undefined) {
        $scope.getloggindfirst = 0;
        if ($scope.profile.firstLogin == 0) {
            $scope.firstTime = 1;
            Restangular.all('loginFirst').post($scope.profile.id);
        }
    } else {
        $scope.getloggindfirst = 1;
    }

    var cache = {};
    $("#tags").autocomplete({
        minLength: 3,
        source: function (request, response) {
            var term = request.term;
            if (term in cache) {
                response(cache[ term ]);
                return;
            }

            $.getJSON('laravel-backend/public/api/groups/getAllCat', request, function (data, status, xhr) {
                cache[ term ] = data;
                response(data);
            });
        }
    });



    $scope.pageChanged = function (per_page) {
        eventService.pageChange($scope.pagination.current_page, per_page.id).then(function (data) {
            $scope.events = data;
            $scope.pagination = $scope.events.metadata;
            $scope.maxSize = 5;
        });
    };

    /*
     * Get all Task and refresh cache.
     * At first check cache, if exist, we return data from cache and if don't exist return from API
     */

    eventService.search($stateParams.id, $stateParams.date).then(function (data) {
        $scope.events = data;
        $scope.pagination = $scope.events.metadata;

        $scope.maxSize = 5;
    });

    var key = $cookieStore.get('key');
    if (!key) {
        getCurrentlocation();
    } else {

        $scope.current_location = $cookieStore.get('key');
    }


    $scope.user = {
        C_location: ""
    }

// Local Validation
    $scope.checkEmpty = function (data) {

        if (data == '') {
            return "This filed is required!";
        }
    };

    $scope.checkName = function () {

        if (typeof $scope.user.C_location == undefined)
            return false;
        //loader();
        Restangular.all('setCurrentLocation').post($scope.user).then(function (Obj) {

            makeGlobalLocation(Obj.data.latitude, Obj.data.longitude)
        }, function (response) {
            SweetAlert(response);
            console.log(response);
        });
    };


    $scope.getGroupByInterest = function (user) {

        if (user.interest == "")
            return false;

        var lng = $cookieStore.get('lng');
        if (lng) {

            user.longitude = lng;
            user.latitude = $cookieStore.get('lat');
        }
        Restangular.all('groups/getGroupByInterest').customPOST(user).then(function (data) {

            $scope.findgroups = data.data;
            $scope.pagination = $scope.findgroups.metadata;

        }, function (response) {
            $scope.findgroups = response.data;
            $scope.pagination = $scope.findgroups.metadata;
        });
    };




    function getCurrentlocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {

                makeGlobalLocation(position.coords.latitude, position.coords.longitude);
            });

        }

    }

    function makeGlobalLocation(lat, lng) {

        var latlng = new google.maps.LatLng(lat, lng);
        // console.log(latlng);
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng': latlng}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {

                    for (var ii = 0; ii < results[0].address_components.length; ii++) {
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_3,political") {
                            addr.city = results[0].address_components[ii].long_name;
                        } else if (types == "administrative_area_level_2,political") {
                            addr.city = results[0].address_components[ii].long_name;

                        } else if (types == "country") {
                            addr.city = results[0].address_components[ii].long_name;
                        }

                        if (types == "postal_code" || types == "postal_code_prefix,postal_code") {
                            addr.postalcode = results[0].address_components[ii].long_name;
                        }
                    }
                    $scope.$apply(function () {
                        //console.log(addr['city']+', '+addr['postalcode']);
                        $scope.current_location=addr['city']+', '+addr['postalcode'];
                        var city_postalcode=addr['city']+', '+addr['postalcode'];
                        var lat=lat;
                        var lng=lng;
                        $cookieStore.put('key',city_postalcode);
                        $cookieStore.put('lat',lat);
                        $cookieStore.put('lng',lng);
                      

                    });
                } else {
                    console.log('Location not found');
                }
            }
        });

    }
});





