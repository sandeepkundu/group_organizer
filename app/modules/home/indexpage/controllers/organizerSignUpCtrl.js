'use strict';

var app = angular.module('ng-laravel');
app.controller('organizerSignUpCtrl', function ($scope, $window,$auth, $state, $location, $cookieStore,$stateParams, Restangular, SweetAlert, $http, $localStorage) {
    
    $window.scrollTo(10, 10);
    $('#secondStep').hide();
    $('#loader').hide();

    if ($auth.isAuthenticated().$$state.value == true) {
        $scope.profile = $auth.getProfile().$$state.value;
    }


    var cache = {};
    $("#tags").autocomplete({
        minLength: 3,
        source: function (request, response) {
            var term = request.term;
            if (term in cache) {
                response(cache[ term ]);
                return;
            }

            $.getJSON('laravel-backend/public/api/groups/getAllCat', request, function (data, status, xhr) {
                cache[ term ] = data;
                response(data);
            });
        }
    });



    var key = $cookieStore.get('key');
    if(!key){
      getCurrentlocation();
    }else{
        
     $scope.current_location = $cookieStore.get('key'); 
     }

    $scope.grp = {
        C_location: "wait..."
    }

// Local Validation
    $scope.checkEmpty = function (data) {

        if (data == '') {
            return "This filed is required!";
        }
    };

    $scope.checkName = function () {

        if (typeof $scope.user.C_location == undefined)
            return false;
        //loader();
        Restangular.all('setCurrentLocation').post($scope.user).then(function (Obj) {

            makeGlobalLocation(Obj.data.latitude, Obj.data.longitude)
        }, function (response) {
            SweetAlert(response);
            console.log(response);
        });
    };


    $scope.getGroupByInterest = function (user) {

        if (user.interest == "")
            return false;

       var lng = $cookieStore.get('lng');
        if(lng){
            
        user.longitude = lng;
        user.latitude = $cookieStore.get('lat');
        }
        Restangular.all('groups/getGroupByInterest').customPOST(user).then(function (data) {

            $scope.findgroups = data.data;
            $scope.pagination = $scope.findgroups.metadata;

        }, function (response) {
            $scope.findgroups = response.data;
            $scope.pagination = $scope.findgroups.metadata;
        });
    };




    function getCurrentlocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {

                makeGlobalLocation(position.coords.latitude, position.coords.longitude);
            });

        }

    }

    function makeGlobalLocation(lat, lng) {

        var latlng = new google.maps.LatLng(lat, lng);
        // console.log(latlng);
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng': latlng}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {

                    for (var ii = 0; ii < results[0].address_components.length; ii++) {
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_3,political") {
                            addr.city = results[0].address_components[ii].long_name;
                        } else if (types == "administrative_area_level_2,political") {
                            addr.city = results[0].address_components[ii].long_name;

                        } else if (types == "country") {
                            addr.city = results[0].address_components[ii].long_name;
                        }

                        if (types == "postal_code" || types == "postal_code_prefix,postal_code") {
                            addr.postalcode = results[0].address_components[ii].long_name;
                        }
                    }
                    $scope.$apply(function () {
                        //console.log(addr['city']+', '+addr['postalcode']);
                        $scope.current_location=addr['city']+', '+addr['postalcode'];
                        var city_postalcode=addr['city']+', '+addr['postalcode'];
                        var lat=lat;
                        var lng=lng;
                        $cookieStore.put('key',city_postalcode);
                        $cookieStore.put('lat',lat);
                        $cookieStore.put('lng',lng);

                    });
                } else {
                    console.log('Location not found');
                }
            }
        });

    }

    $scope.signup = function (user) {
        $scope.user_login = {};
        $scope.isDisabled = true;
        Restangular.all('create-group').customPOST(user).then(function (data) {
            $scope.user_login.email = user.email;
            $scope.user_login.password = user.password;
            $location.path('/home/orgpayment/gid=' + user.tempGroupID + '&id=' + user.email);
            //$state.go('home.getConfirmationMail', { "usermail":data.data});
        }, function (response) {
            SweetAlert.swal({title: "Error", text: displayError(response), type: "error", html: true});
            $scope.isDisabled = false;
        });
    };


    /**
     * Foreach Function for show validation error
     */
    function displayError(param) {
        var str = '';
        angular.forEach(param.data.error, function (value, key) {
            angular.forEach(value, function (v, k) {
                str += v + '<br>';
            })
        });
        return str;
    }


    var myarray = [];
    $scope.getCategory = function () {

        $http({
            method: 'GET',
            url: 'laravel-backend/public/api/groups/category',
        }).then(function successCallback(response) {
            //console.log(response.data);
            var a = [];
            if (response.data != "") {
                for (var i in response.data) {

                    $('#getInterest').append($('<option>', {value: response.data[i].id, text: response.data[i].category_name}));
                }
            } else {
                $('#getInterest').append($('<option>', {value: 0, text: test}));
            }

        }, function errorCallback(response) {
            console.log('error with groups interest while creating new .');
        });
    };

}).directive('steps', function (SweetAlert, $location,$http) {
    
    return {
        restrict: 'EA',
        link: function (scope, element, attrs) {

            scope.user = {};
            var form = element;
            form.steps({
                headerTag: "h3",
                bodyTag: "fieldset",
                transitionEffect: "slideLeft",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Allways allow previous action even if the current form is not valid!
                  //alert(newIndex);
                    if(newIndex==1)
                    {
                       window.scrollTo(2, $(".panel-body").offset().top);
                       // $(".content").css("min-height", '18em');
                    }
                   if(currentIndex==1){
                         
                   }else if(currentIndex==2){
                    $(".content").css("min-height", '18em');
                      window.scrollTo(0, $(".panel-body").offset().top);
                        //$(".content").css("min-height", '35em');
                    }else if(newIndex==2){
                        window.scrollTo(0, $(".panel-body").offset().top);
                        //$(".content").css("min-height", '80em');
                    }else{
                        //alert('asdsad');
                        $(".content").css("min-height", '18em');
                    }
                    if(newIndex==2){
                        //alert('sa');

                        $(".content").css("min-height", '35em');
                    }
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }
                    // Needed in some cases if the user went back (clean up)
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        form.find(".body:eq(" + newIndex + ") label.error").remove();
                        form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                    }
                    form.validate().settings.ignore = ":disabled,:hidden";
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                   // alert(currentIndex);
                    // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
                    

                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        //console.log(scope.profile);
                        //console.log("previous");
                        form.steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    form.validate().settings.ignore = ":disabled";
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    $('.btn').attr('disabled', true);

                    var allInterest = $('#getInterest').val();
                    var formAttr = $('#example-advanced-form').serialize();// + '&allInt=' + allInterest;
                    var formgroupname = $('#getGroupname').val();
                    var formgrouplocation = $('#changelocation').val();
                    
                    $('#loader').show();
                    $http({
                        method: 'POST',
                        url: 'laravel-backend/public/api/groups/check',
                        data:{'group_name':formgroupname,'loc':formgrouplocation},
                    }).then(function successCallback(response) {
                       if(response.data=='false'){
                           $.post("laravel-backend/public/api/create-temp-group", formAttr)

                            .done(function (dataObj) {

                                if (scope.profile == undefined) {
                                    $('#firstStep').hide();
                                    $('#secondStep').show();
                                    $('.btn').attr('disabled', false);
                                    scope.user.tempGroupID = dataObj.data;
                                    $('#loader').hide();

                                    $('#setorgLink').html('<a href="#/home/login?grpnewID=' + dataObj.data + '">Sign In</a>');

                                } else {

                                    window.location.href = '#/registered/orgpayment/' + dataObj.data;
                                }
                            })
                            .fail(function (response) {
                                var str = '';
                                var responseText = jQuery.parseJSON(response.responseText);
                                $.each(responseText.error, function (k, v) {
                                    str += v + '<br>';
                                });
                                SweetAlert.swal({title: "Error", text: str, type: "error", html: true});
                                $('#loader').hide();
                                $('.btn').attr('disabled', false);
                            })
                            .always(function () {
                                //scope.user.tempGroupID=0;
                                $('#loader').show();
                            });
                       }else{
                           $('#loader').hide();
                           $('#name_error').html('<span style="color:red;"><b>* This group name is already taken. Please choose another name.</b></span>');
                           $('.btn').attr('disabled', false);
                       }
                    }, function errorCallback(response) {
                       
                    });
//                    
                    //console.log(formAttr);

                }
            }).validate({
                
                errorPlacement: function errorPlacement(error, element) {
                    element.after(error);
                    console.log(element[0]);
                    if(element[0].id=='changelocation'){
                       $(".content").css("min-height", '15em');
                   }
                   if(element[0].name=='interest'){
                       $(".content").css("min-height", '25em');
                   }
                   if(element[0].id=='getGroupname'){
                        //alert('sa');
                        $(".content").css("min-height", '42em');
                    }
                    if(element[0].id=='header'){
                        //alert('sa');
                        $(".content").css("min-height", '42em');
                    }
                    if(element[0].id=='description-2'){
                        //alert('sa');
                        $(".content").css("min-height", '42em');
                    }
                    
                },
                rules: {
                    confirm: {
                        equalTo: "#password-2"
                    }
                }
            });
        }
    }
});



setTimeout(function () {
$(".content").css("min-height", '15em');
    var cache = {};
    $("#changelocation").autocomplete({
        minLength: 3,
        source: function (request, response) {
            var term = request.term;
            if (term in cache) {
                response(cache[ term ]);
                return;
            }
            var data = '';
            var val = '';
           $('#makeItloaded').show();
//            $.getJSON('laravel-backend/public/api/getAllCat', request, function (data, status, xhr) {
            $.getJSON('laravel-backend/public/api/getaddress', request, function (data, status, xhr) {
                data = data.data;
                cache[ term ] = data;
                response(data);
                $('#makeItloaded').hide();
            });
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val('');
            }
        }
    });
    $('#next').addClass("bevylife_next");
    $('#finish').addClass("bevylife_next");
    $('#previous').addClass("bevylife_cancel");
}, 500);
