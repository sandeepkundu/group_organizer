"use strict";


var app = angular.module('ng-laravel', ['xeditable', 'ui.bootstrap', 'ngStorage']);
app.controller('oneGroupCtrl', function ($scope, $stateParams, $window, findGroupService, $auth, $cookieStore,$localStorage, Restangular, SweetAlert, $http) {

    $window.scrollTo(0, 0);
    if ($auth.isAuthenticated().$$state.value == true) {
        $scope.profile = $auth.getProfile().$$state.value;
    }
    if ($scope.profile != undefined) {
        $scope.getloggindfirst = 0;
        if ($scope.profile.firstLogin == 0) {
            $scope.firstTime = 1;
            Restangular.all('loginFirst').post($scope.profile.id);
        }
    } else {
        $scope.getloggindfirst = 1;
    }
    //alert($window.location.href);
    /*var base_url =  window.location.origin;
    var grp_id = $stateParams.id;
    if(base_url=='http://localhost'){
        $scope.iframe_url= '<iframe width="560" height="315" src="http://localhost/bevylife/#/home/group-details/'+grp_id+'" frameborder="0" allowfullscreen></iframe>';
    }
    if(base_url=='http://dev.bevylife.com'){
        $scope.iframe_url= '<iframe width="560" height="315" src="http://dev.bevylife.com/#/home/group-details/'+grp_id+'" frameborder="0" allowfullscreen></iframe>';
    }
    if(base_url=='http://www.bevylife.com'){
        $scope.iframe_url= '<iframe width="560" height="315" src="http://www.bevylife.com/#/home/group-details/'+grp_id+' frameborder="0" allowfullscreen></iframe>';
    }
    
    $scope.copy = function(){
        document.querySelector("#text_iframe").select();
        document.execCommand('copy');
        $('#succ_msg').html('<span style="color:green;">Copied to clipboard</span>');
    };*/
    
    
    var cache = {};
    $("#tags").autocomplete({
        minLength: 3,
        source: function (request, response) {
            var term = request.term;
            if (term in cache) {
                response(cache[ term ]);
                return;
            }

            $.getJSON('laravel-backend/public/api/groups/getAllCat', request, function (data, status, xhr) {
                cache[ term ] = data;
                response(data);
            });
        }
    });

    $scope.pageChanged = function (per_page) {
        UserService.pageChange($scope.pagination.current_page, per_page.id).then(function (data) {
            $scope.users = data;
            $scope.pagination = $scope.users.metadata;
            $scope.maxSize = 5;
        });
    };
    /*
     * Get all Task and refresh cache.
     * At first check cache, if exist, we return data from cache and if don't exist return from API
     */

    findGroupService.cachedShow($stateParams.id).then(function (data) {
        $scope.Onegroups = data;
        
        $.each($scope.Onegroups.members, function( index, value ) {
            if($scope.profile.id==value.id)
                $scope.is_member=value.id;
          });

        //console.log($scope.Onegroups.avatar_url);

    });

    $scope.group_assign = function (group_id) {


        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/groups/group_join',
            data: {'id': $scope.profile.id, 'group_id': group_id}
        }).then(function successCallback(response) {
            SweetAlert.swal({title: "Success", text: 'Joining request sent', type: "success"});
            location.reload();
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };

    var key = $cookieStore.get('key');
    if(!key){
      getCurrentlocation();
    }else{
        
     $scope.current_location = $cookieStore.get('key'); 
     }


    $scope.user = {
        C_location: ""
    }
    $scope.currLocal = window.location.href;

// Local Validation
    $scope.checkEmpty = function (data) {

        if (data == '') {
            return "This filed is required!";
        }
    };

    $scope.checkName = function () {

        if (typeof $scope.user.C_location == undefined)
            return false;
        //loader();
        Restangular.all('setCurrentLocation').post($scope.user).then(function (Obj) {

            makeGlobalLocation(Obj.data.latitude, Obj.data.longitude)
        }, function (response) {
            SweetAlert(response);
            console.log(response);
        });
    };


    $scope.getGroupByInterest = function (user) {

        if (user.interest == "")
            return false;

        var lng = $cookieStore.get('lng');
        if(lng){
            
        user.longitude = lng;
        user.latitude = $cookieStore.get('lat');
        }
        Restangular.all('groups/getGroupByInterest').customPOST(user).then(function (data) {

            $scope.Onegroups = data.data;
            $scope.pagination = $scope.Onegroups.metadata;

        }, function (response) {
            $scope.Onegroups = response.data;
            $scope.pagination = $scope.Onegroups.metadata;
        });
    };




    function getCurrentlocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {

                makeGlobalLocation(position.coords.latitude, position.coords.longitude);
            });

        }

    }

    function makeGlobalLocation(lat, lng) {

        var latlng = new google.maps.LatLng(lat, lng);
        // console.log(latlng);
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng': latlng}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {

                    for (var ii = 0; ii < results[0].address_components.length; ii++) {
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_3,political") {
                            addr.city = results[0].address_components[ii].long_name;
                        } else if (types == "administrative_area_level_2,political") {
                            addr.city = results[0].address_components[ii].long_name;

                        } else if (types == "country") {
                            addr.city = results[0].address_components[ii].long_name;
                        }

                        if (types == "postal_code" || types == "postal_code_prefix,postal_code") {
                            addr.postalcode = results[0].address_components[ii].long_name;
                        }
                    }
                    $scope.$apply(function () {
                        //console.log(addr['city']+', '+addr['postalcode']);
                        $scope.current_location=addr['city']+', '+addr['postalcode'];
                        var city_postalcode=addr['city']+', '+addr['postalcode'];
                        var lat=lat;
                        var lng=lng;
                        $cookieStore.put('key',city_postalcode);
                        $cookieStore.put('lat',lat);
                        $cookieStore.put('lng',lng);

                    });
                } else {
                    console.log('Location not found');
                }
            }
        });

    }

});



// fullCalendar custom directive
app.directive('fullCalendar', function ($window, $stateParams) {
    var full_url = window.location.href;
    if(full_url.indexOf('home')>5){
        var temp_url = 'home';
    }else{
        var temp_url = 'registered';
    }
    
    return{
        restrict: 'A',
        link: function ($scope, element, attrs) {

            var calendar = element.fullCalendar({
                //isRTL: true,
                buttonHtml: {
                    prev: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                },
                header: {
                    left: 'prev,next ', //prev,next today
                    center: 'title',
                    right: ''
                },
                events: function (start, end, timezone, callback) {
                    $.ajax({
                        url: 'laravel-backend/public/api/groups/' + $stateParams.id,
                        method: 'GET',
                        dataType: 'json',
                        success: function (doc) {
                            var events = [];
                            $.each(doc.events, function (k, v) {
                                events.push({
                                    title: v.name+'\n'+'Attending: '+v.count_confirm+'\n'+'Remaining Seat: '+v.remaining_mem,
                                    allDay:true,
                                    start: moment(v.event_date),
                                    url: '#/'+temp_url+'/single-event/'+v.id
                                });
                            });
                            callback(events);
                        }
                    });
                },
                editable: false,
                droppable: false,
                selectable: true,
                selectHelper: true,
                dayNamesShort: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                views: {
                    month: {// name of view
                        titleFormat: 'MMMM YYYY'
                                // other view-specific options here
                    }
                },
                select: function (start, end, allDay) {
                    //console.log(allDay);
                    var m = moment(start._d).format('YYYY-MM-DD');
                    ;
                    $.ajax({
                        url: 'laravel-backend/public/api/events_groups?id=' + $stateParams.id+ '&date=' + m,
                        method: 'GET',
                        dataType: 'json',
                        success: function (doc) {
                            var events = [];

                            $.each(doc.events, function (k, v) {
                                if(v.name){
                                    $window.location.href = '#/home/all-event?id=' + $stateParams.id + '&date=' + m;
                                }
                            });
                        }
                    });
                    calendar.fullCalendar('unselect');
                }

            })
        }
    }
});