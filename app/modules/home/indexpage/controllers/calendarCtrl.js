"use strict";


var app = angular.module('ng-laravel', ['xeditable', 'ui.bootstrap', 'ngStorage']);
app.controller('calendarCtrl', function ($scope, $stateParams, $window, findGroupService, $auth, $cookieStore,$localStorage, Restangular, SweetAlert, $http) {

 });



// fullCalendar custom directive
app.directive('fullCalendar', function ($window, $stateParams) {
    return{
        restrict: 'A',
        link: function ($scope, element, attrs) {

            var calendar = element.fullCalendar({
                //isRTL: true,
                buttonHtml: {
                    prev: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                },
                header: {
                    left: 'prev,next ', //prev,next today
                    center: 'title',
                    right: ''
                },
                events: function (start, end, timezone, callback) {
                    $.ajax({
                        url: 'laravel-backend/public/api/groups/' + $stateParams.id,
                        method: 'GET',
                        dataType: 'json',
                        success: function (doc) {
                            var events = [];
                            $.each(doc.events, function (k, v) {

                                events.push({
                                    title: v.name,
                                    allDay:true,
                                    start: moment(v.event_date)
                                });
                            });
                            callback(events);
                        }
                    });
                },
                editable: false,
                droppable: false,
                selectable: true,
                selectHelper: true,
                dayNamesShort: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thrusday', 'Friday', 'Saturday'],
                views: {
                    month: {// name of view
                        titleFormat: 'MMMM YYYY'
                                // other view-specific options here
                    }
                },
                select: function (start, end, allDay) {
                    //console.log(allDay);
                    var m = moment(start._d).format('YYYY-MM-DD');
                    ;
                    $.ajax({
                        url: 'laravel-backend/public/api/events_groups?id=' + $stateParams.id+ '&date=' + m,
                        method: 'GET',
                        dataType: 'json',
                        success: function (doc) {
                            var events = [];

                            $.each(doc.events, function (k, v) {
                                if(v.name){
                                    $window.location.href = '#/home/all-event?id=' + $stateParams.id + '&date=' + m;
                                }
                            });
                        }
                    });
                    calendar.fullCalendar('unselect');
                }

            })
        }
    }
});