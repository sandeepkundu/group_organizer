"use strict";


var app = angular.module('ng-laravel',['ui.bootstrap']);
app.controller('doanddontsCtrl',function($scope,$stateParams,$http,$window){
    $window.scrollTo(0, 0);
    $scope.doanddonts = function () {
        $http({
            method: 'GET',
            url: 'laravel-backend/public/api/doanddonts',
        }).then(function successCallback(response) {
            $scope.cmsTemplate = response.data;
            $('#description').html(response.data.description);
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
 
 
    $scope.doanddonts();
});