'use strict';

angular.module('ng-laravel').service('findGroupService', function($rootScope, Restangular,CacheFactory) {
    /*
     * Build collection /group
     */
    var _groupService = Restangular.all('groups');
    if (!CacheFactory.get('groupsCache')) {
        var groupsCache = CacheFactory('groupsCache');
    }else{
        var groupsCache=CacheFactory.get('groupsCache');
    }
    /*
     * Get list of groups from cache.
     * if cache is empty, data fetched and cache create else retrieve from cache
     */
    this.cachedList = function() {
        // GET /api/group
        if (!groupsCache.get('list')) {
            return this.list();
        } else{
            return groupsCache.get('list');
        }
    };


    /*
     * Get list of groups
     */
    this.list = function() {
        // GET /api/user
        var data = _groupService.getList();
        groupsCache.put('list',data);
        return data;
    };



    /*
     * Pagination change
     */
    this.pageChange = function(pageNumber,per_page) {
        // GET /api/user?page=2
        return _groupService.getList({page:pageNumber,per_page:per_page});
    };


    this.cachedShow = function(id) {
        // GET /api/user/:id
        //console.log(id);
        if (!groupsCache.get('show'+id)) {
            return this.show(id);
        } else{
            return groupsCache.get('show'+id);
        }
    };

    /*
     * Show specific user by Id
     */
    this.show = function(id) {
        // GET /api/user/:id
        //console.log(id);
        var data = _groupService.get(id);
        groupsCache.put('show'+id,data);
        return data;
    };



    /*
     * Search in groups
     */
    this.search = function(query,per_page) {
        // GET /api/user/search?query=test&per_page=10
        if(query !=''){
            return _groupService.customGETLIST("search",{query:query, per_page:per_page});
        }else{
            return _groupService.getList();
        }
    }

});

