"use strict";

var app = angular.module('ng-laravel', ['dropzone', 'xeditable', 'ui.bootstrap', 'checklist-model', 'bootstrap-image-gallery', 'oitozero.ngSweetAlert']);
// app.directive('jqdatepicker', function () {
//     return {
//         restrict: 'A',
//         require: 'ngModel',
//         link: function (scope, element, attrs, ngModelCtrl) {
//             element.datepicker({
//                 dateFormat: 'mm/dd/yy',
//                 onSelect: function (date) {
//                     scope.event.event_date = date;
//                     scope.$apply();
//                 }
//             });

//             element.css('position', 'relative');
//             element.css('z-index', 9999);
//         }
//     };
// });


app.controller('GroupEditCtrl', function ($scope, $filter, $q, GroupService, $window, $location, $rootScope, SweetAlert, RoleService, CacheFactory, $stateParams, resolvedItems1, $http, resolvedItems, $translatePartialLoader, Notification, trans) {

    /*
     * Edit mode user
     * Get from resolvedItems function in this page route (config.router.js)
     */
    //    list of all user that can be a group owner


    $scope.getuser = function () {

        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/user/user_list',
        }).then(function successCallback(response) {

            $scope.grPwner = response;
            //console.log(response);
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };

    $scope.data = resolvedItems;
    $scope.group = $scope.data;
    $scope.getEvent = $scope.data.events;
    $scope.getImage = $scope.data.ImageGallery;
    $scope.categories = resolvedItems1;
//    $scope.categories
//    console.log($scope.categories);
    // check in backend if equal 8 asterisk, password doesn't change


    $scope.temp_date = function (){
        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        $scope.temp = new Date($scope.single_event.event_date);
        var weekday = $scope.temp.getDay();
        $scope.week_day = days[weekday];
        //alert($scope.week_day);
        //$scope. = new Date($scope.event.event_date);
        
        $("#datepicker1").datepicker(
                {format: 'mm/dd/yyyy',
                    autoclose: true,
                    minDate: $scope.temp,
                }
        );


        
        
    }





    /*
     * Get user and refresh cache.
     * At first check cache, if exist, we return data from cache and if don't exist return from API
     */
//    GroupService.show($stateParams.id).then(function (data) {
//
//        $scope.group = data;
//        $scope.getEvent = data.events;
//        $scope.getImage = data.ImageGallery;
////       console.log(data.members);
//    });
    $scope.pageTemp = 'EditPage';
    $scope.dtOption1 = {
        // "ajax": {
        //     url: "http://datatable.getsandbox.com/datatable",
        //         "dataSrc": "users",
        //         "headers": "Content-Type: application/json"
        // },
        // "columns": [
        //    { "data":"name" },
        //    { "data":"age" },
        //    { "data":"position" },
        //    { "data":"office" },
        //    { "data":"name" },
        // ],

        responsive: true
    };

    /*
     * Get all Roles
     */
//    RoleService.list().then(function(data){
//        $scope.roles = data;
//    });


    $scope.dropzoneOption1 = {
        url: "laravel-backend/public/api/groupimages",
        paramName: "file", // The name that will be used to transfer the file
        sending: function (file, xhr, formData) {
            formData.append("group_id", $stateParams.id);
        },
        acceptedFiles: 'image/jpeg,image/png,image/gif',
        maxFilesize: 50, // MB
        maxFiles: 10,
        maxfilesexceeded: function (file) {
            this.removeAllFiles();

            this.addFile(file);
        },
        addRemoveLinks: true,
        dictDefaultMessage:
                '<span class="bigger-150 bolder"><i class=" fa fa-caret-right red"></i> Drop files</span> to upload \
            <span class="smaller-80 grey">(or click)</span> <br /> \
            <i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
        dictResponseError: 'Error while uploading file!',
        init: function () {

            this.on("removedfile", function (file) {

                $.ajax({
                    type: 'POST',
                    url: 'laravel-backend/public/api/delgro/' + $scope.currImgID,
                    //data: {name:$scope.currImgID},
                    dataType: 'html',
                    success: function (data) {
                        var rep = JSON.parse(data);
                        if (rep.code == 200)
                        {

                        }

                    }
                });

            });
            this.on("success", function (file) {
                if (file.width < 400 || file.height < 400) {
                    $rootScope.$broadcast('group.imageError');
                    this.removeAllFiles();
                    //this.addFile(file);
                }
            });

        },
        error: function (file, response) {

        },
        success: function (file, done) {

            $scope.currImgID = done.filename;
        }
    };


    $scope.status = function (status, id) {
//       console.log(id);
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/status',
            data: {'status': status, 'id': id},
        }).then(function successCallback(response) {
            location.reload();
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };

    if (!CacheFactory.get('groupsCache')) {
        var groupsCache = CacheFactory('groupsCache');
    }

    $scope.event_rsvp = function (id) {

        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/event_rsvp',
            data: {'id': id},
        }).then(function successCallback(response) {
            $scope.event_rs = response.data;

        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });

    };




    $scope.delete_member = function (m_id, g_id) {



        var data = {'group_id': g_id, 'user_id': m_id}
        // $scope.getEvent.splice(index, 1);
        var chkmemb = "chk_" + m_id;

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Member!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#CAE197",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            html: false
        }, function () {


            return $http.post('laravel-backend/public/api/delete/member', data)
                    .success(function (data) {
                        if (data == "success") {
                            $("#" + chkmemb).closest("tr").hide();
                            swal("Deleted!", "selected Member delete successfully!", "success");

                            // swal({
                            //   title: "Deleted!",
                            //   text: "selected Event delete successfully",
                            //   type: "success",
                            //   showCancelButton: true,
                            //   confirmButtonColor: "#DD6B55",
                            //   confirmButtonText: "Reload the page",
                            //   closeOnConfirm: false,
                            //   html: false
                            // }, function(){
                            //  location.reload();
                            // });


                        } else {
                            swal("Error", "Error while deleting Member!", "error");
                        }
                    });
        });
    };


    /*
     * Update user
     */
    $scope.update = function (group) {
        $scope.isDisabled = true;
        GroupService.update(group);
    };

    $scope.delete_event = function (id) {
        //console.log($scope);  
        $rootScope.areYouSureDelete,
                function (isConfirm) {
                    if (isConfirm) {
                        location.reload();
                        //console.log(id);
                        //GroupService.delete_event(id);
                    }
                };
    };



    $scope.fromDateOptions = {
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 3,
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        onClose: function (selectedDate) {
            $("#to").datepicker("option", "maxDate", selectedDate);
        }
    };

    $scope.toDateOptions = {
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 3,
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        onClose: function (selectedDate) {
            $("#from").datepicker("option", "minDate", selectedDate);
        }
    };

    // x-editable sample


    // $scope.eventlist = function() {
    //     return $scope.groups.length ? null : $http.get('laravel-backend/public/api/event/show/1').success(function(data) {
    //         console.log(data);   
    //         $scope.groups = data;
    //     });
    // };



    $scope.opened = {};

    $scope.open = function ($event, elementOpened) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened[elementOpened] = !$scope.opened[elementOpened];
    };


    // delete images from group
    $scope.removeimage = function (image) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Image!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#CAE197",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            html: false
        }, function () {

            $http({
                method: 'POST',
                url: 'laravel-backend/public/api/grpgroup/deleteimage',
                data: {'image': image},
            }).then(function successCallback(response) {

                location.reload();

            }, function errorCallback(response) {

            });

        });
    };


    // Local Validation
    $scope.checkName = function (data) {
        if (data == '') {
            return "This field is required!";
        }
    };

    // Local Validation
    $scope.checkDecs = function (data) {
        if (data == '') {
            return "This field is required!";
        }
    };

    // Remote validation
    $scope.checkRemote = function (data) {
        var d = $q.defer();
        $http.post('/checkName', {value: data}).success(function (res) {
            res = res || {};
            if (res.status === 'ok') { // {status: "ok"}
                d.resolve()
            } else { // {status: "error", msg: "Username should be `awesome`!"}
                d.resolve(res.msg)
            }
        }).error(function (e) {
            d.reject('Server error!');
        });
        return d.promise;
    };

    // Select, local array, custom display
    $scope.statuses = [
        {value: 0, text: 'No'},
        {value: 1, text: 'Yes'},
    ];

    $scope.setDateformate = function (user) {

        var dts = new Date(user);
        var dateN = $filter('date')(dts, "dd/MM/yyyy"); // for conversion to string
        // console.log(dateN);
        return (user) ? dateN : '--';


    };


    // Select, remote array, no buttons
    $scope.groups = [];
    $scope.loadGroups = function () {
        return $scope.groups.length ? null : $http.get('laravel-backend/public/api/group').success(function (data) {
            $scope.groups = data;
        });
    };
    $scope.$watch('user.group', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            var selected = $filter('filter')($scope.groups, {id: $scope.user.group});
            $scope.user.groupName = selected.length ? selected[0].text : null;
        }
    });

    $scope.showGroup = function (user) {
        if (user.group && $scope.groups.length) {
            var selected = $filter('filter')($scope.groups, {id: user.group});
            return selected.length ? selected[0].text : 'Not set';
        } else {
            return user.groupName || 'Not set';
        }
    };
   
    $scope.show_modal = function () {
         
        $scope.single_event = {};
        $scope.single_event.avatar_url = '';
        $('#eventmodal').modal('show');
         $( "#datepicker" ).datepicker(
            { dateFormat: 'mm/dd/yy',
                minDate: 1,
        }
          );
          $('#startDate').timepicker();
          $('#endDate').timepicker();
          $('#startDate').focus(function() {
            $('.bootstrap-timepicker-widget').css('z-index','9999');
          });
          $('#endDate').focus(function() {
            $('.bootstrap-timepicker-widget').css('z-index','9999');
          });
    };

    
    $scope.saveEvent = function (data) {
        if(data.max_member){
            if (String(data.max_member).match(/[a-z]/i)) {
                SweetAlert.swal({title: "Error", text: 'Enter a valid Max Member', type: "error", html: true});
                return false;
             }
        }
        if (data.cost){
            if (data.cost.match(/[a-z]/i)) {
                SweetAlert.swal({title: "Error", text: 'Enter a valid Cost', type: "error", html: true});
                return false;
             }
        }
        
        if(data.recurring_week && !data.recur__week_number && !data.id){
            SweetAlert.swal({title: "Error", text: 'Enter a valid Week Number', type: "error", html: true});
            return false;
        }
        if(data.recurring_month && !data.recur__mon_number && !data.id){
            SweetAlert.swal({title: "Error", text: 'Enter a valid Month Number', type: "error", html: true});
            return false;
        }
        var temp_date = data.event_date.split('/');
        if (data.recur__mon_number){
            if (data.recur__mon_number.match(/[a-z]/i)) {
                SweetAlert.swal({title: "Error", text: 'Enter a valid Month Number', type: "error", html: true});
                return false;
             }
             if(temp_date[1]==30 || temp_date[1]==31){
                SweetAlert.swal({title: "Error", text: 'Enter a Valid Month Number', type: "error", html: true});
                return false;
            }
        }
        if (data.recur__week_number){
            if (data.recur__week_number.match(/[a-z]/i)) {
                SweetAlert.swal({title: "Error", text: 'Enter a Valid Week Number', type: "error", html: true});
                return false;
             }
             if(temp_date[1]==30 || temp_date[1]==31){
                SweetAlert.swal({title: "Error", text: 'Enter a Valid Week Number', type: "error", html: true});
                return false;
            }
        }
        if (data.recur__mon_number){
            if (data.recur__mon_number>99){
             SweetAlert.swal({title: "Error", text: 'Max 99 Month allowed', type: "error", html: true});
             return false;
             }
        }
        if (data.recur__week_number){
            if (data.recur__week_number>99){
                SweetAlert.swal({title: "Error", text: 'Max 99 Week allowed', type: "error", html: true});
                return false;
            }
        }
        
        var group_id = $stateParams.id;
        angular.extend(data, {group_id: group_id});
//        if (data.start_time > data.end_time) {
//            swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
//            return false;
//        }
        var startdate=data.event_date;
        var enddate=data.end_date;
        
        var shours = Number(data.start_time.match(/^(\d+)/)[1]);
        var sminutes = Number(data.start_time.match(/:(\d+)/)[1]);
        var sAMPM = data.start_time.match(/\s(.*)$/)[1].toLowerCase();
        
        var ehours = Number(data.end_time.match(/^(\d+)/)[1]);
        var eminutes = Number(data.end_time.match(/:(\d+)/)[1]);
        var eAMPM = data.end_time.match(/\s(.*)$/)[1].toLowerCase();
        //alert(sAMPM);
        if(sAMPM=='pm' && eAMPM=='am'){
            SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
            return false;
        }
        if (enddate < startdate) {
            SweetAlert.swal({title: "Error", text: 'Enter Valid End Date', type: "error", html: true});
            return false;
        }
        

        if((sAMPM=='pm' && eAMPM=='pm')){
            if(shours>ehours && shours!=12){
               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }else if(shours==ehours && sminutes>eminutes){

               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }else if(shours==ehours && sminutes==eminutes){
               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }
        }

        if((sAMPM=='am' && eAMPM=='am')){
            if(shours>ehours && shours!=12){
               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }else if(shours==ehours && sminutes>eminutes){

               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }else if(shours==ehours && sminutes==eminutes){
               SweetAlert.swal({title: "Error", text: 'Enter Valid Time', type: "error", html: true});
               return false; 
            }
        }
        $('#event_buttons').prop('disabled', true);
        return $http.post('laravel-backend/public/api/event/add', data)
                .success(function (data) {
                    $('#eventmodal').modal('hide');
                    swal("Success", "Event Created", "success");
                    //alert()
                    location.reload();
                })
                .error(function (response) {
                    //$scope.single_event = {};
                    swal({title: "Error", text: displayError(response), type: "error", html: true});
                    $('#event_buttons').prop('disabled',false);
                   
                });
    };

    function displayError(param) {
        var str = '';
        angular.forEach(param.error, function (value, key) {
            angular.forEach(value, function (v, k) {
                str += v + '<br>';
            })
        });
        return str;
    }

    $scope.show_event = function (event_id) {
        
        $scope.single_event = {};
      
        $( "#datepicker" ).datepicker(
            { dateFormat: 'mm/dd/yy',
                minDate: 1,
        }
          );
  $( "#datepicker1" ).datepicker(
            { dateFormat: 'mm/dd/yy',
                minDate: 1,
        }
          );
         $('#startDate').timepicker();
          $('#endDate').timepicker();
          $('#startDate').focus(function() {
            $('.bootstrap-timepicker-widget').css('z-index','9999');
          });
          $('#endDate').focus(function() {
            $('.bootstrap-timepicker-widget').css('z-index','9999');
          });
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/show_event',
            data: {'id': event_id},
        }).then(function successCallback(response) {
            $('#eventmodal').modal('show');
            
            $scope.single_event = response.data;
            //console.log($scope.single_event);
        }, function errorCallback(response) {
            
        });
    }




    // remove event
    $scope.removeUser = function (index) {

        if (!index) {
            return false;
        }
        var chkevets = "mkeve_" + index;
        var data = {'id': index}
        // $scope.getEvent.splice(index, 1);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Event!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#CAE197",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            html: false
        }, function () {


            return $http.post('laravel-backend/public/api/event/delete', data)
                    .success(function (data) {
                        //console.log(data); // 1
                        if (data == "success") {
                            $("#" + chkevets).closest("tr").hide();
                            swal("Deleted!", "selected Event delete successfully!", "success");
                        } else {
                            swal("Error", "Error while deleting event!", "error");
                        }
                    });
        });
    };

    // add user
    $scope.addNewEvent = function () {
        //console.log('sdf');
        var today = new Date();
        var dd = today.getDate();

        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (dd < 10)
        {
            dd = '0' + dd;
        }

        if (mm < 10)
        {
            mm = '0' + mm;
        }
        var today = yyyy + '-' + mm + '-' + dd;
        $scope.inserted = {
            // id: $scope.getEvent.length+1,
            name: '',
            event_date: today,
            description: '',
            recurring: 1
        };
        //console.log($scope.inserted);
        $scope.getEvent.push($scope.inserted);
    };




    /*
     * Dropzone file uploader initial
     */
    $scope.dropzoneConfig = {
        options: {// passed into the Dropzone constructor
            url: '../laravel-backend/public/api/uploadimage',
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: .5, // MB
            acceptedFiles: 'image/jpeg,image/png,image/gif',
            maxFiles: 1,
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            addRemoveLinks: true,
            dictDefaultMessage: '<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
            dictResponseError: 'Error while uploading file!',
        },
        'eventHandlers': {
            'removedfile': function (file, response) {
                $http({
                    method: "POST",
                    url: "laravel-backend/public/api/deleteimage/" + $scope.group.avatar_url
                }).then(function mySucces(response) {
                    $scope.deleteMessage = response.data;
                    $scope.group.avatar_url = '';
                });
            },
            'success': function (file, response) {
                if (file.width < 400 || file.height < 400) {
                    $rootScope.$broadcast('group.imageError');
                    this.removeAllFiles();
                }
                $scope.group.avatar_url = response.filename;
            }
        }
    };
//     $scope.single_event={};
//     $scope.single_event.avatar_url = '';
//    For Event image
    $scope.dropzoneConfig2 = {
        options: {// passed into the Dropzone constructor
            url: 'laravel-backend/public/api/uploadimage',
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: .5, // MB
            acceptedFiles: 'image/jpeg,image/png,image/gif',
            maxFiles: 1,
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            addRemoveLinks: true,
            dictDefaultMessage: '<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
            dictResponseError: 'Error while uploading file!',
        },
        'eventHandlers': {
            'removedfile': function (file, response) {
                $http({
                    method: "POST",
                    url: "laravel-backend/public/api/deleteimage/" + $scope.single_event.avatar_url
                }).then(function mySucces(response) {
                    $scope.deleteMessage = response.data;
                    $scope.single_event.avatar_url = '';
                });
            },
            'success': function (file, response) {
               
                $scope.single_event.avatar_url = response.filename;
            }
        }
    };

    $scope.$on('group.imageError', function (event) {

        Notification({message: 'Please upload image bigger then 400x400 pixels', templateUrl: 'app/vendors/angular-ui-notification/tpl/rejected.tpl.html'}, 'warning');
        $scope.isDisabled = false;
    });



    $scope.countriesmn = new Array("Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czeck Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia, The", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcaim Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romainia", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Spratly Islands", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "United States", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");

    $scope.updateState = function () {

        var indexCountry = $scope.countriesmn.indexOf($scope.group.country);
        console.log(indexCountry);

        $scope.states = BlockID(indexCountry + 1).split("|");

//             console.log($scope.states);
        if ($scope.states.length == 1) {
            $scope.states = $scope.states;
        }
        $scope.group.state = $scope.states['0'];
        //indexCountry correspond to the "Select" label
        if (indexCountry == 0) {
            $scope.states = new Array("");
        }
        //$scope.states = ['Afghanistan','Afghanistans','Afghanistanasd'];
    }

    $scope.$watch('countriesmn', function () {

        var indexCountry = $scope.countriesmn.indexOf($scope.group.country);

        $scope.states = BlockID(indexCountry + 1).split("|");

        if ($scope.states.length >= 1) {
            $scope.states = $scope.states;
        }
        if (indexCountry == 0) {
            $scope.states = new Array("");
        }

    });



    $scope.export = function (selection, export_type) {
//        console.log(selection);
//        SweetAlert.swal($rootScope.exportSelect,
//            function (isConfirm) {
//                if (isConfirm) {
//                    var recordType = $("input[name=exportSelect]:checked").val();
        var recordType = 1;
        GroupService.downloadExport(recordType, selection, export_type);
//                }
//            });
    };




    // Update user event listener
    $scope.$on('group.update', function () {
        Notification({message: 'group.form.groupUpdateSuccess', templateUrl: 'app/vendors/angular-ui-notification/tpl/success.tpl.html'}, 'success');
        $location.path('/admin/groups');
        $scope.isDisabled = false;
    });

    // user form validation event listener
    $scope.$on('group.validationError', function (event, errorData) {
        Notification({message: errorData, templateUrl: 'app/vendors/angular-ui-notification/tpl/validation.tpl.html'}, 'warning');
        $scope.isDisabled = false;
    });
});

