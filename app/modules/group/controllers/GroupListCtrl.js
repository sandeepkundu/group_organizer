"use strict";
var app = angular.module('ng-laravel',['ui.bootstrap']);

app.controller('GroupListCtrl',function($scope,$http,GroupService,$window,SweetAlert,Notification,$location,resolvedItems,$translatePartialLoader,$translate,$rootScope,trans){

    /*
     * Define initial value
     */
    $scope.query ='';
    
    $scope.groups = resolvedItems;
    $scope.pagination = $scope.groups.metadata;
    $scope.maxSize = 5;
    
    $window.scrollTo(0, 0);

    /*
     * Get all Task and refresh cache.
     * At first check cache, if exist, we return data from cache and if don't exist return from API
     */
//    GroupService.list().then(function(data){
//        $scope.groups = data;
//        $scope.pagination = $scope.groups.metadata;
//    });


    /*
     * Remove selected users
     */
//    $scope.delete = function(id) {
//        //console.log(id);
//        SweetAlert.swal($rootScope.areYouSureDelete,
//        function(isConfirm){
//            if (isConfirm) {
////                console.log(user);
//                //GroupService.delete(id);
//            }
//        });
//    };
    
    $scope.delete = function(id) {
//        console.log(id);
        var group_id={id:id};
//        console.log(group_id);
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this Group!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#CAE197",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false,
          html: false
            }, function(){
            
           
        return $http.post('laravel-backend/public/api/delete/group', group_id)
        .success(function(group_id){
        if(group_id=="success"){
        swal("Deleted!", "Group delete successfully!", "success");
            $rootScope.$broadcast('category.delete');
            location.reload();
        }else{
            swal("Error", "Error while deleting Group!", "error");
        }
            });
        });
    };
    
    
    
    
    
    
    /*
     * Pagination user list
     */
    $scope.units = [
        {'id': 10, 'label': 'Show 10 Item Per Page'},
        {'id': 15, 'label': 'Show 15 Item Per Page'},
        {'id': 20, 'label': 'Show 20 Item Per Page'},
        {'id': 30, 'label': 'Show 30 Item Per Page'},
    ]
    $scope.perPage= $scope.units[0];
    $scope.pageChanged = function(per_page) {
        GroupService.pageChange($scope.pagination.current_page,per_page.id).then(function(data){
            $scope.groups = data;
            $scope.pagination = $scope.groups.metadata;
            $scope.maxSize = 5;
        });
    };


    /*
     * Search in users
     */
    $scope.search = function(query,per_page) {
        
        GroupService.search(query,per_page.id).then(function(data){
            $scope.groups = data;
            $scope.pagination = $scope.groups.metadata;
            $scope.maxSize = 5;
        });
    };

    /*
     * Download Export
     */
    $scope.export = function (selection,export_type) {
        SweetAlert.swal($rootScope.exportSelect,
            function (isConfirm) {
                if (isConfirm) {
                    var recordType = $("input[name=exportSelect]:checked").val();
                    GroupService.downloadExport(recordType,selection,export_type);
                }
            });
    };
    
    
    $scope.status = function(status,id){
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/status',
           data: {'status':status,'id':id },
        }).then(function successCallback(response) {
            $rootScope.$broadcast('group.status');
            //location.reload();
           
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }
    
    
    $scope.$on('group.status', function () {
        Notification({message: 'Group Status Change', templateUrl: 'app/vendors/angular-ui-notification/tpl/success.tpl.html'}, 'success');
        location.reload();
        $scope.isDisabled = false;
    });

    /**********************************************************
     * Event Listener
     **********************************************************/
        // Get list of selected user to do actions
    $scope.selection=[];
    $scope.toggleSelection = function toggleSelection(userId) {
        // toggle selection for a given user by Id
        var idx = $scope.selection.indexOf(userId);
        // is currently selected
        if (idx > -1) {
            $scope.selection.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.selection.push(userId);
        }
    };

    // update list when user deleted
    $scope.$on('group.delete', function() {
        SweetAlert.swal($rootScope.recordDeleted);//define in AdminCtrl
        GroupService.list().then(function(data){
            $location.path('/admin/groups');
            $scope.groups =data;
            $scope.selection=[];
        });
    });

    // update list when user not deleted
    $scope.$on('group.not.delete', function() {
        SweetAlert.swal($rootScope.recordNotDeleted);//define in AdminCtrl
        GroupService.list().then(function(data){
            $scope.groups =data;
            $scope.selection=[];
        });
    });



});
