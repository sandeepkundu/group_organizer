"use strict";

var app = angular.module('ng-laravel',['dropzone']);

app.directive('jqdatepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
         link: function (scope, element, attrs, ngModelCtrl) {
            element.datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function (date) {
                    scope.event.event_date = date;
                    scope.$apply();
                }
            });
            
            element.css('position', 'relative');
            element.css('z-index',9999);
        }
    };
});


app.controller('GroupEventCtrl',function($scope,GroupService,RoleService,$stateParams,$http,resolvedItems,$translatePartialLoader,Notification,trans){

    
    $scope.event = resolvedItems;
    
    /*
     * Get user and refresh cache.
     * At first check cache, if exist, we return data from cache and if don't exist return from API
     */
    GroupService.show($stateParams.id).then(function(data) {
        $scope.group = data;
       
    });

   
    /*
     * Update user
     */
    $scope.update = function(group) {
       // $scope.isDisabled = true;
        GroupService.update(group);
    };

    $scope.delete_event = function(id) {
    //console.log(id);  
    $http({
        method: 'POST',
        url: 'laravel-backend/public/api/event/delete',
        data: {id:id}
      }).then(function successCallback(response) {
           location.reload();
        }, function errorCallback(response) {
          // called asynchronously if an error occurs
          // or server returns response with an error status.
        });
    };
    
    $scope.create_event = function(event) {
       //console.log(event);
       $http({
                method: 'POST',
                url: 'laravel-backend/public/api/event/add',
                data: event
              }).then(function successCallback(response) {
                   location.reload();
                }, function errorCallback(response) {
                  // called asynchronously if an error occurs
                  // or server returns response with an error status.
                });
     };
     
     $scope.show_event = function(id) {
       
       $http({
                method: 'POST',
                url: 'laravel-backend/public/api/event/show',
                data: {id:id}
              }).then(function successCallback(response) {
                   console.log(response);
                }, function errorCallback(response) {
                  // called asynchronously if an error occurs
                  // or server returns response with an error status.
                });
     };
    /*
     * Dropzone file uploader initial
     */
    $scope.dropzoneConfig = {
        options: { // passed into the Dropzone constructor
            url: '../laravel-backend/public/api/uploadimage',
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: .5, // MB
            acceptedFiles: 'image/jpeg,image/png,image/gif',
            maxFiles: 1,
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            addRemoveLinks: true,
            dictDefaultMessage: '<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
            dictResponseError: 'Error while uploading file!',
        },
        'eventHandlers': {
            'removedfile': function (file,response) {
                $http({
                    method : "POST",
                    url : "../laravel-backend/public/api/deleteimage/"+ $scope.group.avatar_url
                }).then(function mySucces(response) {
                    $scope.deleteMessage = response.data;
                    $scope.group.avatar_url='';
                });
            },
            'success': function (file, response) {
                $scope.group.avatar_url = response.filename;
            }
        }
    };


    /********************************************************
     * Event Listeners
     * User event listener related to UserEditCtrl
     ********************************************************/
    // Edit user event listener
    $scope.$on('group.edit', function(scope, group) {
        $scope.group = group;
    });

    // Update user event listener
    $scope.$on('group.update', function() {
        Notification({message: 'group.form.groupUpdateSuccess' ,templateUrl:'app/vendors/angular-ui-notification/tpl/success.tpl.html'},'success');
        $scope.isDisabled = false;
    });

    // user form validation event listener
    $scope.$on('group.validationError', function(event,errorData) {
        Notification({message: errorData ,templateUrl:'app/vendors/angular-ui-notification/tpl/validation.tpl.html'},'warning');
        $scope.isDisabled = false;
    });
});

