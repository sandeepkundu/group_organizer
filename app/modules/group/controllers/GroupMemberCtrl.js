"use strict";

var app = angular.module('ng-laravel',['dropzone']);


app.controller('GroupMemberCtrl',function($scope,$rootScope,$window,$stateParams,$location,GroupService,$http,$translatePartialLoader,Notification,trans){


    $scope.id = $stateParams.id;
    
    $scope.member_list = function() {
       //console.log($scope.id);
       $http({
                method: 'POST',
                url: 'laravel-backend/public/api/member/list',
                data: {id:$scope.id}
              }).then(function successCallback(response) {
                    $scope.member_list=response.data;
                    if($scope.member_list.length==0){
                        $location.path('/admin/groups');
                    }
                }, function errorCallback(response) {
                  // called asynchronously if an error occurs
                  // or server returns response with an error status.
                });
     };
    
    $scope.member_accept = function(id) {
       //console.log($scope.id);
       
       $http({
                method: 'POST',
                url: 'laravel-backend/public/api/member/accept',
                data: {id:id}
              }).then(function successCallback(response) {
                    $scope.$broadcast('group.accepted');
                    
                }, function errorCallback(response) {
                  // called asynchronously if an error occurs
                  // or server returns response with an error status.
                });
     };
     
     $scope.member_reject = function(id,user_id) {
       //console.log($scope.id);
       $http({
                method: 'POST',
                url: 'laravel-backend/public/api/member/reject',
                data: {id:id}
              }).then(function successCallback(response) {
                   $scope.$broadcast('group.rejected');
                }, function errorCallback(response) {
                  // called asynchronously if an error occurs
                  // or server returns response with an error status.
                });
     };
    

    // Update user event listener
    $scope.$on('group.accepted', function() {
//        console.log("in function");
        Notification({message: 'group.form.memberrequestedaccepted' ,templateUrl:'app/vendors/angular-ui-notification/tpl/success.tpl.html'},'success');
        $location.path('/admin/groups');
//        location.reload();
        $scope.isDisabled = false;
    });
    
    $scope.$on('group.rejected', function() {
        Notification({message: 'group.form.memberrequestedrej' ,templateUrl:'app/vendors/angular-ui-notification/tpl/rejected.tpl.html'},'warning');
//        location.reload(); 
        $location.path('/admin/groups');
        $scope.isDisabled = false;
    });

    // user form validation event listener
    $scope.$on('group.validationError', function(event,errorData) {
        Notification({message: errorData ,templateUrl:'app/vendors/angular-ui-notification/tpl/validation.tpl.html'},'warning');
        $scope.isDisabled = false;
    });
});

