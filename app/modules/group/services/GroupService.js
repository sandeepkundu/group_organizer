'use strict';

angular.module('ng-laravel').service('GroupService', function($rootScope, Restangular,CacheFactory) {
    /*
     * Build collection /user
     */
    var _groupService = Restangular.all('group');
    if (!CacheFactory.get('groupsCache')) {
        var groupsCache = CacheFactory('groupsCache');
    }

    /*
     * Get list of users from cache.
     * if cache is empty, data fetched and cache create else retrieve from cache
     */
    this.cachedList = function() {
        // GET /api/user
     if(groupsCache){
        groupsCache.removeAll();
    }
        
        if (!groupsCache.get('list')) {
            return this.list();
        } else{
            return groupsCache.get('list');
        }
    };
     

    /*
     * Get list of users
     */
    this.list = function() {
        // GET /api/user
        
        var data = _groupService.getList();
        groupsCache.put('list',data);
        return data;
    };



    /*
     * Pagination change
     */
    this.pageChange = function(pageNumber,per_page) {
        // GET /api/user?page=2
        return _groupService.getList({page:pageNumber,per_page:per_page});
    };


    this.cachedShow = function(id) {
        // GET /api/user/:id
        if (!groupsCache.get('show'+id)) {
            return this.show(id);
        } else{
            return groupsCache.get('show'+id);
        }
    };

    /*
     * Show specific user by Id
     */
    this.show = function(id) {
        // GET /api/user/:id
        //console.log("yash");
        var data = _groupService.get(id);
        groupsCache.put('show'+id,data);
        return data;
    };
    
   

    /*
     * Create user (POST)
     */
    this.create = function(group) {
        // POST /api/user/:id
        _groupService.post(group).then(function() {
            $rootScope.$broadcast('group.create');
        },function(response) {
            
            $rootScope.$broadcast('group.validationError',response.data.error);
        });
    };


    /*
     * Update user (PUT)
     */
    this.update = function(group) {
        // PUT /api/user/:id
        group.put().then(function() {
            if(groupsCache){
                groupsCache.removeAll();
            }
            $rootScope.$broadcast('group.update');
        },function(response) {
            $rootScope.$broadcast('group.validationError',response.data.error);
        });
    };
    
    
//    this.create_event = function(event) {
//        // PUT /api/user/:id
//       
//        _groupService.post(group).then(function() {
//            $rootScope.$broadcast('group.create');
//        },function(response) {
//            
//            $rootScope.$broadcast('group.validationError',response.data.error);
//        });
//    };
    

    /*
     * Delete user
     * To delete multi record you should must use 'Restangular.several'
     */
    this.delete = function(selection) {
        // DELETE /api/user/:id
        
        Restangular.several('group',selection).remove().then(function() {
            $rootScope.$broadcast('group.delete');
        },function(response){
            $rootScope.$broadcast('group.not.delete');
        });
    };

    /*
     * Delete event
     */
    this.delete_event = function(selection) {
        // DELETE /api/user/:id
//        console.log(selection);
//        Restangular.several('group',selection).remove().then(function() {
//            $rootScope.$broadcast('group.delete');
//        },function(response){
//            $rootScope.$broadcast('group.not.delete');
//        });
    };
    
    /*
     * Search in users
     */
    this.search = function(query,per_page) {
        // GET /api/user/search?query=test&per_page=10
        if(query !=''){
            return _groupService.customGETLIST("search",{query:query, per_page:per_page});
        }else{
            return _groupService.getList();
        }
    };


    /*
     * Download Exported File
     */
    this.downloadExport = function(recordType,selection,export_type){
        _groupService.withHttpConfig({responseType: 'blob'}).customGET('export/file',{record_type:recordType,export_type:export_type,'selection[]':selection}).then(function(response) {
            var url = (window.URL || window.webkitURL).createObjectURL(response);
            var anchor = document.createElement("a");
            document.body.appendChild(anchor);//required in FF, optional for Chrome
            anchor.download = "exportfile."+export_type;
            anchor.href = url;
            anchor.click();
        })
    };


    /*
     * Import Data From File
     */
    this.fetchFields = function(fileName){
       return _groupService.customPOST({importname: "fetch"},'import_excel_csv',{file_name:fileName,module_name:'users'},{});
    };
    //
    this.importData = function(mapform){
        return _groupService.customPOST({importname: "import"},'import_excel_csv_database', mapform,{});
    };


    /*
     * Edit user profile
     */
    this.editProfile = function (group) {
        return _groupService.customPUT({}, group.id + '/profile',group,{}).then(function (data) {
            $rootScope.$broadcast('group.updateProfile');
        },function (error) {
            $rootScope.$broadcast('group.validationError',error.data.error);
        });
    }

});

