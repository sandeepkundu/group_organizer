"use strict";

var app = angular.module('ng-laravel',['dropzone','angularCountryState']);
app.controller('UserCreateCtrl',function($scope,UserService,$window,$location,CategoryService,GroupService,$http,$rootScope,$translatePartialLoader,trans,Notification){


    /*
     * Define initial value
     */
    $scope.user={};
    $scope.user.avatar_url='';


    /*
     * Create a user
     */
    $scope.create = function(user) {

        $scope.isDisabled = true;
        UserService.create(user);
    };

    $scope.get_group=function (){
       
       $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/get_list',
            
        }).then(function successCallback(response) {
           $scope.groupnames = response.data.group;
           $scope.categories = response.data.category;
        }, function errorCallback(response) {
            
        }); 
    };
    $scope.get_group();
//    $scope.categories = resolvedItems;
//    $scope.groupnames = resolvedItems1;
    
    
    $scope.dropzoneConfig = {
        options: { // passed into the Dropzone constructor
            url: '../laravel-backend/public/api/uploadimage',
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: .5, // MB
            acceptedFiles: 'image/jpeg,image/png,image/gif',
            maxFiles: 1,
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            addRemoveLinks: true,
            dictDefaultMessage: '<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
            dictResponseError: 'Error while uploading file!',
        },
        'eventHandlers': {
            'removedfile': function (file,response) {
                $http({
                    method : "POST",
                    url : "../laravel-backend/public/api/deleteimage/"+$scope.user.avatar_url
                }).then(function mySucces(response) {
                    $scope.deleteMessage = response.data;
                    $scope.user.avatar_url='';
                });
            },
            'success': function (file, response) {
                if(file.width < 400 ||  file.height<400){
                    $rootScope.$broadcast('user.imageError');
                    //swal({title:"Please Upload Image Higher than 400x400 Resolution",text:$rootScope.areYouSureDelete});
                    this.removeAllFiles();
                }
                $scope.user.avatar_url = response.filename;
            }
        }
    };

    
    $scope.$on('user.imageError', function(event) {
        
        Notification({message: 'Please upload image bigger then 400x400 pixels' ,templateUrl:'app/vendors/angular-ui-notification/tpl/rejected.tpl.html'},'warning');
        $scope.isDisabled = false;
    });
    
    $scope.countriesmn = new Array("Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czeck Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia, The", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcaim Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romainia", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Spratly Islands", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "United States", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");

        $scope.updateState=function() {
                
             var indexCountry = $scope.countriesmn.indexOf($scope.user.country);
//             console.log(indexCountry);

             $scope.states= BlockID(indexCountry+1).split("|");

//             console.log($scope.states);
            if($scope.states.length == 1){
                $scope.states =$scope.states;
            }
            //indexCountry correspond to the "Select" label
            $scope.user.state=$scope.states['0'];
            if(indexCountry == 0){
                $scope.states = new Array("");
            }
           //$scope.states = ['Afghanistan','Afghanistans','Afghanistanasd'];
        }
        $scope.$watch('countriesmn', function() {
         
        var indexCountry = $scope.countriesmn.indexOf($scope.user.country);
        
        $scope.states= BlockID(indexCountry+1).split("|");
        
        if($scope.states.length >= 1){
            $scope.states =$scope.states;
        }
        if(indexCountry == 0){
            $scope.states = new Array("");
        }

    }); 

    /********************************************************
     * Event Listeners
     * user event listener related to UserCreateCtrl
     ********************************************************/
    // Create user event listener
    $scope.$on('user.create', function() {
        $scope.user ={};
        //$rootScope.$broadcast('dropzone.removeallfile');
        Notification({message: 'user.form.userAddSuccess' ,templateUrl:'app/vendors/angular-ui-notification/tpl/success.tpl.html'},'success');
//        $state.go(admin.users, params, options);
        $location.path('/admin/users');
        $scope.isDisabled = false;
    });

    //Validation error in create user event listener
    $scope.$on('user.validationError', function(event,errorData) {
        
        Notification({message: errorData ,templateUrl:'app/vendors/angular-ui-notification/tpl/validation.tpl.html'},'warning');
        $scope.isDisabled = false;
    });
    

    
});