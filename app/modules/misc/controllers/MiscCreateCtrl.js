"use strict";

var app = angular.module('ng-laravel',['ui.tinymce']);
app.controller('MiscCreateCtrl',function($scope,$window,$stateParams,$location,$http,$rootScope,$translatePartialLoader,trans,Notification){
    
    $scope.tinymceOptions = {
        menubar:false,
        skin: 'light',
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak code  ",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking spellchecker",
            "fullscreen table contextmenu directionality emoticons paste textcolor  "
        ],
        image_advtab: true,
        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | insertdatetime nonbreaking spellchecker contextmenu directionality emoticons paste textcolor codemirror | image | media | link unlink anchor | print preview |  forecolor backcolor | hr anchor pagebreak searchreplace wordcount visualblocks visualchars | code | fullscreen |  styleselect | fontselect fontsizeselect | table | cut copy paste",
    };  
    
    
    $scope.create = function(email) {

       $http({
                method: 'POST',
                url: 'laravel-backend/public/api/email/create',
                data: email,
              }).then(function successCallback(response) {
                   $rootScope.$broadcast('email.create');
                }, function errorCallback(response) {
                   $rootScope.$broadcast('email.validationError',response.data.error);
                });
     };
    
    
    
    
    /********************************************************
     * Event Listeners
     * user event listener related to UserCreateCtrl
     ********************************************************/
    // Create user event listener
    $scope.$on('email.create', function() {
        Notification({message: 'misc.form.emailAddSuccess' ,templateUrl:'app/vendors/angular-ui-notification/tpl/success.tpl.html'},'success');
        $location.path('/admin/miscellaneous');
        $scope.isDisabled = false;
    });

    //Validation error in create user event listener
    $scope.$on('email.validationError', function(event,errorData) {
        
        Notification({message: errorData ,templateUrl:'app/vendors/angular-ui-notification/tpl/validation.tpl.html'},'warning');
        $scope.isDisabled = false;
    });
    
});