"use strict";

var app = angular.module('ng-laravel');
//var app = angular.module('ng-laravel',['ui.tinymce']);
app.controller('MiscEditCtrl',function($scope,$rootScope,$window,$location,$stateParams,$http,$translatePartialLoader,Notification,trans){
    
      $scope.tinymceOptions = {
        menubar:false,
        skin: 'light',
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak code  ",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking spellchecker",
            "fullscreen table contextmenu directionality emoticons paste textcolor  "
        ],
        image_advtab: true,
        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | insertdatetime nonbreaking spellchecker contextmenu directionality emoticons paste textcolor codemirror | image | media | link unlink anchor | print preview |  forecolor backcolor | hr anchor pagebreak searchreplace wordcount visualblocks visualchars | code | fullscreen |  styleselect | fontselect fontsizeselect | table | cut copy paste",
    };   
    

    angular.element(document).ready(function () {

       $http({
                method: 'POST',
                url: 'laravel-backend/public/api/email/show',
                data: {'id':$stateParams.id},
              }).then(function successCallback(response) {
                   $scope.email=response.data;
                   $scope.tinymceModel = $scope.email.email_body;
//                   console.log($scope.email);
                }, function errorCallback(response) {
                  // called asynchronously if an error occurs
                  // or server returns response with an error status.
                });
     });
     
     //For update the email
    $scope.update = function(email) {

       $http({
                method: 'POST',
                url: 'laravel-backend/public/api/email/update',
                data: email,
              }).then(function successCallback(response) {
                   $rootScope.$broadcast('email.update');
                }, function errorCallback(response) {
                   $rootScope.$broadcast('email.validationError',response.data.error);
                });
     };
     
     
     
    $scope.$on('email.update', function() {
        Notification({message: 'misc.form.emailUpdateSuccess' ,templateUrl:'app/vendors/angular-ui-notification/tpl/success.tpl.html'},'success');
        $location.path('/admin/miscellaneous');
        $scope.isDisabled = false;
    });

    // user form validation event listener
    $scope.$on('email.validationError', function(event,errorData) {
        Notification({message: errorData ,templateUrl:'app/vendors/angular-ui-notification/tpl/validation.tpl.html'},'warning');
        $scope.isDisabled = false;
    });
    

    
     
});

