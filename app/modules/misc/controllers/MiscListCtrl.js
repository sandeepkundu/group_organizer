"use strict";

var app = angular.module('ng-laravel', ['ui.bootstrap']);
app.controller('MiscListCtrl', function ($scope,$http, SweetAlert,Restangular, $translatePartialLoader, $translate, $rootScope, trans) {


    $scope.getlist=function(){
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/email/index',
            
        }).then(function successCallback(response) {
            $scope.email_details=response.data;
        }, function errorCallback(response) {
            
        });
    }
    
    
    $scope.delete = function(id) {
        swal({title:"Are you sure?"},
        function(isConfirm){
       $http({
                method: 'POST',
                url: 'laravel-backend/public/api/email/delete',
                data: {id:id},
              }).then(function successCallback(response) {
                   location.reload();
                }, function errorCallback(response) {
                   
                });
                
        });
     };
     
     
     
 
});
