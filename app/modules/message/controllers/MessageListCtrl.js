"use strict";

var app = angular.module('ng-laravel', ['ui.bootstrap']);
app.controller('MessageListCtrl', function ($scope,$http, UserService, SweetAlert, resolvedItems, $translatePartialLoader, $translate, $rootScope, trans) {
    $('#loader').hide();
    /*
     * Define initial value
     */
  
    /*
     * Get all users
     * Get from resolvedItems function in this page route (config.router.js)
     */
    $scope.users = resolvedItems;
    $scope.pagination = $scope.users.metadata;
    $scope.maxSize = 5;


    /*
     * Get all Task and refresh cache.
     * At first check cache, if exist, we return data from cache and if don't exist return from API
     */
    UserService.list().then(function (data) {
        $scope.users = data;
        $scope.pagination = $scope.users.metadata;
    });
    
    
    $scope.search_usertype = function(user_type) {
//    console.log(user_type);  
    $http({
        method: 'POST',
        url: 'laravel-backend/public/api/user/search_usertype',
        data: {type:user_type}
      }).then(function successCallback(response) {
//          location.reload();
//        console.log(response);
          $scope.users=response.data.data;
        }, function errorCallback(response) {
          // called asynchronously if an error occurs
          // or server returns response with an error status.
        });
    };


    /*
     * Remove selected users
     */
    $scope.delete = function (user) {
        //console.log(user);
        SweetAlert.swal($rootScope.areYouSureDelete,
                function (isConfirm) {
                    if (isConfirm) {
                        //console.log(user);
                        UserService.delete(user);
                    }
                });
    };

    

   
    /*
     * Pagination user list
     */
    $scope.units = [
        {'id': 10, 'label': 'Show 10 Item Per Page'},
        {'id': 15, 'label': 'Show 15 Item Per Page'},
        {'id': 20, 'label': 'Show 20 Item Per Page'},
        {'id': 30, 'label': 'Show 30 Item Per Page'},
    ]
    $scope.perPage = $scope.units[0];
    $scope.pageChanged = function (per_page) {
        UserService.pageChange($scope.pagination.current_page, per_page.id).then(function (data) {
            $scope.users = data;
            $scope.pagination = $scope.users.metadata;
            $scope.maxSize = 5;
        });
    };


    /*
     * Search in users
     */
    $scope.search = function (query, per_page) {
        UserService.search(query, per_page.id).then(function (data) {
            $scope.users = data;
            $scope.pagination = $scope.users.metadata;
            $scope.maxSize = 5;
        });
    };

    /*
     * Download Export
     */
    $scope.export = function (selection, export_type) {
        SweetAlert.swal($rootScope.exportSelect,
                function (isConfirm) {
                    if (isConfirm) {
                        var recordType = $("input[name=exportSelect]:checked").val();
                        UserService.downloadExport(recordType, selection, export_type);
                    }
                });
    };


    /**********************************************************
     * Event Listener
     **********************************************************/
    // Get list of selected user to do actions
    $scope.selection = [];
    $scope.toggleSelection = function toggleSelection(userId) {
        // toggle selection for a given user by Id
        var idx = $scope.selection.indexOf(userId);
        // is currently selected
        if (idx > -1) {
            $scope.selection.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.selection.push(userId);
        }
    };

    // update list when user deleted
    $scope.$on('user.delete', function () {
        SweetAlert.swal($rootScope.recordDeleted);//define in AdminCtrl
        UserService.list().then(function (data) {
            $scope.users = data;
            $scope.selection = [];
        });
    });

    // update list when user not deleted
    $scope.$on('user.not.delete', function () {
        SweetAlert.swal($rootScope.recordNotDeleted);//define in AdminCtrl
        UserService.list().then(function (data) {
            $scope.users = data;
            $scope.selection = [];
        });
    });

    
    // for multiple mail
    $scope.multi_mail = function (email) {
        $scope.form_title = "Email Details";
        $scope.employee = {'email':email};
        $('#myModal').modal('show');
    }




    //show modal form
    $scope.toggle = function (email) {
        //$scope.modalstate = modalstate;
        $scope.form_title = "Email Details";
        $scope.employee = {'email':email};
        $('#myModal').modal('show');
    }

    //save new record / update existing record
    $scope.send_mail = function (mail) {
//        console.log(mail);
        $('#loader').show();
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/user_mail/send',
            data: mail
            
        }).then(function successCallback(response) {
            //$scope.cms=response.data;
            $scope.employee.email = null;
            $scope.employee.subject = null;
            $scope.employee.message = null;
            $('#loader').hide();
            $('#myModal').modal('hide');
            SweetAlert.swal({ title: "Success", text:'Mail has been sent', type: "success"}); 
            location.reload();
        }, function errorCallback(response) {
            SweetAlert.swal({ title: "Error", text: displayError(response.data.error) , type: "error", html:true});
//            $rootScope.$broadcast('user.validationError',response.data.error);
        });

    }

    function displayError(param) {
//        console.log(param);
        var str = '';
        angular.forEach(param, function (value, key) {
            angular.forEach(value, function (v, k) {
                str +='<spam style=color:red;>* '+ v +'<br></spam>';
            })
        });
        return str;
    }

});
