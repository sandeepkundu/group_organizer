"use strict";

var app = angular.module('ng-laravel',['dropzone']);
app.controller('CategoryCtrl',function($scope,$http,CategoryService,$window,SweetAlert,Restangular,$rootScope,resolvedItems,$translatePartialLoader,Notification,trans){

    /*
     * Define initial value
     */
    $scope.category={};
    $window.scrollTo(0, 0);

    /*
     * Get all categories
     * Get from resolvedItems function in this page route (config.router.js)
     */
    $scope.categories = resolvedItems;
    $scope.pagination = $scope.categories.metadata;
    //$scope.maxSize = 5;


    /*
     * Get all category and refresh cache.
     * At first check cache, if exist, we return data from cache and if don't exist return from API
     */
//    CategoryService.list().then(function(data){
//        $scope.categories = data;
//    });


    /*
     * Create a category
     */
    $scope.single_record=0;
    
    $scope.get_cat = function () {
//        console.log($scope.profile.id);
        $scope.single_record = $scope.single_record + 5;

        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/get_cat',
            data: {'records':$scope.single_record},
        }).then(function successCallback(response) {
           $scope.cat = response.data;
           
           $('#loader').hide();
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    $scope.get_cat();
    
    
    $scope.create = function(category) {
        $scope.notification={};
        $scope.isDisabled = true;
        CategoryService.create(category);
    };


    /*
     * Remove selected customers
     */
    $scope.delete = function(category) {
        //console.log(category);
       swal({title:"Are you sure?",text:$rootScope.areYouSureDelete},//define in AdminCtrl
        function(isConfirm){
            if (isConfirm) {
                CategoryService.delete(category);
            }
        });
    };
    
    $scope.dropzoneConfig = {
        
        options: { // passed into the Dropzone constructor
            url: '../laravel-backend/public/api/uploadimage',
            paramName: "file", // The name that will be used to transfer the file
            
            maxFilesize: 2, // MB
            acceptedFiles: 'image/jpeg,image/png,image/gif',
            maxFiles: 5,
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            
            addRemoveLinks: true,
            dictDefaultMessage: '<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
            dictResponseError: 'Error while uploading file!',
        },
        
        'eventHandlers': {
            
            'removedfile': function (file,response) {
                $http({
                    method : "POST",
                    url : "../laravel-backend/public/api/deleteimage/"+ $scope.category.avatar_url
                }).then(function mySucces(response) {
                    $scope.deleteMessage = response.data;
                    $scope.category.avatar_url='';
                });
            },
            'success': function (file, response) {
                if(file.width < 400 ||  file.height<400){
                    swal({title:"Please Upload Image Higher than 400x400 Resolution",text:$rootScope.areYouSureDelete});
                    this.removeAllFiles();
                }
                $scope.category.avatar_url = response.filename;
            }
        }
    };
    
    
    
    $scope.delete = function(category) {
        category={id:category};
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this Category!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#CAE197",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false,
          html: false
            }, function(){
            

        return $http.post('laravel-backend/public/api/delete/category', category)
        .success(function(category){
        if(category=="success"){
        swal("Deleted!", "Category delete successfully!", "success");
            $rootScope.$broadcast('category.delete');
            
        }else{
            swal("Error", "Error while deleting Category!", "error");
        }
            });
        });
    };
    
    


    /*
     * Edit mode category - Copy category to edit form
     */
    $scope.edit = function(category) {
        var categoryCopy = Restangular.copy(category);
        $rootScope.$broadcast('category.edit', categoryCopy);
    };


    /*
     * Update category
     */
    $scope.update = function(category) {
        $scope.isDisabled = true;
        CategoryService.update(category);
        location.reload();
//        $scope.category.avatar_url = '';
    };


    /**********************************************************
     * Event Listener
     ***********************************************************/
    // Create category event listener
    $scope.$on('category.create', function() {
        CategoryService.list().then(function(data){
            $scope.categories = data;
            location.reload();
            $scope.category={};
            Notification({message: 'category.form.categoryAddSuccess' ,templateUrl:'app/vendors/angular-ui-notification/tpl/success.tpl.html'},'success');
            $scope.isDisabled = false;
        });
    });

    //Validation error in create category event listener
    $scope.$on('category.validationError', function(event,errorData) {
        Notification({message: errorData ,templateUrl:'app/vendors/angular-ui-notification/tpl/validation.tpl.html'},'warning');
        $scope.isDisabled = false;
    });

    // update list when category deleted
    $scope.$on('category.delete', function() {
        SweetAlert.swal($rootScope.recordDeleted);//define in AdminCtrl
        CategoryService.list().then(function(data){
            $scope.categories =data;
            $scope.selection=[];
        });
    });

    // update list when category not deleted
    $scope.$on('category.not.delete', function() {
        SweetAlert.swal($rootScope.recordNotDeleted);//define in AdminCtrl
        CategoryService.list().then(function(data){
            $scope.categories =data;
            $scope.selection=[];
        });
    });

    // copy category to form for update
    $scope.$on('category.edit', function(scope, category) {
        $scope.category = category;
        $scope.updateMode= true;
    });

    // Update category event listener
    $scope.$on('category.update', function() {
        Notification({message: 'category.form.categoryUpdateSuccess' ,templateUrl:'app/vendors/angular-ui-notification/tpl/success.tpl.html'},'success');
        $scope.isDisabled = false;
        $scope.category={};
        CategoryService.list().then(function(data){
            $scope.categories = data;
        })
    });
});