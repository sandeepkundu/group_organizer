"use strict";

var app = angular.module('ng-laravel',['dropzone']);
app.controller('CategoryEditCtrl',function($scope,$http,$state,$window,SweetAlert,CategoryService,$stateParams,Restangular,$rootScope,resolvedItems,$translatePartialLoader,Notification,trans){


    $window.scrollTo(0, 0);
    /*
     * Define initial value
     */
    $scope.category_id=$stateParams.id;
    
    $scope.categories_id = CategoryService.show($scope.category_id);
    $scope.category = $scope.categories_id.$object;
//    console.log($scope.category);
    /*
     * Get all categories
     * Get from resolvedItems function in this page route (config.router.js)
     */
    $scope.categories = resolvedItems;
    
   
//    console.log($scope.category);
    /*
     * Create a category
     */
    $scope.create = function(category) {
        $scope.notification={};
        $scope.isDisabled = true;
        CategoryService.create(category);
    };

    
    
    $scope.dropzoneConfig = {
        
        options: { // passed into the Dropzone constructor
            url: '../laravel-backend/public/api/uploadimage',
            paramName: "file", // The name that will be used to transfer the file
            minFilesize: 2, // MB
            acceptedFiles: 'image/jpeg,image/png,image/gif',
            maxFiles: 5,
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            
            addRemoveLinks: true,
            dictDefaultMessage: '<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
            dictResponseError: 'Error while uploading file!',
        },
        
        'eventHandlers': {
            'removedfile': function (file,response) {
                $http({
                    method : "POST",
                    url : "../laravel-backend/public/api/deleteimage/"+ $scope.category.avatar_url
                }).then(function mySucces(response) {
                    $scope.deleteMessage = response.data;
                    $scope.category.avatar_url='';
                });
            },
            'success': function (file, response) {
                if(file.width < 400 ||  file.height<400){
                    swal({title:"Please Upload Image Higher than 400x400 pixels",text:$rootScope.areYouSureDelete});
                    this.removeAllFiles();
                }
                $scope.category.avatar_url = response.filename;
            }
        }
    };
    
    

    /*
     * Update category
     */
    $scope.update = function(category) {
        $http({
        method: 'POST',
        url: 'laravel-backend/public/api/category/update',
        data: {'id':category.id,'category_name':category.category_name,'avatar_url':category.avatar_url}
      }).then(function successCallback(response) {
          CategoryService.update(category);
          $rootScope.$broadcast('category.update');
        }, function errorCallback(response) {
          // called asynchronously if an error occurs
          // or server returns response with an error status.
        });



    };
//        $scope.update = function(category) {
//            $scope.isDisabled = true;
//            CategoryService.update(category);
//            $rootScope.$broadcast('category.update');
//    //        $scope.category.avatar_url = '';
//        };

    /**********************************************************
     * Event Listener
     ***********************************************************/
    // Create category event listener
    $scope.$on('category.create', function() {
        CategoryService.list().then(function(data){
            $scope.categories = data;
            location.reload();
            $scope.category={};
            Notification({message: 'category.form.categoryAddSuccess' ,templateUrl:'app/vendors/angular-ui-notification/tpl/success.tpl.html'},'success');
            $scope.isDisabled = false;
        });
    });

    //Validation error in create category event listener
    $scope.$on('category.validationError', function(event,errorData) {
        Notification({message: errorData ,templateUrl:'app/vendors/angular-ui-notification/tpl/validation.tpl.html'},'warning');
        $scope.isDisabled = false;
    });

    // update list when category deleted
    $scope.$on('category.delete', function() {
        SweetAlert.swal($rootScope.recordDeleted);//define in AdminCtrl
        CategoryService.list().then(function(data){
            $scope.categories =data;
            $scope.selection=[];
        });
    });

    // update list when category not deleted
    $scope.$on('category.not.delete', function() {
        SweetAlert.swal($rootScope.recordNotDeleted);//define in AdminCtrl
        CategoryService.list().then(function(data){
            $scope.categories =data;
            $scope.selection=[];
        });
    });


    // Update category event listener
    $scope.$on('category.update', function() {
        Notification({message: 'category.form.categoryUpdateSuccess' ,templateUrl:'app/vendors/angular-ui-notification/tpl/success.tpl.html'},'success');
        $scope.isDisabled = false;
//        $window.location.href = '/bevylife/#/admin/categories';
        $state.go('admin.categories');
        
    });
});