"use strict";

var app = angular.module('ng-laravel',['ui.tinymce','dropzone']);
app.controller('CmsEditCtrl',function($scope,$rootScope,$window,$location,$stateParams,$http,$translatePartialLoader,Notification,trans){
    
      $scope.tinymceOptions = {
        menubar:false,
        skin: 'light',
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak code  ",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking spellchecker",
            "fullscreen table contextmenu directionality emoticons paste textcolor  "
        ],
        image_advtab: true,
        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | insertdatetime nonbreaking spellchecker contextmenu directionality emoticons paste textcolor codemirror | image | media | link unlink anchor | print preview |  forecolor backcolor | hr anchor pagebreak searchreplace wordcount visualblocks visualchars | code | fullscreen |  styleselect | fontselect fontsizeselect | table | cut copy paste",
    };   
    //Check for cache for cms
    this.cachedShow = function(id) {
        // GET /api/user/:id
        if (!cmsCache.get('show'+id)) {
            return this.show(id);
        } else{
            return cmsCache.get('show'+id);
        }
    };


    
    //For show the CMS
    $scope.show = function() {

       $http({
                method: 'POST',
                url: 'laravel-backend/public/api/cms/show',
                data: {'id':$stateParams.id},
              }).then(function successCallback(response) {
                  
                  $.each(response.data,function(k,v){
                    //console.log(v);
                    $scope.cms=v;
                   $scope.tinymceModel = $scope.cms.description;
                   });
 
                }, function errorCallback(response) {
                  // called asynchronously if an error occurs
                  // or server returns response with an error status.
                });
     };
     
     //For update the CMS
    $scope.update = function(cms) {

       $http({
                method: 'POST',
                url: 'laravel-backend/public/api/cms/update',
                data: cms,
              }).then(function successCallback(response) {
                   $rootScope.$broadcast('cms.update');
                }, function errorCallback(response) {
                   $rootScope.$broadcast('cms.validationError',response.data.error);
                });
     };
     
     
    
    /*
     * Dropzone file uploader initial
     */
    $scope.dropzoneConfig = {
        options: { // passed into the Dropzone constructor
            url: '../laravel-backend/public/api/uploadimage',
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: .5, // MB
            acceptedFiles: 'image/jpeg,image/png,image/gif',
            maxFiles: 20,
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            addRemoveLinks: true,
            dictDefaultMessage: '<i class="upload-icon fa fa-cloud-upload blue fa-3x"></i>',
            dictResponseError: 'Error while uploading file!',
        },
        'eventHandlers': {
            'removedfile': function (file,response) {
                $http({
                    method : "POST",
                    url : "../laravel-backend/public/api/deleteimage/"+ $scope.cms.image
                }).then(function mySucces(response) {
                    $scope.deleteMessage = response.data;
                    $scope.cms.image='';
                });
            },
            'success': function (file, response) {
                if(file.width < 400 ||  file.height<400){
                    $rootScope.$broadcast('user.imageError');
                    //swal({title:"Please Upload Image Higher than 400x400 Resolution",text:$rootScope.areYouSureDelete});
                    this.removeAllFiles();
                }
                $scope.cms.image = response.filename;
            }
        }
    };
    
    $scope.$on('user.imageError', function(event) {
        
        Notification({message: 'Please upload image bigger then 400x400 pixels' ,templateUrl:'app/vendors/angular-ui-notification/tpl/rejected.tpl.html'},'warning');
        $scope.isDisabled = false;
    });
    
    $scope.$on('cms.update', function() {
        Notification({message: 'cms.form.cmsUpdateSuccess' ,templateUrl:'app/vendors/angular-ui-notification/tpl/success.tpl.html'},'success');
        $location.path('/admin/cms');
        $scope.isDisabled = false;
    });

    // user form validation event listener
    $scope.$on('cms.validationError', function(event,errorData) {
        Notification({message: errorData ,templateUrl:'app/vendors/angular-ui-notification/tpl/validation.tpl.html'},'warning');
        $scope.isDisabled = false;
    });
    
     
});

