"use strict";

var app = angular.module('ng-laravel', ['ui.bootstrap']);
app.controller('CmsListCtrl', function ($scope,$http, SweetAlert,Restangular,$window, $translatePartialLoader, $translate, $rootScope, trans) {

    $window.scrollTo(0, 0);
    $scope.getlist=function(){
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/cms/index',
            
        }).then(function successCallback(response) {
            $scope.cms_detail=response.data;
            //console.log($scope);
            //location.reload();
        }, function errorCallback(response) {
            
        });
    }
    
 
});
