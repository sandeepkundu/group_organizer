'use strict';

var app = angular.module('ng-laravel');
app.controller('TransactionListCtrl',function($scope,$http){
    
    $scope.list = function() {
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/transaction/index',
          }).then(function successCallback(response) {
          $scope.transactions=response.data.data;
          //console.log($scope.transactions.data);
            }, function errorCallback(response) {

            });
    };
    
    $scope.dtOption1 = {
        responsive: true
    };
    
    $scope.user_type_name='';
    
    //Search User by name
    $scope.search = function(text) {
    $http({
        method: 'POST',
        url: 'laravel-backend/public/api/transaction/search',
        data: {text:text,user_type_name:$scope.user_type_name}
      }).then(function successCallback(response) {

          $scope.transactions=response.data;
        }, function errorCallback(response) {
          // called asynchronously if an error occurs
          // or server returns response with an error status.
        });
    };
    
    // Search user by type
    
    $scope.search_usertype = function(user_type) {
        $scope.user_type_name=user_type;
    $http({
        method: 'POST',
        url: 'laravel-backend/public/api/transaction/search_usertype',
        data: {type:user_type}
      }).then(function successCallback(response) {
          $scope.transactions=response.data.data;
        }, function errorCallback(response) {
          
        });
    };
    
    // Reset Page
    $scope.reset = function(text) {
        location.reload();
    }
 })    