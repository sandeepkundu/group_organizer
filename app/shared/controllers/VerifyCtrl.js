"use strict";

var app = angular.module('ng-laravel');
app.controller('VerifyCtrl',function($scope,$state,$stateParams,Restangular,SweetAlert){
    /*
     * Define initial value
     */
     
    $scope.status = 'loading';

    /**
     * verify code
     */
    Restangular.one('register/verify/', $stateParams.code).get().then(function(response) {
        $scope.status = 'success';
        SweetAlert.swal({ title: "Success", text: response, type: "success"});
        $state.go('home.login');
    },function (error) {
        $scope.status = 'error';
        SweetAlert.swal({ title: "Error", text: error.data , type: "error", html:true});
    })


});