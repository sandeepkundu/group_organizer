"use strict";

var app = angular.module('ng-laravel');
app.controller('DashboardCtrl', function ($scope, $http, resolvedItems, resolvedItems1,$filter) {
    $scope.users = resolvedItems;

    $scope.groups = resolvedItems1;
    //console.log($scope.groups.event);
    $scope.get_data = function () {

        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/group/dashboard',
        }).then(function successCallback(response) {

            $scope.data = response.data;
            //console.log($scope.data.event);
            //getEventData($scope.data);
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    $scope.list = function() {
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/transaction/index',
          }).then(function successCallback(response) {
          $scope.trans=response.data.data;

            }, function errorCallback(response) {

            });
    };
    
    

    //console.log($scope.users);
    $scope.limitRangeDate = 25;
    $scope.maxRangeDate = 20;
    $scope.minRangeDate = 30;

    // custom calendar with ui-jq & ui-options plugin
    $scope.date = new Date();
    $scope.d = $scope.date.getDate();
    $scope.m = $scope.date.getMonth();
    $scope.y = $scope.date.getFullYear();
    
    
    $scope.getdatafrom=[
        
      
            {
                title: 'anshu',
                start: new Date($scope.y, $scope.m, $scope.d),
                allDay: true
            }
          
    ];
    // custom calendar with ui-jq & ui-options plugin
    function getEventData(allDt){
        var objArr=[];
        for(var i in allDt.event){
            
            $scope.date = new Date(allDt.event[i].event_date);
            $scope.d = $scope.date.getDate();
            $scope.m = $scope.date.getMonth();
            $scope.y = $scope.date.getFullYear();
            
            var obj={
                'title': allDt.event[i].name,
                'start': new Date($scope.y, $scope.m, $scope.d),
                'allDay': true
                 };
           objArr.push(obj);
            //console.log(allDt.event[i].event_date);
            //console.log(allDt.event[i].name);
        }
//        console.log(objArr);
         $scope.getdatafroms=objArr;
        
    }
 

    $scope.chartData = [
        {y: '2014', a: 4, b: 1},
        {y: '2015', a: 8, b: 0},
        {y: '2016', a: 5, b: 2},
        {y: '2017', a: 10, b: 0},
        {y: '2018', a: 4, b: 1},
        {y: '2019', a: 16, b: 3},
        {y: '2020', a: 5, b: 1},
        {y: '2021', a: 11, b: 5},
        {y: '2022', a: 6, b: 2},
        {y: '2023', a: 11, b: 3},
        {y: '2024', a: 30, b: 2},
        {y: '2025', a: 13, b: 0},
        {y: '2026', a: 4, b: 2},
        {y: '2027', a: 3, b: 8},
        {y: '2028', a: 3, b: 0},
        {y: '2029', a: 6, b: 0},
    ];

    $scope.chartOptions = {
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Total Income', 'Total Outcome'],
        fillOpacity: 0.6,
        behaveLikeLine: true,
        resize: true,
        pointStrokeColors: ['black'],
        lineColors: ['blue'],
        pointFillColors: ['#00ff00'],
        lineWidth: [1],
        pointSize: [.5],
        hideHover: 'always'
    }
});


// fullCalendar custom directive
app.directive('fullCalendar',function($window){
    return{
        restrict:'A',
        link: function($scope,element,attrs){
            
            var calendar = element.fullCalendar({
                //isRTL: true,
                buttonHtml: {
                    prev: '<i class="fa fa-chevron-left"></i>',
                    next: '<i class="fa fa-chevron-right"></i>'
                },
                  header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: ''
                },
                 events: function(start, end, timezone, callback) {
                    $.ajax({
                        url: 'laravel-backend/public/api/groups/events',
                        method:'GET',
                        dataType: 'json',
                        success: function(doc) {
                            var events = [];

                            $.each(doc,function(k,v) {
                                //console.log(v);
                               var date = new Date(v.event_date);
                                events.push({
                                    title: v.name,
                                    allDay:true,
                                    start: new Date(date.getFullYear(),date.getMonth(),date.getDate())
                                });
                            });
                            callback(events);
                        }
                    });
                },
                editable: false,
                droppable: false,
                selectable: true,
                selectHelper: true,
                select: function(start, end, allDay) {

                   var m = moment(start._d).format('YYYY-MM-DD');;
                    //console.log(m);

                    //$state.go('home.viewallevnt({date:all})');
                    //home.viewallevnt({date:'all'})
                    //$window.location.href = '#/home/all-event/'+m;
                    calendar.fullCalendar('unselect');
                }

            })
        }
    }
});
