"use strict";


var app = angular.module('ng-laravel');
app.controller('homesCtrl',function($scope,$http,SweetAlert,$rootScope,$window){
    $window.scrollTo(0, 0);
    /* show loading on page change */
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if (toState.resolve) {
            $scope.loader = true;
        }
    });
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        if (toState.resolve) {
            $scope.loader = false;
        }
    });
    
    $scope.suggestion='';
    $scope.email ='';
    $scope.year = (new Date()).getFullYear();
    $scope.sugg = function (sugg) {
        
    //    alert(sugg);
        
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/suggestion',
            data: {'sugg': sugg},
        }).then(function successCallback(response) {
            $('#sugg').modal('hide');
            //$('#sugg').hide();
            SweetAlert.swal({ title: "Success", text:'Thank you, your suggestion has been sent!\n\Feedback from our users is very important to us, and we take ample time to review every idea that is submitted. Please know that your suggestion will be carefully considered. One of our team members will get back to you only if we need further clarification or information.', type: "success"});
        }, function errorCallback(response) {
            SweetAlert.swal({ title: "Error", text: response.data.error, type: "error"});
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    $scope.terms_privacy = function () {
        
        $http({
            method: 'GET',
            url: 'laravel-backend/public/api/terms_privacy',
        }).then(function successCallback(response) {
            //$('#sugg').modal('hide');
            //$('#sugg').hide();
            $scope.terms = response.data.terms;
            $('#terms_desc').html(response.data.terms.description);
            $('#privacy_desc').html(response.data.privacy.description);
            $scope.privacy = response.data.privacy;
           // SweetAlert.swal({ title: "Success", text:'Thank you, your suggestion has been sent!\n\Feedback from our users is very important to us, and we take ample time to review every idea that is submitted. Please know that your suggestion will be carefully considered. One of our team members will get back to you only if we need further clarification or information.', type: "success"});
        }, function errorCallback(response) {
            SweetAlert.swal({ title: "Error", text: "Terms of Use and Privacy Policy not found.", type: "error"});
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    $scope.terms_privacy();
    $scope.news = function (email) {
        if(email.length==0){
            SweetAlert.swal({ title: "Error", text: "Please enter Email ID", type: "error"});
        }
    };
 
 
    //$scope.sugg();
    $('#find_group').click(function () {
        if(window.location.hash=='#/home/find-group')
        location.reload();
    });
    
});