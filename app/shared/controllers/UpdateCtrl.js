/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


"use strict";

var app = angular.module('ng-laravel');
app.controller('UpdateCtrl',function($scope,$state,$stateParams,Restangular,SweetAlert){
    /*
     * Define initial value
     */
     
    $scope.status = 'loading';

    /**
     * verify code
     */
    
    Restangular.one('register/update/', $stateParams.code).get().then(function(response) {
        $scope.status = 'success';
        SweetAlert.swal({ title: "Success", text: response, type: "success"});
        $state.go('home.login');
    },function (error) {
        $scope.status = 'error';
        SweetAlert.swal({ title: "Error", text: error.data , type: "error", html:true});
    })


});