"use strict";

var app = angular.module('ng-laravel');
app.controller('RegisCtrl',function($scope,$auth,$state,$cookieStore,$rootScope,$http,$translate,$window){
    
    
    
    $(document).ready(function()
{      $scope.temp=0;
       $scope.temp_1=0;
     setInterval(function() {
            
            
            var t_time=new Date();
        
        var curr_time = t_time.setHours(t_time.getHours());
        var cookie_store = $cookieStore.get('time');
        var key = $cookieStore.get('key');
        var time_diff=cookie_store-curr_time;
        
        
             if(time_diff <= 60000 && $scope.temp == 0){
                $scope.temp=1;
            swal({
                title: "Are you still there?",
                text: "Due to inactivity, you be will be logged out of your session in 1 minute",
                type: "success",
                showCancelButton: true,
                showConfirmButton: false,
                confirmButtonColor: "#CAE197",
                confirmButtonText: "Ok",
                closeOnConfirm: true,
                html: false
            });
            
            
//            $cookieStore.$reset();
        }
        else if(cookie_store<curr_time && $scope.temp_1 == 0){
            $scope.temp_1=1;
            
            swal({
                title: "your session has timed out.",
                text: "Please log back in to continue.",
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#CAE197",
                confirmButtonText: "Ok",
                closeOnConfirm: true,
                html: false
            },  function(isConfirm){

                if(isConfirm){
                         $cookieStore.remove('key');
                         $cookieStore.remove('lat');
                         $cookieStore.remove('lng');
                          $state.go('home.login');
                 } else {
                   
                   var tmp_time=new Date();
                                      
                                      var time = tmp_time.setHours(tmp_time.getHours() + 2);
                                      $cookieStore.put('time',time);
                                      var cookie_store = $cookieStore.get('time');
                   console.log(cookie_store)
                   
                 }
              });
            
            
//            $cookieStore.$reset();
        }
        
     }, 5000);
});
     $window.scrollTo(0, 0);
    /* show loading on page change */
    $rootScope.$on('$stateChangeStart', function(toState) {
        $scope.loader = true;
        if (toState.resolve) {
            $scope.loader = true;
        }
    });
    $rootScope.$on('$stateChangeSuccess', function(toState) {
        $scope.loader = false;
        if (toState.resolve) {
            $scope.loader = false;
        }
    });
    $scope.suggestion='';
    $scope.email='';
    $scope.year = (new Date()).getFullYear();
    $scope.sugg = function (sugg) {
        //alert(sugg);
        
        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/suggestion',
            data: {'sugg': sugg},
        }).then(function successCallback(response) {
            $('#sugg').modal('hide');
            //$('#sugg').hide();
            SweetAlert.swal({ title: "Success", text:'Thank you, your suggestion has been sent!\n\Feedback from our users is very important to us, and we take ample time to review every idea that is submitted. Please know that your suggestion will be carefully considered. One of our team members will get back to you only if we need further clarification or information.', type: "success"});
        }, function errorCallback(response) {
            SweetAlert.swal({ title: "Error", text: response.data.error, type: "error"});
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    
    $scope.terms_privacy = function () {
        
        $http({
            method: 'GET',
            url: 'laravel-backend/public/api/terms_privacy',
        }).then(function successCallback(response) {
            //$('#sugg').modal('hide');
            //$('#sugg').hide();
            $scope.terms = response.data.terms;
            $('#terms_desc').html(response.data.terms.description);
            $('#privacy_desc').html(response.data.privacy.description);
            $scope.privacy = response.data.privacy;
           // SweetAlert.swal({ title: "Success", text:'Thank you, your suggestion has been sent!\n\Feedback from our users is very important to us, and we take ample time to review every idea that is submitted. Please know that your suggestion will be carefully considered. One of our team members will get back to you only if we need further clarification or information.', type: "success"});
        }, function errorCallback(response) {
            SweetAlert.swal({ title: "Error", text: "Terms of Use and Privacy Policy not found.", type: "error"});
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };
    $scope.terms_privacy();
    $scope.news = function (email) {
        if(email.length==0){
            SweetAlert.swal({ title: "Error", text: "Please enter Email ID", type: "error"});
        }
//        $http({
//            method: 'POST',
//            url: 'laravel-backend/public/api/suggestion',
//            data: {'sugg': sugg},
//        }).then(function successCallback(response) {
//            $('#sugg').modal('hide');
//            //$('#sugg').hide();
//            SweetAlert.swal({ title: "Success", text:'Mail Sent', type: "success"});
//        }, function errorCallback(response) {
//            SweetAlert.swal({ title: "Error", text: response.data.error, type: "error"});
//            // called asynchronously if an error occurs
//            // or server returns response with an error status.
//        });
    };
    
    function makeGlobalLocation (lat ,lng){
            
          var latlng = new google.maps.LatLng(lat ,lng);
//          console.log(latlng);
        
              var addr = {};
                  var geocoder = new google.maps.Geocoder();
                      geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                          
                          if(status=='ZERO_RESULTS'){
                                $('#errormsg').html('Invalid Location');
                            }else{
                                $('#errormsg').html('');
                            }
                          if (status == google.maps.GeocoderStatus.OK) {
                              if (results[1]) {

                              for (var ii = 0; ii < results[0].address_components.length; ii++) {
                                  var types = results[0].address_components[ii].types.join(",");
                                  if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_3,political") {
                                      addr.city = results[0].address_components[ii].long_name;
                                  }else if(types == "administrative_area_level_2,political"){
                                      addr.city = results[0].address_components[ii].long_name;

                                  }else if(types == "country"){
                                      addr.city = results[0].address_components[ii].long_name;
                                  }

                                  if (types == "postal_code" || types == "postal_code_prefix,postal_code") {
                                      addr.postalcode = results[0].address_components[ii].long_name;
                                  }
                              }
                              $scope.$apply(function() {
                                  
                                      //console.log(addr['city']+', '+addr['postalcode']);
                                      $scope.current_location=addr['city']+', '+addr['postalcode'];
                                      var city_postalcode=addr['city']+', '+addr['postalcode'];
                                      var lat=lat;
                                      var lng=lng;
                                      $cookieStore.put('key',city_postalcode);
                                      $cookieStore.put('lat',lat);
                                      $cookieStore.put('lng',lng);
                                      var tmp_time=new Date();
                                      
                                      var time = tmp_time.setHours(tmp_time.getHours() + 2);
                                      $cookieStore.put('time',time);
                                        //alert($cookieStore.lat=lat);
                                      
                                      
                              });
                              } else {
                                  console.log('Location not found');
                              }
                          }
                      });

        }

    /* Get user profile info */
    $scope.profile = $auth.getProfile().$$state.value;
    if(1 == $scope.profile.id){
        $state.go('admin.dashboard');
    }
    console.log($scope.profile.memberships);
    if($scope.profile.memberships !="freeuser"){
        $scope.setPermissionUser=true;
    }
    // set language by profile language set
    $translate.use($scope.profile.language);
    $rootScope.currentLanguage = $scope.profile.language;



        $http({
            method: 'POST',
            url: 'laravel-backend/public/api/user/memberType',
        }).then(function successCallback(response) {
            if(response.data.groupOwner !=null && response.data.groupMember !=null){
                $scope.bothMember='Yes';   
            }else if(response.data.groupOwner !=null){
                $scope.groupOwner='Yes';
            }else{
                $scope.groupMember='Yes';
            }  
        });

});